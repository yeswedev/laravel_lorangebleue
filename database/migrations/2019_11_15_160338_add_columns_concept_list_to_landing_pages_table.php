<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsConceptListToLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_page_translations', function (Blueprint $table) {
            $table->text('concept_list_fitness')->nullable();
            $table->text('concept_list_wellness')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_page_translations', function (Blueprint $table) {
            $table->dropColumn(['concept_list_fitness','concept_list_wellness']);
        });
    }
}
