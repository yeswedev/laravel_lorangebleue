<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsOnHomeEsAndIsHighlightedEsOnOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->boolean('is_on_home_es')->default(0);
            $table->boolean('is_on_banner_es')->default(0);
            $table->boolean('is_highlighted_es')->default(0);
            $table->renameColumn('is_on_home', 'is_on_home_fr');
            $table->renameColumn('is_on_banner', 'is_on_banner_fr')->default(0);
            $table->renameColumn('is_highlighted', 'is_highlighted_fr')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('is_on_home_es');
            $table->dropColumn('is_on_banner_es');
            $table->dropColumn('is_highlighted_es');
            $table->renameColumn('is_on_home_fr', 'is_on_home');
            $table->renameColumn('is_on_banner_fr', 'is_on_banner');
            $table->renameColumn('is_highlighted_fr', 'is_highlighted');
        });
    }
}
