<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitiesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('stuff_club');
        Schema::dropIfExists('stuffs');
        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('type');
            $table->string('image')->nullable();
            $table->string('description');
            $table->string('url');
            $table->string('link');
            $table->timestamps();
        });
        Schema::create('facility_club', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('facility_id')->unsigned()->nullable();
            $table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');

            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_club');
        Schema::dropIfExists('facilities');
        Schema::create('stuffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('type');
            $table->string('image')->nullable();
            $table->string('description');
            $table->string('url');
            $table->string('link');
            $table->timestamps();
        });
        Schema::create('stuff_club', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('stuff_id')->unsigned()->nullable();
            $table->foreign('stuff_id')->references('id')->on('stuffs')->onDelete('cascade');

            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }
}
