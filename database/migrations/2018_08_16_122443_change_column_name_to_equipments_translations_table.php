<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnNameToEquipmentsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipments_translations', function (Blueprint $table) {
            $table->renameColumn('equipments_id', 'equipment_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipments_translations', function (Blueprint $table) {
            $table->renameColumn('equipment_id', 'equipments_id')->unsigned();
        });
    }
}
