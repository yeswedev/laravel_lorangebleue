<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateActivitiesAndActivityTranslationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->tinyInteger('rate_calorie');
        });
        Schema::table('activity_translations', function (Blueprint $table) {
            $table->text('objectives')->nullable();
            $table->text('targeted_part')->nullable();
            $table->text('calories')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('rate_calorie');
        });
        Schema::table('activity_translations', function (Blueprint $table) {
            $table->dropColumn('objectives');
            $table->dropColumn('targeted_part');
            $table->dropColumn('calories');
        });
    }
}
