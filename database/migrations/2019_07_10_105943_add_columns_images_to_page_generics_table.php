<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsImagesToPageGenericsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_generics', function (Blueprint $table) {
            $table->string('image_wellness')->nullable();
            $table->string('image_alt_wellness')->nullable();
            $table->string('image_plan')->nullable();
            $table->string('image_presentation')->nullable();
            $table->string('image_alt_presentation')->nullable();
            $table->string('image_reseau_1')->nullable();
            $table->string('image_alt_reseau_1')->nullable();
            $table->string('image_reseau_2')->nullable();
            $table->string('image_alt_reseau_2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_generics', function (Blueprint $table) {
            $table->dropColumn('image_wellness');
            $table->dropColumn('image_alt_wellness');
            $table->dropColumn('image_plan');
            $table->dropColumn('image_presentation');
            $table->dropColumn('image_alt_presentation');
            $table->dropColumn('image_reseau_1');
            $table->dropColumn('image_alt_reseau_1');
            $table->dropColumn('image_reseau_2');
            $table->dropColumn('image_alt_reseau_2');
        });
    }
}
