<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsReduceToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('reduced_commitment')->default(0)->nullable();
            $table->integer('reduced_price')->default(0)->nullable();
            $table->integer('price_offer')->default(0)->nullable();
            $table->integer('reduced_price_offer')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('reduced_commitment');
            $table->dropColumn('reduced_price');
            $table->dropColumn('price_offer');
            $table->dropColumn('reduced_price_offer');
        });
    }
}
