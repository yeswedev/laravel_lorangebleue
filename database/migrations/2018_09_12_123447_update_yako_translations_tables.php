<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateYakoTranslationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->renameColumn('link', 'detail')->nullable();
        });
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->text('detail')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->string('detail')->nullable()->change();
        });
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->renameColumn('detail', 'link')->nullable();
        });
    }
}
