<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixForeignKeyRelationLandingPageClub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_pages_clubs', function (Blueprint $table) {
            $table->dropForeign('landing_pages_clubs_landing_page_id_foreign');
            $table->dropForeign('landing_pages_clubs_club_id_foreign');
            $table->foreign('landing_page_id')->references('id')->on('landing_pages')->onDelete('cascade');
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_pages_clubs', function (Blueprint $table) {
            $table->dropForeign('landing_pages_clubs_landing_page_id_foreign');
            $table->dropForeign('landing_pages_clubs_club_id_foreign');
            $table->foreign('landing_page_id')->references('id')->on('landing_pages');
            $table->foreign('club_id')->references('id')->on('clubs');
        });
    }
}
