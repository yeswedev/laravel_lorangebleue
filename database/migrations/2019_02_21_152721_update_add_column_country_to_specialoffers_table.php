<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnCountryToSpecialoffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialoffers', function (Blueprint $table) {
            $table->string('country')->default('fr');
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('subtitle');
            $table->text('legal_mentions');
            $table->text('legal_mentions_home');
            $table->text('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('h1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialoffers', function (Blueprint $table) {
            $table->dropColumn('country');
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('url');
            $table->dropColumn('subtitle');
            $table->dropColumn('legal_mentions');
            $table->dropColumn('legal_mentions_home');
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('h1');
        });
    }
}
