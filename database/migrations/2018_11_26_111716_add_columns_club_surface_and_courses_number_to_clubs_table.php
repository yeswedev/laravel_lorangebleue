<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsClubSurfaceAndCoursesNumberToClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->integer('club_surface')->default(0)->nullable();
            $table->integer('courses_number')->default(0)->nullable();
            $table->integer('coachs_number')->default(0)->nullable();
            $table->boolean('is_access_option')->default(0)->nullable();
            $table->text('team_users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('club_surface');
            $table->dropColumn('courses_number');
            $table->dropColumn('coachs_number');
            $table->dropColumn('is_access_option');
            $table->dropColumn('team_users');
        });
    }
}
