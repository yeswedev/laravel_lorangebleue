<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modals', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('position', [null, 'tunnel_abo'])->nullable();
            $table->integer('inactivity_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('modal_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modal_id')->unsigned();
            $table->foreign('modal_id')->references('id')->on('modals')->onDelete('cascade');
            $table->string('title');
            $table->string('text');
            $table->string('label');
            $table->string('url');
            $table->string('locale')->index();
            $table->unique(['modal_id','locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modal_translations');
        Schema::dropIfExists('modals');
    }
}
