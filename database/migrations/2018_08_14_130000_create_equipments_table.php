<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentsTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('facility_translations');
        Schema::dropIfExists('facility_club');
        Schema::dropIfExists('facilities');

        Schema::create('equipments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('type');
            $table->string('image')->nullable();
            $table->string('description');
            $table->string('url');
            $table->string('link');
            $table->timestamps();
        });
        Schema::create('equipment_club', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('equipment_id')->unsigned()->nullable();
            $table->foreign('equipment_id')->references('id')->on('equipments')->onDelete('cascade');

            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
        Schema::create('equipments_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipments_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('link');
            $table->string('locale')->index();

            $table->unique(['equipments_id','locale']);
            $table->foreign('equipments_id')->references('id')->on('equipments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment_club');
        Schema::dropIfExists('equipments_translations');
        Schema::dropIfExists('equipments');
        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('type');
            $table->string('image')->nullable();
            $table->string('description');
            $table->string('url');
            $table->string('link');
            $table->timestamps();
        });
        Schema::create('facility_club', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('facility_id')->unsigned()->nullable();
            $table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');

            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }
}
