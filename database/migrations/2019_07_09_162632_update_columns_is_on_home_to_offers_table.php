<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Offer;

class UpdateColumnsIsOnHomeToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_translations', function (Blueprint $table) {
            $table->boolean('is_on_home')->default(0);
            $table->boolean('is_on_banner')->default(0);
            $table->boolean('is_highlighted')->default(0);
        });

        $offers = Offer::get();

        foreach($offers as $offer)
        {
            if($offer->is_on_home_fr == 1)
            {
                $offer->translate('fr')->is_on_home = $offer->is_on_home_fr;
                $offer->save();
            }
            if($offer->is_on_banner_fr == 1)
            {
                $offer->translate('fr')->is_on_banner = $offer->is_on_banner_fr;
                $offer->save();
            }
            if($offer->is_highlighted_fr == 1)
            {
                $offer->translate('fr')->is_highlighted = $offer->is_highlighted_fr;
                $offer->save();
            }

            //es
            if($offer->is_on_home_es == 1)
            {
                $offer->translate('es')->is_on_home = $offer->is_on_home_es;
                $offer->save();
            }
            if($offer->is_on_banner_es == 1)
            {
                $offer->translate('es')->is_on_banner = $offer->is_on_banner_es;
                $offer->save();
            }
            if($offer->is_highlighted_es == 1)
            {
                $offer->translate('es')->is_highlighted = $offer->is_highlighted_es;
                $offer->save();
            }
        }

        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('is_on_home_fr');
            $table->dropColumn('is_on_banner_fr');
            $table->dropColumn('is_highlighted_fr');
            $table->dropColumn('is_on_home_es');
            $table->dropColumn('is_on_banner_es');
            $table->dropColumn('is_highlighted_es');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->boolean('is_on_home_fr')->default(0);
            $table->boolean('is_on_banner_fr')->default(0);
            $table->boolean('is_highlighted_fr')->default(0);
            $table->boolean('is_on_home_es')->default(0);
            $table->boolean('is_on_banner_es')->default(0);
            $table->boolean('is_highlighted_es')->default(0);
        });

        $offers = Offer::get();

        foreach($offers as $offer)
        {
            if($offer->translate('fr'))
            {
                if($offer->translate('fr')->is_on_home == 1)
                {
                    $offer->is_on_home_fr = $offer->translate('fr')->is_on_home;
                    $offer->save();
                }
                if($offer->translate('fr')->is_on_banner == 1)
                {
                    $offer->is_on_banner_fr = $offer->translate('fr')->is_on_banner;
                    $offer->save();
                }
                if($offer->translate('fr')->is_highlighted == 1)
                {
                    $offer->is_highlighted_fr = $offer->translate('fr')->is_highlighted;
                    $offer->save();
                }
            }

            //es
            if($offer->translate('es'))
            {
                if($offer->translate('es')->is_on_home == 1)
                {
                    $offer->is_on_home_es = $offer->translate('es')->is_on_home;
                    $offer->save();
                }
                if($offer->translate('es')->is_on_banner == 1)
                {
                    $offer->is_on_banner_es = $offer->translate('es')->is_on_banner;
                    $offer->save();
                }
                if($offer->translate('es')->is_highlighted == 1)
                {
                    $offer->is_highlighted_es = $offer->translate('es')->is_highlighted;
                    $offer->save();
                }
            }
        }

        Schema::table('offer_translations', function (Blueprint $table) {
            $table->dropColumn('is_on_home');
            $table->dropColumn('is_on_banner');
            $table->dropColumn('is_highlighted');
        });
    }
}
