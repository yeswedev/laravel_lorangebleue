<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsReduceToSpecialoffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialoffers', function (Blueprint $table) {
            $table->integer('reduced_commitment')->default(0)->nullable();
            $table->integer('reduced_price')->default(0)->nullable();
            $table->integer('reduced_price_offer')->default(0)->nullable();
            $table->renameColumn('free_month', 'commitment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialoffers', function (Blueprint $table) {
            $table->dropColumn('reduced_commitment');
            $table->dropColumn('reduced_price');
            $table->dropColumn('reduced_price_offer');
            $table->renameColumn('commitment', 'free_month')->nullable();
        });
    }
}
