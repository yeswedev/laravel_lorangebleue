<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeoColumnH1OnManyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_translations', function (Blueprint $table) {
            $table->text('h1')->nullable();
        });
        Schema::table('equipments_translations', function (Blueprint $table) {
            $table->text('h1')->nullable();
        });
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->text('h1')->nullable();
        });
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->text('h1')->nullable();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->text('h1')->nullable();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->text('h1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_translations', function (Blueprint $table) {
            $table->dropColumn('h1');
        });
        Schema::table('equipments_translations', function (Blueprint $table) {
            $table->dropColumn('h1');
        });
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->dropColumn('h1');
        });
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->dropColumn('h1');
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('h1');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('h1');
        });
    }
}
