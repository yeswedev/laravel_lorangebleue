<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnNullableToMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->string('file')->nullable()->change();
            $table->float('ratio')->nullable()->change();
            $table->integer('width')->nullable()->change();
            $table->integer('height')->nullable()->change();
            $table->integer('size')->nullable()->change();
            $table->string('type')->nullable()->change();
            $table->string('status')->nullable()->change();
            $table->string('version')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->string('file')->nullable(false)->change();
            $table->float('ratio')->nullable(false)->change();
            $table->integer('width')->nullable(false)->change();
            $table->integer('height')->nullable(false)->change();
            $table->integer('size')->nullable(false)->change();
            $table->string('type')->nullable(false)->change();
            $table->string('status')->nullable(false)->change();
            $table->string('version')->nullable(false)->change();
        });
    }
}
