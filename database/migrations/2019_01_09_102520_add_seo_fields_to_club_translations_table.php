<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeoFieldsToClubTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club_translations', function (Blueprint $table) {
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
        });
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->dropColumn('meta_description');
        });
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->text('meta_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club_translations', function (Blueprint $table) {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
        });
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->dropColumn('meta_description');
        });
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->string('meta_description')->nullable();
        });
    }
}
