<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExTranslatedAttributesToClubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('address');
            $table->string('email');
            $table->string('team_title')->nullable();
            $table->text('team_description')->nullable();
            $table->string('zip_code');
            $table->string('city');
            $table->string('url');
            $table->string('department');
        });
        foreach (\App\ClubTranslation::where('locale', 'fr')->get() as $clubTranslation) {
            $club = \App\Club::find($clubTranslation->club_id);
            $club->title = $clubTranslation->title;
            $club->description = $clubTranslation->description;
            $club->address = $clubTranslation->address;
            $club->email = $clubTranslation->email;
            $club->team_title = $clubTranslation->team_title;
            $club->team_description = $clubTranslation->team_description;
            $club->zip_code = $clubTranslation->zip_code;
            $club->city = $clubTranslation->city;
            $club->url = $clubTranslation->url;
            $club->department = $clubTranslation->department;
            $club->save();
        }
        Schema::dropIfExists('club_translations');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // TODO reconstruire les translations à partir des nouveaux champs de club
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('address');
            $table->dropColumn('email');
            $table->dropColumn('team_title');
            $table->dropColumn('team_description');
            $table->dropColumn('zip_code');
            $table->dropColumn('city');
            $table->dropColumn('url');
            $table->dropColumn('department');
        });
    }
}
