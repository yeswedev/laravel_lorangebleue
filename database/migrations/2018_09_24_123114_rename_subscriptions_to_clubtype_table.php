<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameSubscriptionsToClubtypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });

        Schema::table('packs', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });
        
        Schema::table('subscription_translations', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });
        Schema::table('subscription_yako', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });

        Schema::rename('subscriptions', 'clubtypes');
        Schema::rename('subscription_translations', 'clubtype_translations');
        Schema::rename('subscription_yako', 'clubtype_yako');

        Schema::table('clubs', function (Blueprint $table) {
            $table->integer('clubtype_id')->unsigned()->nullable();
            $table->foreign('clubtype_id')->references('id')->on('clubs')->onDelete('cascade');
        });

        Schema::table('packs', function (Blueprint $table) {
            $table->integer('clubtype_id')->unsigned()->nullable();
            $table->foreign('clubtype_id')->references('id')->on('packs')->onDelete('cascade');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('clubtype_id')->unsigned()->nullable();
            $table->foreign('clubtype_id')->references('id')->on('orders')->onDelete('cascade');
        });
        
        Schema::table('clubtype_translations', function (Blueprint $table) {
            $table->integer('clubtype_id')->unsigned()->nullable();
            $table->foreign('clubtype_id')->references('id')->on('clubtype_translations')->onDelete('cascade');
        });
        Schema::table('clubtype_yako', function (Blueprint $table) {
            $table->integer('clubtype_id')->unsigned()->nullable();
            $table->foreign('clubtype_id')->references('id')->on('clubtype_yako')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign(['clubtype_id']);
            $table->dropColumn('clubtype_id');
        });

        Schema::table('packs', function (Blueprint $table) {
            $table->dropForeign(['clubtype_id']);
            $table->dropColumn('clubtype_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['clubtype_id']);
            $table->dropColumn('clubtype_id');
        });
        
        Schema::table('clubtype_translations', function (Blueprint $table) {
            $table->dropForeign(['clubtype_id']);
            $table->dropColumn('clubtype_id');
        });
        Schema::table('clubtype_yako', function (Blueprint $table) {
            $table->dropForeign(['clubtype_id']);
            $table->dropColumn('clubtype_id');
        });

        Schema::rename('clubtypes', 'subscriptions');
        Schema::rename('clubtype_translations', 'subscription_translations');
        Schema::rename('clubtype_yako', 'subscription_yako');

        Schema::table('clubs', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('clubs')->onDelete('cascade');
        });

        Schema::table('packs', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('packs')->onDelete('cascade');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('orders')->onDelete('cascade');
        });
        
        Schema::table('subscription_translations', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscription_translations')->onDelete('cascade');
        });
        Schema::table('subscription_yako', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscription_yako')->onDelete('cascade');
        });
    }
}
