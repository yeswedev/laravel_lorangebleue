<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->dropColumn('title');
        });

        Schema::create('pack_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pack_id')->unsigned();
            $table->string('title');
            $table->string('locale')->index();

            $table->unique(['pack_id','locale']);
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packs', function (Blueprint $table) {
            $table->string('title');
        });
        Schema::dropIfExists('pack_translations');
    }
}
