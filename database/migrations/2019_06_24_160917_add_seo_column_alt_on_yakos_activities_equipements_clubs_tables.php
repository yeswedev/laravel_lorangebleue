<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoColumnAltOnYakosActivitiesEquipementsClubsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_translations', function (Blueprint $table) {
            $table->text('image_alt')->nullable();
        });
        Schema::table('equipments_translations', function (Blueprint $table) {
            $table->text('image_alt')->nullable();
        });
        Schema::table('yakos', function (Blueprint $table) {
            $table->string('color')->nullable()->change();
        });
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->text('image_alt')->nullable();
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->text('image_alt')->nullable();
            $table->text('team_image_alt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_translations', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('equipments_translations', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('yakos', function (Blueprint $table) {
            $table->string('color')->nullable(false)->change();
        });
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->dropColumn('image_alt');
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('image_alt');
            $table->dropColumn('team_image_alt');
        });
    }
}
