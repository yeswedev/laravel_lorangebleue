<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('address');
            $table->dropColumn('email');
            $table->dropColumn('team_title');
            $table->dropColumn('team_description');
            $table->dropColumn('team_image');
        });

        Schema::create('club_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('address');
            $table->string('email');
            $table->string('team_title');
            $table->string('team_description');
            $table->string('team_image');
            $table->string('locale')->index();

            $table->unique(['club_id','locale']);
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('title');
            $table->text('description');
            $table->string('address');
            $table->string('email');
            $table->string('team_title');
            $table->string('team_description');
            $table->string('team_image');
        });
        Schema::dropIfExists('club_translations');
    }
}
