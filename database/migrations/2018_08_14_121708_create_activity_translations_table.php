<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('presentation');
            $table->dropColumn('url');
            $table->dropColumn('link');
        });

        Schema::create('activity_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('presentation');
            $table->string('url');
            $table->string('link');
            $table->string('locale')->index();

            $table->unique(['activity_id','locale']);
            $table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->string('title');
            $table->text('description');
            $table->string('presentation');
            $table->string('url');
            $table->string('link');
        });
        Schema::dropIfExists('activity_translations');
    }
}
