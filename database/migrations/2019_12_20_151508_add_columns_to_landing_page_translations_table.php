<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToLandingPageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landing_page_translations', function (Blueprint $table) {
            $table->text('h1')->nullable();
            $table->text('subtitle')->nullable();
            $table->text('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('landing_page_translations', function (Blueprint $table) {
            $table->dropColumn(['h1', 'subtitle', 'price']);
        });
    }
}
