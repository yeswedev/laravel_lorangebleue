<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImportantFixRenameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign(['clubtype_id']);
            $table->dropColumn('clubtype_id');
        });
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('offer_translations');
        Schema::dropIfExists('offer_club');
        Schema::dropIfExists('offers');

        Schema::dropIfExists('subscription_translations');
        Schema::dropIfExists('subscription_offer');
        Schema::dropIfExists('subscriptions');

        Schema::dropIfExists('clubtypes');
        Schema::dropIfExists('clubtype_translations');
        Schema::dropIfExists('clubtype_yako');

        Schema::dropIfExists('club_subscription');
        Schema::enableForeignKeyConstraints();

        Schema::create('specialoffers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('free_month')->nullable();
            $table->integer('price')->nullable();
            $table->integer('price_offer')->nullable();
            $table->timestamps();
        });
        Schema::create('specialoffer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specialoffer_id')->unsigned();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('url')->nullable();
            $table->string('locale')->index();

            $table->unique(['specialoffer_id','locale']);
            $table->foreign('specialoffer_id')->references('id')->on('specialoffers')->onDelete('cascade');
        });

        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
        Schema::create('subscription_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();

            $table->string('url')->nullable();

            $table->string('title1')->nullable();
            $table->text('presentation1')->nullable();

            $table->string('title2')->nullable();
            $table->text('presentation2')->nullable();

            $table->string('title3')->nullable();
            $table->text('presentation3')->nullable();

            $table->string('title4')->nullable();
            $table->text('presentation4')->nullable();
            $table->string('locale')->index();

            $table->unique(['subscription_id','locale']);
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
        Schema::create('subscription_yako', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');

            $table->integer('yako_id')->unsigned()->nullable();
            $table->foreign('yako_id')->references('id')->on('yakos')->onDelete('cascade');
        });

        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commitment')->nullable();
            $table->integer('price')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();

            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
        Schema::create('offer_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->string('locale')->index();

            $table->unique(['offer_id','locale']);
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
        Schema::create('offer_specialoffer', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('offer_id')->unsigned()->nullable();
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');

            $table->integer('specialoffer_id')->unsigned()->nullable();
            $table->foreign('specialoffer_id')->references('id')->on('specialoffers')->onDelete('cascade');
        });
        Schema::create('specialoffer_club', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('specialoffer_id')->unsigned()->nullable();
            $table->foreign('specialoffer_id')->references('id')->on('specialoffers')->onDelete('cascade');

            $table->integer('club_id')->unsigned()->nullable();
            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade');
        });

        Schema::table('clubs', function (Blueprint $table) {
            $table->renameColumn('price_subscription', 'price_offer')->nullable();

            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
