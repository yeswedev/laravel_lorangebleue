<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnNullableToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('free_month')->default(0)->nullable()->change();
            $table->integer('price')->default(0)->nullable()->change();
            $table->integer('price_subscription')->default(0)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('free_month')->nullable(false)->change();
            $table->integer('price')->nullable(false)->change();
            $table->integer('price_subscription')->nullable(false)->change();
        });
    }
}
