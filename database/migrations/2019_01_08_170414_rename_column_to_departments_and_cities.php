<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnToDepartmentsAndCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('meta-title');
            $table->dropColumn('meta-description');
            $table->string('meta_title');
            $table->text('meta_description');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('meta-title');
            $table->dropColumn('meta-description');
            $table->string('meta_title');
            $table->text('meta_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
