<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocGenericTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloc_generic_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bloc_generic_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
            $table->string('image_alt')->nullable();
            $table->string('image_alt_2')->nullable();
            $table->string('locale')->index();

            $table->unique(['bloc_generic_id','locale']);
            $table->foreign('bloc_generic_id')->references('id')->on('bloc_generics')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloc_generic_translations');
    }
}
