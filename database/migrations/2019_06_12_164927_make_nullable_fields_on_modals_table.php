<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableFieldsOnModalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modal_translations', function (Blueprint $table) {
            $table->string('title')->nullable()->change();
            $table->string('text')->nullable()->change();
            $table->string('label')->nullable()->change();
            $table->string('url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modal_translations', function (Blueprint $table) {
            $table->string('title')->nullable(false)->change();
            $table->string('text')->nullable(false)->change();
            $table->string('label')->nullable(false)->change();
            $table->string('url')->nullable(false)->change();
        });
    }
}
