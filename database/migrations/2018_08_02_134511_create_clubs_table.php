<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('address');
            $table->string('email');
            $table->boolean('is_sauna')->default(0);
            $table->boolean('is_access')->default(0);
            $table->boolean('is_try')->default(0);
            $table->boolean('is_clim')->default(0);
            $table->boolean('is_defibrillator')->default(0);
            $table->integer('price_pack');
            $table->string('team_title');
            $table->string('team_description');
            $table->string('team_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
