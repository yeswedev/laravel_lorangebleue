<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilityTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facilities', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('url');
            $table->dropColumn('link');
        });

        Schema::create('facility_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('facility_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('link');
            $table->string('locale')->index();

            $table->unique(['facility_id','locale']);
            $table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facilities', function (Blueprint $table) {
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('link');
        });
        Schema::dropIfExists('facility_translations');
    }
}
