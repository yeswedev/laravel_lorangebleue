<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnsLabelUrlOnModalTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modal_translations', function (Blueprint $table) {
            $table->dropColumn('label');
            $table->dropColumn('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modal_translations', function (Blueprint $table) {
            $table->text('label')->nullable();
            $table->text('url')->nullable();
        });
    }
}
