<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLegalMentionsToSpecialofferTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->text('legal_mentions')->nullable();
            $table->text('legal_mentions_home')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialoffer_translations', function (Blueprint $table) {
            $table->dropColumn('legal_mentions');
            $table->dropColumn('legal_mentions_home');
        });
    }
}
