<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRelationshipPackOfferAndRemoveColumnPackIdToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropForeign(['pack_id']);
            $table->dropColumn('pack_id');
        });
        Schema::create('pack_offer', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pack_id')->unsigned()->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');

            $table->integer('offer_id')->unsigned()->nullable();
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('pack_id')->unsigned()->nullable();
            $table->foreign('pack_id')->references('id')->on('packs')->onDelete('cascade');
        });
        Schema::dropIfExists('pack_offer');
    }
}
