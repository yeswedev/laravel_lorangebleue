<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnToClubtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubtypes', function (Blueprint $table) {
            $table->string('type')->nullable();
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubtypes', function (Blueprint $table) {
            $table->dropColumn('type');
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('type')->nullable();
        });
    }
}
