<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TranslateFrToMa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = DB::select('SHOW TABLES');

        $DBName = 'Tables_in_'.env('DB_DATABASE') ;

        foreach($tables as $table)
        {
            $tableName = $table->$DBName ;
            if(strchr($tableName, '_translations')){
                DB::table($tableName)->where('locale', 'ma')->delete();
            }
        }

        foreach($tables as $table)
        {
            $tableName = $table->$DBName ;
            // offer_translations : les offres s'affichent suivant si elles sont traduites dans la locale ou non. On part de 0 pour la marocaine
            if(strchr($tableName, '_translations') && $tableName != 'offer_translations'){
                $toTranslate = DB::table($tableName)->where('locale', 'fr')->get();

                $toTranslate->map(function ($item, $key) use($tableName) {
                    $item->locale = 'ma' ;
                    unset($item->id);

                    DB::table($tableName)->insert((array) $item);
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = DB::select('SHOW TABLES');

        $DBName = 'Tables_in_'.env('DB_DATABASE') ;

        foreach($tables as $table)
        {
            $tableName = $table->$DBName ;
            if(strchr($tableName, '_translations')){
                DB::table($tableName)->where('locale', 'ma')->delete();
            }
        }
    }
}
