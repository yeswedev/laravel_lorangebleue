<?php

use Illuminate\Database\Migrations\Migration;

class UpdateActivitiesYakosAndEquipmentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE activities CHANGE rate_calorie rate_calorie TINYINT(4) UNSIGNED NULL DEFAULT 0;');
        DB::statement('ALTER TABLE yakos CHANGE rate_calorie rate_calorie TINYINT(4) UNSIGNED NULL DEFAULT 0;');
        DB::statement('ALTER TABLE equipment CHANGE rate_calorie rate_calorie TINYINT(4) UNSIGNED NULL DEFAULT 0;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE activities CHANGE rate_calorie rate_calorie TINYINT(4) UNSIGNED NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE yakos CHANGE rate_calorie rate_calorie TINYINT(4) UNSIGNED NULL DEFAULT NULL;');
        DB::statement('ALTER TABLE equipment CHANGE rate_calorie rate_calorie TINYINT(4) UNSIGNED NULL DEFAULT NULL;');
    }
}
