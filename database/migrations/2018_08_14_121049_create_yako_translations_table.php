<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYakoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yakos', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('url');
            $table->dropColumn('link');
        });

        Schema::create('yako_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('yako_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('link');
            $table->string('locale')->index();

            $table->unique(['yako_id','locale']);
            $table->foreign('yako_id')->references('id')->on('yakos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yakos', function (Blueprint $table) {
            $table->string('title');
            $table->text('description');
            $table->string('url');
            $table->string('link');
        });
        Schema::dropIfExists('yako_translations');
    }
}
