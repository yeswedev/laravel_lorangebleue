<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnToYakoTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->text('structure')->nullable();
            $table->text('advantage')->nullable();
            $table->text('public')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yako_translations', function (Blueprint $table) {
            $table->dropColumn('structure');
            $table->dropColumn('advantage');
            $table->dropColumn('public');
        });
    }
}
