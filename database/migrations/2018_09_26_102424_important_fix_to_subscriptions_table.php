<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImportantFixToSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('subscription_translations');
        Schema::dropIfExists('subscription_offer');
        Schema::dropIfExists('subscriptions');

        Schema::dropIfExists('clubtypes');
        Schema::dropIfExists('clubtype_translations');
        Schema::dropIfExists('clubtype_yako');
        Schema::enableForeignKeyConstraints();

        Schema::create('clubtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
        Schema::create('clubtype_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clubtype_id')->unsigned();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['clubtype_id','locale']);
            $table->foreign('clubtype_id')->references('id')->on('clubtypes')->onDelete('cascade');
        });
        Schema::create('clubtype_yako', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('clubtype_id')->unsigned()->nullable();
            $table->foreign('clubtype_id')->references('id')->on('clubtypes')->onDelete('cascade');

            $table->integer('yako_id')->unsigned()->nullable();
            $table->foreign('yako_id')->references('id')->on('yakos')->onDelete('cascade');
        });

        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commitment')->nullable();
            $table->integer('price')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();

            $table->integer('club_type_id')->unsigned()->nullable();
            $table->foreign('club_type_id')->references('id')->on('clubtypes')->onDelete('cascade');
        });
        Schema::create('subscription_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->string('locale')->index();

            $table->unique(['subscription_id','locale']);
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });
        Schema::create('subscription_offer', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');

            $table->integer('offer_id')->unsigned()->nullable();
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
