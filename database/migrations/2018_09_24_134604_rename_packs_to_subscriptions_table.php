<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePacksToSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pack_translations', function (Blueprint $table) {
            $table->dropForeign(['pack_id']);
            $table->dropColumn('pack_id');
        });
        Schema::table('pack_offer', function (Blueprint $table) {
            $table->dropForeign(['pack_id']);
            $table->dropColumn('pack_id');
        });
        Schema::table('club_pack', function (Blueprint $table) {
            $table->dropForeign(['pack_id']);
            $table->dropColumn('pack_id');
        });

        Schema::rename('packs', 'subscriptions');
        Schema::rename('pack_translations', 'subscription_translations');
        Schema::rename('pack_offer', 'subscription_offer');
        Schema::rename('club_pack', 'club_subscription');

        Schema::table('subscription_translations', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscription_translations')->onDelete('cascade');
        });
        Schema::table('subscription_offer', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('subscription_offer')->onDelete('cascade');
        });
        Schema::table('club_subscription', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->foreign('subscription_id')->references('id')->on('club_subscription')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_translations', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });
        Schema::table('subscription_offer', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });
        Schema::table('club_subscription', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });

        Schema::rename('subscriptions', 'packs');
        Schema::rename('subscription_translations', 'pack_translations');
        Schema::rename('subscription_offer', 'pack_offer');
        Schema::rename('club_subscription', 'club_pack');

        Schema::table('pack_translations', function (Blueprint $table) {
            $table->integer('pack_id')->unsigned()->nullable();
            $table->foreign('pack_id')->references('id')->on('pack_translations')->onDelete('cascade');
        });
        Schema::table('pack_offer', function (Blueprint $table) {
            $table->integer('pack_id')->unsigned()->nullable();
            $table->foreign('pack_id')->references('id')->on('pack_offer')->onDelete('cascade');
        });
        Schema::table('club_pack', function (Blueprint $table) {
            $table->integer('pack_id')->unsigned()->nullable();
            $table->foreign('pack_id')->references('id')->on('club_pack')->onDelete('cascade');
        });
    }
}
