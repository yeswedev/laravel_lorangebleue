<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageGenericTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_generic_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_generic_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
            $table->text('text')->nullable();
            $table->text('text_2')->nullable();
            $table->text('text_3')->nullable();
            $table->string('alt_image')->nullable();
            $table->string('locale')->index();

            $table->unique(['page_generic_id','locale']);
            $table->foreign('page_generic_id')->references('id')->on('page_generics')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_generic_translations');
    }
}
