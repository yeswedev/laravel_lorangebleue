<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnTeamImageToClubTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club_translations', function (Blueprint $table) {
            $table->dropColumn('team_image');
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('team_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club_translations', function (Blueprint $table) {
            $table->string('team_image')->nullable();
        });
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('team_image');
        });
    }
}
