<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRelationshipYakoClubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yako_club', function (Blueprint $table) {
            $table->string('status')->nullable()->change();
            $table->string('version')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yako_club', function (Blueprint $table) {
            $table->string('status')->change();
            $table->string('version')->change();
        });
    }
}
