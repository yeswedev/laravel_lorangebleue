<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddColumnPresentationToSubscriptionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_translations', function (Blueprint $table) {
            $table->string('title1')->nullable();
            $table->text('presentation1')->nullable();

            $table->string('title2')->nullable();
            $table->text('presentation2')->nullable();

            $table->string('title3')->nullable();
            $table->text('presentation3')->nullable();

            $table->string('title4')->nullable();
            $table->text('presentation4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_translations', function (Blueprint $table) {
            $table->dropColumn('title1');
            $table->dropColumn('presentation1');

            $table->dropColumn('title2');
            $table->dropColumn('presentation2');

            $table->dropColumn('title3');
            $table->dropColumn('presentation3');

            $table->dropColumn('title4');
            $table->dropColumn('presentation4');
        });
    }
}
