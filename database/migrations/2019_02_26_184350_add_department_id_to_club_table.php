<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartmentIdToClubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->integer('department_id')->unsigned()->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
        $defaultDepartment = \App\Department::first();
        foreach (\App\Club::get() as $club) {
            $department = \App\Department::where('name', $club->department)->first();
            if ($department) {
                $club->department_id = $department->id;
                $club->save();
            } else {
                $club->department_id = $defaultDepartment->id;
                \Log::error('Club sans department:'.$club->title. ' : '.$club->department);
            }
        }
        Schema::table('clubs', function (Blueprint $table) {
            $table->renameColumn('department', 'department_old');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign('department_id');
            $table->dropColumn('department_id');
            $table->renameColumn('department_old', 'department');
        });
    }
}
