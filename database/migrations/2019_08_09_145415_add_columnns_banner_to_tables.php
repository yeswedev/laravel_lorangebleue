<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnnsBannerToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('banner')->nullable();
        });
        Schema::table('equipment', function (Blueprint $table) {
            $table->string('banner')->nullable();
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->string('banner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('banner');
        });
        Schema::table('equipment', function (Blueprint $table) {
            $table->dropColumn('banner');
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('banner');
        });
    }
}
