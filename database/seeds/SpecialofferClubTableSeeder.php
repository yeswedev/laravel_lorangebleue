<?php

use Illuminate\Database\Seeder;

class SpecialofferClubTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubsName = ["AGEN","Aix-en-Provence","AIZENAY","ALBI","ALENCON","ANCENIS","ANGERS","ANGOULINS","ANNECY","AURAY","AVIGNON","AVRANCHES","AZAY LE BRULÉ","BAIN DE BRETAGNE","BAR LE DUC","BAULE","BAYEUX","BEAUPREAU","BETTON","BIGANOS","BISCHHEIM","BLAGNAC","BLAIN","BOISSY SAINT LEGER","BORDEAUX CENTRE Saint-CHRISTOLY","BOURGES","BREST CENTRE","BRETTEVILLE SUR ODON","BREUILLET","BRISSAC","BROU","BRUZ","BUC","BUXEROLLES","CARCASSONNE","CARENTAN","CARHAIX","CASTELJALOUX","CASTETS","CASTRES","CERGY","CESSON SEVIGNE","CESSON SEVIGNE 2","CHALLANS","CHAMBERY","CHAMPHOL","CHAMPIGNY SUR MARNE CENTRE","CHAMPIGNY SUR MARNE TREMBLAY","CHAMPNIERS","CHANTONNAY","CHARMES","CHARTRES","CHATEAU THIERRY","CHATEAUBOURG","CHATEAUBRIANT","CHATEAUGIRON","CHATEAULIN","CHATELLERAULT","jardres","CHEMILLÉ","CHENÔVE","CLICHY","COLMAR","CONCARNEAU","COUTANCES","CRAON","CRÉPY EN VALOIS","DAVEZIEUX","DECINES","DIGNE-LES-BAINS","DIJON NORD","DINARD","DIVES SUR MER","DOLE","DORMANS","DOUAI","EAUBONNE","ENGLOS","EPAGNY","ERNEE","FALAISE","FÉCAMP","FEYTIAT","FLERS","FLEURY-SUR-ORNE","FLOIRAC","FONTENAY LE COMTE","FOUESNANT","FOUGERES","GETIGNE","GOUESNOU","GOUSSAINVILLE","GRANVILLE","GRÉSY-SUR-AIX","GUEUGNON","GUINGAMP / PLOUMAGOAR","HALLUIN","HAZEBROUCK","HONFLEUR","JAUNAY CLAN","LA BAULE","LA CHAPELLE BASSE MER","LA CHAPELLE SUR ERDRE","LA FERTE MACE","LA FLÈCHE","LA GUERCHE DE BRETAGNE","LA TESTE DE BUCH","LAMBALLE","LANDERNEAU","LANDIVISIAU","LANESTER","LANGON","LANNEMEZAN","LANNILIS","LANNION","LAON","LAVAL","LAVAU","LE MANS EST","Le Mans nord - Saint-Pavace","LE MANS OUEST","LE VIEIL EVREUX","LENS","Pau / Lescar","LIBERCOURT","LIEVIN","LISIEUX","LONGUENESSE","LORIENT","LUCÉ","LUCON","MACON","MAGNY EN VEXIN","MALEMORT","MAMERS","Mantes-la-Ville","MARMANDE","MARSEILLE","MASNY","MAYENNE","MELLAC","MELLE","MENNECY","MESSEIN","METZ CENTRE","MILLAU","MIMIZAN","MONDEVILLE","MONTAIGU - BOUFFÉRÉ","MONTAUBAN NORD","MONTAUBAN SUD","MONTCEAU-LES-MINES","Montlouis-sur-Loire","MONTLUÇON","MONTPELLIER","MONTREUIL JUIGNE","MONTROUGE","MORLAIX","MUZILLAC","NEAUPHLETTE","NEUILLY-PLAISANCE","NIEPPE","NORT SUR ERDRE","PACY SUR EURE","PARTHENAY","Pau est / Assat","PÉRONNAS","PÉRONNE","PLERIN","PLOEMEUR","Plougastel-Daoulas","PONT L' ABBE","PORNIC","PUTEAUX","Dinan / Quévert","QUIMPER","REDON","REMILLY","RENNES COLOMBIER","RENNES GARE","RENNES SAINT GREGOIRE","Rennes Saint-Jacques","RENNES STADE","RIVIERES","ROMILLY SUR SEINE","ROYAN","ROYE","RUFFEC","RUMILLY","SABLÉ SUR SARTHE","SAINT HILAIRE DU HARCOUET","SAINT LO","saint-maur-des-fosses","SAINT QUENTIN","SAINT ANDRÉ LEZ LILLE","SAINT BRICE EN COGLES","SAINT FORT","Saint-Gilles-Croix-de-Vie","SAINT HERBLAIN","SAINT LYS","Saint-Marcel _ Vernon","Saint-Méen-le-Grand","Saint-Philbert-de-Grand-Lieu","SAINT POL DE LEON","SAINT QUAY PORTRIEUX","SAINT-EGRÈVE","Saint-Macaire-en-Mauges / Sèvremoine","SAINT-MÉDARD","Martigues","SAINT-PRIEST OUEST","SALLANCHES","SAVENAY","SCIONZIER","SEGRÉ","SÉNÉ","Serres-Castets","SIERENTZ","Strasbourg La Meinau","STRASBOURG","SURGERES","TARBES","TEMPLEMARS","THONON-LES-BAINS","TIERCE","Tignieu-Jameyzieu","TONNERRE","TOULOUSE","TOURS","TOURS NORD","TREGUEUX","Périgueux / Trélissac","TRIE CHATEAU","TRIGNAC","TULLE","VALENCIENNES","VANNES","VANVES","Vern-sur-Seiche","VERQUIGNEUL","VERSAILLES","VERTOU","VILLEFRANCHE","VILLEJUIF","VILLERS COTTERETS","VIRE","VIRIAT","VITRÉ","VITROLLES","VOIRON","YVETOT"];
        $specialOffer = \App\Specialoffer::find(5);
        if (!$specialOffer) {
            return ;
        }
        foreach ($clubsName as $name) {
            $slugName = str_slug($name);
            $found = false;
            foreach (\App\Club::with('translations')->get() as $club) {
                if (str_slug($club->title) == $slugName || str_slug($club->city) == $slugName || str_slug($club->url) == $slugName) {
                    $found = true;
                    if (!$specialOffer->specialofferClub->contains($club->id)) {
                        $specialOffer->specialofferClub()->attach($club->id);
                        break;
                    }
                }
            }
            if (!$found) {
                echo $name. ' - '.$slugName. " not found\n";
            }
        }
    }
}
