<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    private function getElementText($element)
    {
        $result = "";
        if ($element->childNodes && count($element->childNodes)) {
            $result = "";
            for ($i = 0; $i < count($element->childNodes); $i++) {
                $child = $element->childNodes->item($i);
                $childText = $this->getElementText($child);
                if ($child->nodeName == "#text") {
                    $result .= $childText;
                } else {
                    $result .= "<" . $child->nodeName . ">" . $childText . "</" . $child->nodeName . ">";
                }
            }
        } else {
            if ($element->nodeValue != "") {
                $result = $element->nodeValue;
            }
        }
        return $result;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\Illuminate\Support\Facades\Storage::disk('cities')->files() as $file) {
            $html = str_replace("\n\n", "<br>", \Illuminate\Support\Facades\Storage::disk('cities')->get($file));

            $dom = new \DOMDocument();
            $dom->loadHTML($html);
            $dom->preserveWhiteSpace = false;
            $elements = $dom->getElementsByTagName('body')->item(0)->childNodes;

            $title = substr($file, 0, strpos($file, "."));
            $title = substr($title, strrpos($title, "_")+1);

            $description = "";
            $isDescription = false;
            $wait = 0;
            foreach ($elements as $element) {
                $paragraph = $element->nodeValue;
                $text = $this->getElementText($element);
                if ($wait && $wait >0) {
                    $wait--;
                } else {
                    if (strpos($paragraph, "Trouvez votre salle") !== false) {
                        $isDescription = true;
                        $wait = 1;
                    } elseif (strpos($text, "interlocutrice") !== false) {
                        $isDescription = false;
                    } else {
                        if ($isDescription) {
                            $description .= $text . "<br/>";
                        }
                    }
                }
            }

            $geoData = \App\Lib\Geocoding::findLongLatFromAddress($title.",france");

            $department = \App\Department::where(['name' => $geoData['department']])->first();
            if (!$department) {
                $department = new \App\Department();
                $department->name = $geoData['department'];
                $department->url = str_slug($geoData['department']);
                $department->country = 'fr';
                $department->save();
            }

            $city = new \App\City();
            $city->name = $geoData['city'];
            $city->url = str_slug($title);
            $city->description = utf8_decode($description);
            $city->latitude = $geoData['lat'];
            $city->longitude = $geoData['lng'];
            $city->department_id = $department->id;
            $city->country = 'fr';
            $city->save();
        }
    }
}
