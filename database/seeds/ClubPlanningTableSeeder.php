<?php

use Illuminate\Database\Seeder;

class ClubPlanningTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\Illuminate\Support\Facades\Storage::disk('planning')->files() as $file) {
            $title = substr($file, 0, strpos($file, "."));
            $club = \App\Club::where('url', $title)->first();
            if ($club) {
                $club->planning_image = '/storage/planning/'.$file;
                $club->save();
            }
        }
    }
}
