<?php

use Illuminate\Database\Seeder;
use App\PageGeneric;
use App\PageGenericTranslation;

class PageGenericSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $homePage = PageGeneric::create([
            'id' => 1,
            'type' => 'home',
            'image' => null,
            'concept' => null,
        ]);

        PageGenericTranslation::create([
            'id' => 1,
            'page_generic_id' => $homePage->id,
            'title' => "Bienvenue chez l'orange bleue",
            'subtitle' => "Plus de 380 salles de sport en France et en Espagne.",
            'description' => null,
            'text' => null,
            'text_2' => null,
            'text_3' => null,
            'alt_image' => null,
            'locale' => 'fr',
        ]);


        $activityPage = PageGeneric::create([
            'id' => 2,
            'type' => 'activity',
            'image' => null,
            'concept' => null,
        ]);

        PageGenericTranslation::create([
            'id' => 2,
            'page_generic_id' => $activityPage->id,
            'title' => "Activités sportives",
            'subtitle' => null,
            'description' => "<p>Que ce soit dans vos centres L’Orange Bleue Mon Coach Fitness ou L’Orange Bleue Mon Coach Wellness, nous vous proposons toute une palette d’activités vous permettant d’atteindre vos objectifs : des activités à la carte ou nos cours collectifs YAKO.</p>",
            'text' => "<p>Profitez de toutes vos activités dans vos clubs L'Orange Bleue Mon Coach Fitness et L'Orange Bleue Mon Coach Wellness.</p>",
            'text_2' => null,
            'text_3' => null,
            'alt_image' => null,
            'locale' => 'fr',
        ]);

        $equipmentPage = PageGeneric::create([
            'id' => 3,
            'type' => 'equipment',
            'image' => null,
            'concept' => null,
        ]);

        PageGenericTranslation::create([
            'id' => 3,
            'page_generic_id' => $equipmentPage->id,
            'title' => "Nos équipements",
            'subtitle' => null,
            'description' => "<p>Dans tous ses clubs, L’Orange Bleue vous propose un large choix d’équipements sportifs. Pour tous les goûts et pour tous les niveaux.</p>",
            'text' => "<p>Profitez de nos équipements pour vous entrainer et progresser dans nos clubs.</p>",
            'text_2' => null,
            'text_3' => null,
            'alt_image' => null,
            'locale' => 'fr',
        ]);

        $findClubPage = PageGeneric::create([
            'id' => 4,
            'type' => 'club',
            'image' => null,
            'concept' => null,
        ]);

        PageGenericTranslation::create([
            'id' => 4,
            'page_generic_id' => $findClubPage->id,
            'title' => "Trouver mon club",
            'subtitle' => null,
            'description' => "<p>Pour reprendre le sport en douceur ou pour entretenir sa forme, les clubs L’Orange Bleue vous accueillent partout en France, en Espagne ainsi qu’en Belgique. Rejoignez une salle de sport proposant des formules complètes et personnalisées pour votre remise en forme, ou pour votre programme de musculation.
            Il y a forcément une salle de sport à proximité de chez vous ! Retrouvez votre club de sport L’Orange Bleue le plus proche, pour profiter des nombreux services proposés par nos coachs diplômés d’État : cours collectifs à intensité variable, séances de musculation et de cardio pour un corps tonique, etc. Nos coachs adaptent le programme à vos objectifs, pour des séances sous le signe du sport plaisir.
            Grâce à la présence nationale de L’Orange Bleue, <a href='https://www.lorangebleue.fr/clubs/departements/'>dans tous les départements</a>, trouvez le cours de sport adapté à votre forme physique proche de chez vous. Dans une ambiance conviviale et énergisante, reprenez goût au sport en salle ! Vous êtes à la recherche d’un club de sport complet et accessible ? Entrez votre ville ou code postal et trouvez le club L’Orange Bleue le plus proche !</p>",
            'text' => null,
            'text_2' => null,
            'text_3' => null,
            'alt_image' => null,
            'locale' => 'fr',
        ]);

        $blogPage = PageGeneric::create([
            'id' => 5,
            'type' => 'blog',
            'image' => null,
            'concept' => null,
        ]);

        PageGenericTranslation::create([
            'id' => 5,
            'page_generic_id' => $blogPage->id,
            'title' => "Le blog",
            'subtitle' => null,
            'description' => null,
            'text' => null,
            'text_2' => null,
            'text_3' => null,
            'alt_image' => null,
            'locale' => 'fr',
        ]);
    }
}
