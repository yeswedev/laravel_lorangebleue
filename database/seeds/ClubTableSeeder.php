<?php

use Illuminate\Database\Seeder;

class ClubTableSeeder extends Seeder
{
    private function getElementText($element)
    {
        $result = "";
        if ($element->childNodes && count($element->childNodes)) {
            $result = "";
            for ($i = 0; $i < count($element->childNodes); $i++) {
                $child = $element->childNodes->item($i);
                $childText = $this->getElementText($child);
                if ($child->nodeName == "#text") {
                    $result .= $childText;
                } else {
                    $result .= "<" . $child->nodeName . ">" . $childText . "</" . $child->nodeName . ">";
                }
            }
        } else {
            if ($element->nodeValue != "") {
                $result = $element->nodeValue;
            }
        }
        return $result;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscription = \App\Subscription::where(['type' => 'fitness'])->first();

        foreach (\Illuminate\Support\Facades\Storage::disk('clubs')->files() as $file) {
            $html = $bodytag = str_replace("\n\n", "<br>", \Illuminate\Support\Facades\Storage::disk('clubs')->get($file));

            $dom = new \DOMDocument();
            $dom->loadHTML($html);
            $dom->preserveWhiteSpace = false;
            $elements = $dom->getElementsByTagName('body')->item(0)->childNodes;

            $title = substr($file, 0, strpos($file, "."));

            $description = "";
            $teamDescription = "";
            $teamTitle =

            $isCoachtext = false;

            foreach ($elements as $element) {
                $paragraph = $element->nodeValue;
                $text = $this->getElementText($element);
                if (strpos($paragraph, "Titre :") !== false) {
                } elseif (strpos($paragraph, "-----------") !== false) {
                    $isCoachtext = true;
                } elseif (strpos($paragraph, "Le mot d") !== false) {
                    $teamTitle = $text;
                } elseif ($isCoachtext) {
                    if ($paragraph != "" && $paragraph != "\n") {
                        $teamDescription .= $text . "<br/>";
                    }
                } else {
                    $description .= $text . "<br/>";
                }
            }
            $description = substr($description, strpos($description, "Description : ") + 14);
            // MAGIE NOIRE
            $description = str_replace("\n", "<br/>", $description);
            $description = str_replace("<br/><br/><br/>", "<br/>", $description);


            $imgName = str_replace("-", "_", str_slug($title));
            $imgName = str_replace(" ", "_", $imgName);

            $club = new \App\Club();
            $club->title = $title;
            $club->description = utf8_decode($description);
            $club->team_title = utf8_decode($teamTitle);
            $club->team_description = utf8_decode($teamDescription);
            $club->price_offer = 0;
            $club->image = 'photos/' . $imgName . '_facade' . '.jpg';
            $club->team_image = 'photos/' . $imgName . '_team' . '.jpg';
            $club->price_offer = 0;
            $club->subscription_id = $subscription ? $subscription->id : null;
            $club->city = str_replace("-", " ", str_slug($title));
            $club->url = $imgName;
            $club->save();
        }
    }
}
