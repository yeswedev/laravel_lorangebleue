<?php

use Illuminate\Database\Seeder;

class ClubOfferTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attach1 = \App\Offer::find(1);
        $attach2 = \App\Offer::find(2);
        $attach3 = \App\Offer::find(3);
        $attach4 = \App\Offer::find(4);
        foreach (\App\Club::get() as $club) {
            if (!count($club->offers)) {
                if ($attach1) {
                    $club->offers()->attach(1);
                }
                if ($attach2) {
                    $club->offers()->attach(2);
                }
                if ($attach3) {
                    $club->offers()->attach(3);
                }
                if ($attach4) {
                    $club->offers()->attach(4);
                }
                $club->save();
            }
        }
        return ;
    }
}
