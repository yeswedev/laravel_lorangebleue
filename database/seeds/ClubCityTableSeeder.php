<?php

use Illuminate\Database\Seeder;

class ClubCityTableSeeder extends Seeder
{
    public function createCityAndDepartmentFromCityName($cityName)
    {
        $geoData = \App\Lib\Geocoding::findLongLatFromAddress($cityName.",france");

        $department = \App\Department::where(['name' => $geoData['department']])->first();
        if (!$department) {
            $department = new \App\Department();
            $department->name = $geoData['department'];
            $department->url = str_slug($geoData['department']);
            $department->country = 'fr';
            $department->save();
        }

        $city = new \App\City();
        $city->name = $geoData['city'];
        $city->url = str_slug($cityName);
        $city->description = "";
        $city->latitude = $geoData['lat'];
        $city->longitude = $geoData['lng'];
        $city->department_id = $department->id;
        $city->country = 'fr';
        $city->save();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Club::get() as $club) {
            $city = \App\City::where('url', str_slug($club->city))->first();
            if (!$city) {
                $this->createCityAndDepartmentFromCityName($club->city);
            }
        }
        foreach (\App\City::getMainCitiesName() as $cityName) {
            $this->createCityAndDepartmentFromCityName($cityName);
        }
    }
}
