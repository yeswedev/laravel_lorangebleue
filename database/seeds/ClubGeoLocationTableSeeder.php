<?php

use Illuminate\Database\Seeder;

class ClubGeoLocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Club::get() as $club) {
            $geoData = \App\Lib\Geocoding::findLongLatFromAddress($club->address." ".$club->zip_code." ".$club->city.",france");
            $club->latitude = $geoData['lat'];
            $club->longitude = $geoData['lng'];
            $club->save();
        }
    }
}
