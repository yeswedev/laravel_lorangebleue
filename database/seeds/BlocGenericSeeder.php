<?php

use Illuminate\Database\Seeder;
use App\BlocGeneric;
use App\BlocGenericTranslation;

class BlocGenericSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conceptBloc = BlocGeneric::create([
            'id' => 1,
            'type' => 'concept',
            'image' => null,
            'image_2' => null,
        ]);

        BlocGenericTranslation::create([
            'id' => 1,
            'bloc_generic_id' => $conceptBloc->id,
            'title' => "Les Concepts de l'orange bleue",
            'subtitle' => "Pour vous permettre d’atteindre au mieux vos objectifs, L’Orange Bleue vous propose deux concepts.",
            'description' => null,
            'image_alt' => null,
            'image_alt_2' => null,
            'locale' => 'fr',
        ]);
    }
}
