<?php

use App\Http\Controllers\Back\BackOfficeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\Front\SpecialofferController;
use App\Http\Controllers\Front\PageController;
use App\Http\Controllers\Front\SubscriptionController;
use App\Http\Controllers\Front\ClubController;
use App\Http\Controllers\Front\LandingPageController;
use App\Http\Controllers\Front\RecruitmentController;
use App\Http\Controllers\Front\ArticleController;
use App\Http\Controllers\Front\ActivityController;
use App\Http\Controllers\Front\EquipmentController;
use App\Http\Controllers\Front\YakoController;
use App\Http\Controllers\Front\FaqController;
use App\Http\Controllers\Front\ContactController;

Route::group(['middleware' => []], function () {

    /* TODO To refactor */
    $languages = \Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales();
    $host = \Request::root();

    foreach ($languages as $locale => $language) {
        if (((!empty($language['tld'])) && ends_with($host, $language['tld']))
            || ((!empty($language['subdomain'])) && starts_with($host, $language['subdomain']))) {
            \Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale($locale);
            break;
        }
    }
    /* END TODO TO REFACTOR */

    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('/robots.txt', [HomeController::class, 'robotstxt']);
    Route::get('/redirect', [HomeController::class, 'redirect'])->name('redirect');
    Route::get('/newsletter', [HomeController::class, 'newsletter'])->name('newsletter');
    Route::get('/sitemapxml', [SitemapController::class, 'index']);

    Route::get('/payins', [BackOfficeController::class, 'listPayin'])->name('listPayin');
    Route::get('/payouts', [BackOfficeController::class, 'listPayoutFailed'])->name('listPayoutFailed');

    Route::get('/transfers', [BackOfficeController::class, 'listTransferFailed'])->name('listTransferFailed');
    Route::get('/transfers/{order}', [BackOfficeController::class, 'doTransfer'])->name('doTransfer');

    Route::get('/verifyRateLimits', [BackOfficeController::class, 'verifyRateLimits'])->name('verifyRateLimits');

    Route::get('/schedules', [BackOfficeController::class, 'schedule'])->name('BOSchedule');
    Route::post('/schedules/import', [BackOfficeController::class, 'scheduleImport'])->name('BOScheduleImport');
    Route::get('/schedules/template', [BackOfficeController::class, 'getTemplate'])->name('BOScheduleTemplate');

    Route::get('/is-free-access', [BackOfficeController::class, 'isFreeAccess'])->name('BOIsFreeAccess');
    Route::post('/is-free-access/change', [BackOfficeController::class, 'changeIsFreeAccess'])->name('BOIsFreeAccessChanges');
    Route::get('/is-free-access/template', [BackOfficeController::class, 'getTemplate'])->name('BOIsFreeAccessTemplate');

    Route::get('/link-yakos-activities-clubs', [BackOfficeController::class, 'linkToClub'])->name('BOLinkToClub');
    Route::post('/link-yakos-activities-clubs/post', [BackOfficeController::class, 'goLinkToClub'])->name('BOLinkToClubPost');
    Route::get('/link-yakos-activities-clubs/template', [BackOfficeController::class, 'getTemplate'])->name('BOLinkToClubTemplate');

    Route::get('/export-plannings', [BackOfficeController::class, 'exportPlannings'])->name('BOExportPlannings');
    Route::get('/export-plannings/get', [BackOfficeController::class, 'exportPlanningsGet'])->name('BOExportPlanningsGet');



    Route::get(LaravelLocalization::transRoute('routes.specialoffer.index'), [SpecialofferController::class, 'index'])->name('specialoffer.index');

    Route::get(LaravelLocalization::transRoute('routes.page.page'), [PageController::class, 'retrieve'])->name('page.retrieve');
    Route::get(LaravelLocalization::transRoute('routes.page.child'), [PageController::class, 'retrieveChild'])->name('page.retrieveChild');
    Route::post(LaravelLocalization::transRoute('routes.contact.open_club'), [PageController::class, 'contact'])->name('contact.open_club');

    Route::get(LaravelLocalization::transRoute('routes.abonnements.index'), [SubscriptionController::class, 'index'])->name('subscription.index');

    Route::get(LaravelLocalization::transRoute('routes.abonnements.step1'), [SubscriptionController::class, 'step1'])->name('subscription.step1');
    Route::post(LaravelLocalization::transRoute('routes.abonnements.step1'), [SubscriptionController::class, 'searchTunnelStep1'])->name('front.subscription.club-search');

    Route::post(LaravelLocalization::transRoute('routes.clubs.contact'), [ClubController::class, 'contact'])->name('pre-inscription.contact')->name('subscription.init');
    Route::get(LaravelLocalization::transRoute('routes.clubs.contactUs'), [ClubController::class, 'contactUs'])->name('contact-us.contact');

    Route::group(['middleware' => ['order']], function () {
        Route::get(LaravelLocalization::transRoute('routes.abonnements.step2'), [SubscriptionController::class, 'step2'])->name('subscription.step2');
        Route::get(LaravelLocalization::transRoute('routes.abonnements.step3'), [SubscriptionController::class, 'step3'])->name('subscription.step3');
        Route::get(LaravelLocalization::transRoute('routes.abonnements.step4'), [SubscriptionController::class, 'step4'])->name('subscription.step4');
        Route::get(LaravelLocalization::transRoute('routes.abonnements.step-payment'), [SubscriptionController::class, 'payment'])->name('subscription.payment');
        Route::get(LaravelLocalization::transRoute('routes.abonnements.step-confirmation'), [SubscriptionController::class, 'stepConfirmation'])->name('subscription.confirmation');
    });

    Route::get(LaravelLocalization::transRoute('routes.departments'), [ClubController::class, 'listDepartmentAvailableClub'])->name('front.departments');
    Route::get(LaravelLocalization::transRoute('routes.cities'), [ClubController::class, 'listMostClubsPerCity'])->name('front.cities');

    Route::get(LaravelLocalization::transRoute('routes.clubs.search'), [ClubController::class, 'search'])->name('front.find-club');
    Route::get(LaravelLocalization::transRoute('routes.clubs.search-by-department'), [ClubController::class, 'searchbydepartment'])->name('front.find-club-by-department');
    Route::get(LaravelLocalization::transRoute('routes.clubs.details'), [ClubController::class, 'show'])->name('front.club-details');
    Route::get(LaravelLocalization::transRoute('routes.clubs.club-list'), [ClubController::class, 'clubsList'])->name('front.club-list');
    Route::get(LaravelLocalization::transRoute('routes.clubs.club-list-by-department'), [ClubController::class, 'clubsList'])->name('front.club-list-by-department');
    Route::get(LaravelLocalization::transRoute('routes.clubs.pre-confirmation'), [ClubController::class, 'preConfirmation'])->name('front.tunnel.confirm-pre');

    Route::group(['middleware' => ['landing-page']], function () {
        Route::get(LaravelLocalization::transRoute('routes.landing-page.club'), [LandingPageController::class, 'club'])->name('landing-page.club');
        Route::get(LaravelLocalization::transRoute('routes.landing-page.city'), [LandingPageController::class, 'club'])->name('landing-page.city');
    });

    Route::post(LaravelLocalization::transRoute('routes.landing-page.club-contact'), [LandingPageController::class, 'clubContact'])->name('landing-page.club-contact');

    Route::get(LaravelLocalization::transRoute('routes.recruitment.index'), [RecruitmentController::class, 'index'])->name('recruitment.index');
    Route::get(LaravelLocalization::transRoute('routes.recruitment-offers'), [RecruitmentController::class, 'listOffers'])->name('recruitment.offers');
    Route::get(LaravelLocalization::transRoute('routes.recruitment-offer'), [RecruitmentController::class, 'offerDetails'])->name('recruitment.offer');
    Route::get(LaravelLocalization::transRoute('routes.recruitment-apply'), [RecruitmentController::class, 'apply'])->name('recruitment.apply');
    Route::post(LaravelLocalization::transRoute('routes.recruitment-applied'), [RecruitmentController::class, 'applied'])->name('recruitment.applied');

    Route::get(LaravelLocalization::transRoute('routes.blog.list'), [ArticleController::class, 'showAll'])->name('blog.showAll');
    Route::get(LaravelLocalization::transRoute('routes.blog.single'), [ArticleController::class, 'retrieve'])->name('blog.retrieve');
    Route::get(LaravelLocalization::transRoute('routes.blog.search'), [ArticleController::class, 'showAll'])->name('blog.search');

    Route::get(LaravelLocalization::transRoute('routes.activites.list'), [ActivityController::class, 'showAll'])->name('activity.showAll');
    Route::get(LaravelLocalization::transRoute('routes.equipements.list'), [EquipmentController::class, 'showAll'])->name('equipment.showAll');
    Route::get(LaravelLocalization::transRoute('routes.activites.yakos'), [YakoController::class, 'showAll'])->name('yako.showAll');

    Route::get(LaravelLocalization::transRoute('routes.equipements.single'), [EquipmentController::class, 'retrieve'])->name('equipment.retrieve');
    Route::get(LaravelLocalization::transRoute('routes.activites.single'), [ActivityController::class, 'retrieve'])->name('activity.retrieve');
    Route::get(LaravelLocalization::transRoute('routes.activites.yako-single'), [YakoController::class, 'retrieve'])->name('yako.retrieve');
    Route::get(LaravelLocalization::transRoute('routes.sitemap'), 'Front\SitemapController@show')->name('sitemap.html');

    Route::get(LaravelLocalization::transRoute('routes.faq'), [FaqController::class, 'showAll'])->name('faq.showAll');

    Route::get(LaravelLocalization::transRoute('routes.contact.index'), [ContactController::class, 'index'])->name('contact.index');
    Route::post(LaravelLocalization::transRoute('routes.contact.mail'), [ContactController::class, 'mail'])->name('contact.mail');
    Route::post(LaravelLocalization::transRoute('routes.contact.mailFromModalTunnelAbo'), [ContactController::class, 'mailFromModalTunnelAbo'])->name('contact.mailFromModalTunnelAbo');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/exportmv', 'HomeController@exportMV')->name('exportmv');
});
