<?php

// Accueil
Breadcrumbs::for('home', function ($trail) {
    $trail->push(trans('trad.breadcrumbs.accueil'), route('index'));
});

// Accueil > Activites
Breadcrumbs::for('activites', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('trad.breadcrumbs.activites'), route('activity.showAll'));
});

// Accueil > Activites > urlActivites
Breadcrumbs::for('single-activites', function ($trail, $activity) {
    $trail->parent('activites');
    $trail->push($activity->title, route('activity.retrieve', $activity->url));
});


// Accueil > Activites > Cours collectifs
Breadcrumbs::for('yakos', function ($trail) {
    $trail->parent('activites');
    $trail->push(trans('trad.breadcrumbs.cours-co'), route('yako.showAll'));
});

// Accueil > Activites > Cours collectifs > yakourl
Breadcrumbs::for('single-yakos', function ($trail, $yako) {
    $trail->parent('activites');
    $trail->push(trans('trad.breadcrumbs.cours-co'), route('yako.showAll'));
    $trail->push('Yako '.$yako->title, route('yako.retrieve', $yako->url));
});

// Accueil > Equipements
Breadcrumbs::for('equipments', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('trad.breadcrumbs.equipments'), route('equipment.showAll'));
});

// Accueil > Equipements > EquipementUrl
Breadcrumbs::for('single-equipments', function ($trail, $equipment) {
    $trail->parent('equipments');
    $trail->push($equipment->title, route('equipment.retrieve', $equipment->url));
});

// Accueil > FAQ
Breadcrumbs::for('faq', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('trad.breadcrumbs.faq'), route('faq.showAll'));
});

// Accueil > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('trad.breadcrumbs.blog'), route('blog.showAll'));
});

// Accueil > Blog > Article
Breadcrumbs::for('single-blog', function ($trail, $blog, $category) {
    $trail->parent('blog', $blog);

    $parent = $blog->parent;
    if ($parent != null) {
        $trail->push($category->url, route('blog.retrieve', $category->url));
        $trail->push(ucfirst(strtolower($blog->title)));
    } else {
        $trail->push(trans('trad.breadcrumbs.categories').$blog->id, route('blog.retrieve', [trans('trad.breadcrumbs.categories').$blog->id,$blog->url]));
        $trail->push(ucfirst(strtolower($blog->title)));
    }
});


// Accueil > Trouver mon club
Breadcrumbs::for('clubs', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('trad.breadcrumbs.find-club'), route('front.find-club'));
});

// Accueil > Trouver mon club > Département
Breadcrumbs::for('results-club-department', function ($trail, $department) {
    $trail->parent('clubs');
    $trail->push($department->name, route('front.club-details', $department->url));
});

// Accueil > Trouver mon club > Département > Ville
Breadcrumbs::for('results-club-city', function ($trail, $department, $result) {
    $trail->parent('clubs');
    $trail->push($department->name, route('front.club-details', $department->url));
    $trail->push(trans('trad.breadcrumbs.salle-de-sport').$result, route('front.find-club'));
});

// Accueil > Trouvez mon club > Département > Ville > Title club
Breadcrumbs::for('single-club', function ($trail, $club) {
    $trail->parent('results-club-department', $club->department);
    $trail->push($club->city, route('front.club-list', [$club->department->url , str_slug($club->city)]));
    $trail->push(trans('trad.breadcrumbs.club').$club->title);
});


// Accueil > page > nom de page
Breadcrumbs::for('page-cms', function ($trail, $page) {
    $trail->parent('home');
    $trail->push(trans('trad.breadcrumbs.page'));
    $trail->push($page->title, route('page.retrieve', $page->url));
});


//Accueil > nom de l'abo
Breadcrumbs::for('single-abonnement', function ($trail, $subscription) {
    $trail->parent('home');
    $trail->push(ucfirst($subscription->title), route('subscription.index', $subscription->url));
});
