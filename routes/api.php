<?php

use Illuminate\Http\Request;
use \App\Http\Controllers\Api\v1\RecruitmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/user', function (Request $request) {
            return $request->user();
        });
    });

    Route::get('/offers', 'Api\v1\OfferController@showAll');
    Route::get('/offers/{offer}', 'Api\v1\OfferController@show');
    Route::get('/orders/{order}', 'Api\v1\OrderController@getTotalPrice');
    Route::get('/clubs/{id?}', 'Api\v1\ClubController@getClubsIds');
    Route::get('/clubs/{club_id}/schedule', 'Api\v1\ClubController@getSchedule');
    Route::get('/clubs/{club_id}/isfitness/{landing_page_id}', 'Api\v1\ClubController@isFitness');
    Route::get('/clubs/{club_id}/subscription', 'Api\v1\ClubController@getSubscription');
    Route::get('/mangopay', [\App\Http\Controllers\Api\v1\PaymentController::class, 'mangopay']);
});
