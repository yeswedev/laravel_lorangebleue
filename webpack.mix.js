let mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copyDirectory('resources/images', 'public/images');
mix.copyDirectory('resources/csv_templates', 'public/csv_templates');
mix.copyDirectory('resources/fonts', 'public/fonts');

if ( ! mix.inProduction()) {
    mix.webpackConfig({
        devtool: 'inline-source-map'
    })
}

mix.js('resources/js/mbox.js', 'public/js')
    .js('resources/js/tag-commander-event.js', 'public/js')
    .js('resources/js/app.js', 'public/js')
    .extract(['vue','jquery','axios','popper.js','numeral'])
    .styles([
       'resources/css/libs/jquery-ui.css',
       'resources/css/libs/jquery-ui.min.css',
       'resources/css/libs/owl.carousel.min.css',
       'resources/css/libs/owl.theme.default.min.css',
       'resources/css/libs/select2.min.css',
    ], 'public/css/all.css')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/nova/nova.scss', 'public/css')
    .version();

if ( ! mix.inProduction()) {
    mix.browserSync('lorangebleue.local');
}

