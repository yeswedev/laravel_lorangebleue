<?php

return [
    "activites"  => [
        "list" => "activites",
        "single" => "activites/{activity}",
        "yakos" => "activites/cours-collectifs",
        "yako-single" => "activites/cours-collectifs/{yakoUrl}",
    ],
    "equipements" => [
        "list" => "equipements",
        "single" => "equipements/{equipment}",
    ],
    "clubs" => [
        "search" => "clubs",
        "search-by-department" => "clubs/departements",
        "details" => "clubs/{clubUrl}",
        "club-list" => "clubs/{department}/{city}",
        "club-list-by-department" => "clubs/{department}",
        "contact" => "clubs/{club}/contact",
        "contactUs" => "clubs/{club}/contact-us",
        "pre-confirmation" => "club/{clubUrl}/pre-confirmation",
    ],
    "blog" => [
        "list" => "blog/{slug?}",
        "single" => "blog/{categoryUrl}/{articleUrl?}",
        "search" => "blog/",
    ],

    "specialoffer" => [
        "index" => "offre-speciale/{specialofferUrl}"
    ],

    "page" => [
        "page" => "page/{pageUrl}",
        "child" => "a-propos/{pageUrl}", // TODO à rendre dynamique, ça ne fonctionne qu'avec a-propos
        "cgv" => "conditions-generales-de-vente",
        "legal-mentions" => "mentions-legales",
        "rgpd" => "protection-des-donnees",
        "ouvrir-salle-sport" => "ouvrir-salle-sport",
        "ouvrir-salle-sport-accompagnement" => "ouvrir-salle-sport-accompagnement"
    ],
    "abonnements" => [
        "index" => "abonnements/{subscriptionUrl}",
        "step1" => "abonnements/{subscriptionUrl}/tunnel-etape1",
        "step2" => "abonnements/{subscriptionUrl}/tunnel-etape2",
        "step3" => "abonnements/{subscriptionUrl}/tunnel-etape3",
        "step4" => "abonnements/{subscriptionUrl}/tunnel-etape4",
        "step-payment" => "abonnements/{subscriptionUrl}/tunnel-payment",
        "step-confirmation" => "abonnements/{subscriptionUrl}/tunnel-confirmation",
    ],
    "recruitment" => [
        "index" => "recrutement",
    ],
    "recruitment-offers" => "recrutement/offres",
    "recruitment-offer" => "recrutement/offres/{offre_id}",
    "recruitment-apply" => "recrutement/postuler",
    "recruitment-applied" => "recrutement/postuler/post",
    "departments"  => "clubs/departments",
    "cities"  => "clubs/villes",
    "sitemap"  => "plan-du-site",
    "faq" => "/faq",
    "contact" => [
        "index" => "/contact",
        "mail" => "/contact/mail",
        "mailFromModalTunnelAbo" => "/contact/modal/tunnel-abo",
        "open_club" => "/contact/ouvrir-salle-sport"
    ],
    "landing-page" => [
        "city" => 'lp/{landingPage}/{city}/',
        "club" => 'lp/{landingPage}/{city}/{club_id}',
        "club-contact" => 'lp/{landingPage}/contact',
    ],
];
