<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Les mots de passe doivent comporter au moins six caractères et correspondre à la confirmation.',
    'reset' => 'Votre mot de passe a été réinitialisé!',
    'sent' => 'Nous avons envoyé un e-mail de votre lien de réinitialisation de mot de passe!',
    'token' => "Ce Token de réinitialisation du mot de passe n'est pas valide.",
    'user' => "Nous ne pouvons pas trouver un utilisateur avec cette adresse e-mail.",

    'resetPassword' => 'Réinitialiser mot de passe',
    'emailAddress' => 'Adresse email',
    'sendPassword' => 'Envoyer le lien de réinitialisation de mot de passe',
    'passwordPWD' => "Mot de passe",
    'confirmPasswordPWD' => "Confirmation de mot de passe",

    'firstLine' => 'Vous recevez cet e-mail, car nous avons reçu une demande de réinitialisation du mot de passe pour votre compte.',
    'secondLine' => "Si vous n'avez pas demandé la réinitialisation du mot de passe, aucune autre action n'est requise.",

];
