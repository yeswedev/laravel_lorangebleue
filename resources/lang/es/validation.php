<?php

return array (
  'accepted' => 'El  :attribute debe ser aceptado.',
  'active_url' => 'El :attribute no es una URL válida.',
  'after' => 'El :attribute debe ser una fecha posterior a ',
  'alpha' => 'El :attribute solo puede contener letras.',
  'alpha_dash' => 'El :attribute solamente puede contener letras, números y guiones.',
  'alpha_num' => 'El :attribute solamente puede contener letras y números.',
  'array' => 'El :attribute debe ser un arreglo.',
  'attributes' => 
  array (
    'address' => 'dirección',
    'age' => 'edad',
    'body' => 'contenido',
    'city' => 'ciudad',
    'content' => 'contenido',
    'country' => 'país',
    'date' => 'fecha',
    'day' => 'día',
    'description' => 'descripción',
    'email' => 'correo electrónico',
    'excerpt' => 'extracto',
    'first_name' => 'nombre',
    'firstname' => 'nombre',
    'gender' => 'género',
    'hour' => 'hora',
    'last_name' => 'apellido',
    'message' => 'mensaje',
    'minute' => 'minuto',
    'mobile' => 'móvil',
    'month' => 'mes',
    'name' => 'nombre',
    'password' => 'contraseña',
    'password_confirmation' => 'confirmación de la contraseña',
    'phone' => 'teléfono',
    'second' => 'segundo',
    'sex' => 'sexo',
    'subject' => 'asunto',
    'time' => 'hora',
    'title' => 'título',
    'username' => 'usuario',
    'year' => 'año',
  ),
  'before' => 'El :attribute debe ser una fecha antes de :date.',
  'between' => 
  array (
    'array' => 'El :attribute debe estar entre :min y :max items.',
    'file' => 'El :attribute debe estar entre :min y :max kilobytes.',
    'numeric' => 'El :attribute debe estar entre :min y :max.',
    'string' => 'El :attribute debe estar entre :min y :max caracteres.',
  ),
  'boolean' => 'El campo :attribute debe ser verdadero o falso',
  'confirmed' => 'La confirmación de :attribute no coincide.',
  'custom' => 
  array (
    'address' => 
    array (
      'required' => 'Une adresse est obligatoire',
    ),
    'birthdate' => 
    array (
      'date_format' => '',
      'required' => 'Une date de naissance est obligatoire',
    ),
    'city' => 
    array (
      'required' => 'Une ville est obligatoire',
    ),
    'civility' => 
    array (
      'required' => 'Veuillez sélectionner un genre',
    ),
    'email' => 
    array (
      'required' => 'Un email est obligatoire',
    ),
    'firstname' => 
    array (
      'required' => 'Un prénom est obligatoire',
    ),
    'lastname' => 
    array (
      'required' => 'Un nom est obligatoire',
    ),
    'mode' => 
    array (
      'required' => 'Veuillez sélectionner un mode de paiement',
    ),
    'radio' => 
    array (
      'required' => 'Veuillez sélectionner une offre',
    ),
    'terms' => 
    array (
      'required' => 'Vous devez accepter les conditions générales de vente et conditions de paiement',
    ),
    'zip_code' => 
    array (
      'required' => 'Un code postal est obligatoire',
    ),
  ),
  'date' => 'El :attribute no es una fecha valida.',
  'date_format' => 'No coincide el formato :format de :attribute',
  'different' => 'El :attribute y:otro debe ser diferente.',
  'digits' => ':attribute deben ser digitos.',
  'digits_between' => 'El :attribute debe estar entre  min y : max digitos.',
  'dimensions' => 'la imagen :attribute tiene dimensiones incorrectas',
  'distinct' => 'El :attribute el campo tiene un valor duplicado',
  'email' => ':attribute debe ser una direccion de correo valida.',
  'emailForm' => 
  array (
    'error' => 'Ha ocurrido un problema, por favor intente de nuevo más tarde',
    'valid' => 'Su correo electrónico ha sido enviado',
  ),
  'exists' => 'El : attribute seleccionado es invalido.',
  'file' => 'El :attribute debe ser un archivo.',
  'filled' => 'El campo :attribute es requerido.',
  'image' => ' :attribute debe ser una imagen.',
  'in' => 'El :attribute seleccionado es invalido.',
  'in_array' => 'El campo :attribute no existe en :other.',
  'integer' => 'El :attribute debe ser un entero.',
  'ip' => 'El :attribute debe ser una direccion ip valida.',
  'json' => 'El :attribute debe ser una cadena valida de JSON',
  'max' => 
  array (
    'array' => 'El :attribute no debe tener más de :max elementos.',
    'file' => 'El :attribute no debe ser mas grande que :max kilobytes.',
    'numeric' => 'El :attribute no debe ser mas grande que :max.',
    'string' => 'El :attribute no debe ser mas grande que :max caracteres.',
  ),
  'mimes' => 'El :attribute debe ser un fichero de tipo: :values',
  'min' => 
  array (
    'array' => 'El :attribute debe ser al menos :min elementos.',
    'file' => 'El :attribute debe ser al menos :min kylobytes.',
    'numeric' => 'El :attribute debe ser al menos :min.',
    'string' => 'El :attribute debe ser al menos :min caracteres.',
  ),
  'not_in' => 'El :attribute seleccionado no és valido.',
  'numeric' => 'El :attribute debe ser un número.',
  'present' => 'El campo del atributo :attribute debe estar presente.',
  'recaptcha' => 'Se produjo un error al validar la :attribute',
  'regex' => 'El formato del :attribute no es válido.',
  'required' => 'El campo :attribute es obligatorio.',
  'required_if' => 'El campo :attribute es requerido cuando :other tiene el valor :value.',
  'required_unless' => 'El campo :attribute es requerido salvo que :other esta en los valores :values.',
  'required_with' => 'El campo :attribute es requerido cuando :values esta presente.',
  'required_with_all' => 'El campo :attribute es requerido cuando :values esta presente.',
  'required_without' => 'El campo :attribute es requerido cuando :values no esta presente.',
  'required_without_all' => 'El campo :attribute es requerido cuando ninguno de los :values estan presentes.',
  'same' => 'El :attribute y :other deben coincidir.',
  'size' => 
  array (
    'array' => 'El :attribute debe ser :size elementos',
    'file' => 'El :attribute debe ser :size kilobytes.',
    'numeric' => 'El :attribute debe ser :size.',
    'string' => 'El :attribute debe ser :size caracteres.',
  ),
  'string' => 'El :attribute debe ser una cadena.',
  'timezone' => 'El :attribute debe ser una zona valida.',
  'unique' => 'El atributo :attribute ya ha sido usado.',
  'url' => 'El formato de :attribute es invalido.',
);
