<?php

return [
    "activites"  => [
        "list" => "actividades",
        "single" => "actividades/{activity}",
        "yakos" => "actividades/clases-dirigidas",
        "yako-single" => "actividades/clases-dirigidas/{yakoUrl}",
    ],
    "equipements" => [
        "list" => "equipos",
        "single" => "equipos/{equipment}",
    ],
    "clubs" => [
        "search" => "gimnasios",
        "search-by-department" => "gimnasios/comunidad-autonomas",
        "details" => "gimnasios/{clubUrl}",
        "club-list" => "gimnasios/{department}/{city}",
        "club-list-by-department" => "gimnasios/{department}",
        "contact" => "gimnasios/{club}/contact",
        "contactUs" => "gimnasios/{club}/contact-us",
        "pre-confirmation" => "gimnasio/{clubUrl}/pre-confirmation",
    ],
    "blog" => [
        "list" => "blog/{slug?}",
        "single" => "blog/{categoryUrl}/{articleUrl?}",
        "search" => "blog/search",
    ],

    "specialoffer" => [
        "index" => "oferta-especial/{specialofferUrl}"
    ],

    "page" => [
        "page" => "pagina/{pageUrl}",
        "child" => "a-proposito/{pageUrl}", // TODO à rendre dynamique, ça ne fonctionne qu'avec a-propos
        "cgv" => "condiciones-generales-de-ventas",
        "legal-mentions" => "menciones-legales",
        "rgpd" => "proteccion-de-datos",
        "ouvrir-salle-sport" => "abrir-un-gimnasio",
        "ouvrir-salle-sport-accompagnement" => "abrir-un-gimnasio-acompanamiento"
    ],
    "abonnements" => [
        "showAll" => "suscripciones/concepts/tunnel-etape1",
        "index" => "suscripciones/{subscriptionUrl}",
        "step1" => "suscripciones/{subscriptionUrl}/tunnel-etape1",
        "step2" => "suscripciones/{subscriptionUrl}/tunnel-etape2",
        "step3" => "suscripciones/{subscriptionUrl}/tunnel-etape3",
        "step4" => "suscripciones/{subscriptionUrl}/tunnel-etape4",
        "step-payment" => "suscripciones/{subscriptionUrl}/tunnel-payment",
        "step-confirmation" => "suscripciones/{subscriptionUrl}/tunnel-confirmation",
    ],
    "recruitment" => [
        "index" => "reclutamiento",
    ],
    "recruitment-offers" => "reclutamiento/offres",
    "recruitment-offer" => "reclutamiento/offres/{offre_id}",
    "recruitment-apply" => "reclutamiento/postuler",
    "recruitment-applied" => "reclutamiento/postuler/post",
    "departments"  => "gimnasios/departments",
    "cities"  => "gimnasios/villes",
    "sitemap"  => "mapa-del-sitio",
    "faq" => "/preguntas-frecuentes",
    "contact" => [
        "index" => "/contact",
        "mail" => "/contact/mail",
        "mailFromModalTunnelAbo" => "/contact/modal/tunnel-abo",
        "open_club" => "/contact/abrir-un-gimnasio"
    ],
    "landing-page" => [
        "city" => 'lp/{landingPage}/{city}',
        "club" => 'lp/{landingPage}/{city}/{club_id}',
        "club-contact" => 'lp/{landingPage}/contact',
    ],
];
