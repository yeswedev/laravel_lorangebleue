<?php

return [
    'message' => "En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies pour vous proposer des offres d'abonnements selon vos centre d'intérêt.",
    'agree' => "J'ai compris",
];
