@extends('layouts.front')

@section('title')
    @if(issetTrad('meta.title.abonnement.'.$subscription->type))
        @lang('meta.title.abonnement.'.$subscription->type)
    @else
        @lang('meta.title.abonnement.default')
    @endif
@endsection

@section('description')
    @if(issetTrad('meta.description.abonnement.'.$subscription->type))
        @lang('meta.description.abonnement.'.$subscription->type)
    @else
        @lang('meta.description.abonnement.default')
    @endif
@endsection
@section('content')
    @include('front.header')
    <div class="subscription-single">
        <div
            @isset($subscription)
                @if($subscription->image)
                class="subscription-single__header subscription-header container--flex container--flex-start"
                style="background: #004888 url({{Storage::url($subscription->image)}}) no-repeat;background-size: cover;background-position-x: 45%;"
                @else
                    class="subscription-single__header subscription-header @if($subscription->type == 'fitness') subscription-header--fitness @elseif($subscription->type == 'wellness') subscription-header--wellness @endif container--flex container--flex-start"
                @endif
            @else
                class="subscription-single__header subscription-header @if($subscription->type == 'fitness') subscription-header--fitness @elseif($subscription->type == 'wellness') subscription-header--wellness @endif container--flex container--flex-start"
            @endisset
        >
            <div class="container container-1170 contain-wellness">
                @if($subscription->type == 'wellness')
                    <div class="subscription-single__header-body container--margin-t-100 style-wellness">
                        @else
                            <div class="subscription-single__header-body container--padding-t-100">
                                @endif
                                <span class="concept-logo">
                                    <img class="subscription-header__image single--logo-type lazy"
                                        data-src="{{ url('/images/logo_orange_bleue_'.$subscription->type.'_white.svg')}}"
                                        width="100%" height="100%"
                                        alt="L'Orange Bleue mon coach {{$subscription->type}}"/> {{-- Image Change selon type (ob-wellness.png) --}}
                                </span>
                                @if($subscription->type == 'wellness')
                                    <h1 class="subscription-header__title subscription-header__title--wellness h1 {{$subscription->type}}">
                                        {{$subscription->title}}
                                    </h1>
                                    <p class="subscription-header__text">
                                        @if($subscription->subtitle)
                                            {{$subscription->subtitle}}
                                        @else
                                            @lang('trad.subscription-single.description-'.$subscription->type)
                                        @endif
                                    </p>
                                @endif
                                @if($subscription->type == 'fitness')
                                    <div class="subscription-single__image--mobile">
                                        {{--<img src="{{ url('/images/coach-sportif.png')}} "--}}
                                        {{--alt="coach sportif"/> --}}{{--  Change selon type ( pas encore de version pour wellness --}}
                                        <div class="subscription-single__content-mobile">
                                            <h1 class="subscription-header__title subscription-header__title--fitness h1 {{$subscription->type}}">
                                                {{$subscription->title}}
                                            </h1> {{-- Class Change selon type --}}
                                            <p class="subscription-header__text">
                                                @if($subscription->subtitle)
                                                    {{$subscription->subtitle}}
                                                @else
                                                    @lang('trad.subscription-single.description-'.$subscription->type)
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                @else
                                @endif
                            </div>

                    </div>
                    <div class="main-header__picto">
                        @include('front.components.picto',['wellness' => true])
                    </div>
            </div>
            <div class="container-full-w container-abo container--bg-l-grey container--padding-t-150 container--padding-b-80">
                <div class="container container-1170">
                    {{-- Disparait en wellness --}}
                    @if($subscription->type == 'fitness')
                        <div class="subscription--margin-b-50">
                            <h2 class="subscription__title h2 text--t-center title--underlined-center--blue text-uppercase">
                                @lang('trad.common.subscription-plural')</h2>
                        </div>
                        <div class="subscription__text">
                            <p class="text--t-center">@lang('trad.subscription-single.subtitle')</p>
                        </div>

                        @include('front.components.subscription-banner', ['subscriptionUrl' => $subscription->url, 'price' => $formattedBannerPrice, 'aside' => false])
                        @if($subscription->type == 'fitness')
                            @include('front.components.offer', ['offers' => \App\Offer::activeOffers(\App\Offer::getIsOnHomeOffersFilter($subscription->offers())), 'club' => null, 'subscriptionUrl' => $subscription->url, 'order' => null])
                        @endif
                    @endif
                </div>
            </div>
            @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'fr')
                <div class="container container-full-w"
                     style="background:url('/images/ORANGE_BLEU_MAP.png') no-repeat center center;background-position: bottom;">
                    <div class="row">
                        <div class="club__overlay"></div>
                        <div class="club__container col-md-12">
                            <div class="club--margin-b-50">
                                <h2 class="club__title h2 text--t-center title--underlined-center--blue text-uppercase">@lang('trad.header.find-club')</h2>
                            </div>

                            <div class="club__text">
                                <p class="text--t-center">@lang('trad.find-club-form.subtitle-'.$subscription->type)</p>
                            </div>

                            <div class="find_club__item">
                                {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex align-items-center')) }}
                                @include('front.clubs.components.club-form')
                                <input type="hidden" name="type" value="{{$subscription->type}}">
                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div>
            @endif
            @if($subscription->presentation1)
                <div class="container container-1170  presentation-container">
                    <div class="presentation-container_content container--padding-t-80">
                        <div class="row">
                            <div class="col-xl-6 col-md-12 text-md-center">
                                <img
                                        @if($subscription->image1)
                                        src="{{Storage::url($subscription->image1)}}"
                                        @else
                                        src="{{ url('/images/femme-fitness.png')}}"
                                        @endif
                                        alt="image présentation 1"/>
                            </div>
                            <div class="col-xl-6 col-md-12 align-self-center">
                                <div class="presentation--container__desc">
                                    <div class="presentation--container__title">
                                        <h2 class="presentation__title h2 title--underlined-center-md--blue title--underlined-left--blue text-uppercase mb-5">
                                            {{$subscription->title1}} </h2>
                                    </div>
                                    <div class="presentation--container__text">
                                        <p>{{$subscription->presentation1}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            {{-- Disparait en wellness --}}
            @if($subscription->type == 'fitness')
                <div class="container container-1170">
                    <div class="container-details__yako">
                        <div class="mb-5">
                            <h2 class="h2 text--t-center title--underlined-center--blue text-uppercase">
                                @lang('trad.yako-slider.title')</h2>
                        </div>
                        <div class="subscription__text mb-5">
                            <p class="text--t-center"> @lang('trad.yako-slider.subtitle')</p>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="details-yako__mainimage">
                                    <div id="js-yako-video-arrow" class="js-yako-arrow">
                                        <div class="details-yako--rounded-blue">
                                            <img class="rounded-blue--image" src="{{url('/images/arrowvideo.png')}}"
                                                 alt="img-player"/>
                                        </div>
                                    </div>
                                    <img id="js-yako-image" class="yako__mainimage-presentation" src=""/>
                                    <iframe id="js-yako-video" frameborder="0" allowfullscreen="1"
                                            allow="encrypted-media"
                                            title="YouTube video player" class="yako__mainimage-presentation"
                                            src=""></iframe>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="details-yako-slider details-yako__list owl-carousel owl-theme ">
                                    @forelse($yako_lists as $yako)
                                        <div class="item">
                                            <div class="js-yako-illustration-selector yako__list__item js-switch-state"
                                                 data-addclass="yako__list__item--activeYako"
                                                 yako_illustration_type="{{$yako['illustration_type']}}"
                                                 yako_illustration_src="{{$yako['illustration_src']}}">
                                                <p>Yako {{$yako['object']->title}}</p>
                                            </div>
                                            <div class="yako__lnk__discover">
                                                <a href="{{route('yako.retrieve', ['yakoUrl' => $yako['object']->url])}}"
                                                   class="lnk--blue yako__lnk--active lnk__underline-transition lnk__underline-transition--blue"> @lang('trad.yako-slider.link') {{$yako['object']->title}}</a>
                                                <img class="" src="{{url('/images/arrowblue.png')}}"
                                                     alt="img-presentation"/>
                                            </div>
                                        </div>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if($subscription->presentation2)
                <div class="container-1170 container presentation-container">
                    <div class="presentation-container_content container--padding-t-80">
                        <div class="row">
                            <div class="col-xl-6 col-md-12 align-self-center">
                                <div class="presentation--container__desc">
                                    <div class="presentation--container__title">
                                        <h2 class="presentation__title h2 title--underlined-center-md--blue title--underlined-left--blue text-uppercase mb-5">
                                            {{$subscription->title2}} </h2>
                                    </div>
                                    <div class="presentation--container__text">
                                        <p>{{$subscription->presentation2}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12 text-md-center">
                                <img
                                        @if($subscription->image2)
                                        src="{{Storage::url($subscription->image2)}}"
                                        @else
                                        src="{{ url('/images/running-homme.png')}}"
                                        @endif
                                        alt="image présentation 2"/>
                            </div>

                        </div>
                    </div>
                </div>
            @endif

            <div class="container container-1170">
                <div class="presentation-container_content">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="">
                                <div class="presentation--container__title">
                                    <img
                                            @if($subscription->image3)
                                            src="{{Storage::url($subscription->image3)}}"
                                            @else
                                            src="{{ url('/images/sourire-femme.png')}}"
                                            @endif
                                            alt="image présentation 3"/>
                                </div>
                            </div>
                        </div>
                        @if($subscription->presentation3)
                            <div class="col-xl-6 col-md-12">
                                <h2 class="presentation__title h2 title--underlined-center-md--blue title--underlined-left--blue text-uppercase mb-5 mt-5">
                                    {{$subscription->title3}} </h2>
                                <p>{{$subscription->presentation3}}</p>
                            </div>
                        @endif
                        @if($subscription->presentation4)
                            <div class="col-xl-6 col-md-12">
                                <h2 class="presentation__title h2 title--underlined-center-md--blue title--underlined-left--blue text-uppercase mb-5 mt-5">
                                    {{$subscription->title4}} </h2>
                                <p>{{$subscription->presentation4}}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            {{-- Disparait en wellness --}}
            @if($subscription->type == 'fitness')
                <div class="container container-1170">
                    <div class="row components-list container--padding-t-80">
                        <div class="components-list__header">
                            <h2 class="components-list__title presentation__title h2 text-center title--underlined-center--blue text-uppercase">
                                @lang('trad.equipment.title')</h2>
                            <p class="components-list__text text--t-center">
                                @lang('trad.equipment.subtitle')
                            </p>
                        </div>
                        <div class="container components-list__content container--padding-t-50">
                            <div class="row parent-activity components-list__cards">
                                @forelse($equipments_lists as $equipment)
                                    <div class="col-lg-2 col-md-3 col-6">
                                        <div class="list__cards__item">
                                            <a
                                                    @if($equipment->url)
                                                    href="{{route('equipment.retrieve', $equipment->url)}}"
                                                    @else
                                                    href=""
                                                    @endif
                                            >
                                                <div class="in_picture {{$equipment->type?:'fitness'}}">
                                                    @isset($equipment->image)
                                                        <img class="in_picture__opacity"
                                                             src="{{Storage::url($equipment->image)}}"
                                                             alt="{{$equipment->title}}"/>
                                                    @else
                                                        <img class="in_picture__opacity"
                                                             src="{{url('/images/equipement-fitness-1.png')}}"
                                                             alt="image equipement fitness"/>
                                                    @endisset
                                                    <div class="in_picture__blue"></div>
                                                    <h3 class="in_picture__title text--uppercase">{{$equipment->title}}</h3>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <div class="components-list__actions col-md-12">
                                <div class="text--t-center">
                                    <a
                                            href="{{route('equipment.showAll')}}"
                                            class="btn btn--bg-white btn--t-orange btn--bd-orange components-list__lnk"
                                    >
                                        @lang('trad.equipment.button')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @isset($testimonials)
                <div class="container container-1170">
                    <div class="subscription-single__testimony container--padding-t-80">
                        <h2 class="presentation__title h2 text--t-center title--underlined-center--blue text-uppercase mb-5">
                            @lang('trad.testimony.title') {{$subscription->type}}</h2>
                        <div class="row">
                            <div class="testimony__item carousel-testimony owl-theme owl-carousel">
                                @forelse($testimonials as $testimonial)
                                    <div class="item d-flex">
                                        <div class="col-xl-6 col-12">
                                            <i class="fa fa-quote-left fa-2x testimony__item__quote"></i>
                                            <p class="testimony__item__text">{{$testimonial->description}}</p>
                                            <small class="testimony__item__small">{{$testimonial->author}}
                                            </small>
                                            <div class="d-flex align-items-center">
                                                @if($testimonial->link)
                                                    <a href="{{$testimonial->link}}" target="_blank"
                                                       class="testimony__item__lnk lnk__underline-transition lnk__underline-transition--blue">
                                                        @lang('trad.testimony.read')
                                                    </a>
                                                    <div class="testimony__item--rounded text--t-center">
                                                        <img src="{{url('/images/mini-arrow.png')}}" alt="img-video"/>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        @if($testimonial->image)
                                            <div class="col-xl-6 col-12">
                                                <img class="testimony__item__image"
                                                     src="{{Storage::url($testimonial->image)}}"
                                                     alt="img-slide"/>
                                            </div>
                                        @else
                                            <div class="col-xl-6 col-12">
                                                <img class="testimony__item__image"
                                                     src="{{url('/images/hommes-gainage.png')}}"
                                                     alt="homme séance gainage"/>
                                            </div>
                                        @endif
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            @endisset

            @if($subscription->type == 'fitness')
                <div class="container-full-w container-subscribe container--bg-l-grey container--padding-t-80 container--padding-b-80">
                    <div class="container container-1170">
                        <div class="container-subscribe__container">
                            <div class="row">
                                <div class="col-md-6 col--no-gutter">
                                    <img class="subscribe__image" src="{{ url('/images/femme-running.png')}}" alt="femme running"/>
                                </div>
                                <div class="col-md-6 col--no-gutter">
                                    <div class="subscribe__content">
                                        <div class="subscribe__content--pad">
                                            <h2 class="h2 subscribe__content__title  title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">
                                                @lang('trad.subscribe-component.title')</h2>
                                            <p class="subscribe__content__text">@lang('trad.subscribe-component.subtitle')</p>
                                            {{-- // TODO : enlever "Ce club ne propose pas l’abonnement en ligne" quand le Maroc le proposera --}}
                                            @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'ma')
                                                <p class="subscribe__content__text--ma">@lang('trad.subscribe-component.no-pre')</p>
                                            @endif
                                            @isset($subscription)
                                                <a href="{{route('subscription.step1', $subscription->url)}}"
                                                   class="btn btn--bg-orange btn--t-white subscribe__content__btn">@lang('trad.button.subscribe')</a>
                                            @else
                                                <button class="btn btn--bg-orange btn--t-white subscribe__content__btn">@lang('trad.button.subscribe')</button>
                                            @endisset
                                        </div>
                                        <div class="subscribe__image__mobile">
                                            <img src="{{ url('/images/femme-sourire-sportive.png')}}"
                                                 alt="femme sourire sportive"/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    {{ Breadcrumbs::render('single-abonnement', $subscription,$subscription->url) }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-abonnement', $subscription,$subscription->url) }}
    @include('partials.footer')
@endsection
