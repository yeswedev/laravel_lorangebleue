@extends('layouts.front')

@section('title')
    {{$yako->getMetaTitle()}}
@endsection

@section('description')
    {{$yako->getMetaDescription()}}
@endsection

@section('content')
    @include('front.header')
    <div class="container-single-yako">
        <div class="container container-full-w container-single-yako__header">
            <h1 class="h1 single-yako__header__title text--t-center text--uppercase">{{ $yako->getH1() }}</h1>
        </div>
        <div class="container container-1170 container-single-yako__content container--padding-t-80 container--padding-b-80">
            <div class="row">
                <div class="col-xl-6 col-md-12 text-md-center">
                    @isset($yako_details['object']->link)
                        <div class="details-yako__mainimage">
                            <iframe id="js-yako-video" frameborder="0" allowfullscreen="1" allow="encrypted-media"
                                    title="YouTube video player" class="yako__mainimage-presentation"
                                    src=""></iframe>
                        </div>
                        <div class="js-yako-illustration-selector yako__list__item"
                             yako_illustration_type="{{$yako_details['illustration_type']}}"
                             yako_illustration_src="{{$yako_details['illustration_src']}}">
                        </div>
                    @elseif($yako_details['object']->image)
                        <img src="{{Storage::url($yako_details['object']->image)}}" alt="{{$yako->getImgAlt()}}"/>
                    @else
                        <img src="{{url('/images/yakosingle.png')}}" alt="seance yako"/>
                    @endisset
                </div>
                <div class="col-xl-6 col-md-12">
                    <h2 class="container-single-yako--name h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase"
                        style="--tooltip-color: #004589; color: #004589;">
                        @lang('trad.yako-single.title') {{ $yako->title }}</h2>
                    @replace($yako->description,"h1","h2")
                </div>
            </div>
        </div>
        <div class="container container-970">
            <div class="container-components__picto">
                <div class="row text--t-center">
                    <div class="col-md-4">
                        <img src="{{url('/images/objectif.png')}}" alt="picto objectif"/>
                        <p class="yakos__card__item__content__title text--uppercase">@lang('trad.activity.yako-goal')</p>
                        <p class="yakos__card__item__content__desc">{{$yako->objectives}}</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/zone.png')}}" alt="picto zone ciblée"/>
                        <p class="yakos__card__item__content__title text--uppercase">@lang('trad.activity.yako-target')</p>
                        <p class="yakos__card__item__content__desc">{{$yako->targeted_part}}</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/cal.png')}}" alt="picto-calories"/>
                        <p class="yakos__card__item__content__title text--uppercase d-flex justify-content-center">
                            @lang('trad.activity.yako-cal')
                            <div class="components__dots components__dots--{{$yako->rate_calorie}}">
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                            </div>
                        </p>
                        <p class="container-components--picto_desc">{{$yako->calories}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-970 container-single-yako__description">
            <div class="container-single-yako__description--header">
                <h2 class="container-single-yako--title_description h2 title--underlined-center--blue text--t-center"
                    style="--tooltip-color: {{ $yako->color }}; color: {{ $yako->color }};">
                    {!! $yako->detail !!}
                </h2>
            </div>
            <div class="container-single-yako__description__content">
                <div class="row">
                    <div class="col-md-6 mb-mob">
                        <div class="container-single-yako__description--leftside">
                            <h3 class="container-single-yako__description--title">@lang('trad.yako-single.public')</h3>
                            {!! $yako->public !!}
                            {{-- <p class="container-single-yako__description--text">Accessible à tous, sans barrière de
                                sexe, d'âge, de poids ou d'aptitudes
                                sportives</p>
                            <ul class="container-single-yako__description--list">
                                <li>Pour le Confirmé : se perfectionner sur les techniques de placements</li>
                                <li>Pour les Débutants : diverses options facilitantes sont proposées tout au long
                                    de la séance
                                </li>
                            </ul> --}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="container-single-yako__description--rightside">
                            <h3 class="container-single-yako__description--title">@lang('trad.yako-single.advantage')</h3>
                            {!! $yako->advantage !!}
                            {{-- <p class="container-single-yako__description--text">Alliant des phases de contraction et
                                des phases d’étirements, ce cours vous permettra
                                de progresser sur :</p>
                            <ul class="container-single-yako__description--list">
                                <li>la souplesse</li>
                                <li> le maintien de la posture</li>
                                <li>le travail de la respiration</li>
                                <li>la concentration</li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(count($yako->yakoActivity) >= 1)
            <div class="container container-1170">
                <div class="row components-list container--padding-t-80 container--padding-b-80">
                    <div class="components-list__header">
                        <h2 class="components-list__title  container-single-yako--name h2 text-center title--underlined-center--blue text-uppercase"
                            style="--tooltip-color: {{ $yako->color }}; color: {{ $yako->color }};">
                            @lang('trad.yako-single.yako-activity-title') {{$yako->title}}</h2>
                        <p class="components-list__text text--t-center"> @lang('trad.yako-single.yako-activity-desc')</p>
                    </div>
                    <div class="container components-list__content">
                        <div class="row js-parent-show-more components-list__cards" data-showmore="2">
                            @forelse($yako->yakoActivity as $activity)
                                <div class="col-lg-3 col-md-6 col-xs-12">
                                    <div class="list__cards__item">
                                        <div class="in_picture">
                                            @if($activity->url)
                                                <a href="{{ route('activity.retrieve', $activity->url) }}">
                                                    @else
                                                        <a href="">
                                                            @endif
                                                            <img class="in_picture__opacity"
                                                                 src="{{url('/images/coach_personnel.png')}}"
                                                                 alt="{{$activity->getImgAlt()}}"/>
                                                            <h3 class="in_picture__title text--uppercase">{{$activity->title}}</h3>
                                                        </a>
                                        </div>
                                    </div>
                                </div>
                            @empty

                            @endforelse
                        </div>
                    </div>
                    <div class="components-list__actions col-md-12">
                        <div class="d-block d-sm-none">
                            <button id="show-more-mobile"
                                    class="btn btn--bg-white btn--t-orange btn--bd-orange">@lang('trad.common.see-more')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="container container-1170">
            <div class="container-yakos__content container--padding-t-80 container--padding-b-80">
                <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase">@lang('trad.yako-single.discover')</h2>
                <div class="row pt-5 js-parent-show-more" data-showmore="2">
                    @forelse($yako_lists as $yak)
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            @if($yak->url)
                                <a href="{{route('yako.retrieve', $yak->url)}}">
                                    @else
                                        <a href="">
                                            @endif
                                            <figure class="figure-hover-effet--2"
                                                    style="background-color: {{ $yak->color }};">
                                                <div class="container-yakos__static yako__content__static">
                                                    <h2 class="content__static__title">Yako
                                                        <strong>{{$yak->title}}</strong></h2>
                                                    <div class="content__static__text">{{ $yak->subtitle }}</div>

                                                </div>
                                                <figcaption class="container__yakos__card figcaption-hover-effet--2">
                                                    <div class="container__yakos__image">
                                                        @isset($yak->image)
                                                            <img src="{{Storage::url($yak->image)}}"
                                                                 class="yakos__card__image"
                                                                 alt="{{$yako->getImgAlt()}}"/>
                                                        @else
                                                            <img src="{{url('/images/Yako-Detente.png')}}"
                                                                 class="yakos__card__image" alt="yako séance détente"/>
                                                        @endisset
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/objectif.png')}}"
                                                             alt="picto objectif"/>
                                                        <div class="yakos__card__item__content">
                                                            <h2 class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-goal')</h2>
                                                            <p class="yakos__card__item__content__desc">{{$yak->objectives}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/zone.png')}}" alt="picto zone ciblée"/>
                                                        <div class="yakos__card__item__content">
                                                            <h2 class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-target')</h2>
                                                            <p class="yakos__card__item__content__desc">{{$yak->targeted_part}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/cal.png')}}" alt="picto-calories"/>
                                                        <div class="yakos__card__item__content d-flex align-items-center">
                                                            <h2 class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-cal')</h2>
                                                            <div class="components__dots components__dots--{{$yak->rate_calorie}}">
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </a>
                        </div>
                    @empty
                    @endforelse
                </div>
                <div class="col-md-12">
                    <a href="{{route('yako.showAll')}}"
                       class="btn btn--bg-white btn--t-orange btn--bd-orange btn-more-yako">
                        @lang('trad.yako-single.discover-all')
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('single-yakos', $yako) }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-yakos', $yako) }}
    @include('partials.footer')
@endsection

