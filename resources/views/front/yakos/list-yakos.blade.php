@extends('layouts.front')

@section('title')
    @lang('meta.title.yako-list')
@endsection

@section('description')
    @lang('meta.description.yako-list')
@endsection


@section('content')
    @include('front.header')
    <div class="container__list__yakos">
        <div class="container-full-w position-relative">
            <div class="list__yakos__header container--padding-t-50">
                <h1 class="h1 text--t-center text-white text--uppercase">@lang('trad.yako-list.title')</h1>
                <p class="yakos__header__text">@lang('trad.yako-list.subtitle')</p>
            </div>
            <div class="main-header__picto">
                <div class="container container-1170 container--shadow container--bg-white">
                    <div class="picto-tiles">
                        <div class="picto-tiles__item col-lg-4 col-md-12 col-sm-12">
                            <div class="picto-tiles__item-container">
                                <div class="img-container img-objectfit">
                                    <img src="{{url('/images/diplome.png')}}" alt="picto diplome"
                                         class="picto-tiles__image">
                                </div>
                                <p class="picto-tiles__text">
                                    @lang('trad.yako-list.first-picto')
                                </p>
                            </div>
                        </div>
                        <div class="picto-tiles__item col-lg-4 col-md-12 col-sm-12">
                            <div class="picto-tiles__item-container">
                                <div class="img-container img-objectfit">
                                    <img src="{{url('/images/illimite.png')}}" alt="picto illimite"
                                         class="picto-tiles__image">
                                </div>
                                <p class="picto-tiles__text">
                                    @lang('trad.yako-list.second-picto')
                                </p>
                            </div>
                        </div>
                        <div class="picto-tiles__item col-lg-4 col-md-12 col-sm-12">
                            <div class="picto-tiles__item-container">
                                <div class="img-container img-objectfit">
                                    <img src="{{url('/images/cours.png')}}" alt="picto-img" class="picto-tiles__image">
                                </div>
                                <p class="picto-tiles__text">
                                    @lang('trad.yako-list.third-picto')
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-1170">
            <div class="container-yakos__content container--padding-t-100 ">
                <div class="row js-parent-show-more" data-showmore="2">
                    @forelse($yako_lists as $yako)
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            @if($yako->url)
                                <a href="{{route('yako.retrieve', $yako->url)}}">
                                    @else
                                        <a href="">
                                            @endif
                                            <figure class="figure-hover-effet--2"
                                                    style="background: {{ $yako->color }} url('@if($yako->picto){{ Storage::url($yako->picto) }}@endif') no-repeat center center; background-size:contain;">
                                                <div class="container-yakos__static yako__content__static">
                                                    <h2 class="content__static__title @if($yako->picto) content-static__title--hidden @endif">Yako
                                                        <strong>{{$yako->title}}</strong></h2>
                                                    <div class="content__static__text">{{ $yako->subtitle }}</div>
                                                </div>
                                                <figcaption class="container__yakos__card figcaption-hover-effet--2">
                                                    <div class="container__yakos__image">
                                                        @isset($yako->image)
                                                            <img src="{{Storage::url($yako->image)}}"
                                                                 class="yakos__card__image" alt="{{$yako->getImgAlt()}}"
                                                                 alt="{{$yako->getImgAlt()}}"/>
                                                        @else
                                                            <img src="{{url('/images/Yako-Detente.png')}}"
                                                                 class="yakos__card__image" alt="yako-img-default"/>
                                                        @endisset
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/objectif.png')}}"
                                                             alt="picto objectif"/>
                                                        <div class="yakos__card__item__content">
                                                            <p class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-goal')</p>
                                                            <p class="yakos__card__item__content__desc">{{$yako->objectives}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/zone.png')}}" alt="picto zone ciblée"/>
                                                        <div class="yakos__card__item__content">
                                                            <p class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-target')</p>
                                                            <p class="yakos__card__item__content__desc">{{$yako->targeted_part}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/cal.png')}}" alt="calories"/>
                                                        <div class="yakos__card__item__content d-flex align-items-center">
                                                            <p class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-cal')</p>
                                                            <div class="components__dots components__dots--{{$yako->rate_calorie}}">
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </a>
                        </div>
                    @empty
                    @endforelse
                </div>
                <div class="col-md-12">
                    <div class="d-block d-sm-none">
                        <button id="show-more-mobile" class="btn btn--bg-white btn--t-orange btn--bd-orange">
                            @lang('trad.button.see-more')
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-1170">
            <div class="row components-list container--padding-t-80 container--padding-b-80">
                <div class="components-list__header">
                    <h2 class="components-list__title presentation__title h2 text-center title--underlined-center--blue text-uppercase">
                        @lang('trad.yako-list.discover-activity')</h2>
                    <p class="components-list__text text--t-center">@lang('trad.yako-list.description-activity')</p>
                </div>
                <div class="container components-list__content container--padding-t-50">
                    <div class="row parent-activity components-list__cards">
                        @forelse($activity_lists as $activity)
                            <div class="list__cards__item col-lg-3 col-md-6 col-6">
                                <div class="in_picture">
                                    @if($activity->url)
                                        <a href="{{ route('activity.retrieve', $activity->url) }}">
                                    @else
                                        <a href="">
                                    @endif
                                            <img class="in_picture__opacity"
                                                 @if ($activity->image)
                                                    src="{{url('storage/'.$activity->image)}}"
                                                 @else
                                                    src="{{url('/images/coach_personnel.png')}}"
                                                 @endif
                                                 alt="{{$activity->getImgAlt()}}"/>
                                            <h3 class="in_picture__title text--uppercase">{{$activity->title}}</h3>
                                        </a>
                                </div>
                            </div>
                        @empty

                        @endforelse
                    </div>
                </div>
                <div class="col-md-12">
                    <a href="{{route('activity.showAll')}}"
                       class="btn btn--bg-white btn--t-orange btn--bd-orange components-list__button">
                        @lang('trad.yako-list.discover-all')
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('yakos') }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'yakos') }}
    @include('partials.footer')
@endsection
