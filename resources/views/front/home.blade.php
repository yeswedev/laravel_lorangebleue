@extends('layouts.front')

@section('title')
    @lang('meta.title.homepage')
@endsection
@section('description')
    @lang('meta.description.homepage')
@endsection

@section('content')
    @include('front.header')
    <div
        @isset($pageGeneric)
            @if($pageGeneric->image)
            class="main-header"
            style="background: #004585 url({{Storage::url($pageGeneric->image)}}) no-repeat;background-size: cover;background-position-x: 45%;"
            @else
                class="main-header main-header--home"
            @endif
        @else
            class="main-header main-header--home"
        @endisset
    >
        <div class="main-header__orange">
            <div class="container-1170 container--flex container--flex-start">
                <div class="main-header__presentation main-header__presentation--home">
                    <p class="main-header__title h1 text-uppercase">
                        @isset($pageGeneric)
                            @if($pageGeneric->title)
                                {{$pageGeneric->title}}
                            @endif
                        @endisset
                    </p>
                    <h1 class="main-header__description">
                        @isset($pageGeneric)
                            @if($pageGeneric->subtitle)
                                {{$pageGeneric->subtitle}}
                            @endif
                        @endisset
                    </h1>
                    <div class="main-header__buttons">
                        <a href="{{route('subscription.step1', \App\Subscription::DEFAULT_URL)}}" class="btn btn--bg-white btn--t-orange btn__header main-header__sub-btn">
                            @lang('trad.button.subscribe')
                        </a>
                    </div>
                    <div class="main-header__discover">
                        <div class="d-flex flex-column flex-sm-row align-items-start align-items-sm-center">
                            <span class="main-header__discover-label mr-4">@lang('trad.homepage.discover')</span>

                            @if(isset($app_subscription_fitness) && $app_subscription_fitness->url != '')
                                <a href="{{route('subscription.index', $app_subscription_fitness->url)}}"
                                   class="lnk lnk--white lnk--hv-white lnk__underline-transition lnk__underline-transition--white home-lnk mr-5 my-2">@lang('trad.button.coach-fitness')
                                </a>
                            @endif

                            @if(isset($app_subscription_wellness) && $app_subscription_wellness->url != '' && LaravelLocalization::getCurrentLocale() != 'ma')
                                <a href="{{route('subscription.index', $app_subscription_wellness->url)}}"
                                   class="lnk lnk--white lnk--hv-white lnk__underline-transition lnk__underline-transition--white home-lnk">@lang('trad.button.coach-wellness')
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header__picto">
            @include('front.components.picto')
        </div>
    </div>
    <div class="container-full-w container-abo container--bg-h-grey container--padding-b-80">
        <div class="container-1170 container container--padding-t-150 container--padding-xl-t-300">
            <div class="subscription--margin-b-50">
                <h2 class="subscription__title h2 text--t-center title--underlined-center--blue text-uppercase">
                    @lang('trad.homepage.subscription_title')</h2>
            </div>
            <div class="subscription__text">
                <p class="text--t-center">@lang('trad.homepage.subscription_subtitle')</p>
            </div>
            @include('front.components.subscription-banner', ['subscriptionUrl' => $app_subscription_fitness?$app_subscription_fitness->url:'', 'price' => $formattedBannerPrice, 'aside' => false])
            @include('front.components.offer', ['offers' => $offers, 'club' => null, 'subscriptionUrl' => $app_subscription_fitness?$app_subscription_fitness->url:'', 'order' => null])
        </div>

        @if(config('laravellocalization.showWellnessSubscription') || LaravelLocalization::getCurrentLocale() == 'fr')
            <div class="container-1170 container">
                <div class="container__wellness">
                    <div class="row">
                        <div class="col-lg-6  col-12">
                            <div class="wellness-illustration">
                                <img class="wellness-illustration__image lazy"
                                    @isset($pageGeneric)
                                        @if($pageGeneric->image_wellness)
                                            src="{{Storage::url($pageGeneric->image_wellness)}}"
                                        @else
                                            data-src="{{ url('/images/home_coach_wellness.png')}}"
                                        @endif
                                        @if($pageGeneric->image_alt_wellness)
                                            alt="{{$pageGeneric->image_alt_wellness}}"
                                        @else
                                            alt="femme en séance de wellness"
                                        @endif
                                    @else
                                        data-src="{{ url('/images/home_coach_wellness.png')}}"
                                        alt="femme en séance de wellness"
                                    @endisset
                                    width="100%" height="100%"
                                />
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="wellness-content d-flex flex-column justify-content-center">
                                <div class="wellness-presentation">
                                    <h2 class="text--uppercase h2 wellness-presentation__title">@lang('trad.homepage.wellness_title')</h2>
                                    <p class="wellness-presentation__desc">
                                        @lang('trad.homepage.wellness_description')
                                    </p>
                                </div>
                                <ul class="wellness-list">
                                    <li class="wellness-list__item">
                                        <img src="{{ url('/images/pictotime.png')}}" alt="picto temps"/>
                                        @lang('trad.homepage.wellness.first-picto')
                                    </li>
                                    <li class="wellness-list__item">
                                        <img src="{{ url('/images/pictolist.png')}}" alt="picto liste"/>
                                        @lang('trad.homepage.wellness.second-picto')
                                    </li>
                                    <li class="wellness-list__item">
                                        <img src="{{ url('/images/pictosmile.png')}}" alt="picto sourire"/>
                                        @lang('trad.homepage.wellness.third-picto')
                                    </li>
                                </ul>
                                <a href="{{route('subscription.index', 'mon-coach-wellness')}}"
                                   class="btn btn--bg-purple-2 btn--t-white btn--bd-purple wellness-lnk"> @lang('trad.homepage.wellness.discover')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'fr')
        <div class="container-full-w club__parent">
            <div class="club__overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="club__container col-md-12">
                        <div class="club--margin-b-50">
                            <h2 class="club__title h2 text--t-center title--underlined-center--blue text-uppercase">@lang('trad.homepage.findClub_title')</h2>
                        </div>

                        <div class="club__text">
                            <p class="text--t-center">@lang('trad.homepage.findClub_subtitle')
                            </p>
                        </div>

                        <div class="find_club__item">
                            {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex align-items-center')) }}
                            @include('front.clubs.components.club-form')
                            {{ Form::close() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container container-concept">
        <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase mb-5">@lang('trad.homepage.concept_title')</h2>
        <div class="presentation__text">
            <p class="text--t-center">@lang('trad.homepage.concept_subtitle')</p>
        </div>
        @include('front.components.concept')
    </div>
    @if(config('laravellocalization.showPulp') || LaravelLocalization::getCurrentLocale() == 'fr')
        @if ( LaravelLocalization::getCurrentLocale() == 'fr' )
            <div class="container-full-w plan"
                @isset($pageGeneric)
                    @if($pageGeneric->image_plan)
                        style="background:url({{Storage::url($pageGeneric->image_plan)}}) no-repeat;background-size: cover;background-position-x: 45%;"
                    @else
                        style="background:url('/images/home_good_plans_banner.png') no-repeat center center;"
                    @endif
                @else
                    style="background:url('/images/home_good_plans_banner.png') no-repeat center center;"
                @endisset
                >
                <div class="row">
                    <div class="plan__container col-md-10 plan__mobile-bg">
                        <div class="plan__content">
                            <div class="plan__outer-title plan--margin-b-50">
                                <h2 class="plan__title h2 title--underlined-left--blue text-uppercase">@lang('trad.homepage.plan_title')</h2>
                            </div>

                            <div class="plan__text">
                                <div class="text--t-left">
                                    @isset($pageGeneric)
                                        @if($pageGeneric->text)
                                            {!!$pageGeneric->text!!}
                                        @endif
                                    @endisset
                                </div>
                            </div>

                            <a href="https://pulp.lorangebleue.fr" target="_blank"
                               class="btn btn--bg-transparent btn--lg btn--t-orange btn--bd-orange plan__btn">@lang('trad.button.advantages')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
    @if(config('laravellocalization.showBlog') || LaravelLocalization::getCurrentLocale() == 'fr')
        <div class="container container-1170 blog">
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-tiles__desc">
                        <div class="blog__outer-title subscription--margin-b-50">
                            <h2 class="blog__title h2 text--t-center title--underlined-center--blue text-uppercase">
                                @lang('trad.homepage.blog_title')</h2>
                        </div>

                        <div class="blog__text bloc-tiles__descp--text">
                            <p class="text--t-center">@lang('trad.homepage.blog_subtitle')</p>
                        </div>
                    </div>
                </div>
            </div>
            @include('front.components.blog')


            <div class="blog-tiles__button col-md-12">
                <a href="{{route('blog.showAll')}}"
                   class="btn btn--bg-white btn--t-orange btn--bd-orange btn--lg">@lang('trad.button.blog')</a>
            </div>
        </div>
    @endif
    <div class="container-full-w container presentation-container container--padding-t-80">
        <div class="presentation-container_content">
            <div class="row">
                <div class="col-xl-6 col-md-12 text-md-center">
                    <p>
                        <img
                        @isset($pageGeneric)
                            @if($pageGeneric->image_presentation)
                                src="{{Storage::url($pageGeneric->image_presentation)}}"
                            @else
                                data-src="{{ url('/images/siege-min.jpg')}}"
                            @endif
                            @if($pageGeneric->image_alt_presentation)
                                alt="{{$pageGeneric->image_alt_presentation}}"
                            @else
                                alt="Siege de L'Orange Bleue" class="lazy"
                            @endif
                        @else
                            data-src="{{ url('/images/siege-min.jpg')}}"
                            alt="Siege de L'Orange Bleue" class="lazy"
                        @endisset
                        />
                    </p>
                </div>
                <div class="col-xl-6 col-md-12">
                    <div class="presentation--container__desc">
                        <div class="presentation--container__title">
                            <h2 class="presentation__title h2 title--underlined-center-md--blue title--underlined-left--blue text-uppercase mb-5">
                                @lang('trad.homepage.presentation_title')</h2>
                        </div>
                        <div class="presentation--container__text">
                            @isset($pageGeneric)
                                @if($pageGeneric->text_2)
                                    {!!$pageGeneric->text_2!!}
                                @else
                                    <p class="">@lang('trad.homepage.presentation_description_first')</p>
                                    <p>@lang('trad.homepage.presentation_description_second')</p>
                                @endif
                            @else
                                <p class="">@lang('trad.homepage.presentation_description_first')</p>
                                <p>@lang('trad.homepage.presentation_description_second')</p>
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div
            class="recruitment network-container container-full-w container--bg-l-grey container--padding-t-80 container--padding-b-80 network container">
        <div class="container-1170">
            <div class="network-tiles row">
                <div class="network-desc col-xl-4 col-md-12">
                    <div class="network-desc-container">
                        <h2 class="network__title network-desc__title h2 title--underlined-left--blue  title--underlined-center-md--blue text-uppercase">
                            @lang('trad.homepage.network.title')</h2>
                        <div class="network__text">
                            <div class="network-desc__body">
                                @isset($pageGeneric)
                                    @if($pageGeneric->text_3)
                                        {!!$pageGeneric->text_3!!}
                                    @else
                                        <p>@lang('trad.homepage.network.subtitle')</p>
                                    @endif
                                @else
                                    <p>@lang('trad.homepage.network.subtitle')</p>
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
                <div class="figure-container col-xl-8 col-md-12 d-flex justify-content-center">
                    @if(LaravelLocalization::getCurrentLocale() == 'fr')
                        <figure class="network-tile__item js-fade-parent-figure">
                            <a
                                @if(config('laravellocalization.newLinkRecrutement'))
                                    href="{{route('recruitment.index')}}"
                                @else
                                    href="http://www.lorangebleuerecrute.fr/"
                                @endif
                            >
                            <img class="network-tile__image lazy"
                                @isset($pageGeneric)
                                    @if($pageGeneric->image_reseau_1)
                                        src="{{Storage::url($pageGeneric->image_reseau_1)}}"
                                        style="height: 370px;"
                                    @else
                                        data-src="{{url('/images/IMG_5704.png')}}"
                                    @endif
                                    @if($pageGeneric->image_alt_reseau_1)
                                        alt="{{$pageGeneric->image_alt_reseau_1}}"
                                    @else
                                        alt="lazy"
                                    @endif
                                @else
                                    data-src="{{url('/images/IMG_5704.png')}}"
                                    alt="lazy"
                                @endisset
                            >
                            <figcaption class="network-tile__caption">
                                <h3>
                                    <span class="network-tile__title network-tile__title--small text--bold">@lang('trad.homepage.network.join')</span>
                                    <span class="network-tile__title text--bold text-uppercase">@lang('trad.homepage.network.club')</span>
                                    <a
                                            @if(config('laravellocalization.newLinkRecrutement'))
                                                href="{{route('recruitment.index')}}"
                                            @else
                                                href="http://www.lorangebleuerecrute.fr/"
                                            @endif
                                                class="network-tiles__lnk lnk__underline-transition--white lnk__underline-transition lnk--white">
                                        @lang('trad.button.more')
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                    </a>
                                </h3>
                            </figcaption>
                            </a>
                        </figure>
                    @endif
                    <figure class="network-tile__item">
                        <a
                                @if(LaravelLocalization::getCurrentLocale() != 'es')
                                href="{{ route('page.retrieve', 'ouvrir-salle-sport') }}"
                                @else
                                href="{{ route('page.retrieve', 'abrir-un-gimnasio') }}"
                                @endif
                        >
                            <img class="network-tile__image lazy"
                                @isset($pageGeneric)
                                    @if($pageGeneric->image_reseau_2)
                                        src="{{Storage::url($pageGeneric->image_reseau_2)}}"
                                        style="height: 370px;"
                                    @else
                                        data-src="{{url('/images/equipement-velo-fitness.png')}}"
                                    @endif
                                    @if($pageGeneric->image_alt_reseau_2)
                                        alt="{{$pageGeneric->image_alt_reseau_2}}"
                                    @else
                                        alt="equipement fitness velo"
                                    @endif
                                @else
                                    data-src="{{url('/images/equipement-velo-fitness.png')}}"
                                    alt="equipement fitness velo"
                                @endisset
                                >
                        </a>
                        <figcaption class="network-tile__caption js-fade-parent-figure">
                            <h3>
                                <span class="network-tile__title network-tile__title--small text--bold">@lang('trad.homepage.network.open')</span>
                                <span class="network-tile__title text--bold text-uppercase">@lang('trad.homepage.network.open_club')</span>
                                <a
                                        @if(LaravelLocalization::getCurrentLocale() != 'es')
                                        href="{{ route('page.retrieve', 'ouvrir-salle-sport') }}"
                                        @else
                                        href="{{ route('page.retrieve', 'abrir-un-gimnasio') }}"
                                        @endif
                                        class="network-tiles__lnk lnk--white lnk__underline-transition--white lnk__underline-transition"
                                >
                                    @lang('trad.button.more')
                                    <i class="fas fa-long-arrow-alt-right"></i>
                                </a>
                            </h3>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </div>
    @include('partials.footer')
    {!! $search_box_SEOcontext !!}
@endsection
