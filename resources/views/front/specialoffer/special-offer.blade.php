@extends('layouts.front')

@section('title')
    {{$specialoffer->getMetaTitle()}}
@endsection
@section('description')
    {{$specialoffer->getMetaDescription()}}
@endsection

@section('content')
    @include('front.header')
    <div class="container-full-w container-offer">
        <div class="main-header" style="background:url('/images/oferta-especial.png') no-repeat center center;background-size: cover;">
            <div class="main-header__orange">
                <div class="container-1170 container--flex container--flex-start">
                    <div class="main-header__presentation">
                        <h1 class="main-header__title h1 text-uppercase">{{$specialoffer->getH1()}}</h1>
                        <h2 class="h2 text--uppercase lnk--white">{{$specialoffer->subtitle}}</h2>
                        <p class="main-header__description">{{$specialoffer->description}}</p>
                        <div class="main-header__buttons">
                            @isset($app_subscription_fitness)
                                <a href="{{route('subscription.step1', $app_subscription_fitness->url)}}" class="btn btn--bg-orange btn--t-white btn__header">@lang('trad.button.subscribe')</a>
                            @else
                                <button class="btn btn--bg-orange btn--t-white btn__header">@lang('trad.button.subscribe')</button>
                            @endisset
                        </div>
                        <div class="subscription__mention">
                            <div class="subscription__mention_body lnk--white">
                                {!! $specialoffer->legal_mentions !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-offer__search-form">
            <div class="container container-970">
            <h2 class="h2 text--t-center text--uppercase">
                @if(count($clubs))
                    {{count($clubs)}} @lang('trad.special-offer.contributor')
                @else
                   @lang('trad.common.no-result')
                @endif
            </h2>
            <div class="find_club__item">
                {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex align-items-center')) }}
                @include('front.clubs.components.club-form')
                {{ Form::close() }}
            </div>
            </div>
        </div>

        <div class="container container-1170">
            <div class="container-offer__list">
                <div class="row">
                    @forelse($clubs as $club)
                        @if($club->is_open)
                            <div class="col-xl-3 col-md-4 col-12">
                                <div class="offer__list__item">
                                    <h2 class="offer__list__title text--uppercase">{{$club->title}}</h2>
                                    <p class="offer__list__text">{{$club->address}} <br/> <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span>
                                    </p>
                                    @if(count($club->offers) > 0 && $club->subscription && $club->subscription->type = 'fitness')
                                        {!!Form::open(array('action' => array('Front\SubscriptionController@step2', $club->subscription->url), 'method' => 'GET', 'class' => 'result__card__footer text--t-center')) !!}
                                        @isset($order)
                                            <input type="hidden" name="token" value="{{$order->order_token}}">
                                        @endisset
                                        <input type="hidden" name="clubId" value="{{$club->id}}">
                                        <button type="submit"
                                                class="btn btn--bg-orange btn--t-white btn--margin-t-0 button-int club__footer__abo-btn js-tag-commander-abo-select-club"
                                                data-city="{{$club->city}}"
                                                data-club="{{str_slug($club->title)}}"
                                        >
                                            @lang('trad.tunnel-step-1.choose')
                                        </button>
                                        {!! Form::close() !!}
                                    @else
                                        <div class="result__card__footer text--t-center">
                                            <a href="{{route('front.club-details', $club->url)}}"
                                               data-toggle="modal" data-target="#basicModal{{$club->id}}"
                                               data-city="{{$club->city}}"
                                               data-club="{{str_slug($club->title)}}"
                                               class="btn btn--bg-white btn--t-orange btn--bd-orange btn--margin-t-0 button-int club__footer__abo-btn js-tag-commander-abo-pre-insc">
                                                @lang('trad.tunnel-step-1.pre-sign-in')
                                            </a>
                                            <p class="result__card__conditions"> @lang('trad.tunnel-step-1.pre-sign-in-text')</p>
                                        </div>
                                    @endif
                                </div>
                                @include('front.components.modal',['club' => $club, 'modal' => true])
                            </div>
                        @endif
                    @empty
                    @endforelse

                </div>
            </div>
        </div>

        <div class="container-offer__band container--padding-t-100 container--padding-b-80">
            <div class="offer__band__content text--t-center">

                <h1 class=" h1 text-uppercase">{{$specialoffer->title}}</h1>
                <h2 class="h2 text--uppercase">{{$specialoffer->subtitle}}</h2>
                <p class="offer__band__desc">{{$specialoffer->description}}</p>
                <div class="main-header__buttons justify-content-center center-btn">
                    @isset($app_subscription_fitness)
                        <a href="{{route('subscription.step1', $app_subscription_fitness->url)}}" class="btn btn--bg-orange btn--t-white offer__band__btn">@lang('trad.button.subscribe')</a>
                    @else
                        <button class="btn btn--bg-orange btn--t-white offer__band__btn">@lang('trad.button.subscribe')</button>
                    @endisset
                </div>
            </div>
        </div>

        <footer class="container-offer__footer container--padding-t-50">
            <div class="offer__footer__content">
                <img src="{{ url('/images/ob-fitness.svg')}}" alt="logo l'orange bleu mon coach fitness"/>
                <p>
                    @lang('trad.footer.copyright',['year' => \Carbon\Carbon::now()->format('Y')]) -
                    <a href="{{route('page.retrieve', trans('routes.page.legal-mentions'))}}">
                        @lang('trad.footer.legal-mention')
                    </a>
                </p>
            </div>
        </footer>
    </div>
@endsection
