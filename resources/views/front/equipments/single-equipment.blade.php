@extends('layouts.front')

@section('title')
    {{$equipment->getMetaTitle()}}
@endsection

@section('description')
    {{$equipment->getMetaDescription()}}
@endsection

@section('content')
    @include('front.header')
    <div class="container-single-equipment">
        <div
        class="container container-full-w single-equipment__header"
        @isset($equipment)
            @if($equipment->banner)
            style="background: #004585 url({{Storage::url($equipment->banner)}}) no-repeat;background-size: cover;background-position-x: 45%;"
            @endif
        @endisset
        >
            <h1 class="h1 container-single-equipment--title single-equipment__title text--t-center text--uppercase">
                {{$equipment->getH1()}}
            </h1>
            <p class="single-equipment__presentation">
                @lang('trad.equipment-single.subtitle')
            </p>
        </div>
        <div class="container container-1170 single-equipment__content container--padding-t-80">
            <div class="row">
                <div class="col-xl-6 col-md-12 text-md-center">

                    <img
                        @if($equipment->url)
                            src="{{Storage::url($equipment->image)}}" alt="{{$equipment->getImgAlt()}}"
                        @else
                            src="{{url('/images/course-tapis.png')}}" alt="equipe course tapis"
                        @endif
                    >

                </div>
                <div class="col-xl-6 col-md-12">
                    <h2 class="h2 equipment__content__title title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">
                        @lang('trad.equipment-single.presentation') : {{$equipment->title}}
                    </h2>
                    <div class="equipment__content__description">
                        @replace($equipment->description,"h1","h2")
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-1170">
            <div class="row components-list container--padding-t-80">
                <div class="components-list__header">
                    <h2 class="components-list__title presentation__title h2 text-center title--underlined-center--blue text-uppercase">
                        @lang('trad.equipment-single.more-title')
                    </h2>
                    <p class="components-list__text text--t-center">@lang('trad.equipment-single.more-subtitle')</p>
                </div>
                <div class="container components-list__content container--padding-t-50">
                    <div class="row components-list__cards js-parent-show-more" data-showmore="2" data-minshow="8">
                        @forelse($equipments_lists as $equip)
                            <div class="col-lg-3 col-6">
                                <div class="list__cards__item">

                                    <a
                                        @if($equip->url)
                                            href="{{route('equipment.retrieve', $equip->url)}}"
                                        @else
                                            href="#"
                                        @endif
                                    >
                                        <div class="in_picture">
                                            <img
                                                class="in_picture__opacity"
                                                @if($equip->image)
                                                    src="{{Storage::url($equip->image)}}"
                                                @else
                                                    src="{{url('/images/equipement-fitness-1.png')}}"
                                                @endif
                                                alt="{{$equip->getImgAlt()}}"
                                            />
                                            <div class="in_picture__blue"></div>
                                            <h3 class="in_picture__title text--uppercase">{{$equip->title}}</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @empty
                        @endforelse

                    </div>
                    <div class="components-list__actions col-md-12">
                        <div class="d-none d-xl-block d-md-block">
                            <button id="show-more-desk"
                                    class="btn btn--bg-white btn--t-orange btn--bd-orange ">@lang('trad.button.see-more')
                            </button>
                        </div>
                        <div class="d-block d-sm-none">
                            <button id="show-more-mobile"
                                    class="btn btn--bg-white btn--t-orange btn--bd-orange">@lang('trad.button.see-more')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-concept">
            <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase">@lang('trad.homepage.concept_title')</h2>

            <div class="container-concept--presentation">
                <p class="presentation__text text--t-center">@lang('trad.homepage.concept_subtitle')</p>
            </div>
            @include('front.components.concept')
        </div>
    </div>
    {{ Breadcrumbs::render('single-equipments', $equipment) }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-equipments', $equipment) }}
    @include('partials.footer')
@endsection
