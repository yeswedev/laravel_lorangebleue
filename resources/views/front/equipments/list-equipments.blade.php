@extends('layouts.front')

@section('title')
   @lang('meta.title.equipment-list')
@endsection
@section('description')
   @lang('meta.description.equipment-list')
@endsection

@section('content')
    @include('front.header')
    <div class="container-list container-list-equipment">
        <div class="container-full-w position-relative">
            <div
                @isset($pageGeneric)
                    @if($pageGeneric->image)
                    class="container-list__header container--padding-t-50"
                    style="background: url({{Storage::url($pageGeneric->image)}}) no-repeat;width: 100%;background-size: cover;"
                    @else
                        class="container-list__header container--padding-t-50"
                    @endif
                @else
                    class="container-list__header container--padding-t-50"
                @endisset
            >
                <h1 class="h1 text--t-center text-white text--uppercase">
                    @isset($pageGeneric)
                        @if($pageGeneric->title)
                            {{$pageGeneric->title}}
                        @endif
                    @endisset
                </h1>
                <div class="list__header_presentation text--t-center">
                    @isset($pageGeneric)
                        @if($pageGeneric->description)
                            {!!$pageGeneric->description!!}
                        @endif
                    @endisset
                </div>
            </div>
            <div class="main-header__picto">
                @include('front.components.picto')
            </div>
        </div>
        <div class="container container-1170">
            <div class="row components-list container--padding-t-100">
                <div class="components-list__header">
                    <h2 class="components-list__title presentation__title h2 text-center title--underlined-center--blue text-uppercase">
                        @lang('trad.equipment-list.list-title')</h2>
                    <div class="components-list__text text--t-center">
                        @isset($pageGeneric)
                            @if($pageGeneric->text)
                                {!!$pageGeneric->text!!}
                            @endif
                        @endisset
                    </div>
                </div>
                <div class="container components-list__content container--padding-t-50">
                    <div class="row components-list__cards js-parent-show-more" data-showmore="2" data-minshow="8">
                        @forelse($equipments_lists as $equipment)
                            <div class="col-lg-3 col-6">
                                <div class="list__cards__item ">
                                    @if($equipment->url)
                                        <a href="{{route('equipment.retrieve', $equipment->url)}}">
                                    @else
                                        <a href="">
                                    @endif
                                        <div class="in_picture {{$equipment->type ?:'fitness'}}">
                                            @isset($equipment->image)
                                                <img class="in_picture__opacity" src="{{Storage::url($equipment->image)}}" alt="{{$equipment->getImgAlt()}}"/>
                                            @else
                                                <img class="in_picture__opacity" src="{{url('/images/equipement-fitness-1.png')}}" alt="image equipement fitness"/>
                                            @endisset
                                            <div class="in_picture__blue"></div>
                                            <h3 class="in_picture__title text--uppercase">{{$equipment->title}}</h3>
                                                <p class="in_picture__pastille">{{ucfirst($equipment->type?:'Fitness')}}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                    <div class="components-list__actions col-md-12">
                        <div class="d-none d-xl-block d-md-block">
                            <button id="show-more-desk" class="btn btn--bg-white btn--t-orange btn--bd-orange ">@lang('trad.button.see-more')
                            </button>
                        </div>
                        <div class="d-block d-sm-none">
                            <button id="show-more-mobile" class="btn btn--bg-white btn--t-orange btn--bd-orange">@lang('trad.button.see-more')
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container container-concept">
                <h2 class="container-concept__title h2 text-center title--underlined-center--blue text-uppercase">@lang('trad.homepage.concept_title')</h2>

                <div class="container-concept__presentation">
                    <p class="text--t-center">@lang('trad.homepage.concept_subtitle')</p>
                </div>
                @include('front.components.concept')
            </div>
        </div>
        {{ Breadcrumbs::render('equipments', $equipment) }}
        @include('partials.footer')
    </div>
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'equipments', $equipment) }}
@endsection
