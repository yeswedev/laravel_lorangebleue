@extends('layouts.front')

@section('title')
    @lang('meta.title.activity-list')
@endsection
@section('description')
    @lang('meta.description.activity-list')
@endsection

@section('content')
    @include('front.header')
    <div class="container-list container-list-activities">
        <div class="container-full-w position-relative">
            <div
            @isset($pageGeneric)
                @if($pageGeneric->image)
                class="container-list__header container--padding-t-50"
                style="background: url({{Storage::url($pageGeneric->image)}}) no-repeat;width: 100%;background-size:cover;background-position: center;"
                @else
                    class="container-list__header container--padding-t-50"
                @endif
            @else
                class="container-list__header container--padding-t-50"
            @endisset
            >
                <h1 class="h1 text--t-center text-white text--uppercase">
                    @isset($pageGeneric)
                        @if($pageGeneric->title)
                            {{$pageGeneric->title}}
                        @endif
                    @endisset
                </h1>
                <div class="list__header_presentation text--t-center">
                    @isset($pageGeneric)
                        @if($pageGeneric->description)
                            {!!$pageGeneric->description!!}
                        @endif
                    @endisset
                </div>
            </div>
            <div class="main-header__picto">
                @include('front.components.picto')
            </div>
        </div>
        <div class="container container-1170">
            <div class="row components-list container--padding-t-100">
                <div class="components-list__header">
                    <h2 class="components-list__title presentation__title h2 text-center title--underlined-center--blue text-uppercase">
                        @lang('trad.activity.list-title')</h2>
                    <div class="components-list__text text--t-center">
                        @isset($pageGeneric)
                            @if($pageGeneric->text)
                                {!!$pageGeneric->text!!}
                            @endif
                        @endisset
                    </div>
                </div>
                @include('front.activities.components.list-activities')
            </div>
        </div>
        <div class="container container-1170">
            <div class="container-yakos__content container--padding-t-80">
                <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase"> @lang('trad.activity.yako-title')</h2>
                <div class="row pt-5">
                    @forelse($yako_lists as $yako)
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            @if($yako->url)
                                <a href="{{route('yako.retrieve', $yako->url)}}">
                                    @else
                                        <a href="">
                                            @endif
                                            <figure class="figure-hover-effet--2"
                                                    style="background-color: {{ $yako->color }};">
                                                <div class="container-yakos__static yako__content__static">
                                                    <h3 class="content__static__title">Yako
                                                        <strong>{{$yako->title}}</strong></h3>
                                                    <div class="content__static__text">{{ $yako->subtitle }}</div>

                                                </div>
                                                <figcaption class="container__yakos__card figcaption-hover-effet--2">
                                                    <div class="container__yakos__image">
                                                        @isset($yako->image)
                                                            <img src="{{Storage::url($yako->image)}}"
                                                                 class="yakos__card__image" alt="{{$yako->getImgAlt()}}"/>
                                                        @else
                                                            <img src="{{url('/images/Yako-Detente.png')}}"
                                                                 class="yakos__card__image" alt="yako séance détente"/>
                                                        @endisset
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/objectif.png')}}" alt="picto objectif"/>
                                                        <div class="yakos__card__item__content">
                                                            <h2 class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-goal')</h2>
                                                            <p class="yakos__card__item__content__desc">{{$yako->objectives}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/zone.png')}}" alt="zone"/>
                                                        <div class="yakos__card__item__content">
                                                            <h2 class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-target')</h2>
                                                            <p class="yakos__card__item__content__desc">{{$yako->targeted_part}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="yakos_card__item d-flex">
                                                        <img src="{{url('/images/cal.png')}}" alt="calories"/>
                                                        <div class="yakos__card__item__content d-flex align-items-center">
                                                            <h2 class="yakos__card__item__content__title text--uppercase">
                                                                @lang('trad.activity.yako-cal')</h2>
                                                            <div class="components__dots components__dots--{{$yako->rate_calorie}}">
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                                <div class="components__dots__item">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </a>
                        </div>
                    @empty
                    @endforelse
                    <div class="col-md-12">
                        <a href="{{route('yako.showAll')}}"
                           class="btn btn--bg-white btn--t-orange btn--bd-orange btn-more-yako">
                            @lang('trad.activity.yako-see-all')
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-concept">
            <h2 class="container-concept__title h2 text-center title--underlined-center--blue text-uppercase">@lang('trad.homepage.concept_title')</h2>

            <div class="container-concept__presentation">
                <p class="text--t-center">@lang('trad.homepage.concept_subtitle')</p>
            </div>
            @include('front.components.concept')
        </div>
        {{ Breadcrumbs::render('activites') }}
        @include('partials.footer')
    </div>
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'activites') }}
@endsection
