<div class="container components-list__content container--padding-t-50 text--t-center">
    <div class="row parent-activity components-list__cards js-parent-show-more" data-showmore="2" data-minshow="8">
        @forelse($activity_lists as $activity)
            @if($activity->url)
                <div class="list__cards__item col-lg-3 col-6">
                    <div class="in_picture {{$activity->type?:'fitness'}}">
                        <a href="{{ route('activity.retrieve', $activity->url) }}">
                            <img
                                class="in_picture__opacity"
                                @if($activity->image)
                                    src="{{Storage::url($activity->image)}}"
                                @else
                                    src="{{url('/images/coach_personnel.png')}}"
                                @endif
                                 alt="{{$activity->getImgAlt()}}"
                            />
                            <h3 class="in_picture__title text--uppercase">{{$activity->title}}</h3>
                            @if($activity->type == 'both')
                                <p class="in_picture__pastille-fitness">Fitness</p>
                                <p class="in_picture__pastille-wellness">Wellness</p>
                            @else
                                <p class="in_picture__pastille">{{ucfirst($activity->type?:'Fitness')}}</p>
                            @endif
                        </a>
                    </div>
                </div>
            @endif
        @empty

        @endforelse
    </div>
    <div class="components-list__actions col-md-12">
        <div class="d-none d-xl-block d-md-block">
            <button id="show-more-desk"
                    class="btn btn--bg-white btn--t-orange btn--bd-orange ">@lang('trad.button.see-more')</button>
        </div>
        <div class="d-block d-sm-none">
            <button id="show-more-mobile"
                    class="btn btn--bg-white btn--t-orange btn--bd-orange">@lang('trad.button.see-more')</button>
        </div>
    </div>

</div>
