@extends('layouts.front')

@section('title')
    {{$activity->getMetaTitle()}}
@endsection

@section('description')
    {{$activity->getMetaDescription()}}
@endsection
@section('content')
    @include('front.header')
    <div class="container-single-activity">
        <div
        class="container container-full-w container-single-activity__header"
        @isset($activity)
            @if($activity->banner)
            style="background: #004585 url({{Storage::url($activity->banner)}}) no-repeat;background-size: cover;background-position-x: 45%;"
            @endif
        @endisset
        >
            <h1 class="h1 container-single-activity--title text--t-center text--uppercase">{{$activity->getH1()}}</h1>
        </div>
        <div class="container container-1170 container-single-activity__content container--padding-t-80 container--padding-b-80">
            <div class="row">
                <div class="col-xl-6 col-md-12 text-md-center">
                    @if($activity->image)
                        <img src="{{Storage::url($activity->image)}}" alt="{{$activity->getImgAlt()}}"/>
                    @else
                        <img src="{{url('/images/activity-default.png')}}" alt="image activite"/>
                    @endif
                </div>
                <div class="col-xl-6 col-md-12">
                    <h2 class="container-single-activity--name h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase">
                        @lang('trad.equipment-single.presentation') : {{$activity->title}}
                    </h2>
                    @replace($activity->description,"h1","h2")
                </div>
            </div>
        </div>
        <div class="container container-970">
            <div class="container-components__picto">
                <div class="row text--t-center">
                    <div class="col-md-4">
                        <img src="{{url('/images/objectif.png')}}" alt="picto objectif"/>
                        <h3 class="yakos__card__item__content__title text--uppercase">@lang('trad.activity.yako-goal')</h3>
                        <p class="yakos__card__item__content__desc">{{$activity->objectives}}</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/zone.png')}}" alt="picto zone" class="cible"/>
                        <h3 class="yakos__card__item__content__title text--uppercase">@lang('trad.activity.yako-target')</h3>
                        <p class="yakos__card__item__content__desc">{{$activity->targeted_part}}</p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('/images/cal.png')}}" alt="calories"/>
                        <h3 class="yakos__card__item__content__title text--uppercase d-flex justify-content-center">
                            @lang('trad.activity.yako-cal')
                            <div class="components__dots components__dots--{{$activity->rate_calorie}}">
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                                <div class="components__dots__item">

                                </div>
                            </div>
                        </h3>
                        <p class="container-components--picto_desc">{{$activity->calories}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container container-1170">
            <div class="row components-list container--padding-t-80">
                <div class="components-list__header">
                    <h2 class="components-list--title presentation__title h2 text-center title--underlined-center--blue text-uppercase mb-5">
                        @lang('trad.activity.all')</h2>
                </div>
                @include('front.activities.components.list-activities')
            </div>
        </div>

        <div class="container container-concept">
            <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase">@lang('trad.homepage.concept_title')</h2>

            <div class="presentation__text">
                <p class="text--t-center">@lang('trad.homepage.concept_subtitle')</p>
            </div>
            @include('front.components.concept')
        </div>
    </div>
    {{ Breadcrumbs::render('single-activites', $activity) }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-activites', $activity) }}
    @include('partials.footer')
@endsection
