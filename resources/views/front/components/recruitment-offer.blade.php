@extends("front.recruitment.recruitment")

@section("recruitment")
    @isset($job)
    <div class="js-tab container--padding-t-80" data-tab="offers">
        <div class="recruitment-container">
            <div class="recruitment-tiles">
                <div class="recruitment-header cms-h2">
                    <h2 class="recruitment-header__title">{{ $job->saisie1 }} {{ $job->saisierecrut1 }}</h2>
                    <p class="recruitment-header__else">
                        {{ \Carbon\Carbon::createFromFormat("d/m/Y H:i:s", $job->datecreation)->format("d/m/Y") }}
                    </p>
                    <p class="recruitment-header__else">
                        Réf: {{ $job->ref ?? '' }}
                    </p>
                </div>
                <div class="recruitment-img">
                    {!! $job->memo3 !!}
                </div>
                <div class="recruitment-text">
                    <div class="recruitment-text__part">
                        <h3 class="recruitment-text__title">Présentation du poste</h3>
                        {!! $job->memo1 !!}
                    </div>
                    <div class="recruitment-text__part">
                        <h3 class="recruitment-text__title">Profil recherché</h3>
                        {!! $job->memo2 !!}
                    </div>
                    <div class="recruitment-text__part">
                        <h3 class="recruitment-text__title">Informations</h3>
                        <table class="recruitment-text__table">
                            <tr>
                                <td>Date de début de travail</td>
                                <td>{{ \Carbon\Carbon::createFromFormat("d/m/Y H:i:s", $job->date1)->format("d/m/Y") }}</td>
                            </tr>
                            <tr>
                                <td>Ville</td>
                                <td>{{ $job->saisie2 }}</td>
                            </tr>
                            <tr>
                                <td>Type de contrat</td>
                                <td>{{ $job->categorie }}</td>
                            </tr>
                            <tr>
                                <td>Horaires de travail</td>
                                <td>{{ $job->saisie4 ?? '' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="container">
                    <a class="btn btn-apply btn--bd-orange btn--bg-white btn--t-orange text--uppercase btn--centered" href="{{ route("recruitment.apply") }}?offerID={{ $job->id }}">
                        Postuler à cette annonce
                    </a>
                </div>
            </div>
            <div class="recruitment-sidebar"></div>
        </div>
    </div>
    @endisset
@endsection
