<div class="map-club__info">
    @if($club->is_open)
        <h3 class="map-club__title text--uppercase"><a href="{{route('front.club-details', $club->url)}}">{{$club->title}}</a></h3>
        <p class="map-club__adress">{{$club->address}}<br/>
            <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span></p>
        <a  href="{{ url('https://www.google.com/maps/search/?api=1&query=lorangebleue+'.$club->title.'&z=12') }}" 
            class="map-club__info__title__lnk">@lang('trad.common.calcul-route')</a> <img
            src="{{url('/images/arrowblue.png')}}" 
            target="_blank"
            alt="arrow-img"
        />
    @else
        <h3 class="map-club__title text--uppercase">{{$club->title}}</h3>
        <p class="map-club__adress">
            @if($club->subscription->type == 'fitness')
                Club
            @else
                Centre
            @endif
                en cours d'ouverture
        </p>
    @endif
</div>
