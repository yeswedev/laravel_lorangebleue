@php
    $blocGeneric = \App\BlocGeneric::where('type', 'concept')->first();
@endphp
<div class="container-970">
    @if(config('laravellocalization.showWellnessSubscription') || LaravelLocalization::getCurrentLocale() == 'fr')
        <div class="concepts-tiles row">
            <div class="owl-carousel owl-theme js-carousel-concept">
                @forelse(\App\Subscription::get() as $subscription)
                    @if($subscription->title)
                        <div class="concepts-tiles__item col-md-6 item">
                            <div class="concepts-tiles__item-container">
                                <div class="concepts-tiles__header-image">
                                    <img
                                        @isset($blocGeneric)
                                            @if($subscription->isFitness())
                                                @if($blocGeneric->image)
                                                    src="{{Storage::url($blocGeneric->image)}}"
                                                @else
                                                    data-src="{{url('/images/wellness-pres-468.jpg')}}"
                                                @endif
                                                @if($blocGeneric->image_alt)
                                                    alt="{{$blocGeneric->image_alt}}"
                                                @else
                                                    alt="image concept"
                                                @endif
                                            @else
                                                @if($blocGeneric->image_2)
                                                    src="{{Storage::url($blocGeneric->image_2)}}"
                                                @else
                                                    data-src="{{url('/images/wellness-pres-468.jpg')}}"
                                                @endif
                                                @if($blocGeneric->image_alt_2)
                                                    alt="{{$blocGeneric->image_alt_2}}"
                                                @else
                                                    alt="image concept"
                                                @endif
                                            @endif
                                        @else
                                            @isset($subscription->image)
                                                data-src="{{Storage::url($subscription->image)}}"
                                                alt="image concept {{$subscription->type}}"
                                            @else
                                                data-src="{{url('/images/wellness-pres-468.jpg')}}"
                                                alt="img concept {{$subscription->type}}"
                                            @endisset
                                        @endisset
                                        class="concepts-tiles__image lazy"
                                    >
                                </div>
                                <div class="concepts-tiles__figcaption">
                                    <h3 class="concepts-tiles__header concepts-tiles__header--bd-btm-orange">
                                        <span class="logo-concept-home">
                                        <img data-src="{{url('/images/'.'ob-'.$subscription->type.'.svg')}}" alt="clubtype"
                                            width="100%" height="100%"
                                            class="lazy">
                                        </span>
                                    </h3>
                                    <div class="concepts-tiles__body">
                                        <p>{{ $subscription->description }}</p>
                                    </div>
                                    <div class="concepts-tiles__footer">
                                        <a href="{{route('subscription.index', $subscription->url)}}"
                                        class="btn btn--bg-white btn--t-orange btn--bd-orange">@lang('trad.button.more')
                                        </a>
                                        @isset($subscription)
                                            <a @if($subscription->url)
                                            href="{{route('subscription.step1', $subscription->url)}}"
                                            @else
                                            href="{{route('subscription.step1', 'mon-coach-fitness')}}"
                                            @endif
                                            class="btn btn--bg-orange btn--t-white">@lang('trad.button.subscribe')
                                            </a>
                                        @else
                                            <a href="{{route('subscription.step1', 'mon-coach-fitness')}}"
                                            class="btn btn--bg-orange btn--t-white">@lang('trad.button.subscribe')
                                            </a>
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @empty
                @endforelse
            </div>
        </div>
    @else
        <div class="concepts-tiles row" style="justify-content: center">
            <div class="owl-carousel owl-theme js-carousel-concept" style="justify-content: center">
                @if ($subscription = \App\Subscription::where('type','fitness')->first())
                    <div class="concepts-tiles__item col-md-6 item">
                        <div class="concepts-tiles__item-container">
                            <div class="concepts-tiles__header-image">
                                <img
                                @isset($blocGeneric)
                                    @if($subscription->isFitness())
                                        @if($blocGeneric->image)
                                            src="{{Storage::url($blocGeneric->image)}}"
                                        @else
                                            data-src="{{url('/images/wellness-pres-468.jpg')}}"
                                        @endif
                                        @if($blocGeneric->image_alt)
                                            alt="{{$blocGeneric->image_alt}}"
                                        @else
                                            alt="image concept"
                                        @endif
                                    @else
                                        @if($blocGeneric->image_2)
                                            src="{{Storage::url($blocGeneric->image_2)}}"
                                        @else
                                            data-src="{{url('/images/wellness-pres-468.jpg')}}"
                                        @endif
                                        @if($blocGeneric->image_alt_2)
                                            alt="{{$blocGeneric->image_alt_2}}"
                                        @else
                                            alt="image concept"
                                        @endif
                                    @endif
                                @else
                                    @isset($subscription->image)
                                        data-src="{{Storage::url($subscription->image)}}"
                                        alt="image concept {{$subscription->type}}"
                                    @else
                                        data-src="{{url('/images/wellness-pres-468.jpg')}}"
                                        alt="img concept {{$subscription->type}}"
                                    @endisset
                                @endisset
                                    class="concepts-tiles__image lazy"
                                >
                            </div>
                            <div class="concepts-tiles__figcaption">
                                <h3 class="concepts-tiles__header concepts-tiles__header--bd-btm-orange">
                                    <span class="logo-concept-home">
                                        <img data-src="{{url('/images/'.'ob-'.$subscription->type.'.svg')}}" alt="clubtype"
                                            width="100%" height="100%"
                                                class="lazy">
                                    </span>
                                </h3>
                                <div class="concepts-tiles__body">
                                    <p>{{ $subscription->description }}</p>
                                </div>
                                <div class="concepts-tiles__footer">
                                    <a
                                        href="{{route('subscription.index', $subscription->url)}}"
                                        class="btn btn--bg-white btn--t-orange btn--bd-orange"
                                    >
                                        @lang('trad.button.more')
                                    </a>
                                    <a
                                        href="{{route('subscription.step1', $subscription->url)}}"
                                        class="btn btn--bg-orange btn--t-white"
                                    >
                                        @lang('trad.button.subscribe')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endif
</div>
