@extends("front.recruitment.recruitment")

@section("recruitment")

    <div class="js-tab container--padding-t-80" data-tab="offers">
        <div class="jobs-tab__menu">
            {{ Form::open(array('method' => 'get', 'url' => route("recruitment.offers"), 'id' => 'filter-offers')) }}
                <div class="jobs-tab__menu__item">
                    <div class="jobs-tab__filter-town">
                        {!! Form::select('job', ['Fonctions' => $functions], Input::get('job'), ['class' => 'js-filter js-filter-job jobs-tab__country-select', 'placeholder' => 'Toutes les Fonctions']) !!}
                    </div>
                    <div class="jobs-tab__filter-town">
                        {!! Form::select('pays', ['Pays' => $pays], Input::get('pays'), ['class' => 'js-filter jobs-tab__country-select js-filter-country', 'placeholder' => 'Tous les Pays']) !!}
                    </div>
                    <div class="jobs-tab__filter-town">
                        <select name="country" class="jobs-tab__country-select js-filter-region js-filter">
                            @if (Input::get('country') == "")
                                <option value="" selected>Toutes les Régions</option>
                            @else
                                <option value="">Toutes les Régions</option>
                            @endif
                            @forelse($countries as $pays => $country)
                                <optgroup label="{{ $pays }}">
                                    @foreach($country as $region)
                                        @if (Input::get('country') == $region)
                                            <option value="{{ $region }}" selected>{{ $region }}</option>
                                        @else
                                            <option value="{{ $region }}">{{ $region }}</option>
                                        @endif
                                    @endforeach
                                </optgroup>
                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
            {{ Form::close() }}
            <button type="submit" form="filter-offers" class="btn jobs-tab__search-btn"><img class="jobs-tab__search-btn__icon" src="/images/recruitment-search.png" alt="picto recherche loupe">Rechercher</button>
        </div>
        <div class="container--padding-t-80">
            <h2 class="h2 text--t-center text--uppercase jobs-tab__title">Nos
                dernières
                offres</h2>
        </div>
        <div id="paginate" class="recruitment-container">
            <div class="row">
                <div class="recruitment-tiles jobs-tab col-12 col-lg-8">
                    @forelse ($job as $j)
                        <div class="col-12 js-recruitment__item">
                            <div class="recruitment__item">
                                <div class="recruitment__content">
                                    <h3 class="recruitment__content-title">{{ $j['nomposte'] }}</h3>
                                    <p class="recruitment__content-subtitle">{{ $j['saisie2'] }}</p>
                                    <div class="recruitment__content-description">{!! $j['memo1'] !!} </div>
                                </div>
                                <div class="recruitment__action">
                                    <p class="recruitment__action-date">Publiée le
                                        {{ \Carbon\Carbon::createFromFormat("d/m/Y H:i:s", $j['datecreation'])->format("d/m/y") }}
                                    </p>
                                    <a href="{{ route("recruitment.offers") }}/{{ $j["id"] }}" class="recruitment__action-btn">Voir l'offre</a>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-12">
                            <p class="text--t-center">@lang('trad.common.no-result')</p>
                        </div>
                    @endforelse
                </div>
                <div class="recruitment-sidebar col-4">
                    <div class="recruitment-sidebar__map-container">
                        @include('front.components.map-style-controls')
                        <div id="map" data-context="recruitement-offers"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var offerCities = [
        @forelse ($cities_with_jobs as $key => $city)
            {
                city: "{!! $key !!}",
                lat: "{!! $city['lat'] !!}",
                lng: "{!! $city['lng'] !!}",
                number: "{!! $city['number'] !!}",
                ids: @json( $city['ids'] ),
                names: @json( $city['names'] )
            },
        @empty
        @endforelse
        ];
    </script>

    @include('partials.mbox')
@endsection
