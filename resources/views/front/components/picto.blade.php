<div class="container-full-w container-border">
    <div class="container container-1170 container--shadow container--bg-white">
        <div class="row">
            <div class="picto-tiles__item col-xl-3 col-lg-3 col-md-6 col-12">
                <div class="picto-tiles__item-container">
                    <div class="img-container img-objectfit">
                        <img
                            @isset($subscription->type)
                                @if($subscription->isFitness())
                                    src="{{url('/images/pictos/haltere.png')}}"
                                @else
                                    src="{{url('/images/pictos/running.png')}}"
                                @endif
                            @else
                                src="{{url('/images/pictos/haltere.png')}}"
                            @endisset
                            alt="picto-img"
                             class="picto-tiles__image">
                    </div>
                    <p class="picto-tiles__text">
                        @lang('trad.picto.stuff')
                    </p>
                </div>
            </div>
            <div class="picto-tiles__item col-xl-3 col-lg-3 col-md-6 col-12">
                <div class="picto-tiles__item-container">
                    <div class="img-container img-objectfit">
                        <img src="{{url('/images/pictos/chrono.png')}}" alt="picto-img" class="picto-tiles__image">
                    </div>
                    <p class="picto-tiles__text">
                        @lang('trad.picto.time')
                    </p>
                </div>
            </div>
            <div class="picto-tiles__item col-xl-3 col-lg-3 col-md-6 col-12">
                <div class="picto-tiles__item-container">
                    <div class="img-container img-objectfit">
                        <img src="{{url('/images/pictos/cours-collectifs.png')}}" alt="picto-img" class="picto-tiles__image">
                    </div>
                    <p class="picto-tiles__text">
                        @if(
                        (\Route::current()->getName() == 'front.club-details' && $club->subscription->type == 'wellness')
                        ||
                        (\Route::current()->getName() == 'subscription.index' && $subscription->type == 'wellness')
                        )
                            @lang('trad.picto.group-training')
                        @else
                            @lang('trad.picto.collective')
                        @endif
                    </p>
                </div>
            </div>
            <div class="picto-tiles__item col-xl-3 col-lg-3 col-md-6 col-12">
                <div class="picto-tiles__item-container">
                    <div class="img-container img-objectfit">
                        <img src="{{url('/images/pictos/medaille.png')}}" alt="picto-img" class="picto-tiles__image">
                    </div>
                    <p class="picto-tiles__text">
                        @lang('trad.picto.coach')
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
