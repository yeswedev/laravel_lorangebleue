@section('recaptcha-scripts')
    <script type="text/javascript">
        var onloadCallback = function() {
        console.log("grecaptcha is ready!");
        };
    </script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
@endsection

<div class="js-contact-modal modal modal-vcenter fade" @isset($club) data-club-id={{$club->id}} id="basicModal{{$club->id}}" @else id="basicModal" @endisset tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        @isset($modal)
            @if($modal)
            {{ Form::open(array('action' => array('Front\ClubController@contactUs', $club->id), 'method' => 'GET', 'class' => 'js-tag-commander-mailto js-modal-contact', 'data-city' => $club->city,
            'data-club' => $club->city."_".$club->title)) }}
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <div class="cross_button" aria-hidden="true">
                    <span>
                        <span class="navicon__line"></span>
                        <span class="navicon__line"></span>
                        <span class="navicon__line"></span>
                    </span>
                </div>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title h2 modal__content__title text--t-center text-uppercase mb-4" id="myModalLabel">
                   @lang('trad.modal-contact.contact')
                </h3>
                <div class="modal__content__text">
                    <p>@lang('trad.modal-contact.text')</p>
                </div>
                    <div class="mt-md-4 mt-3 mt-modal">
                        <div class="row">
                            <div class="block-sample col-12">
                                <div class="in_header">
                                    <p class="in_text__input_header modal_header"><strong>{{$club->title}}</strong></p>
                                    <input type="hidden" name="club-modal" value="{{$club->id}}">
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="mt-modal">
                    <div class="row">
                        <div class="col-xl-6 col-md-12">
                            <div class="blocks">
                                <div class="block-sample">
                                    <div class="in_text">
                                        {!! Form::text('lastname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                        <label class="in_text__label" for="lastname">@lang('trad.modal-contact.lastname-label')</label>
                                        <span class="in_text__validmark"></span>
                                        {!! $errors->first('lastname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12">
                            <div class="blocks">
                                <div class="block-sample">
                                    <div class="in_text">
                                        {!! Form::text('firstname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                        <label class="in_text__label" for="firstname">@lang('trad.modal-contact.firstname-label')</label>
                                        <span class="in_text__validmark"></span>
                                        {!! $errors->first('firstname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-modal">
                    <div class="row">
                        <div class="col-xl-6 col-md-12">
                            <div class="blocks">
                                <div class="block-sample">
                                    <div class="in_text in_popover">
                                        {!! Form::email('email', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                                        <label class="in_text__label" for="email">@lang('trad.modal-contact.email-label')</label>
                                        <span class="in_popover__span css-popover-container">
                                            <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                            <span class="css-popover">@lang('trad.modal-contact.popover-email')</span>
                                        </span>
                                        {!! $errors->first('email', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12">
                            <div class="blocks">
                                <div class="block-sample">
                                    <div class="in_text in_popover">
                                        {!! Form::text('phone_number', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                                        <label class="in_text__label" for="phone_number">@lang('trad.modal-contact.tel-label')</label>
                                        <span class="in_popover__span css-popover-container">
                                            <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                            <span class="css-popover">@lang('trad.modal-contact.popover-tel')</span>
                                        </span>
                                        {!! $errors->first('phone_number', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-modal">
                    <div class="row">
                        <div class="col-12">
                            <div class="blocks">
                                <div class="in_textarea">
                                    {!! Form::textarea('message-modal', null, array('class' => 'in_text__input in_textarea__input', 'required' => 'required')) !!}
                                    <label class="in_text__label" for="message-modal">@lang('trad.modal-contact.message-label')</label>
                                    <span class="in_text__validmark"></span>
                                    {!! $errors->first('message-modal', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex mt-md-4">
                    <div class="block-sample mt-md-0 mt-3">
                        <div class="modal_in_checkbox">
                            {!! Form::checkbox('contact', 'contact', false,array('class' => 'in_checkbox__input--modal','id' => 'initial'.$club->id, 'required' => 'required')); !!}
                            <label class="in_checkbox__label--modal" for="initial{{$club->id}}">
                                @lang('trad.modal-contact.form-submit')
                            </label>
                        </div>
                    </div>
                </div>
                <div class="mt-3 mt-modal">
                    <div @isset($club) id="basicModalCaptcha-{{$club->id}}" @else id="basicModalCaptcha" @endisset class="modal_row modal_row--center"></div>
                </div>
                <div class="mt-3 mt-modal">
                    <div class="modal_row modal_row--center">
                        <div class="step2__action d-flex align-items-center justify-content-between">
                            <button
                                data-city="{{$club->city}}" data-subscription="{{$club->subscription->type}}" type="submit" class="btn btn--bg-orange btn--t-white btn--margin-t-0 button-int modal_btn">
                                @lang('trad.modal-contact.form-send')
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mt-3 mb-4 mt-modal">
                    <div class="modal_row">
                        <p class="modal_text">@lang('trad.modal-contact.mandatory-field')</p>
                    </div>
                    <div class="modal_row">
                        <p class="modal_text">@lang('trad.modal-contact.condition')
                        </p>
                    </div>
                </div>
            </div>
            </div>
            {!! Form::close() !!}
            @endif
        @endisset
    </div>
</div>
