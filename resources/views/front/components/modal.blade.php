<div class="modal modal-vcenter fade" @isset($club) id="basicModal{{$club->id}}" @else id="basicModal"
     @endisset tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        @isset($modal)
            @if($modal)
                {{
                    Form::open(
                        array(
                            'action' => array('Front\ClubController@contact', $club->id),
                            'method' => 'POST',
                            'onsubmit' => 'commanderFormPreInsc(event, \''.$club->subscription->type.'\', \''.App::getLocale().'\', \''.str_slug($club->city).'\')'
                        )
                    )
                }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <div class="cross_button" aria-hidden="true">
                    <span>
                        <span class="navicon__line"></span>
                        <span class="navicon__line"></span>
                        <span class="navicon__line"></span>
                    </span>
                            </div>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2 class="modal-title h2 modal__content__title text--t-center text-uppercase mb-3"
                            id="myModalLabel">
                            @lang('trad.modal-pre-sign-in.title')
                        </h2>
                        <div class="modal__content__text">
                            <p>@lang('trad.modal-pre-sign-in.text')</p>
                        </div>
                        <div class="mt-modal">
                            <div class="row">
                                <div class="col-xl-6 col-md-12">
                                    <div class="blocks">
                                        <div class="block-sample">
                                            <div class="in_text">
                                                {!! Form::text('lastname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                                <label class="in_text__label" for="lastname">@lang('trad.modal-contact.lastname-label')</label>
                                                <span class="in_text__validmark"></span>
                                                {!! $errors->first('lastname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-12">
                                    <div class="blocks">
                                        <div class="block-sample">
                                            <div class="in_text">
                                                {!! Form::text('firstname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                                <label class="in_text__label" for="firstname">@lang('trad.modal-contact.firstname-label')</label>
                                                <span class="in_text__validmark"></span>
                                                {!! $errors->first('firstname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-modal">
                            <div class="row">
                                <div class="col-xl-6 col-md-12">
                                    <div class="blocks">
                                        <div class="block-sample">
                                            <div class="in_text in_popover">
                                                {!! Form::email('email', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                                                <label class="in_text__label" for="email">@lang('trad.modal-contact.email-label')</label>
                                                <span class="in_popover__span css-popover-container">
                                                    <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                                    <span class="css-popover">@lang('trad.modal-contact.popover-email')</span>
                                                </span>
                                                {!! $errors->first('email', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-12">
                                    <div class="blocks">
                                        <div class="block-sample">
                                            <div class="in_text in_popover">
                                                {!! Form::text('phone_number', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                                                <label class="in_text__label d-flex justify-content-between" for="phone_number">@lang('trad.modal-contact.tel-label')</label>
                                                <span class="in_popover__span css-popover-container">
                                                    <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                                    <span class="css-popover">@lang('trad.modal-contact.popover-tel')</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex mt-4">
                            <div class="block-sample mt-md-0">
                                <div class="modal_in_checkbox">
                                    {!! Form::checkbox('contact', 'contact', false,array('class' => 'in_checkbox__input--2','id' => 'initial'.$club->id, 'required' => 'required')); !!}
                                    <label class="in_checkbox__label--2" for="initial{{$club->id}}">
                                        @lang('trad.modal-pre-sign-in.send')
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 mt-modal">
                            <div class="modal_row modal_row--center">
                                <div class="step2__action d-flex align-items-center justify-content-between">
                                    <button
                                            data-city="{{$club->city}}"
                                            data-subscription="{{$club->subscription->type}}" type="submit"
                                            class="btn btn--bg-orange btn--t-white btn--margin-t-0 button-int modal_btn">
                                        @lang('trad.modal-pre-sign-in.validate')
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-4 mt-4 mt-modal">
                            <div class="modal_row">
                                <p class="modal_text">@lang('trad.modal-contact.mandatory-field')</p>
                            </div>
                            <div class="modal_row">
                                <p class="modal_text">@lang('trad.modal-contact.condition')</p>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            @endif
        @endisset
    </div>
</div>
