<div class="row">
    <div class="js-carousel-blog owl-carousel owl-theme d-flex flex-wrap">
        @forelse($blog_lists as $key => $blog)
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 pt-3 pb-3">
                <div class="blog-tiles__item js-item-carousel">
                    @php($parent = $blog->parent)
                    @if($parent != null)
                        <a href="{{route('blog.retrieve', [$parent->url, $blog->url])}}" class="blog-tiles__main_lnk">
                            @else
                                <a href="{{route('blog.retrieve', ['categorie-'.$blog->id, $blog->url])}}"
                                   class="blog-tiles__main_lnk">
                                    @endif
                                    <div class="blog-tiles__item-container js-fade-parent"
                                         data-finditem="blog-tiles__lnk">
                                        <div class="blog-tiles__metadatas-header">
                                            @if($blog->attachments && $blog->attachments->where('type','img')->first())
                                                <img data-src="{{Storage::url($blog->attachments->where('type','img')->first()->src)}}"
                                                     alt="{!! $blog->attachments->where('owner_id',$blog->id)->first()->title !!}"
                                                     class="lazy"/>
                                            @else
                                                <img data-src="{{url('/images/Final_FP2.png')}}"
                                                     alt="image representant un article du blog"
                                                     class="blog-tiles__image lazy">
                                            @endisset
                                        </div>
                                        <div class="blog-tiles__metadatas">
                                            <p class="blog-tiles__category text--uppercase text--bold">
                                                @php($parent = $blog->parent)
                                                @if($parent != null)
                                                    {{$parent->title}}
                                                @else
                                                    @lang('trad.common.category')
                                                @endif
                                            </p>
                                            <h3 class="blog-tiles__title text--uppercase text--bold">{!! $blog->title !!}</h3>
                                        </div>
                                        <div class="blog-tiles__metadatas-footer">
                                            <p class="blog-tiles__date text--uppercase ">
                                                {{$blog->created_at->format('d/m/Y')}}
                                            </p>
                                            @php($parent = $blog->parent)
                                            @if($parent != null)
                                                <a href="{{route('blog.retrieve', [$parent->url, $blog->url])}}"
                                                   class="blog-tiles__lnk lnk lnk--blue lnk--underline">@lang('trad.button.more')
                                                    <i
                                                            class="fas fa-long-arrow-alt-right"></i></a>
                                            @else
                                                <a href="{{route('blog.retrieve', ['categorie-'.$blog->id, $blog->url])}}"
                                                   class="blog-tiles__lnk lnk lnk--blue lnk--underline">@lang('trad.button.more')
                                                    <i
                                                            class="fas fa-long-arrow-alt-right"></i></a>
                                            @endif
                                        </div>
                                    </div>
                                </a>
                        </a>
                </div>
            </div>
        @empty
            <div class="d-flex flex-column align-items-center">
                @lang('trad.blog.noresult')

                <div class="block-sample mt-3 d-xl-inline-block">
                    <div class="in_text">
                        <form method="GET" action="{{ route('blog.search') }}"
                              class="d-flex align-items-xl-center article__aside_form mt-4">
                            <input type="text" name="keyword" id="keyword" class="in_text__input"
                                   required>
                            <label class="in_text__label"
                                   for="keyword">@lang('trad.blog.retry')</label>
                            <span class="in_text__validmark"></span>
                            <button class="ml-2 btn btn--bg-blue btn--t-white btn-responsive">@lang('trad.common.submit-ok')
                            </button>
                        </form>
                    </div>
                </div>

                <a href="{{ route('blog.showAll') }}"
                   class="btn btn--bg-orange btn--t-white mt-4">@lang('trad.blog.return')</a>
            </div>
        @endforelse
    </div>
</div>
