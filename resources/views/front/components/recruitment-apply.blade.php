@extends("front.recruitment.recruitment")

@section('recruitment')
    @include('partials.success')
    <div class="js-tab container--padding-t-80" data-tab="apply">
        <div class="recruitment-container">
            {{--@dd($job)--}}
            <div class="recruitment-tiles">
                @if($job != [])
                    <div class="recruitment-header cms-h2">
                        <p class="recruitment-header__categorie">
                            Catégorie
                        </p>
                        <h2 class="recruitment-header__title">{{ $job->saisie1 }}</h2>
                        <p class="recruitment-header__else">
                            {{ \Carbon\Carbon::createFromFormat("d/m/Y H:i:s", $job->datecreation)->format("d/m/Y") }}
                        </p>
                        <p class="recruitment-header__else">
                            @isset($job->ref)
                                Réf: {{ $job->ref }}
                            @else
                                Pas de référence
                            @endisset
                        </p>
                    </div>
                    <div class="recruitment-img">
                        {!! $job->memo3 !!}
                    </div>
                @endif
                <div class="recruitment-form">
                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    @endif
                    {{ Form::open(array('method' => 'post', 'url' => route('recruitment.applied'), 'id' => 'postulate-form', 'files' => true)) }}
                    @if($job != [])
                        {{ Form::hidden('offer', route('recruitment.offers').'/'.$job->id) }}
                        @isset($job->ref)
                                {{ Form::hidden('job_ref', $job->ref) }}
                        @endisset
                        @isset($job->saisie1)
                            {{ Form::hidden('job_nomposte', $job->saisie1) }}
                        @endisset
                    @endif
                    <div class="recruitment-infos">
                        <h2 class="recruitment-infos__title">Candidature</h2>
                        <div class="recruitment-infos__field">
                            <div>
                                <label for="cv">CV *</label>
                                <p class="form-field__help">@lang('trad.recruitment.form.files')</p>
                            </div>
                            <label for="cv" class="recruitment-infos__field__file-label recruitment-infos__field__file-label-button js-input-file">
                                <span>Joindre un fichier...</span>
                            </label>
                            {{ Form::file('cv', $attributes = array('class' => 'recruitment-infos__field__file', 'required' => 'required')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <div>
                                <label for="motivation">Lettre de motivation</label>
                                <p class="form-field__help">@lang('trad.recruitment.form.files')</p>
                            </div>
                            <label for="motivation" class="recruitment-infos__field__file-label recruitment-infos__field__file-label-button js-input-file">
                                <span>Joindre un fichier...</span>
                            </label>
                            {{ Form::file('motivation', $attributes = array('class' => 'recruitment-infos__field__file')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="mobility"> Mobilité régions</label>
                            {{ Form::select('mobility[]', $countries, null, ['id' => "mobility__select", 'multiple'=>'multiple']) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="card"> Carte professionnelle</label>
                            {{ Form::select('card', ['oui' => 'Oui', 'non' => 'Non', 'encours' => 'En cours']) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="disponibility">Disponibilité</label>
                            {{ Form::select('disponibility', ['immediate' => 'Immédiate', '1' => '1 mois', '2' => '2 mois', '+3' => '3 mois ou plus',]) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="hours">Volume horaire souhaité</label>
                            {{ Form::select('hours', ['full' => 'Temps plein', 'part' => 'Temps partiel']) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="diploma1">Diplôme 1</label>
                            {{ Form::text('diploma1') }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="diploma2">Diplôme 2</label>
                            {{ Form::text('diploma2') }}
                        </div>
                    </div>

                    <div class="recruitment-infos">
                        <h2 class="recruitment-infos__title">Coordonnées</h2>
                        <div class="recruitment-infos__field">
                            <label for="civility">Civilité *</label>
                            {{ Form::select('civility', ['M.' => 'M.', 'Mme' => 'Mme' ], '', array('required' => 'required')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">Nom *</label>
                            {{ Form::text('name', '', array('required' => 'required') ) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">Prénom *</label>
                            {{ Form::text('firstname',  '', array('required' => 'required')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">Portable *</label>
                            {{ Form::text('phone',  '', array('required' => 'required')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">Email *</label>
                            {{ Form::email('email', '', array('required' => 'required')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">CP *</label>
                            {{ Form::text('postal_code', '', array('required' => 'required')) }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">Ville</label>
                            {{ Form::text('city') }}
                        </div>
                        <div class="recruitment-infos__field">
                            <label for="">Pays *</label>
                            {{ Form::select('country', ['fr' => 'France', 'es'=> 'Espagne', 'ma'=> 'Maroc'], '', array('required' => 'required')) }}
                        </div>
                    </div>

                    <p>* champs obligatoires</p>

                    {{ Form::close() }}
                </div>
                <div class="container">
                    <button
                        type="submit"
                        class="btn btn-apply btn--bd-orange btn--bg-white btn--t-orange text--uppercase btn--centered"
                        form="postulate-form">
                        Envoyer votre candidature
                    </button>
                </div>
                <div>
                    <br>
                    <br>
                    <p>
                        L'Orange Bleue respecte le Règlement général sur la protection des données (RGPD). Dans ce cadre, vous bénéficiez d'un droit d'accès de rectification, de portabilité, d'effacement de vos données ou une limitation du traitement. Vous pouvez aussi vous opposer au traitement des données vous concernant et disposez du droit de retirer votre consentement à tout moment en nous précisant votre demande par l'intermédiaire de
                        <a href="{{ route('contact.index') }}">ce formulaire</a>.
                    </p>

                </div>
            </div>
            <div class="recruitment-sidebar"></div>
        </div>
    </div>
@endsection
