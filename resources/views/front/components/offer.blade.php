<div id="app">
    
        <div class="subscription__switch">
            <div class="switch">
                @if( isset($hasStudentOffers) && $hasStudentOffers )
                    <p>@lang('trad.button.switch')</p>
                    <is-student
                            @isset($order)
                                init-is-student="{{$order->is_student}}"
                            @endisset
                    ></is-student>
                @endif
            </div>
        </div>
        

    <div class="subscription-tiles row">
        <div class="owl-carousel owl-theme js-carousel-container">
            @isset($offers)
                @foreach($offers as $offer)
                    <offer-component
                            locale="{{LaravelLocalization::getCurrentLocale()}}"
                            id="{{$offer->id}}"

                            @isset($subscriptionUrl)
                            @else
                                @isset($club)
                                    total-price="{{formatPrices($offer->getUniqueTotalPriceOrder($offer->getPrices($club->id)) / 100)}}@lang('meta.devise')"
                                @endisset
                            @endisset

                            @isset($order)
                                is-checked="{{$offer->id == $order->offer_id}}"
                            @else
                                is-checked="0"
                            @endisset

                            @isset($club)
                                :club-id="{{$club->id}}"
                            @endisset

                            @isset($subscriptionUrl)
                                @isset($club)
                                    subscription-url="{{route('subscription.step3', ['subscriptionUrl' => $club->subscription->url, 'clubId' => $club->id, 'offerId' => $offer->id ])}}"
                                @else
                                    subscription-url="{{route('subscription.step1', $subscriptionUrl)}}"
                                @endisset
                            @else
                                subscription-url="{{route('subscription.step1', \App\Subscription::DEFAULT_URL)}}"
                            @endif
                    ></offer-component>
                @endforeach
            @endisset
        </div>
        <div class="subscription__description">
            <p class="text-uppercase">@lang('trad.offer-component.title')</p>
            <ul>
                <li>@lang('trad.offer-component.first-item')</li>
                <li>@lang('trad.offer-component.second-item')</li>
                <li>@lang('trad.offer-component.third-item')</li>
                @if(LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>@lang('trad.offer-component.fourth-item')</li>
                @endif
            </ul>
        </div>
    </div>
    <div class="subscription__mention">
        <div class="subscription__mention_body">
            @if(isset($no_legal_mentions) && $no_legal_mentions)
                @if(LaravelLocalization::getCurrentLocale() == 'fr')
                    @if(count($offers) && $offers->first()->getBestAvailableOffers($club))
                        {!! $offers->first()->getBestAvailableOffers($club)->legal_mentions_home !!}
                    @else
                        @isset($offers[0])
                            @if ($offers[0] && $offers[0]->reduced_commitment)
                                @lang('trad.common.legal-mention-reduced-commitment', [
                                    'devise' => __('meta.devise'),
                                    'reduced_price' => formatPrices($offers[0]->reduced_price/100),
                                    'reduced_commitment' => $offers[0]->reduced_commitment,
                                    'price' => formatPrices($offers[0]->price/100),
                                    'commitment' => $offers[0]->commitment - $offers[0]->reduced_commitment,
                                    ])
                            @endif
                            @lang('trad.common.legal-mention', [
                                'devise' => __('meta.devise'),
                                'reduced_price' => formatPrices($offers[0]->reduced_price/100),
                                'reduced_commitment' => $offers[0]->reduced_commitment,
                                'price' => formatPrices($offers[0]->price/100),
                                'commitment' => $offers[0]->commitment - $offers[0]->reduced_commitment,
                                ])
                        @endisset
                    @endif
                @else
                    @isset($offers[0])
                        @if ($offers[0] && $offers[0]->reduced_commitment)
                            @lang('trad.common.legal-mention-reduced-commitment', [
                                'devise' => __('meta.devise'),
                                'reduced_price' => formatPrices($offers[0]->reduced_price/100),
                                'reduced_commitment' => $offers[0]->reduced_commitment,
                                'price' => formatPrices($offers[0]->price/100),
                                'commitment' => $offers[0]->commitment - $offers[0]->reduced_commitment,
                                ])
                        @endif
                        @lang('trad.common.legal-mention', [
                            'devise' => __('meta.devise'),
                            'reduced_price' => formatPrices($offers[0]->reduced_price/100),
                            'reduced_commitment' => $offers[0]->reduced_commitment,
                            'price' => formatPrices($offers[0]->price/100),
                            'commitment' => $offers[0]->commitment - $offers[0]->reduced_commitment,
                            ])
                    @endisset
                @endif
            @elseif (isset($no_legal_mentions) && !$no_legal_mentions)
                @if ( $offers->first()->legal_mentions )
                    {!! $offers->first()->legal_mentions !!}
                @endif
            @else
                @isset($offers[0])
                    @if ($offers[0]->legal_mentions)
                        {!! $offers[0]->legal_mentions !!}
                    @else
                        @if ($offers[0] && $offers[0]->reduced_commitment)
                            @lang('trad.common.legal-mention-reduced-commitment', [
                                'devise' => __('meta.devise'),
                                'reduced_price' => formatPrices($offers[0]->reduced_price/100),
                                'reduced_commitment' => $offers[0]->reduced_commitment,
                                'price' => formatPrices($offers[0]->price/100),
                                'commitment' => $offers[0]->commitment - $offers[0]->reduced_commitment,
                                ])
                        @endif
                        @lang('trad.common.legal-mention', [
                            'devise' => __('meta.devise'),
                            'reduced_price' => formatPrices($offers[0]->reduced_price/100),
                            'reduced_commitment' => $offers[0]->reduced_commitment,
                            'price' => formatPrices($offers[0]->price/100),
                            'commitment' => $offers[0]->commitment - $offers[0]->reduced_commitment,
                            ])
                    @endif
                @endisset
            @endif
            <p>
                <a href="{{route('page.retrieve', trans('routes.page.legal-mentions'))}}">
                    @lang('trad.button.more')
                </a>
            </p>
        </div>
    </div>
</div>

@foreach($offers as $offer)
    @isset($club)
        <script>
            window['commanderAboClubPage'+{{$offer->id}}] = function() {
                window.commanderAboClubPage('{{$club->title}}','{{$offer->title}}')
            }
        </script>
        @include('front.components.modal',['club' => $club])
    @endisset
@endforeach
