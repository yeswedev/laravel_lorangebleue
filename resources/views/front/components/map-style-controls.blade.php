<div class="map__styler">
    <input
        class="js-map-style-changer map__radio"
        id='plan'
        type='radio'
        name='map-style'
        value='plan'
        checked
    >
    <label for='plan'>@lang('trad.club-detail.plan')</label>
    <input
        class="js-map-style-changer map__radio"
        id='satellite'
        type='radio'
        name='map-style'
        value='satellite'
    >
    <label for='satellite'>@lang('trad.club-detail.satellite')</label>
</div>
