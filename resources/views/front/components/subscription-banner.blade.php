<div class="subscription-band-v2 @if(isset($aside) && $aside)) subscription-band-v2--aside @endif">
    <div class="subscription-band-v2__title">
        @lang('trad.subscription.offer-title')
    </div>

    <div class="subscription-band-v2__princing-container">
        <div class="subscription-band-v2__princing-offer">
            <span class="subscription-band-v2__start-from">@lang('trad.subscription.start-from')</span>

            <div class="subscription-band-v2__offer-price">
                <span class="subscription-band-v2__euros">
                    {{explode('.', $price)[0]}}
                </span>

                <div class="subscription-band-v2__next-to-euros">
                    @isset(explode('.', $price)[1])
                        <span class="subscription-band-v2__cents">
                            @if(trans('meta.devise') != '€')
                                ,{{explode('.', $price)[1]}}@lang('meta.devise')
                            @else
                                @lang('meta.devise'){{explode('.', $price)[1]}}
                            @endif
                        </span>
                    @endisset

                    <span class="subscription-band-v2__per-month">@lang('trad.subscription.month')*</span>
                </div>
            </div>
        </div>

        @isset($avantageDiscount)
        <div class="subscription-band-v2__princing-discount">
            <span class="subscription-band-v2__discount-price">
                    {{$avantageDiscount}}<sup>@lang('meta.devise')</sup>
                </span>

            <span class="subscription-band-v2__discount-text">@lang('trad.subscription.advantages')**</span>
        </div>
        @endisset
    </div>

    @isset($subscriptionUrl)
        <div class="subscription-band-v2__button">
            <a href="{{route('subscription.step1', $subscriptionUrl)}}"
               class="btn btn--bg-orange btn--t-white js-tag-commander-abo-ban"
               data-city="@isset($club) {{$club->city}} @endisset"
               data-club="@isset($club) {{$club->city}}_{{$club->title}} @endisset"
            >
                @lang('trad.button.subscribe')
            </a>
        </div>
    @endisset
</div>
