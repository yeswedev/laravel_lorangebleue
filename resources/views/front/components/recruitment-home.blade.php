@extends('front.recruitment.recruitment')

@section('recruitment')
    <div class="container container--padding-t-80 js-tab" data-tab="home">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <iframe src="https://player.vimeo.com/video/276187909?title=0&byline=0" width="570" height="310" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="max-width:100%"></iframe>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="">
                    <h2 class="cms-h2 text--uppercase" style="font-size: 25px;line-height: 30px;">
                        Convivialité, fun, esprit d'équipe, professionnalisme... Rejoignez dès aujourd'hui l'Orange Bleue !
                    </h2>
                    <div class="cms-content">
                        <div>
                            N°1 de la remise en forme, nous accompagnons chaque jour de plus en plus d’adhérents dans
                            leur souhait de remise en forme et de bien-être. Pour cela nous recherchons partout en
                            France et à l’international des professionnels, coachs sportifs et responsables de salle,
                            motivés et ambitieux. Mais l’Orange Bleue c’est aussi une marque commerciale qui a besoin de
                            nouveaux talents afin de se développer encore plus chaque jour au sein de son siège. Alors
                            n’hésitez plus et commencez une nouvelle aventure avec nous.
                            <br><br>
                            Quelques chiffres :
                            <ul>
                                <li>
                                    Déjà 400 clubs en France et bientôt 600 dans le monde.
                                </li>
                                <li>
                                    Vous êtes plus de 1 000 à travailler avec nous et nous en sommes fiers !
                                </li>
                                <li>
                                    Un concept de cours uniques et novateurs : les Yako©.
                                </li>
                                <li>
                                    Nous vous proposons plus de 250 formations sur toute la France pour monter en compétence.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="">
                    <h2 class="cms-h2 text--uppercase" style="font-size: 25px;line-height: 30px;">
                        Vos compétences, notre force
                    </h2>
                    <div class="cms-content">
                        <div>
                            Depuis toujours, nous faisons des compétences de nos salariés une véritable force. Et
                            aujourd’hui, ce pari a payé puisque l’Orange Bleue est le leader de la remise en forme en
                            France. Notre métier évolue rapidement ; sans cesse en mouvement, nous apprenons à moduler
                            nos pratiques et relever de nouveaux défis.
                            <br><br>
                            La maîtrise de ces nouvelles compétences est une priorité de notre marque. Aussi, le réseau
                            a créé l’Orange Bleue Académie, qui a pour mission de permettre à tous de progresser dans
                            leur pratique professionnelle. L’Orange Bleue Académie propose à tous les collaborateurs de
                            notre marque de se former sur plusieurs aspects : cours collectifs, relation clients,
                            management, etc.
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{ url('/images/coaches-lorange-bleue.jpg')}}" alt="">
            </div>
        </div>
    </div>
    <div class="container--padding-t-80">
        <h2 class="h2 text--t-center title--underlined-center--blue text--uppercase components-list__title">Nos dernières
            offres</h2>
        <p class="text--t-center components-list__text">
            Découvrez toutes nos offres en France et à l'international
        </p>
    </div>
    <div class="recruitment-home-container">
        <div class="recruitment-tiles home-tab">
            @forelse ($job as $key => $j)
                <div class="col-12 p-0">
                    <div class="recruitment__item">
                        <div class="recruitment__content">
                            <h3 class="recruitment__content-title">{{ $j['nomposte'] }}</h3>
                            <p class="recruitment__content-subtitle">{{ $j['saisie2'] }}</p>
                            <div class="recruitment__content-description">{!! $j['memo1'] !!} </div>
                        </div>
                        <div class="recruitment__action">
                            <p class="recruitment__action-date">Publiée le {{ \Carbon\Carbon::createFromFormat("d/m/Y H:i:s", $j['datecreation'])->format("d/m/y") }}</p>
                            <a href="{{ route("recruitment.offers") }}/{{ $j["id"] }}" class="recruitment__action-btn">Voir l'offre</a>
                        </div>
                    </div>
                </div>
                @if ($key == 3)
                    @break
                @endif
            @empty
                <div class="col-12">
                    <p class="text--t-center">Aucun résultat, réessayer plus tard</p>
                </div>
            @endforelse
        </div>
        <div class="components-list__actions">
            <a href="{{ route("recruitment.offers") }}" class="btn btn--centered btn--bg-white btn--t-orange btn--bd-orange">Voir toutes les offres</a>
        </div>
    </div>
@endsection
