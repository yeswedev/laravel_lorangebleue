@extends('layouts.front')

@section('title')
 Confirmation d'inscription
@endsection
@section('description')
 Confirmation d'inscription
@endsection

@section('content')
    @include('front.tunnel.components.header')
    @include('partials.success')
    <div class="container-full-w container__tunnel tunnel__confirm tunnel__step3">
        <div class="container__tunnel__header">
            <div class="container container-970">
                <h1 class="text--t-center text--uppercase h1-abo">@lang('trad.tunnel-confirm.title')</h1>
            </div>
        </div>
        <div class="container-full-w container--bg-beige tunnel__confirm__content">
            <div class="container container-970">
                <div class="text-center mb-5">
                    <img src="{{url('/images/picto-bras-muscle.png')}}" alt="picto bras muscle"/>
                </div>
                <p>@lang('trad.tunnel-confirm.subtitle')</p>
                @isset($message)
                    <div class="alert alert-success"> {{ $message }}</div>
                @endisset
                <div class="confirm__content__recap">
                    <h2 class="text--uppercase content__recap_title text-center">@lang('trad.tunnel-confirm.summary')</h2>
                    <div class="content__recap_details">
                        <div class="d-flex flex-wrap">
                            <div class="pr-5">
                                <div class="col-xl-12">
                                    <div class="">
                                        <div class="recap_details--minHei">
                                            <h3 class="recap_details_title">@lang('trad.tunnel-confirm.my-club')</h3>
                                            <span class="logo">
                                                <img src="{{url('/images/'.'ob-'.$club->subscription->type.'.svg')}}"
                                                width="100%" height="100%"
                                                alt="picto-{{$club->subscription->type}}"/>
                                            </span>
                                            <p class="recap__club__name">{{$club->title}}</p>
                                            <p class="">{{$club->address}}<br/>
                                                <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span></p>
                                        </div>
                                    </div>
                                    <div class="recap__border_mobile">
                                        <h3 class="recap_details_title  recap_details_title--border">@lang('trad.tunnel-confirm.my-formula')</h3>
                                        <p class="text--uppercase recap__formule__title">
                                            {{$offer->title}}
                                            @isset($is_student)
                                            <br>
                                                @if($is_student == 1)
                                                    <span class="recap__formule__span">« Sous réserve d’un justificatif à jour »</span>
                                                @endif
                                            @endisset
                                        </p>
                                        <p>
                                            @if($order)
                                                @if($payment->type == 'unique')
                                                    <strong>{{($order->getUniqueTotalPriceOrder(false) /100)}}@lang('meta.devise')</strong>
                                                @else
                                                    <strong>{{$offer->price / 100}}@lang('meta.devise')</strong> @lang('trad.common.during') <strong>{{$offer->commitment}} @lang('trad.common.month')</strong>
                                                @endif
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row flex-column">
                                <div class="col-xl-12">
                                    <div class="">
                                        <div class="recap_details--minHei">
                                            <h3 class="recap_details_title">@lang('trad.tunnel-confirm.contact-information')</h3>
                                            <ul class="recap_details_list">
                                                <li>{{$order->civility}}</li>
                                                <li>{{$order->firstname}} {{$order->lastname}}</li>
                                                <li>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$order->birthdate)->format('d/m/Y')}}</li>
                                                <li>{{$order->email}} - {{$order->phone_number}}</li>
                                                <li>{{$order->address}}</li>
                                                <li>{{$order->alt_address}}</li>
                                                <li>{{$order->zip_code}} - {{$order->city}}</li>
                                                <li>Opt-in newsletter</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="">
                                        <h3 class="recap_details_title recap_details_title--border">@lang('trad.tunnel-confirm.payment-method')</h3>
                                        @if($payment->type == 'unique')
                                            <p>@lang('trad.tunnel-confirm.unique-payment')</p>
                                        @else
                                            <p>@lang('trad.tunnel-confirm.automated-payment')</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-1170">
            <div class="confirm__card">
                <div class="pt-5 home-redirect">
                    <a href="/" class="btn btn--bg-white btn--t-orange btn--bd-orange btn__home_redirect">@lang('trad.tunnel-confirm.home')
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('front.tunnel.components.tunnel_modal')
    @include('front.tunnel.components.footer')
    @include('partials.tag-commander-event')
@endsection
