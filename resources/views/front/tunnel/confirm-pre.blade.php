@extends('layouts.front')

@section('title')
 Confirmation de pré-inscription
@endsection
@section('description')
 Confirmation de pré-inscription
@endsection

@section('content')
    @include('front.tunnel.components.header')
    @include('front.components.modal-contact')
    <div class="container-full-w container__tunnel tunnel__confirm tunnel__pre-confirm">
        <div class="container__tunnel__header">
            <div class="container container-1170">
                <h1 class="text--t-center text--uppercase h1-abo">@lang('trad.tunnel-pre-confirm.title')</h1>
            </div>
        </div>
        <div class="container-full-w container--bg-beige tunnel__confirm__content">
            <div class="container container-970">
                <div class="text-center mb-5">
                    <img src="{{url('/images/picto-bras-muscle.png')}}" alt="picto bras muscle"/>
                </div>
                <p class="tunnel__pre-confirm__headDesc"><strong>@lang('trad.tunnel-pre-confirm.subtitle')</strong></p>
                <p class="tunnel__pre-confirm__desc">@lang('trad.tunnel-pre-confirm.desc') </p>
            </div>

            <div class="container-970 container">
                <div class="pre-confirm__block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pre-confirm__block__img">
                                @if($club->subscription->type == 'fitness')
                                    <img class="card-club__main-image"
                                         src="{{url('/images/recherche-mcf.jpg')}}"
                                         alt="image salle club fitness"/>
                                @elseif($club->subscription->type == 'wellness')
                                    <img class="card-club__main-image"
                                         src="{{url('/images/recherche-mcw.jpg')}}"
                                         alt="image salle club wellness"/>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pre-confirm__card">
                                <div class="confirm__card__content">
                                    @if($club->subscription->type == 'fitness')
                                        <img class="pre-confirm__card__img" src="{{url('/images/logo-ob-mcf-fondblanc.svg')}}"
                                             alt="logo mon coach fitness"/>
                                    @else
                                        <img class="pre-confirm__card__img" src="{{url('/images/wellness-sm.svg')}}"
                                             alt="logo mon coach wellness"/>
                                    @endif
                                    <p class="pre-confirm__card__title text--uppercase">{{$club->title}}</p>
                                    <p class="pre-confirm__card__text">
                                        {{$club->address}} <br/>
                                        <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span>
                                    </p>
                                </div>
                                <div class="confirm__card__footer">
                                    <div class="d-flex flex-wrap-reverse">
                                        <a href="{{route('front.club-details', $club->url)}}"
                                           class="btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn">
                                           @lang('trad.tunnel-pre-confirm.discover')
                                        </a>
                                        {{-- TODO put phone number show function here --}}
                                        <button class="js-tag-commander-tel btn btn--bg-white btn--t-orange btn--bd-orange club__actions js-switch-state js-show-phone"
                                            data-addclass="club__actions--active" data-city="{{$club->city}}">
                                            <span class="text">
                                                <img src="{{url('/images/phoneicon.png')}}" alt="picto téléphone"/>
                                            </span>
                                            <span class="phone_number">{{$club->phone}}</span>
                                        </button>

                                        <a href="" data-toggle="modal" data-target="#basicModal{{$club->id}}"
                                            data-city="{{$club->city}}"
                                            class="btn btn--bg-white btn--t-orange btn--bd-orange card-club__image button-int club__footer__actions club__footer__actions_phone"
                                            style=""
                                        >
                                            <img src="{{url('/images/mailicon.png')}}" alt="picto mail"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-1170">
            <div class="confirm__card">
                <div class="pt-5 home-redirect">
                    <a href="/" class="btn btn--bg-white btn--t-orange btn--bd-orange btn__home_redirect">@lang('trad.tunnel-confirm.home')
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('front.tunnel.components.footer')
    @include('partials.tag-commander-event')
@endsection
