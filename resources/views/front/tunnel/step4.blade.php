@extends('layouts.front')

@section('title')
    Tunnel d'abonnement - étape 4
@endsection
@section('description')
    Tunnel d'abonnement - étape 4
@endsection
@section('meta')
    <meta name="robots" content="noindex">
@endsection

@section('content')
    @include('front.tunnel.components.header')
    @include('partials.errors')
    @include('partials.success')
    <div class="container-full-w container__tunnel tunnel__step3 tunnel__step4">
        <div class="container__tunnel__header">
            <div class="container container-970">
                <h1 class="text--t-center text--uppercase h1-abo">@lang('trad.tunnel-step-4.title')</h1>
            </div>
        </div>
        <div class="tunnel__content">
            <div class="tunnel__content__mobile">
                <div class="container container-1170 container__tunnel__content">
                    <div class="tunnel__step d-flex container--shadow">
                        <a class="tunnel__step__lnk" href="{{route('subscription.step1', 'mon-coach-fitness')}}">
                            <div class="tunnel__step__item--validate tunnel__step__item d-flex justify-content-center">
                                <p class="align-self-center">1 <span
                                            class="item--hide-mobile"> - @lang('trad.tunnel-header.my-club')</span></p>
                            </div>
                        </a>
                        <a href="{{route('subscription.step2', $club->subscription->url)}}" class="tunnel__step__lnk">
                            <div class="tunnel__step__item tunnel__step__item--validate d-flex justify-content-center">
                                <p class="align-self-center">2 <span
                                            class="item--hide-mobile"> - @lang('trad.tunnel-header.my-formula')</span>
                                </p>
                            </div>
                        </a>
                        <a class="tunnel__step__lnk" href="{{route('subscription.step3', ['subscriptionUrl' => $club->subscription->url, 'clubId' => $club->id, 'offerId' => $offer->id ])}}">
                            <div class="tunnel__step__item tunnel__step__item--validate d-flex justify-content-center">
                                <p class="align-self-center">3 <span
                                            class="item--hide-mobile">- @lang('trad.tunnel-header.my-contact')</span>
                                </p>
                            </div>
                        </a>
                        <div class="tunnel__step__item tunnel__step__item--active d-flex justify-content-center">
                            <p class="align-self-center">4 <span
                                        class="item--hide-mobile">- @lang('trad.tunnel-header.payment')</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container container-1170">
                {!!Form::open(array('action' => array('Front\SubscriptionController@payment', $subscription->url), 'method' => 'GET')) !!}
                <div class="row">
                    <div class="col-xl-8 col-md-12">
                        @isset($order)
                            <input type="hidden" name="token" value="{{$order->order_token}}">
                        @endisset
                        <div class="step4__paiement">
                            <p class="step4__paiment__pres">@lang('trad.tunnel-step-4.subtitle')</p>
                            @if($errors->has('message'))
                                <div class="alert alert-danger"> {{ $errors->first() }}</div>
                            @endif
                            {!! $errors->first('terms', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                            {!! $errors->first('mode', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                            <div class="step4__paiement__choice js-switch-state js-ajax-loaded"
                                 data-addclass="paiement__choice__rounded--active">
                                <div class="d-flex align-items-center">
                                    {{-- <div class="paiement__choice__rounded"></div> --}}
                                    <div class="btn-offer-round-tiny">
                                        <input type="radio" name="mode" value="automatique" id="automatique"/>
                                        <label for="automatique"></label>
                                    </div>
                                    <div class="d-flex align-items-center step4__paiement__choice--columnMobile">
                                        <div class="d-flex flex-column paiement__choice__text">
                                            <p class="paiement__choice_title">@lang('trad.tunnel-step-4.automated-title')</p>
                                            <p class="paiement__choice_desc">@lang('trad.tunnel-step-4.automated-desc')</p>
                                        </div>
                                        <div class="d-flex flex-column flex-grow-1 step4__price__mobile">
                                            <p class="paiement__choice_nopad">
                                                <span class="paiement__choice_price">
                                                    {{formatPrices($offer->getPriceOffer($prices) / 100)}}@lang('meta.devise')
                                                    +
                                                    {{formatPrices($offer->getFirstMonthPrice($prices) / 100)}}@lang('meta.devise')
                                                </span>
                                                <br/>
                                                @lang('trad.common.then') <span class="paiement__choice_price">{{$prices['price'] / 100}}@lang('meta.devise')</span>/@lang('trad.common.month')
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step4__paiement__choice  step4__paiement__choice__second js-switch-state js-ajax-loaded"
                                 data-addclass="paiement__choice__rounded--active">
                                <div class="d-flex align-items-center">
                                    <div class="btn-offer-round-tiny">
                                        <input type="radio" name="mode" value="unique" id="unique"/>
                                        <label for="unique"></label>
                                    </div>
                                    <div class="d-flex align-items-center step4__paiement__choice--columnMobile">
                                        <div class="d-flex flex-column paiement__choice__text">
                                            <p class="paiement__choice_title">@lang('trad.tunnel-step-4.unique-title')</p>
                                            <p class="paiement__choice_desc">@lang('trad.tunnel-step-4.unique-desc')
                                            </p>
                                        </div>
                                        <div class="d-flex flex-column flex-grow-1 step4__price__mobile">
                                            <p class="paiement__choice_nopad">
                                                <span class="paiement__choice_price">
                                                    {{formatPrices($order->getUniqueTotalPriceOrder($prices) / 100)}}@lang('meta.devise')
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex mt-md-4 mt-3 in_checkbox__container">
                                <div class="block-sample mt-md-0 mt-3">
                                    <div class="in_checkbox">
                                        <input class="in_checkbox__input--2"
                                               type="checkbox"
                                               id="initial"
                                               name="terms">
                                        <label class="in_checkbox__label--2" for="initial">
                                            @lang('trad.tunnel-step-4.read')
                                            <a href="{{route('page.retrieve', trans('routes.page.cgv'))}}">
                                                @lang('trad.tunnel-step-4.general-condition')
                                            </a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="step4__action d-flex align-items-center justify-content-between">
                            <div class="tunnel__lnk__back">
                                <a href="{{route('subscription.step3', ['subscriptionUrl' => $club->subscription->url, 'clubId' => $club->id, 'offerId' => $offer->id ])}}"
                                   class="lnk lnk--blue"> < Modifier mes coordonnées</a>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 align-items-end ">
                                <button style="width: 100%;"
                                        class="btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn">
                                    @lang('trad.tunnel-step-4.validate')
                                </button>
                                <small>@lang('trad.tunnel-step-4.validate-condition')</small>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="col-xl-4 col-md-12">
                        @include('front.tunnel.components.recap')
                        <div class="step4__action step4__action__responsive d-flex align-items-center justify-content-between mt-5">
                            <div class="tunnel__lnk__back">
                                <a href="{{route('subscription.step3', ['subscriptionUrl' => $club->subscription->url, 'clubId' => $club->id, 'offerId' => $offer->id ])}}"
                                   class="lnk lnk--blue"> < Modifier mes coordonnées
                                </a>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 align-items-end max-width-flex">
                                <button type="submit"
                                        class="btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn">
                                    Valider mon inscription
                                </button>
                                <small>Avec obligation de paiement</small>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            @include('front.tunnel.components.legal-mentions')
        </div>
    @include('front.tunnel.components.tunnel_modal')
    @include('front.tunnel.components.footer')
    @include('partials.tag-commander-event')
@endsection
