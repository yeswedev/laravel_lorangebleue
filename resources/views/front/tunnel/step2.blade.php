@extends('layouts.front')

@section('title')
    Tunnel d'abonnement - étape 2
@endsection
@section('description')
    Tunnel d'abonnement - étape 2
@endsection

@section('content')

    @include('front.tunnel.components.header')
    @include('partials.errors')
    @include('partials.success')

    <div class="container-full-w container__tunnel tunnel__step2">
        <div class="container__tunnel__header">
            <div class="container container-970">
                <h1 class="text--t-center text--uppercase h1-abo">@lang('trad.tunnel-step-2.title')</h1>
            </div>
        </div>
        <div class="tunnel__content">
            <div class="tunnel__content__mobile">
                <div class="container container-1170 container__tunnel__content">
                    <div class="tunnel__step d-flex container--shadow">
                        <a class="tunnel__step__lnk" href="{{route('subscription.step1', 'mon-coach-fitness')}}">
                            <div class="tunnel__step__item--validate tunnel__step__item d-flex justify-content-center">
                                <p class="align-self-center">1 <span
                                            class="item--hide-mobile"> - @lang('trad.tunnel-header.my-club')</span></p>
                            </div>
                        </a>
                        <div class="tunnel__step__item tunnel__step__item--active d-flex justify-content-center">
                            <p class="align-self-center">2 <span
                                        class="item--hide-mobile"> - @lang('trad.tunnel-header.my-formula')</span></p>
                        </div>
                        <div class="tunnel__step__item d-flex justify-content-center">
                            <p class="align-self-center">3 <span
                                        class="item--hide-mobile">- @lang('trad.tunnel-header.my-contact')</span></p>
                        </div>
                        <div class="tunnel__step__item d-flex justify-content-center">
                            <p class="align-self-center">4 <span
                                        class="item--hide-mobile">- @lang('trad.tunnel-header.payment')</span></p>
                        </div>
                    </div>
                </div>
                {!!Form::open(array('action' => array('Front\SubscriptionController@step3', $subscription->url), 'method' => 'GET')) !!}
                @isset($order)
                    <input type="hidden" name="token" value="{{$order->order_token}}">
                @endisset
                <div class="container-full-w container--bg-beige tunnel__step2___content">
                    <div class="container container-1170">
                        <p class="step2__description">@lang('trad.tunnel-step-2.subtitle')</p>
                        {!! $errors->first('radio', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}

                        @include('front.components.offer', ['offers' => $club->activeOffers(), 'club' => $club, 'subscriptionUrl' => null, 'order' => null])

                        <div class="step2__action d-flex align-items-center justify-content-between">
                            <div class="tunnel__lnk__back">
                                <a href="{{route('subscription.step1', $club->subscription->url)}}"
                                   class="lnk lnk--blue"> < @lang('trad.tunnel-step-2.modify')</a>
                            </div>
                            <div class="result__card__footer text--t-center">
                                <button type="submit" style="width: 100%;"
                                        class="btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn">
                                    @lang('trad.tunnel-step-2.validate')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            @include('front.tunnel.components.legal-mentions')
        </div>
    </div>
    @include('front.tunnel.components.tunnel_modal')
    @include('front.tunnel.components.footer')
    @include('partials.tag-commander-event')
@endsection
