@extends('layouts.front')

@section('title')
    {{$step1Title}}
@endsection
@section('description')
    {{$step1MetaDescription}}
@endsection

@section('content')
    @include('front.tunnel.components.header')
    @include('partials.errors')
    @include('partials.success')

    @isset($message)
        <div class="alert alert-danger"> {{ $message }}</div>
    @endisset

    <div class="container-full-w container__tunnel">
        <div class="container__tunnel__header">
            <div class="container container-970">
                <h1 class="text--t-center text--uppercase h1-abo">
                    {{$step1H1}}
                </h1>
            </div>
        </div>
        <div class="tunnel__content">
            <div class="tunnel__content__mobile">
                <div class="container-1170 container__tunnel__content">
                    <div class="tunnel__step d-flex container--shadow">
                        <div class="tunnel__step__item tunnel__step__item--active d-flex justify-content-center">
                            <p class="align-self-center">
                                1 <span class="item--hide-mobile"> - @lang('trad.tunnel-header.my-club')</span>
                            </p>
                        </div>
                        <div class="tunnel__step__item d-flex justify-content-center">
                            <p class="align-self-center">
                                2 <span class="item--hide-mobile"> - @lang('trad.tunnel-header.my-formula')</span>
                            </p>
                        </div>
                        <div class="tunnel__step__item d-flex justify-content-center">
                            <p class="align-self-center">
                                3 <span class="item--hide-mobile">- @lang('trad.tunnel-header.my-contact')</span>
                            </p>
                        </div>
                        <div class="tunnel__step__item d-flex justify-content-center">
                            <p class="align-self-center">
                                4 <span class="item--hide-mobile">- @lang('trad.tunnel-header.payment')</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="container-1170 parent-popup-error">
                    <h2 class="subtitle-h2 text--t-center mb-4">@lang('trad.tunnel-step-1.subtitle')</h2>

                    {{ Form::open(array('action' => array('Front\SubscriptionController@searchTunnelStep1', $subscriptionUrl), 'method' => 'POST', 'class' => 'd-flex justify-content-center mb-4 flex-wrap')) }}
                    @include('front.clubs.components.club-form')
                    @isset($order)
                        <input type="hidden" name="token" value="{{$order->order_token}}">
                    @endisset
                    {{ Form::close() }}
                    <p class="text--t-center form__result__text mb-4">
                        @isset($club_lists)
                            @if(count($club_lists))
                                {{count($club_lists)}} {{str_plural(trans('trad.tunnel-step-1.hall'),count($club_lists))}} @lang('trad.tunnel-step-1.proximity')
                            @else
                                @lang('trad.common.no-result')
                            @endif
                        @endisset
                    </p>
                    @include('partials.popup-error')
                    <div class="search__result__tunnel">
                        <div class="row">
                            @isset($club_lists)
                                @forelse($club_lists as $club)
                                    <div class="col-xl-4 col-md-6 col-12">
                                        <div class="search-result__card">
                                            <div class="d-flex justify-content-between">
                                                @isset($club->clubtype->type)
                                                    <span class="card-logo-svg card-logo-tunnel">
                                                        <img src="{{url('/images/'.$club->clubtype->type.'-sm.svg')}}" alt="club-type"/>
                                                    </span>
                                                @else
                                                    <span class="card-logo-svg card-logo-tunnel">
                                                        @if($club->subscription_id == '1')
                                                            <img src="{{url('/images/logo-ob-mcf-fondblanc.svg')}}" alt="logo mon coach fitness"/>
                                                        @else
                                                            <img src="{{url('/images/wellness-sm.svg')}}" alt="logo mon coach wellness"/>
                                                        @endif
                                                    </span>
                                                @endisset
                                                @if(round($club->distance, 1) > 0)
                                                    <p class="search-result__distance">
                                                        <i class="fa fa-map-marker-alt" style="color: #f36c21;"></i>
                                                        {{round($club->distance, 1)}} km
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="result__card__content">
                                                <p class="search-result__club-title text--uppercase">{{$club->title}}</p>
                                                <p class="search-result__club-adress">{{$club->address}} <br/>
                                                    <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span>
                                                </p>
                                            </div>
                                            @if(count($club->offers) > 0 && $club->subscription->type = 'fitness')
                                                {!!Form::open(array('action' => array('Front\SubscriptionController@step2', $club->subscription->url), 'method' => 'GET', 'class' => 'result__card__footer text--t-center')) !!}
                                                @isset($order)
                                                    <input type="hidden" name="token" value="{{$order->order_token}}">
                                                @endisset
                                                <input type="hidden" name="clubId" value="{{$club->id}}">
                                                <button type="submit"
                                                        class="btn btn--bg-orange btn--t-white btn--margin-t-0 button-int club__footer__abo-btn js-tag-commander-abo-select-club"
                                                        @if($locale == 'es') style="padding: 11px 30px;" @endif
                                                        data-city="{{$club->city}}"
                                                        data-club="{{str_slug($club->title)}}"
                                                >
                                                    @lang('trad.tunnel-step-1.choose')
                                                </button>
                                                {!! Form::close() !!}
                                            @else
                                                <div class="result__card__footer text--t-center">
                                                    <a href="@if($club->url){{route('front.club-details', $club->url)}}@else # @endif"
                                                       data-toggle="modal"
                                                       data-target="#basicModal{{$club->id}}"
                                                       data-city="{{$club->city}}"
                                                       data-club="{{str_slug($club->title)}}"
                                                       class="btn btn--bg-white btn--t-orange btn--bd-orange btn--margin-t-0 button-int js-tag-commander-abo-pre-insc club__footer__abo-btn"
                                                    >
                                                        @lang('trad.tunnel-step-1.pre-sign-in')
                                                    </a>
                                                    <p class="result__card__conditions">
                                                        @lang('trad.tunnel-step-1.pre-sign-in-text')
                                                    </p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    @include('front.components.modal')
                                @empty
                                    <div class="col-xl-12 col-md-12 col-12">
                                        <p class="text-center">
                                            @lang('trad.common.no-result')
                                        </p>
                                    </div>
                                @endforelse
                            @endisset
                        </div>
                    </div>
                    <div class="tunnel__lnk__back">
                        <a href="/" class="lnk lnk--blue"> < @lang('trad.button.back')</a>
                    </div>
                </div>
            </div>
            @include('front.tunnel.components.legal-mentions')
        </div>
    @include('front.tunnel.components.tunnel_modal')
    @include('front.tunnel.components.footer')
    @include('partials.tag-commander-event')
@endsection
