@extends('layouts.front')

@section('title')
    Tunnel d'abonnement - étape 3
@endsection
@section('description')
    Tunnel d'abonnement - étape 3
@endsection
@section('meta')
    <meta name="robots" content="noindex">
@endsection

@section('content')
    @include('front.tunnel.components.header')
    @include('partials.errors')
    @include('partials.success')
    <div class="container-full-w container__tunnel tunnel__step3">
        <div class="container__tunnel__header">
            <div class="container container-970">
                <h1 class="text--t-center text--uppercase h1-abo">@lang('trad.tunnel-step-3.title')</h1>
            </div>
        </div>
        <div class="tunnel__content">
            <div class="tunnel__content__mobile">
                <div class="container container-1170 container__tunnel__content">
                    <div class="tunnel__step d-flex container--shadow">
                        <a class="tunnel__step__lnk" href="{{route('subscription.step1', 'mon-coach-fitness')}}">
                            <div class="tunnel__step__item--validate tunnel__step__item d-flex justify-content-center">
                                <p class="align-self-center">1 <span
                                            class="item--hide-mobile"> - @lang('trad.tunnel-header.my-club')</span></p>
                            </div>
                        </a>
                        <a href="{{route('subscription.step2', $club->subscription->url)}}" class="tunnel__step__lnk">
                            <div class="tunnel__step__item tunnel__step__item--validate d-flex justify-content-center tunnel__step__item--custom">
                                <p class="align-self-center">2 <span
                                            class="item--hide-mobile"> - @lang('trad.tunnel-header.my-formula')</span>
                                </p>
                            </div>
                        </a>
                        <div class="tunnel__step__item tunnel__step__item--active d-flex justify-content-center">
                            <p class="align-self-center tunnel__step__item__custom">3 <span
                                        class="item--hide-mobile">- @lang('trad.tunnel-header.my-contact')</span></p>
                        </div>
                        <div class="tunnel__step__item d-flex justify-content-center">
                            <p class="align-self-center">4 <span
                                        class="item--hide-mobile">- @lang('trad.tunnel-header.payment')</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container container-1170">
                <div class="pt-2 pb-5">
                    <div class="row">
                        <div class="col-xl-8 col-md-12">
                            <div class="tunnel__step3__form">
                                <p>@lang('trad.tunnel-step-3.subtitle') </p>
                                {{ Form::open(array('action' => array('Front\SubscriptionController@step4', $subscription->url), 'method' => 'GET')) }}
                                <div class="block-sample">
                                    <div class="in_checkbox">
                                        <p class="civility__label">Civilité *</p>
                                        <div class="d-flex in_checkbox__radio">
                                            {!! Form::radio('civility', 'monsieur',$order->civility == 'monsieur' ? $order->civility : null,array('class' => 'in_checkbox__input--1','id' => 'homme')); !!}
                                            <label for="homme" class="in_checkbox__label--1">
                                                <span class="in_text__validmark"></span>
                                                @lang('trad.tunnel-form.sir')
                                            </label>
                                            {!! Form::radio('civility', 'madame',$order->civility == 'madame' ? $order->civility : null,array('class' => 'in_checkbox__input--1','id' => 'femme','required' => 'required')); !!}
                                            <label for="femme" class="in_checkbox__label--1">
                                                <span class="in_text__validmark"></span>
                                                @lang('trad.tunnel-form.madam')
                                            </label>
                                        </div>
                                        {!! $errors->first('civility', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="block-sample">
                                            <div class="in_text">
                                                {!! Form::text('lastname',$order ? $order->lastname : null,array('class' => 'in_text__input','required' => 'required')) !!}
                                                {!! Form::label('lastname', trans('trad.tunnel-form.lastname'),['class' => 'in_text__label']); !!}
                                                <span class="in_text__validmark"></span>
                                                {!! $errors->first('lastname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="block-sample mt-md-0 mt-3">
                                            <div class="in_text">
                                                {!! Form::text('firstname',$order ? $order->firstname : null,array('class' => 'in_text__input','required' => 'required')) !!}
                                                {!! Form::label('firstname', trans('trad.tunnel-form.firstname'),['class' => 'in_text__label']); !!}
                                                <span class="in_text__validmark"></span>
                                                {!! $errors->first('firstname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-md-4 mt-3">
                                    <div class="row">
                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="block-sample">
                                                <div class="in_text in_popover js-date-parent">
                                                    {!! Form::text('birthdate',$order->birthdate ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$order->birthdate)->format('d/m/Y') : null,array('class' => 'birthdate-picker in_text__input js-date','required' => 'required','autocomplete'=>"off")) !!}
                                                    {!! Form::label('birthdate', trans('trad.tunnel-form.birthdate'),['class' => 'in_text__label d-flex justify-content-between']); !!}
                                                    <span class="in_popover__span">
                                                        <img src="{{url('/images/calendar.png')}}" alt="picto calendrier"/>
                                                    </span>
                                                    <span class="in_popover__error">
                                                        <img src="{{url('/images/Erreur.png')}}" alt="picto erreur"/>
                                                    </span>
                                                    <span class="in_text__validmark"></span>
                                                    <span class="in_text_error">Veuillez entrer une date de naissance valide.</span>
                                                    {!! $errors->first('birthdate', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-md-4 mt-3">
                                    <div class="row">
                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="block-sample">
                                                <div class="in_text in_popover">
                                                    {!! Form::text('email',$order ? $order->email : null,array('class' => 'in_text__input js-email','required' => 'required')) !!}
                                                    {!! Form::label('email', trans('trad.tunnel-form.email'),['class' => 'in_text__label']); !!}
                                                    <span class="in_popover__span css-popover-container">
                                                        <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                                        <span class="css-popover">@lang('trad.modal-contact.popover-email')</span>
                                                    </span>
                                                    <span class="in_popover__error"><img
                                                                src="{{url('/images/Erreur.png')}}" alt="picto erreur"/></span>
                                                    <span class="in_text__validmark"></span>
                                                    <span class="in_text_error">@lang('trad.tunnel-form-error.email')</span>
                                                    {!! $errors->first('email', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="block-sample mt-md-0 mt-3">
                                                <div class="in_text in_popover">
                                                    {!! Form::tel('phone_number',$order ? $order->phone_number : null,array('class' => 'in_text__input','required' => 'required')) !!}
                                                    {!! Form::label('phone_number', trans('trad.tunnel-form.phone'),['class' => 'in_text__label d-flex justify-content-between']); !!}
                                                    <span class="in_popover__span css-popover-container">
                                                        <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                                        <span class="css-popover">@lang('trad.modal-contact.popover-tel')</span>
                                                    </span>
                                                    <span class="in_popover__error"><img
                                                                src="{{url('/images/Erreur.png')}}" alt="picto erreur"/></span>
                                                    <span class="in_text__validmark"></span>
                                                    <span class="in_text_error">@lang('trad.tunnel-form-error.phone')</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-md-4 mt-3">
                                    <div class="row">
                                        <div class="col-xl-10 col-12">
                                            <div class="block-sample block-sample--max-width">
                                                <div class="in_text">
                                                    {!! Form::text('address',$order ? $order->address : null,array('class' => 'in_text__input','required' => 'required')) !!}
                                                    {!! Form::label('address', trans('trad.tunnel-form.address'),['class' => 'in_text__label']); !!}
                                                    <span class="in_text__validmark"></span>
                                                    {!! $errors->first('address', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-md-4 mt-3 ">
                                    <div class="row">
                                        <div class="col-xl-6 col-md-6 col-12">
                                            <div class="block-sample">
                                                <div class="in_text">
                                                    {!! Form::text('alt_address',$order ? $order->alt_address : null,array('class' => 'in_text__input is-empty')) !!}
                                                    {!! Form::label('alt_address', trans('trad.tunnel-form.alt-address'),['class' => 'in_text__label']); !!}
                                                    <span class="in_text__validmark"></span>
                                                    {!! $errors->first('alt_address', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-md-4 mt-3 mt-s">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="block-sample">
                                                    <div class="in_text">
                                                        {!! Form::text('zip_code',$order ? $order->zip_code : null,array('class' => 'in_text__input js-postal-code','required' => 'required')) !!}
                                                        {!! Form::label('zip_code', trans('trad.tunnel-form.zip'),['class' => 'in_text__label']); !!}
                                                        <span class="in_text__validmark"></span>
                                                        <span class="in_popover__error"><img
                                                                    src="{{url('/images/Erreur.png')}}"
                                                                    alt="picto erreur"/></span>
                                                        <span class="in_text_error">@lang('trad.tunnel-form-error.zip')</span>
                                                        {!! $errors->first('zip_code', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="block-sample mt-md-0 mt-3">
                                                    <div class="in_text">
                                                        {!! Form::text('city',$order ? $order->city : null,array('class' => 'in_text__input','required' => 'required')) !!}
                                                        {!! Form::label('city', trans('trad.tunnel-form.city'),['class' => 'in_text__label']); !!}
                                                        <span class="in_text__validmark"></span>
                                                        {!! $errors->first('city', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mt-md-4 mt-3">
                                    <div class="block-sample mt-md-0 mt-3">
                                        <div class="in_checkbox">
                                            {!! Form::checkbox('news', 'news', false, array('class' => 'in_checkbox__input--2','id' => 'initial')); !!}
                                            <label class="in_checkbox__label--2" for="initial">
                                                @lang('trad.tunnel-form.newsletter')
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <p class="step3__form__mention">@lang('trad.tunnel-form.mandatory')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-12">
                            @include('front.tunnel.components.recap')
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8 col-md-12">
                        <div class="step2__action d-flex align-items-center justify-content-between">
                            <div class="tunnel__lnk__back">
                                <a href="{{route('subscription.step2', $club->subscription->url)}}"
                                   class="lnk lnk--blue"> < @lang('trad.tunnel-step-3.back')</a>
                            </div>
                            <button
                                    type="submit" style="padding: 11px 20px;"
                                    class="btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn">
                                @lang('trad.tunnel-step-3.validate')
                            </button>
                        </div>
                    </div>
                </div>
                @isset($order)
                    <input type="hidden" name="token" value="{{$order->order_token}}">
                @endisset
                {!! Form::close() !!}
            </div>
            @include('front.tunnel.components.legal-mentions')
        </div>
    </div>
    @include('front.tunnel.components.tunnel_modal')
    @include('front.tunnel.components.footer')
    @include('partials.tag-commander-event')
@endsection
