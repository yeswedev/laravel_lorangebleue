@if ( $modal )
    <div class="modal fade" id="tunnel_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content tunnel-modal_content">
                <div class="modal-body tunnel-modal_body">
                    <button class="tunnel-modal__closeBtn" data-dismiss="modal" aria-label="Close">
                        <span></span>
                    </button>
                    <h2 class="club__title h2 text--t-center title--underlined-center--blue text-uppercase">{{ $modal->title }}</h2>

                    <div class="container--padding-t-50">
                        <p class="text-center">{{ $modal->text }}</p>
                    </div>

                    {!! Form::open([
                    'route' => 'contact.mailFromModalTunnelAbo',
                    'method' => 'post',
                    'class' => 'tunnel-modal_form',
                    'onsubmit' => 'commanderFormTunnelModal(event)'
                    ]) !!}
                    <div class="container">
                        <div class="row">
                            @if ( isset( $clubs ) )
                                {!! Form::select('club_id', $clubs, null, ['class' => 'form-control with-arrow', 'placeholder' => 'Choisissez un club']) !!}
                            @elseif ( isset( $club ) )
                                {!! Form::select('club_id', [ $club->id => $club->getH1() ] , $club->id, ['class' => 'form-control with-arrow']) !!}
                            @endif
                        </div>
                        <div class="row justify-content-between align-items-center">
                            {!! Form::text('lastname', null, ['class' => 'form-control w-49', 'placeholder' => 'Nom']) !!}
                            {!! Form::text('firstname', null, ['class' => 'form-control w-49', 'placeholder' => 'Prénom']) !!}
                        </div>
                        <div class="row justify-content-between align-items-center">
                            {!! Form::text('email', null, ['class' => 'form-control w-49', 'placeholder' => 'Email']) !!}
                            {!! Form::text('phone', null, ['class' => 'form-control w-49', 'placeholder' => 'Numéro de téléphone']) !!}
                        </div>
                        <div class="row justify-content-center align-items-center mt-5">
                            <button class="btn btn--bg-orange btn--t-white contact-btn" type="submit">Envoyer</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <script>
        window.modal = {
            @if( $modal->inactivity_time )
                inactivity_time : {{ $modal->inactivity_time }}
            @else
                inactivity_time : null
            @endif
        }
    </script>
@endif
