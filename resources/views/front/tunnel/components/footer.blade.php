<footer>
    @include('front.components.picto')
    <div class="container-tunnel__footer">
        <div class="container container-1170">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <div class="tunnel__footer__picto d-flex align-items-center flex-wrap">
                    <div class="tunnel__footer__secure d-flex align-items-center">
                        <img src="{{url('/images/cadenas.png')}}" alt="cadenas"/>
                        <p class="r">@lang('trad.footer.secure-paiement')</p>
                    </div>
                    <div class="tunnel__footer__img">
                        <img src="{{url('/images/cb.png')}}" alt="cb-picto"/>
                        <img src="{{url('/images/visa.png')}}" alt="visa-picto"/>
                        <img src="{{url('/images/master_card.png')}}" alt="mastercard-picto"/>
                        <img src="{{url('/images/maestro.png')}}" alt="maestro picto"/>
                        <img src="{{url('/images/diners.png')}}" alt="diners-picto"/>
                    </div>
                </div>
                <div class="tunnel__footer__lnk">
                    <ul>
                        <li>
                            <a href="{{route('page.retrieve', trans('routes.page.legal-mentions'))}}" >
                                @lang('trad.footer.legal-mention')
                            </a>
                        </li>
                        <li>-</li>
                        <li>
                            <a href="{{route('page.retrieve', trans('routes.page.cgv'))}}">
                                @lang('trad.footer.cgv')
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
