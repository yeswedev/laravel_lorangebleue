<header class="tunnel__header">
    <div class="bottom__header bottom__header-scroll-mobile">
        <div class="container-1170 container--flex container--flex-between container--padding-tb-15 bottom__header-container bottom__header-container-scroll-mobile">
            <a href="/" class="logo js-logo">
                <span class="logo__desktop" data-img-desktop="{{url('/images')}}">
                    <img src="{{url('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo Orange Bleue">
                </span>
                <span class="logo__mobile" data-img-mobile="{{url('/images')}}">
                    <img src="{{url('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo Orange Bleue">
                </span>
            </a>
        </div>
    </div>
</header>
