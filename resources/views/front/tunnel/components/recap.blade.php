<div class="tunnel__step3__recap">
    <div class="js-toggle-dropdown d-flex justify-content-between align-items-center">
        <h2 class="step3__recap__title text--uppercase">@lang('trad.tunnel-step-3.summary')</h2>
        <img class="step3__recap__img" src="{{url('/images/dropdown.png')}}"
             alt="dropdown-img"/>
    </div>
    <div class="js-toggle-dropdown--item">
        <div class="step3__recap__club">
            <h3 class="recap__club__title">@lang('trad.tunnel-step-3.my-club')</h3>
                <span class="card-logo-svg card-logo-tunnel">
                    <img class="mb-3" src="{{url('/images/'.'ob-'.$club->subscription->type.'.svg')}}"
                        width="100%" height="100%"
                        alt="picto-{{$club->subscription->type}}"/>
                </span>
            <p class="recap__club__name text--uppercase">{{$club->title}}</p>
            <p class="recap__club___adress">{{$club->address}}<br/>
                <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span></p>
        </div>
        <div class="step3__recap__formule">
            <h3>@lang('trad.tunnel-step-2.title')</h3>
            <p class="recap__formule__title recap__formule__title-mb-20 text--uppercase">
                {{$offer->title}}
                <br>
                @if(Session::get('order')->is_student == 1)
                    <span class="recap__formule__span">« Sous réserve d’un justificatif à jour »</span>
                @endif
            </p>
        </div>
        <div class="step3__recap__totalMonth">
            <h3>@lang('trad.tunnel-step-3.total-per-month')</h3>
            @if(intval($prices['reduced_price']) > 0)
                <div class="d-flex justify-content-between">
                    <p class="recap__totalMonth__detail">@lang('trad.common.of')
                        <strong>{{Carbon\Carbon::today()->format('d/m/Y')}} </strong> @lang('trad.common.the')
                        <strong>{{Carbon\Carbon::today()->addMonths($prices['reduced_commitment'])->addDay(-1)->format('d/m/Y')}} </strong>
                    </p>
                    <p class="recap__totalMonth__price"> {{formatPrices($prices['reduced_price'] / 100)}}
                        @lang('meta.devise')</p>
                </div>
            @endif
            <div class="d-flex justify-content-between">
                <p class="recap__totalMonth__detail">@lang('trad.common.of')
                    <strong>{{Carbon\Carbon::today()->addMonths($prices['reduced_commitment'])->format('d/m/Y')}} </strong> @lang('trad.common.the')
                    <strong>{{Carbon\Carbon::today()->addMonths($prices['commitment'])->addDay(-1)->format('d/m/Y')}} </strong>
                </p>
                <p class="recap__totalMonth__price"> {{formatPrices($prices['price'] / 100)}}
                    @lang('meta.devise')</p>
            </div>
        </div>
        <div class="step3__recap__totalInscription  js-state-total"
             data-order-id="{{$order->id}}" data-app-url="{{config('app.url')}}">
            <h3>@lang('trad.tunnel-step-3.total-to-pay')</h3>

            <div class="d-flex justify-content-between">
                <p class="recap__totalInscription__pack">@lang('trad.tunnel-step-3.pack')</p>
                <p class="recap__totalInscription__price">

                    @if($club->price_offer != $prices['price_offer'])
                    <span class="recap__totalInscription__price--promo">{{formatPrices($prices['price_offer'] / 100)}}@lang('meta.devise')</span>
                    {{formatPrices($prices['reduced_price_offer'] / 100)}}@lang('meta.devise')
                    @else
                    {{formatPrices($club->price_offer / 100)}}@lang('meta.devise')
                    @endif
                </p>
            </div>
            @if(intval($prices['reduced_price']) > 0)
                <div class="d-flex justify-content-between">
                    <p class="recap__totalInscription__pack">@lang('trad.tunnel-step-3.first-month')</p>
                    <p class="recap__totalInscription__price">
                        @if($prices['reduced_price'] != $prices['price'])
                        <span class="recap__totalInscription__price--promo">{{formatPrices($prices['price'] / 100)}}@lang('meta.devise')</span>
                        {{formatPrices($prices['reduced_price'] / 100)}}@lang('meta.devise')
                        @else
                        {{formatPrices($offer->price / 100)}}@lang('meta.devise')
                        @endif
                    </p>
                </div>
                @else
                <div class="d-flex justify-content-between">
                    <p class="recap__totalInscription__pack">@lang('trad.tunnel-step-3.first-month')</p>
                    <p class="recap__totalInscription__price">
                    <p class="recap__totalInscription__price"><strong>{{formatPrices($prices['price'] / 100)}}
                            @lang('meta.devise')</strong>
                    </p>
                </div>
            @endif
        </div>
        <div class="step3__recap__total d-flex justify-content-between align-items-center">
            <h2 class="recap__total__title text--uppercase">@lang('trad.tunnel-step-3.total')</h2>
            <p class="recap__total__price" id="totalPrice">{{formatPrices($order->getTotalPriceOrder()/100)}}
                @lang('meta.devise')</p>
        </div>
    </div>
</div>
