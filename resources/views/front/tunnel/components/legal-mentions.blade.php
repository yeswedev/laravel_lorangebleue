<p class="mt-5">
    @lang('trad.tunnel-footer.rgpd') 
    <a href="{{route('page.retrieve', trans('routes.page.rgpd'))}}" target="_blank">
        @lang('trad.footer.rgpd')
    </a>.
</p>