<div class="cms-container open-club">
    <div class="cms-row">
        <div class="cms-col-12">
            {{ Form::open(array('action' => array('Front\PageController@contact'), 'method' => 'POST', 'files' => true, 'onsubmit' => 'commanderFormOpenclub(event)', 'class' => 'open-club__form')) }}
            <div class="modal-content">
                <div class="modal-header">
                    @if(session()->has('message-open-club'))
                        <div style="width: 80%;margin: 0 auto;" class="mt-4">
                            <div class="alert alert-success">
                                {{ session()->get('message-open-club') }}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <h2 class="modal-title h2 modal__content__title text--t-center text-uppercase mb-3"
                        id="myModalLabel">
                        @lang('trad.contact-open-club.title')
                    </h2>
                    <div class="mt-modal">
                        <div class="row">
                            <div class="col-xl-6 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="in_text">
                                            {!! Form::text('name', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                            <label class="in_text__label" for="name">@lang('trad.modal-contact.lastname-label')</label>
                                            <span class="in_text__validmark"></span>
                                            {!! $errors->first('name', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="in_text">
                                            {!! Form::text('firstname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                            <label class="in_text__label" for="firstname">@lang('trad.modal-contact.firstname-label')</label>
                                            <span class="in_text__validmark"></span>
                                            {!! $errors->first('firstname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-modal">
                        <div class="row">
                            <div class="col-xl-6 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="in_text in_popover">
                                            {!! Form::email('email', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                                            <label class="in_text__label" for="email">@lang('trad.modal-contact.email-label')</label>
                                            {!! $errors->first('email', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="in_text in_popover">
                                            {!! Form::text('phone', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                                            <label class="in_text__label d-flex justify-content-between" for="phone">@lang('trad.modal-contact.tel-label')</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-modal">
                        <div class="row">
                            <div class="col-xl-6 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="in_text">
                                            {!! Form::text('zip_code', null, array('class' => 'in_text__input js-postal-code','required' => 'required')) !!}
                                            <label class="in_text__label" for="zip_code">@lang('trad.contact-open-club.zip')</label>
                                            <span class="in_text__validmark"></span>
                                            {!! $errors->first('zip_code', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="in_text">
                                            {!! Form::text('city', null, array('class' => 'in_text__input','required' => 'required')) !!}
                                            <label class="in_text__label" for="city">@lang('trad.contact-open-club.city')</label>
                                            <span class="in_text__validmark"></span>
                                            {!! $errors->first('city', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-modal">
                        <div class="row">
                            <div class="col-12">
                                <div class="blocks">
                                    <div class="in_textarea">
                                        {!! Form::textarea('message', null, array('class' => 'in_text__input in_textarea__input', 'rows' => 5, 'cols' => 54, 'required' => 'required')) !!}
                                        <label class="in_text__label" for="message-modal">@lang('trad.contact-open-club.message')</label>
                                        <span class="in_text__validmark"></span>
                                        {!! $errors->first('message', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-modal">
                        @isset($pdf)
                            <div class="d-flex mt-2 lnk-flex">
                                <div class="block-sample mt-md-0">
                                    <div>
                                        <a href="{{$pdf}}"
                                            class="lnk open-club__lnk"
                                            target="_blank">
                                            @lang('trad.contact-open-club.pdf')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="row">
                            <div class="col-xl-12 col-md-12">
                                <div class="blocks">
                                    <div class="block-sample">
                                        <div class="content-button">
                                            <div class="file-upload">
                                                <div class="file-select">
                                                    <div class="button-file-select">
                                                        <div class="col-xl-8 col-md-12 in_text__input col-spe">
                                                            <div class="in_text__label" id="noFile">@lang('trad.contact-open-club.noFileChoosen')</div>
                                                            <div class="over-label">@lang('trad.contact-open-club.inscription')</div>
                                                        </div>
                                                        <div class="col-xl-4 col-md-12">
                                                            <div class="btn btn--bg-white btn--t-orange btn--bd-orange open-clu__btn" id="fileName">@lang('trad.contact-open-club.choose')</div>
                                                        </div>
                                                    </div>
                                                    {{ Form::file('file', array('class'=>'custom-file-input', 'id' => 'chooseFile')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex mt-4">
                        <div class="block-sample mt-md-0">
                            <div class="modal_in_checkbox">
                                {!! Form::checkbox('contact', 'contact', false,array('class' => 'in_checkbox__input--2','id' => 'contact', 'required' => 'required')); !!}
                                <label class="in_checkbox__label--2" for="contact">
                                    @lang('trad.contact-open-club.send')
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 mt-modal">
                        <div class="modal_row modal_row--center">
                            <div class="step2__action d-flex align-items-center justify-content-between">
                                <button
                                        type="submit"
                                        class="btn btn--bg-orange btn--t-white btn--margin-t-0 button-int modal_btn">
                                    @lang('trad.contact-open-club.validate')
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4 mt-4 mt-modal">
                        <div class="modal_row">
                            <p class="modal_text">@lang('trad.modal-contact.condition')</p>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('subject', config('contact.open_club.label-'.$locale), array()) !!}
            @if($pageUrl == trans('routes.page.ouvrir-salle-sport-accompagnement'))
                {!! Form::hidden('page_url', trans('routes.page.ouvrir-salle-sport-accompagnement'), array()) !!}
            @else
                {!! Form::hidden('page_url', trans('routes.page.ouvrir-salle-sport'), array()) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
</div>
