@extends('layouts.front')

@section('title')
    @lang('meta.title.contact')
@endsection
@section('description')
    @lang('meta.description.contact')
@endsection

@section('content')
    @include('front.header')

    @section('recaptcha-scripts')
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    @endsection

    <div class="container-full-w">
        <div class="list-header--content">
            <h1 class="h1 title--overlay--centered text--t-center text-uppercase mb-4" id="myModalLabel">
               @lang('trad.contact.title')
            </h1>
        </div>
        <div class="container-1170 container--padding-t-50 container--padding-b-80">
            {{ Form::open(array('url' => route('contact.mail'), 'method' => 'post', 'onsubmit' => 'commanderFormContactPage(event)')) }}
            <div class="container">
                <div class="content">
                    <h2 class="content__text"> @lang('trad.contact.subtitle')</h2>
                </div>
                <div class="container">
                    <div class="row">
                        @include('partials.success')
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 contact_field">
                            {{ Form::text('name', null, ['class' => 'contact_field__input in_text__input', 'required' => 'required']) }}
                            <label class="in_text__label" for="name">@lang('trad.contact.name-label')</label>
                            {!! $errors->first('name', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                        </div>
                        <div class="col-md-6 col-sm-12 contact_field">
                            {{ Form::text('firstname', null, ['class' => 'contact_field__input in_text__input', 'required' => 'required']) }}
                            <label class="in_text__label" for="firstname">@lang('trad.contact.firstname-label')</label>
                            {!! $errors->first('firstname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 contact_field">
                            {{ Form::text('email', null, ['class' => 'contact_field__input in_text__input', 'required' => 'required']) }}
                            <label class="in_text__label" for="email">@lang('trad.contact.email-label')</label>
                            {!! $errors->first('email', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                        </div>
                        <div class="col-md-6 col-sm-12 contact_field">
                            {{ Form::text('phone', null, ['class' => 'contact_field__input in_text__input', 'required' => 'required']) }}
                            <label class="in_text__label" for="phone">@lang('trad.contact.phone-label')</label>
                            {!! $errors->first('phone', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                        </div>
                    </div>
                </div>
                @php($locale = LaravelLocalization::getCurrentLocale())
                <div class="container">
                    <div class="row">
                        <div class="col-12 contact_field">
                            <select name="subject" class="contact_field__input--lg">
                                <option selected disabled hidden>@lang('trad.contact.select-label')</option>
                                <option value="open_club" @if(Request::query('subject') == 'open_club') selected="selected" @endif>{{ config('contact.open_club.label-'.$locale) }}</option>
                                <option value="become_coach">{{ config('contact.become_coach.label-'.$locale) }}</option>
                                <option value="rgpd_partners">{{ config('contact.rgpd_partners.label-'.$locale) }}</option>
                                <option value="rgpd_employees">{{ config('contact.rgpd_employees.label-'.$locale) }}</option>
                                <option value="else">{{ config('contact.else.label-'.$locale) }}</option>
                            </select>
                            {!! $errors->first('subject', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12 contact_field">
                            {{ Form::textarea('message', null, ['class' => 'contact_field__input--lg in_text__input', 'required' => 'required']) }}
                            <label class="in_text__label" for="message">@lang('trad.contact.message-label')</label>
                            {!! $errors->first('message', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="modal_in_checkbox">
                            {{ Form::checkbox('check', 'true', false, ['class' => 'in_checkbox__input--2', 'id' => "check", 'required' => 'required']) }}
                            <label class="in_checkbox__label--2" for="check">
                                @lang('trad.modal-pre-sign-in.send')
                            </label>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center align-items-center g-recaptcha" data-sitekey="{{ config("app.recaptcha.key") }}"></div>
                {!! $errors->first('g-recaptcha-response', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}

                {!! $errors->first('check', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                <button type='submit' class="btn btn--bg-orange btn--t-white contact-btn">
                    @lang('trad.contact.form-send')</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('partials.footer')
@endsection
