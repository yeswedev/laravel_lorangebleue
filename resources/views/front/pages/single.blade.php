@extends('layouts.front')

@section('title')
    @lang('meta.title.'.$page->url)
@endsection

@section('description')
    @lang('meta.description.'.$page->url)
@endsection

@section('content')
    @include('front.header')

    @isset($open_club)
        @include('pages.single', ['open_club' => $open_club])
    @else
        @include('pages.single')
    @endisset

    @isset($app_page_url)
        @if(Request::segment(2) == $app_page_url)
            <div class="container-full-w container  container-concept-cms">
                <div class="container-concept">
                    <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase mb-5">@lang('trad.homepage.concept_title')</h2>

                    <div class="presentation__text">
                        <p class="text--t-center">@lang('trad.homepage.concept_subtitle')</p>
                    </div>
                    @include('front.components.concept')
                </div>
                <div class="cms-picto container container-1170 no-gutters">
                    <div class="row flex-wrap">
                        <div class="col-xl-3 col-md-6 col-12 text--t-center">
                            <img src="{{url('/images/picto1cms.png')}}" alt="picto calendrier"/>
                            <p>@lang('trad.page-picto.first-item')</p>
                        </div>
                        <div class="col-xl-3 col-md-6 col-12 text--t-center">
                            <img src="{{url('/images/picto2cms.png')}}" alt="picto certificat"/>
                            <p>@lang('trad.page-picto.second-item')</p>
                        </div>
                        <div class="col-xl-3 col-md-6 col-12 text--t-center">
                            <img src="{{url('/images/picto3cms.png')}}" alt="picto temps"/>
                            <p>@lang('trad.page-picto.third-item')</p>
                        </div>
                        <div class="col-xl-3 col-md-6 col-12 text--t-center">
                            <img src="{{url('/images/picto4cms.png')}}" alt="picto sportifs"/>
                            <p>@lang('trad.page-picto.fourth-item')</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endisset
    {{ Breadcrumbs::render('page-cms',$page) }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'page-cms', $page) }}
    @include('partials.footer')
@endsection
