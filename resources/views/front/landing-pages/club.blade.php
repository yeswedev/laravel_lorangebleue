@extends('layouts.front')

@section('title')
    {{ $club->title }}
@endsection

@section('content')
    @include('front.header')
    <div class="landing-page__header" @desktop id="page-top" @enddesktop>
        <div class="landing-page__colored-header">
            <div class="container-1170">
                <div class="landing-page__header__inside-text">
                    <h1 class="main-header__title h1 text-uppercase js-changing_color js-text_changing"
                        @if ($club->subscription->type != 'fitness')
                            style="color: #44c8f5;"
                        @endif
                    >
                        @if( $landingPage->h1 )
                            {!! $landingPage->h1 !!}
                        @else
                            {{ __('trad.landing-page.header.title', ['type' => $club->subscription->type]) }} {{ $club->city }}
                        @endif
                    </h1>
                    <div class="main-header__description --bold js-text_changing">
                        @if( $landingPage->subtitle )
                            {!! $landingPage->subtitle !!}
                        @else
                            {{ __('trad.landing-page.header.subtitle', ['type' => $club->subscription->type]) }}
                        @endif
                    </div>
                    <div  @handheld id="page-top" @endhandheld class="main-header__description js-abo-details @if ($club->subscription->type != 'fitness') hidden @endif">
                        @if( $landingPage->price )
                            {!! $landingPage->price !!}
                        @else
                            @lang('trad.landing-page.header.price')
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="landing-page__form">
            <p class="landing-page__form__title h2">@lang("trad.landing-page.form.title")</p>
            {!! Form::open(['route' => ['landing-page.club-contact', $landingPage->id],
            'method' => 'post',
            'data-city' => $club->city,
            'onsubmit' => 'commanderFormLandingPage(event)']) !!}
            <div class="row landing-page__form-row">
                <div class="in_text">
                    {!! Form::text('lastname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                    <label class="in_text__label" for="lastname">@lang('trad.modal-contact.lastname-label')</label>
                    <span class="in_text__validmark"></span>
                    {!! $errors->first('lastname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                </div>
                <div class="in_text">
                    {!! Form::text('firstname', null, array('class' => 'in_text__input','required' => 'required')) !!}
                    <label class="in_text__label" for="firstname">@lang('trad.modal-contact.firstname-label')</label>
                    <span class="in_text__validmark"></span>
                    {!! $errors->first('firstname', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                </div>
            </div>
            <div class="row landing-page__form-row">
                <div class="in_text in_popover">
                    {!! Form::email('email', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                    <label class="in_text__label" for="email">@lang('trad.modal-contact.email-label')</label>
                    <span class="in_popover__span css-popover-container">
                                            <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                            <span class="css-popover">@lang('trad.modal-contact.popover-email')</span>
                                        </span>
                    {!! $errors->first('email', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                </div>
                <div class="in_text in_popover">
                    {!! Form::text('phone_number', null, array('class' => 'in_text__input padding-pastille','required' => 'required')) !!}
                    <label class="in_text__label" for="phone_number">@lang('trad.modal-contact.tel-label')</label>
                    <span class="in_popover__span css-popover-container">
                                            <img src="{{url('/images/infopicto.png')}}" alt="picto information"/>
                                            <span class="css-popover">@lang('trad.modal-contact.popover-tel')</span>
                                        </span>
                    {!! $errors->first('phone_number', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                </div>
            </div>
            <div class="row landing-page__form-row">
                <div class="landing-page__form__in_text">
                    <label class="landing-page__form__in_text__label" for="clubs">@lang("trad.landing-page.form.select-label")</label>
                    {!! Form::select('club', $clubs, $club->id,['class' => 'form-control', 'data-lp' => $landingPage->id]) !!}
                    <i class="fas fa-chevron-down landing-page__form__chevron-select"></i>
                    {!! $errors->first('phone_number', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                </div>
            </div>
            <div class="mt-4 d-flex align-items-center justify-content-center">
                <div class="three-quaters">
                    <button type="submit" class="js-tag-commander-mail btn btn--bg-orange btn--t-white btn--margin-t-0 button-int modal_btn js-changing_background"
                            data-city="{{ $club->city }}"
                            data-club="{{ $club->city }}_ {{ $club->title }}"
                    @if ($club->subscription->type != 'fitness')
                        style="background-color: #004589;"
                    @endif> @lang('trad.landing-page.form.submit-button') </button>
                </div>
            </div>
            <p class="landing-page__form__mentions">@lang('trad.landing-page.form.legal-mentions')</p>
            @include('partials.success')
            {!! Form::close() !!}
        </div>
        <div class="container-1170">
            <div class="landing-page__header__outside-text subscription__description js-list-concept">
                @if ($club->subscription->type == 'fitness')
                    {!! $landingPage->concept_list_fitness !!}
                @else
                    @if ( $landingPage->concept_list_wellness != "" )
                        {!! $landingPage->concept_list_wellness !!}
                    @else
                        {!! $landingPage->concept_list_fitness !!}
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="container-1170">
        <div class="landing-page__body">
            <div class="club__infos">
                <div class="club__info club__infos__badges">
                    <p class="club__info__name js-club_name">@lang('trad.landing-page.map.title') <br>{{ $club->title }}</p>
                    <div class="club__infos__badge club__infos__freeaccess-badge">
                        @if ( $club->is_free_access )
                            <img class="js-is_free_access_badge" src="{{ url('/images/pastille-libre-acces.svg') }}" alt="Badge Accès Libre"/>
                        @endif
                    </div>
                </div>
                <div class="club__info">
                    <p class="subtitle">@lang('trad.landing-page.map.address.title')</p>
                    <p class="club__infos__text js-club_address pt-1">{{ $club->address }} <br> <span class="text--uppercase">{{ $club->zip_code }} {{ $club->city }}</span></p>
                </div>
                <div class="club__info">
                    <p class="subtitle club__info__subtitle">@lang('trad.landing-page.map.schedule.title')</p>
                    <div class="js-toggleAddress address-block--is-hover address-block pt-3">
                        <div class="d-flex flex-column">
                            <div class="club__infos__text">
                                @lang('trad.club-detail.framing-hours')
                            </div>
                            <div class="club__infos__text js-schedule">
                                <span class="js-club__infos--is-opened"></span>
                                @if( $schedule != [] )
                                    @foreach( $schedule as $key => $day )
                                        <span class="js-club__infos--opening-hour @if( $loop->first ) club__infos--opening-hour @endif">
                                            {{ $key }} - {{ $day }}
                                        </span>
                                    @endforeach
                                @else
                                    @lang('trad.club-detail.schedule')
                                @endif
                            </div>
                        </div>
                        <i class="fas fa-chevron-down"></i>
                    </div>
                </div>
            </div>
            <div
                id="map-club"
                class="lat-lng-club data-clubs map-container"
                data-app-url="{{config('app.url')}}"
                data-img-marker="{{ url('/images/markerclub.png')}}"
                data-club-single="{{$club->id}}"
                data-club-lat="{{$club->latitude}}"
                data-club-lng="{{$club->longitude}}"
            >
                <div id="map" class="full-height" data-context="club-details"></div>
            </div>
        </div>
    </div>
    <div class="cms-container landing-page-cms">
        @if( $landingPage != null && count($landingPage->blocks) > 0 )
            @forelse($landingPage->blocks as $block)
                @include('pages.blocks.'.$block->layout)
            @empty
            @endforelse
        @endif
    </div>
    <div class="container-1170 block-img">
        <img class="js-img-changing" src="{{ url('/images/landing-page_'.$club->subscription->type.'.png') }}"
             srcset="{{ url('/images/landing-page_'.$club->subscription->type.'-sm.png') }} 360w"
             alt="Coach">
        <div class="text-container">
            <h2 class="main-header__title h1 text-uppercase js-changing_color"
            @if ($club->subscription->type != 'fitness')
                style="color: #44c8f5;"
            @endif> @lang('trad.landing-page.image.title') <span class="js-text_changing">{{ $club->subscription->type }}</span>
            </h2>
            <p class="--bold">@lang('trad.landing-page.image.text')</p>
            <a href="#page-top" class="btn btn--bg-orange btn--t-white btn--margin-t-0 js-changing_background"
               @if ($club->subscription->type != 'fitness')
               style="background-color: #004589;"
               @endif
            >
                @lang('trad.landing-page.image.button-label')
            </a>
        </div>
    </div>
    @if ( $landingPage->video_link != null )
        <div class="container-1170">
            <div class="cms-row">
                <div class="cms-col-4">
                    <div class="landing-page__youtube-text">
                        @if ( $landingPage->video_title )
                            <h2 class="mb-5 h2 text-left-m-center title--underlined-left-m-center--blue">{{ $landingPage->video_title }}</h2>
                        @endif
                        <div class="cms-content">{!! $landingPage->video_text !!}</div>
                    </div>
                </div>
                <div class="cms-col-8">
                    <div class="landing-page__youtube-vid__container">
                        <iframe width="100%"  class="landing-page__youtube-vid" src="{{ $video_link }}"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @include('partials.footer')
    @include('partials.tag-commander-event')
    @include('partials.mbox')
    <script>
        window.club_detail = @json(trans('trad.club-detail'));
    </script>
@endsection