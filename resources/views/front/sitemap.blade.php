@extends('layouts.front')

@section('title')
    @lang('meta.title.sitemap')
@endsection
@section('description')
    @lang('meta.description.sitemap')
@endsection

@section('content')
    @include('front.header')
    <div class="container-sitemap">
        <div class="container-full-w">
            <div>
                <div class="sitemap__header">
                    <div class="in_picture__blue"></div>
                    <div class="sitemap__header_center">
                        <h1 class="h1 text--uppercase text--uppercase text-white">
                            @lang('trad.sitemap.title')
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container sitemap-content">
            <h2>
                <a class="sitemap-home" href="/">
                    @lang('trad.sitemap.home')
                </a>
            </h2>
            <ul class="sitemap-list">
                @forelse($subscriptions as $subscription)
                    @if($subscription->url)
                        <li>
                            <a href="{{route('subscription.index', $subscription->url)}}"
                               class="lnk-sitemap">
                                {{$subscription->title}}
                            </a>
                        </li>
                    @endif
                @empty
                @endforelse
                @if($specialOffer)
                    <li>
                        <a href="{{route('specialoffer.index', $specialOffer->url)}}"
                           class="lnk-sitemap">
                            @lang('trad.header.special-offer')
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{route('front.find-club')}}"
                       class="lnk-sitemap">
                        @lang('trad.header.find-club')
                    </a>
                </li>

                @if(LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        @if(Route::has('front.find-club-by-departement'))
                            <a href="{{route('front.find-club-by-department')}}"
                               class="lnk-sitemap">
                                @lang('trad.header.find-club-by-department')
                            </a>
                        @endif
                    </li>
                @endif
                <li>
                    <a href="{{route('activity.showAll')}}"
                       class="lnk-sitemap">
                        @lang('trad.header.activity')
                    </a>
                </li>
                <li>
                    <a href="{{route('equipment.showAll')}}"
                       class="lnk-sitemap">
                        @lang('trad.sitemap.equipment')
                    </a>
                </li>
                <li>
                    <a href="{{route('yako.showAll')}}"
                       class="lnk-sitemap">
                        @lang('trad.sitemap.yako')
                    </a>
                </li>
                @if(config('laravellocalization.showBlog') || LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        <a href="{{route('blog.showAll')}}"
                           class="lnk-sitemap">
                            @lang('trad.header.blog')
                        </a>
                    </li>
                @endif
                @if(config('laravellocalization.showBecameCoach') || LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        <a href="{{config('link.encp.url')}}" class="lnk-sitemap">
                            {{ config('link.encp.label') }}
                        </a>
                    </li>
                @endif
                @if(config('laravellocalization.showPulp') || LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        <a href="{{config('link.pulp.url')}}" class="lnk-sitemap">
                            {{ config('link.pulp.label') }}
                        </a>
                    </li>
                @endif

                @if(LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        <a class="lnk-sitemap"
                           href="{{route('recruitment.index')}}">@lang('trad.sitemap.recrutement')
                        </a>
                    </li>
                    <li>
                        <a class="lnk-sitemap" href="{{route('page.retrieve', trans('routes.page.legal-mentions'))}}">
                            @lang('trad.footer.legal-mention')
                        </a>
                    </li>
                    @if(config('laravellocalization.showBecameCoach') || LaravelLocalization::getCurrentLocale() == 'fr')
                        <li>
                            <a href="{{ route('page.retrieve', 'devenir-coach') }}" class="lnk-sitemap">
                                @lang('trad.sitemap.coach') <!-- TODO dynamique title -->
                            </a>
                        </li>
                    @endif
                    <li>
                        <a
                            @if(LaravelLocalization::getCurrentLocale() != 'es')
                                href="{{ route('page.retrieve', 'ouvrir-salle-sport') }}"
                            @else
                                href="{{ route('page.retrieve', 'abrir-un-gimnasio') }}"
                            @endif
                            class="lnk-sitemap"
                        >
                        @lang('trad.sitemap.club') <!-- TODO dynamique title -->
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('page.retrieve', 'ouvrir-salle-sport-accompagnement') }}/"
                           class="lnk-sitemap">
                        @lang('trad.sitemap.accompagnement') <!-- TODO dynamique title -->
                        </a>
                    </li>
                @endif

                @php($aboutPage = \App\Lib\About::getAboutPage())
                @if($aboutPage)
                    @foreach ($aboutPage->children as $aboutPageChild)
                        <a
                                href="{{route('page.retrieveChild', ['childUrl' => $aboutPageChild->url])}}"
                                class="lnk-sitemap">
                            {{$aboutPageChild->title}}
                        </a>
                    @endforeach
                @endif

                @if(LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        <a class="lnk-sitemap"
                        @if(config('laravellocalization.newLinkRecrutement'))
                        href="{{route('recruitment.index')}}"
                        @else
                        href="http://www.lorangebleuerecrute.fr/"
                                @endif
                        >@lang('trad.sitemap.recrutement')
                        </a>
                    </li>
                @endif
                <li>
                    <a class="lnk-sitemap" href="{{route('page.retrieve', trans('routes.page.legal-mentions'))}}" >
                        @lang('trad.footer.legal-mention')
                    </a>
                </li>
                @if(config('laravellocalization.showBecameCoach') || LaravelLocalization::getCurrentLocale() == 'fr')
                    <li>
                        <a href="{{ route('page.retrieve', 'devenir-coach') }}" class="lnk-sitemap">
                        @lang('trad.sitemap.coach') <!-- TODO dynamique title -->
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('page.retrieve', trans('routes.page.ouvrir-salle-sport')) }}"
                            class="lnk-sitemap"
                    >
                        @lang('trad.sitemap.club') <!-- TODO dynamique title -->
                    </a>
                </li>
                <li>
                    <a
                            @if(LaravelLocalization::getCurrentLocale() != 'es')
                            href="{{ route('page.retrieve', 'ouvrir-salle-sport-accompagnement') }}/"
                            @else
                            href="{{route('page.retrieveChild', ['childUrl' => 'abrir-un-gimnasio-acompanamiento'])}}"
                            @endif
                            class="lnk-sitemap">
                    @lang('trad.sitemap.accompagnement') <!-- TODO dynamique title -->
                    </a>
                </li>
                <li>
                    <a href="{{ route('contact.index') }}" class="lnk-sitemap">
                        @lang('trad.modal-contact.contact')
                    </a>
                </li>
                <li>
                    <strong>@lang('trad.sitemap.clubs')</strong>
                    <ul>
                        @foreach($clubsByDepartment as $clubsFromOneDepartment)
                            @if(count($clubsFromOneDepartment['clubs']))
                                <li>
                                    <a href="{{route('front.club-details', $clubsFromOneDepartment['department']->url)}}">
                                        @lang('trad.sitemap.club-phrase'){{$clubsFromOneDepartment['department']->name}}
                                    </a>
                                    <ul>
                                        @foreach($clubsFromOneDepartment['clubs'] as $club)
                                            <li>
                                                <a href="{{route('front.club-list', ['city' => str_slug($club->formattedTitle), 'department' => str_slug($club->department->url)])}}">
                                                    Club @if($club->where('city', 'rennes')->first()) fitness  @else {{$club->subscription->type}} @endif {{ucwords($club->formattedTitle)}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    @include('partials.footer')
@endsection

