@extends('layouts.front')

@section('title')
    @lang('meta.title.faq')
@endsection
@section('description')
    @lang('meta.description.faq')
@endsection

@section('content')
    @include('front.header')
    <div class="container-faq">
        <div class="container-full-w position-relative">
            <div class="container-faq__header">
                <h1 class="h1 text--t-center text-white text--uppercase">@lang('trad.faq.title')</h1>
                <p class="list__header_presentation text--t-center">@lang('trad.faq.subtitle')
                </p>
            </div>
        </div>
        <div class="container-1170 container">
            <div id="faqgroup" class="col-lg-12 list-faq-list-container">
            @forelse($faq_list as $indexKey => $faq)
                <div class="list-faq-item-container">
                    <h2 class="list-faq-item-question collapsed" data-toggle="collapse" data-target="#list-faq-item-{{$indexKey}}">
                        {{$faq->question}}
                    </h2>
                    <div class="list-faq-item-answer collapse" id="list-faq-item-{{$indexKey}}" data-parent="#faqgroup">
                        <span>{!!$faq->answer!!}</span>
                    </div>
                </div>
            @empty
            @endforelse
            </div>

            <div class="col-lg-12 text--t-center">
                <p class="faq__text text--bold">@lang('trad.faq.line1')</p>
                <a href="/contact" class="btn btn--bg-orange btn--t-white btn__faq">@lang('trad.button.contact-us')</a>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('faq') }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'faq') }}
    @include('partials.footer')
    @isset($job_posting_SEOcontext)
        {!! $job_posting_SEOcontext !!}
    @endisset
@endsection
