@extends('layouts.front')

@section('title')
    {{$entity->getMetaTitle()}}
@endsection
@section('description')
    {{$entity->getMetaDescription()}}
@endsection

@section('content')
    @include('front.header')
    <div class="container-search-result">
        <div class="container-full-w search-result__header">
            <div class="container container-1170 search-result__header__content">
                <h1 class="h2 text--uppercase text--t-center">
                    @if(!empty($club_lists) && count($club_lists))
                        {{ trans_choice('trad.find-club-step2.title', count($club_lists),['place' => $entity->name]) }}
                    @else
                        @lang('trad.common.no-result')
                    @endif
                </h1>
                @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'fr')
                    {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex p-2 align-items-center',
                        'style' => 'justify-content: center;'
                    )) }}
                    @include('front.clubs.components.club-form')
                    <div style="display:none !important;"
                         class="search-result__more_criteria text--t-center d-flex align-items-center">
                        <i class="fa fa-plus-circle fa-1x"></i><a href=""
                                                                  class="text--uppercase">@lang('trad.find-club-step2.criteria')</a>
                    </div>
                    {{ Form::close() }}
                @endif
            </div>
        </div>
    </div>
    <div class="container container-full-w container-criteria__mobile d-md-none  d-sm-block">
        <div class="row">
            <div class="col-md-12 d-flex">
                @lang('trad.find-club-step2.display')
                <div class="js-switch-state active" data-addclass="active"
                     data-contentrelated="search__content__map"><span class="criteria__list"><i
                                class="fa fa-list"></i>@lang('trad.find-club-step2.list')</span>
                </div>
                <div class="js-switch-state" data-addclass="active" data-contentrelated="lat-lng-club"><span
                            class="criteria__carte"><i
                                class="fa fa-map-marker-alt"></i>@lang('trad.find-club-step2.map')</span>
                </div>

            </div>
        </div>
    </div>
    <div class="container container-full-w container-search-result__content container--padding-b-80 data-clubs"
         data-app-url="{{config('app.url')}}"
         @isset($club_ids)
         data-clubs="{{http_build_query($club_ids)}}">
        @endisset
        <div class="row">
            <div class="col-md-6 d-flex flex-wrap search__content__left">
                @isset($club_lists)
                    @forelse($club_lists as $club)
                        <div class="col-xl-6 col-12 lat-lng-club club-loop-{{$club->id}}"
                            @if($club->is_open)
                                data-img-marker="{{ url('/images/markerclub.png')}}"
                            @else
                                data-img-marker="{{ url('/images/markerclub-grey.png')}}"
                            @endif
                             data-club-lat="{{$club->latitude}}"
                             data-club-lng="{{$club->longitude}}">
                            <div class="card-club @isset($club->subscription->type) {{$club->subscription->type}} @else fitness @endisset">
                                @if($club->is_open && $club->url)
                                <a class="" href="{{ route('front.club-details', $club->url) }}">
                                @endif
                                    @if($club->subscription->type == 'fitness')
                                        <img class="card-club__main-image"
                                             src="{{url('/images/recherche-mcf.jpg')}}"
                                             alt="image salle club fitness"/>
                                    @elseif($club->subscription->type == 'wellness')
                                        <img class="card-club__main-image"
                                             src="{{url('/images/recherche-mcw.jpg')}}"
                                             alt="image salle club wellness"/>
                                    @endif
                                    <div class="p-md-3 d-flex flex-column">
                                        <div class="card-club__header d-flex justify-content-between p-2">
                                            @isset($club->subscription->type)
                                                <span class="card-logo-svg">
                                                    <img src="{{url('/images/'.$club->subscription->type.'-sm.svg')}}"
                                                        width="100%" height="100%"
                                                        alt="club-img"
                                                        />
                                                </span>
                                            @else
                                                <span class="card-logo-svg">
                                                    <img src="{{url('/images/logo-ob-mcf-fondblanc.svg')}}"
                                                        width="100%" height="100%"
                                                        alt="club-img"/>
                                                </span>
                                            @endisset
                                            @isset($search)
                                                @if($department)
                                                    <p class="card-club__details"><i
                                                                class="fa fa-map-marker-alt"></i>{{round($club->distance, 1)}}
                                                        km</p>
                                                @endif
                                            @endisset
                                        </div>
                                        <div class="card-club__content p-2">
                                            <h2 class="card-club__title text--uppercase">
                                                {{$club->title}}
                                            </h2>
                                            <p class="card-club__adress">
                                                @if($club->is_open) 
                                                    {{$club->address}}
                                                    <br> 
                                                    {{$club->city}}
                                                @else 
                                                    @if($club->subscription->type == 'fitness') Club @else @lang('trad.club-center') @endif @lang('trad.club-opening')
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                @if($club->is_open) 
                                </a>
                                @endif
                                <div class="card-club__footer d-flex p-md-2 pb-2 flex-wrap justify-content-center">
                                    @if($club->is_open && $club->url)
                                        @if(count($club->offers) > 0 && $club->subscription->type == 'fitness')
                                            {!!Form::open(array('action' => array('Front\SubscriptionController@step2', $club->subscription->url), 'method' => 'GET', 'style' => 'z-index:1;','class' => 'mr-2')) !!}
                                            @isset($order)
                                                <input type="hidden" name="token"
                                                    value="{{$order->order_token}}">
                                            @endisset
                                            <input type="hidden" name="clubId" value="{{$club->id}}">
                                            <button
                                                type="submit"
                                                data-city="{{$club->city}}"
                                                data-club="{{$club->city}}_{{$club->title}}"
                                                class=" btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn js-tag-commander-abo-list-club-page"
                                            >
                                                @lang('trad.button.subscribe')
                                            </button>
                                            {!! Form::close() !!}
                                        @else
                                            <div class="result__card__footer text--t-center"
                                                style="z-index:1;">
                                                <a href="{{route('front.club-details', $club->url)}}"
                                                class="btn btn--bg-orange btn--t-white mr-2 btn--margin-t-0 button-int club__footer__abo-btn">
                                                    @lang('trad.button.more')
                                                </a>
                                            </div>
                                        @endif


                                        @mobile
                                        <a href="tel:{{$club->phone}}"
                                        data-city="{{$club->city}}"
                                        data-club="{{$club->city}}_{{$club->title}}"
                                        class="js-tag-commander-tel btn btn--bg-white btn--t-orange btn--bd-orange card-club__image button-int club__footer__actions"><img
                                                    src="{{url('/images/phoneicon.png')}}"
                                                    alt="picto téléphone"/>
                                        </a>
                                        @elsemobile
                                        <button class="js-tag-commander-tel btn btn--bg-white btn--t-orange btn--bd-orange card-club__image button-int club__footer__actions club__footer__actions_phone
                        js-switch-state-generic" data-addclass="club__footer__actions--active"
                                                data-city="{{$club->city}}"
                                                data-club="{{$club->city}}_{{$club->title}}"
                                        >
                                            <img src="{{url('/images/phoneicon.png')}} "
                                                alt="picto téléphone"/><span
                                                    class="phone_number">{{$club->phone}}</span></button>
                                        @endmobile
                                        <a href=""
                                        data-toggle="modal" data-target="#basicModal{{$club->id}}"
                                        data-city="{{$club->city}}"
                                        data-club="{{$club->city}}_{{$club->title}}"
                                        class="js-tag-commander-mail btn btn--bg-white btn--t-orange btn--bd-orange card-club__image button-int club__footer__actions"><img
                                                    src="{{url('/images/mailicon.png')}}"
                                                    alt="picto mail"/>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @include('front.components.modal-contact')
                    @empty
                        <div class="col-xl-12 col-md-12 col-12">
                            <p class="text-center"
                               style="margin-top:10px;">@lang('trad.common.no-result')</p>
                        </div>
                    @endforelse
                @else
                    <div class="col-xl-12 col-md-12 col-12">
                        <p class="text-center"
                           style="margin-top:10px;">@lang('trad.common.no-result')</p>
                    </div>
                @endisset
            </div>
            <div class="col-md-6 pl-md-0 search__content__map">
                @include('front.components.map-style-controls')
                <div id="map" data-context="find-club"></div>
            </div>
        </div>
    </div>
    <div class="container-full-w presentation-container">
        <div class="presentation-container_content">
            <div class="row">
                <div class="col-xl-6 col-md-12 text-md-center">
                    <img src="{{ url('/images/image-velo-fitness.png')}} " alt="image velo fitness"/>
                </div>
                <div class="col-xl-6 col-md-12">
                    <div class="presentation--container__desc">
                        <div class="presentation--container__title">
                            <h2 class="presentation__title h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">
                                @if($entity->title)
                                    {{ $entity->title }}
                                @else
                                   @lang('trad.find-club-step2.block-seo-title'){{$entity->name}}
                                @endif
                            </h2>
                        </div>
                        <div class="presentation--container__text">
                            {!! preg_replace("/<p>(.*)<\/p>/i",'', $entity->description) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @isset($search)
    @else
    <div class="container-full-w city-container">
        <h2 class="presentation__title h2 title--underlined-center-md--blue text-uppercase mb-5">
            {{ trans('trad.find-club-city.title',['place' => $entity->name]) }}
        </h2>
        <ul class="list-unstyled">
            @forelse($cities as $city)
                <li>
                    <a href="{{route('front.club-list', [$entity->url, $city->url])}}">{{ trans('trad.find-club-city.list-title',['city' => $city->name]) }}</a>
                </li>
            @empty
            @endforelse
        </ul>
    </div>
    @endisset
    @isset($search)
        {{ Breadcrumbs::render('results-club-city', $department, $search) }}
        {{ Breadcrumbs::view('breadcrumbs::json-ld', 'results-club-city', $department, $search) }}
    @else
        {{ Breadcrumbs::render('results-club-department', $department) }}
        {{ Breadcrumbs::view('breadcrumbs::json-ld', 'results-club-department', $department, $department ? $department->name: $club_lists->first()->city) }}
    @endisset
    @include('partials.footer')
    @include('partials.mbox')
@endsection
