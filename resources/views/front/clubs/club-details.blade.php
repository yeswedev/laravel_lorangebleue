@extends('layouts.front')

@section('title')
    {{$club->getMetaTitle()}}
@endsection
@section('description')
    {{$club->getMetaDescription()}}
@endsection

@section('content')
    @include('front.components.modal-contact')
    @include('front.header')
    <div class="container-club-details">

        <div
        class="container-club-details__header @if($club->subscription->type == 'wellness') container-club-details__header-wellness @endif"
        @isset($club)
            @if($club->banner)
            style="background: #004585 url({{Storage::url($club->banner)}}) no-repeat;background-size: cover;"
            @endif
        @endisset
        >
            <div class="header-map__controls header-map__controls--hide">
                @include('front.components.map-style-controls')
            </div>
            <div class="js-map" id="header-map"></div>
            <img class="d-sm-none js-photo"
                 @if($club->subscription->type == 'fitness')
                     src="{{url('/images/salle-equipement-musculation.png')}}"
                     alt="salle avec divers équipements de fitness"
                @else
                    src="{{url('/images/salle-equipement-wellness.jpg')}}"
                    alt="salle avec divers équipements de wellness"
                @endif
            />
            <div class="container-club__infos">
                <div class="club__adress">
                    <span class="logo-svg">
                        <img src="{{url('/images/ob-'.$club->subscription->type.'.svg')}}"
                            width="100%" height="100%"
                            alt="logo {{$club->subscription->type}}"/>
                    </span>
                    <h1 class="club__adress__title">
                        @lang('trad.club-detail.title-fitness') {{$club->getH1()}}
                    </h1>
                    <div class="club__infos__badges">
                        @if ( $club->is_free_access )
                            <div class="club__infos__badge club__infos__freeaccess-badge">
                                <img src="{{ url('/images/pastille-libre-acces.svg') }}" alt="Badge Accès Libre"/>
                            </div>
                        @endif
                        @if ( $club->is_yako360 )
                            <div class="club__infos__badge club__infos__yako-badge">
                                <img src="{{ url('/images/pastille-yako-360.svg') }}" alt="Badge Yako 360"/>
                            </div>
                        @endif
                    </div>
                    <h2 class="club__infos__subtitle club__infos__subtitle--no-before">@lang('trad.club-detail.address')</h2>
                    <p class="club__infos__text">{{$club->address}} <br/>
                        <span class="text--uppercase">{{$club->zip_code}} {{$club->city}}</span>
                    </p>
                </div>

                <div class="container-club__hours">
                    <a class="club__infos--lnk club__filter__span lnk__underline  js-show-on-map">
                        @lang('trad.club-detail.display-map')
                    </a>
                    <img src="{{url('/images/arrowblue.png')}}" alt="arrow"/>
                    <h2 class="club__infos__subtitle">
                        @if($club->subscription->type == 'fitness')
                            @lang('trad.club-detail.hour-fitness')
                        @else
                            @lang('trad.club-detail.hour-wellness')
                        @endif
                    </h2>
                    <div class="club__infos__free-access">{!! $club->free_access_schedule !!}</div>
                    @if($club->planning_image)
                        <a href="{{Storage::url($club->planning_image)}}" class="btn btn--bg-white btn--t-orange btn--bd-orange"
                           target="_blank">
                            @lang('trad.club-detail.planing')
                        </a>
                    @endif
                    <div class="js-toggleAddress address-block--is-hover address-block pt-3">
                        <div class="d-flex flex-column">
                            <div class="club__infos__text">
                                @lang('trad.club-detail.framing-hours')
                            </div>
                            <div class="club__infos__text">
                                <span class="js-club__infos--is-opened"></span>
                                @if( $schedule != [] )
                                    @foreach( $schedule as $key => $day )
                                        <span class="js-club__infos--opening-hour @if( $loop->first ) club__infos--opening-hour @endif">
                                            {{ $key }} - {{ $day }}
                                        </span>
                                    @endforeach
                                @else
                                    @lang('trad.club-detail.schedule')
                                @endif
                            </div>
                        </div>
                        <i class="fas fa-chevron-down"></i>
                    </div>
                </div>
                <div class="container-club__contact">
                    <h2 class="club__infos__subtitle">@lang('trad.club-detail.contact') :</h2>
                    <div class="d-flex">
                        @if($club->phone)
                            @mobile
                            <a href="tel:{{$club->phone}}" data-city="{{$club->city}}"
                               class="js-tag-commander-tel btn btn--bg-white btn--t-orange btn--bd-orange"
                               style="margin-right:10px;">@lang('trad.club-detail.phone')</a>
                            @elsemobile
                            <button class="js-tag-commander-tel btn btn--bg-white btn--t-orange btn--bd-orange club__actions js-switch-state"
                                    data-addclass="club__actions--active" data-city="{{$club->city}}">
                                <span class="text">@lang('trad.club-detail.phone')</span>
                                <span class="phone_number">{{$club->phone}}</span>
                            </button>
                            @endmobile
                        @endif
                        <a href="" data-toggle="modal" data-target="#basicModal{{$club->id}}"
                           data-city="{{$club->city}}"
                           class="btn btn--bg-white btn--t-orange btn--bd-orange card-club__image button-int club__footer__actions club__footer__actions_phone"
                           style="">@lang('trad.club-detail.mail')</a>
                        @if($club->social_link_fb)
                            <a href="{{$club->social_link_fb}}" target="_blank"
                               class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                        class="fab fa-facebook-f"></i></a>
                        @else
                            @if($club->subscription->type == 'fitness')
                                <a href="{{config('link.facebook.url-fr')}}" target="_blank"
                                   class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                            class="fab fa-facebook-f"></i></a>
                            @else
                                <a href="{{config('link.facebook.url-wellness')}}" target="_blank"
                                   class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                            class="fab fa-facebook-f"></i></a>
                            @endif
                        @endif

                        @if($club->social_link_insta)
                            <a href="{{$club->social_link_insta}}" target="_blank"
                               class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                        class="fab fa-instagram"></i></a>
                        @endif
                    </div>

                </div>
            </div>

            <div class="container-club__filter club__filter d-none d-xl-block">
                <div class="d-flex">
                    <div>
                        <span class="js-switch-state club__filter__span club__filter__span--btn-photo club__filter__span--active-filter"
                              data-addclass="club__filter__span--active-filter">@lang('trad.club-detail.photo')</span>
                    </div>
                    <div><span class="js-switch-state js-toggle-map club__filter__span club__filter__span--carte"
                               data-addclass="club__filter__span--active-filter">@lang('trad.find-club-step2.map')</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="container container-1170 container--padding-t-200">
            @include('partials.errors')
            @if(count($club->activeOffers()) > 0)
                @if($club->subscription->type == 'fitness')
                    <div class="subscription--margin-b-50">
                        <h2 class="subscription__title h2 text--t-center title--underlined-center--blue text-uppercase">
                            @lang('trad.common.subscription-plural')</h2>
                    </div>
                    <div class="subscription__text">
                    </div>
                    @include('front.components.offer', ['offers' => $club->activeOffers(), 'club' => $club, 'subscriptionUrl' => $club->subscription->url, 'order' => null, ])
                @endif
            @endif

            @if($club->subscription && $club->subscription->type == 'fitness')
                @include('front.components.subscription-banner', ['subscriptionUrl' => $club->subscription->url, 'price' => $formattedBannerPrice, 'aside' => false])
            @endif
        </div>

        <div class="container container-1170">
            <div class="container-club-details__presentation container--padding-t-80">
                <div class="row">
                    <div class="col-xl-6 col-md-12">
                        <div class="container-club-details--bigimage">
                            <img
                                @if($club->image)
                                    src="{{Storage::url($club->image)}}"
                                    alt="{{$club->getImgAlt()}}"
                                @elseif($club->subscription->type == 'fitness')
                                    src="{{url('/images/accueil-club.png')}}"
                                    alt="Salle de sport {{$club->city}}"
                                @else
                                    src="{{url('/images/default_wellness_club.jpg')}}"
                                    alt="Salle de sport {{$club->city}}"
                                @endif
                            />
                        </div>
                        <div class="container-club-details__image">
                            <div class="slider-presentation owl-carousel owl-theme">
                                @if($club->image)
                                <div class="item container-club-details--item activeImg js-switch-state"
                                     data-addclass="activeImg">
                                    <img
                                        @if($club->image)
                                            src="{{Storage::url($club->image)}}" alt="{{$club->getImgAlt()}}"
                                        @else
                                            src="" alt="image club"
                                        @endif
                                    />
                                </div>
                                @endif
                                @forelse($club->medias as $media)
                                    <div class="item container-club-details--item js-switch-state"
                                         data-addclass="activeImg">
                                        <img
                                            src="{{Storage::url($media->file)}}"
                                            @if($media->description)
                                                alt="{{$media->description}}"
                                            @else
                                                alt="images du club"
                                            @endif
                                        />
                                    </div>
                                @empty
                                    @if(!$club->image)
                                        <div class="item container-club-details--item js-switch-state"
                                             data-addclass="activeImg">
                                            @if($club->subscription->type == 'fitness')
                                                <img src="{{url('/images/accueil-club.png')}}"
                                                     alt="accueil club"/>
                                            @else
                                                <img src="{{url('/images/default_wellness_club.jpg')}}"
                                                     alt="accueil club"/>
                                            @endif
                                        </div>
                                    @endif
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-12">
                        <div class="presentation--container__desc">
                            <div class="presentation--container__title">
                                <h2 class="presentation__title h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">
                                    @lang('trad.club-detail.pres-title') {{$club->title}}</h2>
                            </div>
                            <div class="presentation--container__text">
                                @replace($club->description,"h1","h2")
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(count($yakos) > 0)
            <div class="container container-1170">
                <div class="container-details__yako container--padding-t-80">
                    <div class="mb-5">
                        <h2 class="h2 text--t-center title--underlined-center--blue text-uppercase">
                            @lang('trad.yako-slider.title')</h2>
                    </div>
                    <div class="subscription__text mb-5">
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="details-yako__mainimage">
                                <div id="js-yako-video-arrow" class="js-yako-arrow">
                                    <div class="details-yako--rounded-blue">
                                        <img class="rounded-blue--image"
                                             src="{{url('/images/arrowvideo.png')}}"
                                             alt="img-player"/>
                                    </div>
                                </div>
                                <img id="js-yako-image" class="details-yako__backImage" src=""/>
                                <iframe id="js-yako-video" frameborder="0" allowfullscreen="1"
                                        allow="encrypted-media"
                                        title="YouTube video player" class="details-yako__backImage"
                                        src=""></iframe>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="details-yako-slider details-yako__list">
                                @isset($yako_lists)
                                    @forelse($yako_lists as $yako)
                                        <div class="js-yako-illustration-selector yako__list__item js-switch-state"
                                            data-addclass="yako__list__item--activeYako"
                                            yako_illustration_type="{{$yako['illustration_type']}}"
                                            yako_illustration_src="{{$yako['illustration_src']}}">
                                            <h3>Yako {{$yako['object']->title}}</h3>
                                        </div>
                                    @empty
                                    @endforelse
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!$club_equipments->isEmpty())
            <div class="container container-1170">
                <div class="row components-list container--padding-t-80">
                    <div class="components-list__header">
                        <h2 class="components-list__title presentation__title h2 text-center title--underlined-center--blue text-uppercase">
                            @lang('trad.club-detail.equipment')
                        </h2>
                    </div>
                    <div class="container components-list__content">
                        <div class="row parent-equip components-list__cards">
                            @forelse($club_equipments as $club_equipment)
                                <div class="col-lg-3 col-6">
                                    <div class="list__cards__item">
                                        <div class="in_picture">
                                            <a
                                                @isset($club_equipment->url)
                                                    href="{{route('equipment.retrieve', $club_equipment->url)}}"
                                                @else
                                                    href=""
                                                @endisset
                                            >
                                                <img class="in_picture__opacity"
                                                     @isset($club_equipment->image)
                                                     src="{{Storage::url($club_equipment->image)}}"
                                                     @else
                                                     src="{{url('/images/equipement-fitness-1.png')}}"
                                                        @endisset
                                                >
                                                <div class="in_picture__blue"></div>
                                                <h3 class="in_picture__title text--uppercase">
                                                    {{$club_equipment->title}}
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="components-list__actions col-md-12">
                            <div class="">
                                <a href="{{ route('equipment.showAll') }}"
                                   class="btn btn--bg-white btn--t-orange btn--bd-orange list__actions__btn">@lang('trad.equipment.button')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(!$advantages->isEmpty())
            <div class="container container-1170">
                <div class="container-advantages container--padding-t-80">
                    <div class="row">
                        <div class="advantages__header">
                            <h2 class="presentation__title h2 text-center title--underlined-center--blue text-uppercase mb-5">
                                @lang('trad.club-detail.premium-title')
                            </h2>
                        </div>
                    </div>
                    <div class="container-advantages__content">
                        <div class="advantage-carousel">
                            @forelse($advantages as $advantage)
                                <div class="col-xl-3 col-md-6 col-12">
                                    <div class="item">
                                        <div class="advantages__item d-flex align-items-center">
                                            @isset($advantage->image)
                                                <img class="advantages__item__image"
                                                     src="{{Storage::url($advantage->image)}}"
                                                     alt="img-advantages"/>
                                            @else
                                                <img class="advantages__item__image"
                                                     src="{{url('/images/sport2000.png')}}"
                                                     alt="logo sport 2000"/>
                                            @endisset
                                            <div class="advantages__item__details">
                                                {{$advantage->description}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="container-full-w container presentation-container">
            <div class="presentation-container_content container--padding-t-80">
                <div class="row">
                    <div class="col-xl-6 text-md-center full-w-mobile">
                        <img
                                @isset($club->team_image)
                                src="{{Storage::url($club->team_image)}}"
                                alt="{{$club->team_image_alt}}"
                                @else
                                @if($club->subscription->type == 'fitness')
                                src="{{ url('/images/teamclub.png')}}"
                                alt="team club {{$club->title}}"
                                @else
                                src="{{ url('/images/default_wellness_team.png')}}"
                                alt="team club {{$club->title}}"
                                @endif
                                @endisset
                                />
                    </div>
                    <div class="col-xl-6">
                        <div class="presentation--container__desc">
                            <div class="presentation--container__title">
                                <h2 class="presentation__title h2 title--underlined-center-md--blue title--underlined-left--blue text-uppercase mb-5">
                                    @lang('trad.club-detail.team') {{$club->title}}</h2>
                            </div>
                            <div class="presentation--container__text">
                                <div>
                                    {{$club->team_title}}
                                </div>
                                <br>
                                <div>
                                    {!! $club->team_description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($club['virtual-tour_link'])
            <div class="container">
                <div class="presentation--container__title ml-5">
                    <h2 class="presentation__title h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">@lang('trad.club-detail.virtual-tour')</h2>
                </div>
    
                <a class="btn btn--bg-white btn--t-orange btn--bd-orange" href="{{ $club['virtual-tour_link'] }}" target="_blank">
                    @lang('trad.club-detail.virtual-tour__cta')
                </a>
            </div>
        @endif

        <div    
            id="map-club"
            class="container container-full-w no-pad lat-lng-club data-clubs"
            data-app-url="{{config('app.url')}}"
            data-img-marker="{{ url('/images/markerclub.png')}}"
            data-club-single="{{$club->id}}"
            data-club-lat="{{$club->latitude}}"
            data-club-lng="{{$club->longitude}}"
        >
            <div class="pt-5 pb-5 pad-mobile">
                <div class="container-map-club">
                    @include('front.components.map-style-controls')
                    <div id="map" data-context="club-details" style="position: absolute;top: 0;right: 0;bottom: 0;left: 0;"></div>
                </div>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('single-club', $club) }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-club', $club) }}
    @include('partials.footer')
    {!! $local_business_SEOcontext !!}
    @include('partials.mbox')
    @include('partials.tag-commander-event')
    <script>
        window.club_detail = @json(trans('trad.club-detail'));
    </script>
@endsection
