<div class="club-in_search in_search InputLocalisation">
    <select 
        id="main-club-search" 
        name="search" 
        class="club__input in_text__input input_city" 
    ></select>
    <label class="in_text__label" for="main-club-search">@lang('trad.form.search-club-label')</label>
    <button type="button" class="club-in_search__span js-find-my-position">
        <span class="club-in_search__picto pointer-icon" style="display: block;"></span>
        <span class="loader-position" style="display:none;"></span>
    </button>
</div>

<input type="hidden" name="longitude" class="longitude" value="">
<input type="hidden" name="latitude" class="latitude" value="">
<input type="hidden" name="department" class="department" value="">
<input type="hidden" name="jscity" class="jscity" value="">
<button type="submit" name="button" class="btn btn--bg-blue btn--t-white btn-responsive">
    @lang('trad.button.search')
</button>
