@extends('layouts.front')

@section('title')
   @lang('meta.title.departement')
@endsection
@section('description')
    @lang('meta.description.departement')
@endsection

@section('content')
    @include('front.header')
    <div class="container container-1170 container-find_club__content">
        <h2 class="find_club__content__presentation__title h2 text--t-center title--underlined-center--blue text-uppercase">
            @lang('trad.club-department.title')</h2>
        <p class="h5 text-uppercase text-center">
            <strong>@lang('trad.club-department.subtitle')</strong>
        </p>
        <div class="container-find-club__list">
            <ul>
                @forelse($departments as $department)
                    <li style="list-style:none;">
                        <a href="{{route('front.club-details', $department->url)}}">{{$department->name}}</a>
                    </li>
                @empty
                @endforelse
            </ul>
        </div>
    </div>
    @include('partials.footer')
@endsection
