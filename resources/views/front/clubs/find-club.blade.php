@extends('layouts.front')

@section('title')
    @lang('meta.title.find-club-page')
@endsection
@section('description')
    @lang('meta.description.find-club-page')
@endsection

@section('content')
    @include('front.header')
    <div class="container-full-w container-find_club">
        <div class="container container-find_club__header">
            <div
                @isset($pageGeneric)
                    @if($pageGeneric->image)
                    class="container-find_club__header__bg"
                    style="background: url({{Storage::url($pageGeneric->image)}}) no-repeat; background-size:cover;background-position: center;"
                    @else
                        class="container-find_club__header__bg"
                    @endif
                @else
                    class="container-find_club__header__bg"
                @endisset
            >
                <div class="club__header__bg_opacity"></div>
            </div>
            @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'fr')
            <h1 class="h1 text--t-center text-white text--uppercase">
                @isset($pageGeneric)
                    @if($pageGeneric->title)
                        {{$pageGeneric->title}}
                    @endif
                @endisset
            </h1>
            @else
                <h1 class="h1 text--t-center text-white text--uppercase" style="margin-top: 80px;">
                    @isset($pageGeneric)
                        @if($pageGeneric->title)
                            {{$pageGeneric->title}}
                        @endif
                    @endisset
                </h1>
            @endif
            @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'fr')
                <div class="find_club__item parent-popup-error">
                    {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex align-items-center')) }}
                    @include('front.clubs.components.club-form')
                    {{ Form::close() }}
                    @include('partials.popup-error')
                </div>
            @endif
        </div>
        <div class="container container-1170 container-find_club__content">
            @if(config('laravellocalization.showSearchInput') || LaravelLocalization::getCurrentLocale() == 'fr')
                <h2 class="find_club__content__presentation__title h2 text--t-center title--underlined-center--blue text-uppercase">
                    @lang('trad.find-club-page.subtitle')</h2>
                @else
                <br/><br/>
            @endif
            <div class="container-find-club__list">
                <div class="row">
                    @isset($pageGeneric)
                        @if(count($pageGeneric->cities) > 0)
                            @forelse($pageGeneric->cities as $city)
                                <div class="col-xl-3 col-lg-3 col-md-4 col-6 find-club__list__parent-picture">
                                    <div class="in_picture">
                                        <a href="{{route('front.club-list', [str_slug($city->department->url) ,str_slug($city->url)])}}">
                                            <img class="in_picture__opacity"
                                            @if($city->image_tiny)
                                                src="{{Storage::url($city->image_tiny)}}"
                                            @elseif (file_exists(public_path().'/images/'.$city->url.'.png'))
                                                src="{{url('/images/'.$city->url.'.png')}}"
                                            @else
                                                src="{{url('/images/videoyako.png')}}"
                                            @endif
                                            @if($city->image_tiny_alt)
                                                alt="{{$city->image_tiny_alt}}"
                                            @else
                                                alt="salles de sport à {{$city->name}}"
                                            @endif
                                            />
                                            <span class="in_picture__blue"></span>
                                            <h3 class="in_picture__title text--uppercase">
                                                {{$city->name}}
                                            </h3>
                                        </a>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        @else
                            {{-- Useless ? --}}
                            @forelse($cities as $city)
                                @if($city->department)
                                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 find-club__list__parent-picture">
                                        <div class="in_picture">
                                            <a href="{{route('front.club-list', [str_slug($city->department->url) ,str_slug($city->url)])}}">
                                                @if (file_exists(public_path().'/images/'.$city->url.'.png'))
                                                    <img class="in_picture__opacity" src="{{url('/images/'.$city->url.'.png')}}"
                                                        alt="salles de sport à {{$city->name}}"/>
                                                @else
                                                    <img class="in_picture__opacity" src="{{url('/images/videoyako.png')}}"
                                                        alt="salles de sport à {{$city->name}}"/>
                                                @endif
                                                <span class="in_picture__blue"></span>
                                                <h3 class="in_picture__title text--uppercase">
                                                    {{$city->name}}
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            @empty
                            @endforelse
                        @endif
                    @else
                        @forelse($cities as $city)
                            @if($city->department)
                                <div class="col-xl-3 col-lg-3 col-md-4 col-6 find-club__list__parent-picture">
                                    <div class="in_picture">
                                        <a href="{{route('front.club-list', [str_slug($city->department->url) ,str_slug($city->url)])}}">
                                            @if (file_exists(public_path().'/images/'.$city->url.'.png'))
                                                <img class="in_picture__opacity" src="{{url('/images/'.$city->url.'.png')}}"
                                                    alt="salles de sport à {{$city->name}}"/>
                                            @else
                                                <img class="in_picture__opacity" src="{{url('/images/videoyako.png')}}"
                                                    alt="salles de sport à {{$city->name}}"/>
                                            @endif
                                            <span class="in_picture__blue"></span>
                                            <h3 class="in_picture__title text--uppercase">
                                                {{$city->name}}
                                            </h3>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @empty
                        @endforelse
                    @endisset
                </div>
            </div>
        </div>

        <div class="container container-full-w container-find_club__seo container--padding-t-80 container--padding-b-80">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text--t-center">
                        <img
                        @isset($pageGeneric)
                            @if($pageGeneric->image_wellness)
                                src="{{Storage::url($pageGeneric->image_wellness)}}"
                            @else
                                src="{{url('/images/salle-velo-fitness.png')}}"
                            @endif
                            @if($pageGeneric->image_alt_wellness)
                                alt="{{$pageGeneric->image_alt_wellness}}"
                            @else
                                alt="salle equipee de velos de fitness"
                            @endif
                        @else
                            src="{{url('/images/salle-velo-fitness.png')}}"
                            alt="salle equipee de velos de fitness"
                        @endisset
                        />
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="d-flex flex-column find_club__seo__content">
                        <h2 class="presentation__title h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">@lang('trad.find-club-page.seo-title')</h2>
                        @isset($pageGeneric)
                            @if($pageGeneric->description)
                                {!!$pageGeneric->description!!}
                            @endif
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('clubs') }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'clubs') }}
    @include('partials.footer')
@endsection
