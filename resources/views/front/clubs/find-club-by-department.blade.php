@extends('layouts.front')

@section('title')
    @lang('meta.title.find-club-by-department')
@endsection
@section('description')
    @lang('meta.description.find-club-by-department')
@endsection

@section('content')
    @include('front.header')
    <div class="container-full-w container-find_club">
        <div class="container container-find_club__header">
            <div class="container-find_club__header__bg">
                <div class="club__header__bg_opacity"></div>
            </div>
            <p class="h1 text--t-center text-white text--uppercase">@lang('trad.header.find-club')</p>
            <div class="find_club__item parent-popup-error">
                {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex align-items-center')) }}
                @include('front.clubs.components.club-form')
                {{ Form::close() }}
                @include('partials.popup-error')
            </div>
        </div>
        <div class="container container-1170 container-find_club__content">
            <h1 class="find_club__content__presentation__title h2 text--t-center title--underlined-center--blue text-uppercase">
                @lang('trad.find-club-by-department.title')</h1>
            <div class="container-find-club__list">
                <div class="row">
                    @forelse($departments as $department)
                        {{--@if($city->department)--}}
                            <div class="col-xl-3 col-lg-3 col-md-4 col-6 find-club__list__parent-picture">
                                <div class="in_picture">
                                    <a href="{{route('front.club-list-by-department', $department->url)}}">
                                        <img class="in_picture__opacity" src="{{url('/images/videoyako.png')}}"
                                             alt="vue paris tour eiffel"/>
                                        <span class="in_picture__blue"></span>
                                        <h3 class="in_picture__title text--uppercase">
                                            {{$department->name}}
                                        </h3>
                                    </a>
                                </div>
                            </div>
                        {{--@endif--}}
                    @empty
                    @endforelse
                </div>
            </div>
        </div>

        <div class="container container-full-w container-find_club__seo container--padding-t-80 container--padding-b-80">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text--t-center">
                        <img src="{{url('/images/salle-velo-fitness.png')}}" alt="salle equipee de velos de fitness"/>
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="d-flex flex-column find_club__seo__content">
                        <h2 class="presentation__title h2 title--underlined-left--blue title--underlined-center-md--blue text-uppercase mb-5">@lang('trad.find-club-by-department.seo-title')</h2>
                        <p>@lang('trad.find-club-page.seo-description')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('clubs') }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'clubs') }}
    @include('partials.footer')
@endsection
