@extends('layouts.front')

@section('title')
 @lang('meta.title.club-cities')
@endsection
@section('description')
    @lang('meta.description.club-cities')
@endsection

@section('content')
    @include('front.header')
    <div class="container container-1170 container-find_club__content">
        <h2 class="find_club__content__presentation__title h2 text--t-center title--underlined-center--blue text-uppercase">
            @lang('trad.club-city.title')</h2>
        <p class="h5 text-uppercase text-center">
            <strong>@lang('trad.club-city.subtitle')</strong>
        </p>
        <div class="container-find-club__list">
            <div class="d-flex flex-wrap">
                @forelse($city_lists as $city)
                    <div class="col-xl-3 col-lg-3 col-md-4 col-6 find-club__list__parent-picture">
                        <div class="in_picture">
                            <a href="{{route('front.club-list', [str_slug($city->department->url) ,str_slug($city->url)])}}">
                                @if (file_exists(public_path().'/images/'.str_slug($city->url).'.png'))
                                    <img class="in_picture__opacity" src="{{url('/images/'.str_slug($city->url).'.png')}}" alt="city-img"/>
                                @else
                                    <img class="in_picture__opacity" src="{{url('/images/paris.png')}}" alt="city-img"/>
                                @endif
                                <span class="in_picture__blue"></span>
                                <h3 class="in_picture__title text--uppercase">{{$city->name}}</h3>
                            </a>
                        </div>
                    </div>
                @empty
                @endforelse
            </div>
        </div>
    </div>
    @include('partials.footer')
@endsection
