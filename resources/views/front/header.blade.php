@php
    $locale = LaravelLocalization::getCurrentLocale()
@endphp

<header>
    <div class="top__header">
        <div class="container-1170 container--flex container--flex-between">
            <div>
                <span class="baseline__title baseline--t-regular">
                    @lang('trad.header.slogan')
                </span>
            </div>
            <nav class="nav--flex">
                <ul class="list list__icon list--flex">
                    @if(config('laravellocalization.showYoutube') || $locale == 'fr')
                        <li>
                            <a href="{{config('link.youtube.url-'.$locale)}}"
                               target="_blank"
                               class="lnk_padding--side lnk__icone--sm lnk__icone--t-grey">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                    @endif
                    @if(config('laravellocalization.showInstagram') || $locale == 'fr')
                        <li>
                            <a href="{{config('link.instagram.url-'.$locale)}}"
                               target="_blank"
                               class="lnk_padding--side lnk__icone--sm lnk__icone--t-grey">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    @endif
                    <li>
                        <a href="{{config('link.facebook.url-'.$locale)}}"
                           target="_blank"
                           class="lnk_padding--side lnk__icone--sm lnk__icone--t-grey">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                </ul>
                <ul class="list list__lnk list--flex">
                    @if(config('laravellocalization.showBecameCoach') || $locale == 'fr')
                        <li>
                            <a href="{{config('link.encp.url')}}" class="lnk lnk--grey lnk--t-light lnk--hv-blue"
                               target="_blank">
                                @lang('trad.header.became-coach')
                            </a>
                        </li>
                    @endif

                    @if($locale == 'fr')
                        <li>

                            @if(config('laravellocalization.newLinkRecrutement'))
                                <a href="{{ route('recruitment.index') }}"
                                   class="lnk lnk--grey lnk--t-light lnk--hv-blue">
                                    @lang('trad.homepage.network.join') @lang('trad.homepage.network.club')
                                </a>
                            @else
                                <a href="http://www.lorangebleuerecrute.fr/"
                                   class="lnk lnk--grey lnk--t-light lnk--hv-blue">
                                    @lang('trad.homepage.network.join') @lang('trad.homepage.network.club')
                                </a>
                            @endif

                        </li>
                    @endif
                    <li>
                        <a
                            @if($locale == 'fr' || $locale == 'ma')
                                href="{{ route('page.retrieve', 'ouvrir-salle-sport') }}"
                            @else
                                href="{{ route('page.retrieve', 'abrir-un-gimnasio') }}"
                            @endif

                            class="lnk lnk--grey lnk--t-light lnk--hv-blue"
                        >
                            @lang('trad.homepage.network.open') @lang('trad.homepage.network.open_club')
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('contact.index') }}" class="lnk lnk--grey lnk--t-light lnk--hv-blue">
                            @lang('trad.modal-contact.contact')
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="bottom__header bottom__header-scroll-mobile">
        <div class="container-1170 container--flex container--flex-between container--padding-tb-15 bottom__header-container bottom__header-container-scroll-mobile">
            <button class="navicon">
                <span>
                    <span class="navicon__line"></span>
                    <span class="navicon__line"></span>
                    <span class="navicon__line"></span>
                </span>
                Menu
            </button>
            <a href="/" class="logo js-logo">
                <span class="logo__desktop" data-img-desktop="{{url('/images')}}">
                    <img src="{{url('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo Orange Bleue">
                </span>
                <span class="logo__mobile" data-img-mobile="{{url('/images')}}">
                    <img src="{{url('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo Orange Bleue">
                </span>
            </a>
            <nav class="nav--flex">
                <ul class="list list__menu list--flex">
                    <li class="{{isActive(trans('trad.active-subscription'))}}">
                        <div class="dropdown show">
                            <div class="lnk lnk--grey lnk--hv-blue" role="button"
                               id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @lang('trad.header.subscription') <i class="fas fas--blue fa-chevron-down"></i>
                            </div>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                @if($locale == 'fr')
                                    @forelse(\App\Subscription::get() as $subscription)
                                        @if($subscription->url)
                                            <a href="{{route('subscription.index', $subscription->url)}}"
                                               class="lnk lnk--grey lnk--t-regular lnk--hv-blue">Mon coach {{$subscription->type}}</a>
                                        @endif
                                    @empty
                                    @endforelse
                                @else
                                    @php($subscription = \App\Subscription::where('type', 'fitness')->get())
                                    <a href="{{route('subscription.index', $subscription[0]->url)}}"
                                       class="lnk lnk--grey lnk--t-regular lnk--hv-blue">{{$subscription[0]->title}}</a>
                                @endif
                            </div>
                        </div>
                    </li>
                    @if(\App\Specialoffer::getCurrentSpecialoffer())
                        <li class="{{isActive(trans('trad.active-special-offer'))}}">
                            <a href="{{route('specialoffer.index', App\Specialoffer::getCurrentSpecialoffer()->url)}}" class="lnk lnk--grey lnk--hv-blue">
                                @lang('trad.header.special-offer')
                            </a>
                        </li>
                    @endif
                    <li class="{{isActive(trans('trad.active-club'))}}">
                        <a href="{{route('front.find-club')}}" class="lnk lnk--grey lnk--t-regular lnk--hv-blue">
                            @lang('trad.header.find-club')
                        </a>
                    </li>
                    <li class="{{isActive(trans('trad.active-activity'))}} li-nth"><a
                                href="{{route('activity.showAll')}}"
                                class="lnk lnk--grey lnk--t-regular lnk--hv-blue">
                            @lang('trad.header.activity')
                        </a>
                    </li>
                    @if(config('laravellocalization.showBlog') || $locale == 'fr')
                        <li class="{{isActive(trans('trad.active-blog'))}} li-nth">
                            <a href="{{route('blog.showAll')}}" class="lnk lnk--grey lnk--t-regular lnk--hv-blue">
                                @lang('trad.header.blog')
                            </a>
                        </li>
                    @endif
                    <li class="{{isActive(trans('trad.active-page'))}} li-nth">
                        <div class="dropdown show">
                            <div class="lnk lnk--grey lnk--hv-blue lnk--weight-normal" role="button"
                               id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @lang('trad.header.about') <i class="fas fas--blue fa-chevron-down"></i>
                            </div>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                                @php($aboutPage = \App\Lib\About::getAboutPage())

                                @if($aboutPage)
                                    @foreach ($aboutPage->children as $aboutPageChild)
                                        <a
                                            href="{{route('page.retrieveChild', ['childUrl' => $aboutPageChild->url])}}"
                                            class="lnk lnk--grey lnk--t-regular lnk--hv-blue">
                                                {{$aboutPageChild->title}}
                                        </a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="btn--flex__desktop">
                @if(Request::is('page/ouvrir-salle-sport*') || Request::is('pagina/abrir-un-gimnasio*'))
                    <a
                        href="{{route('contact.index')}}?subject=open_club"
                        class="js-tag-commander-abo btn btn--bg-orange btn--t-white btn--margin-right btn--margin-t-0 btn__subscription">
                        @lang('trad.homepage.network.open') @lang('trad.homepage.network.open_club')
                    </a>
                @else
                    <a
                        href="{{route('subscription.step1', \App\Subscription::DEFAULT_URL)}}"
                        @isset($club)
                            data-city="{{$club->city}}"
                            data-club="{{$club->city}}_{{$club->title}}"
                        @else
                            data-city=""
                            data-club=""
                        @endisset
                            class="js-tag-commander-abo btn btn--bg-orange btn--t-white btn--margin-right btn--margin-t-0 btn__subscription">
                        @lang('trad.button.subscribe')
                    </a>
                    @if(config('laravellocalization.showPulp') || $locale == 'fr')
                        @if ($locale == 'fr')
                            <a href="https://pulp.lorangebleue.fr/" target="_blank" class="btn btn--bg-white btn--t-orange btn--bd-orange btn__pulp">
                                @lang('trad.button.pulp')
                            </a>
                        @endif
                    @endif
                    <a
                        href="{{route('subscription.step1', \App\Subscription::DEFAULT_URL)}}"
                        class="btn btn--bg-white btn--t-orange btn--bd-orange btn--margin-right btn__subscription_mobile">
                        @lang('trad.button.subscribe')
                    </a>
                @endif
            </div>
        </div>
    </div>
    @include('partials.tag-commander-event')
</header>
