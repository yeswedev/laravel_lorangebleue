@extends('layouts.front')

@section('title')
 Recrutement
@endsection
@section('description')
 Liste d'offre d'emploi
@endsection

@section('content')
    @include('front.header')
    <div class="container-blog">
        <div class="container-full-w">
            <div class="">
                <div class="recruitment__header">
                    <h1 class="h1 text--t-center text-white text--uppercase recruitment__header__title">Rejoignez-nous</h1>
                </div>
            </div>
        </div>
        <div class="container-1170 container">
            <div class="blog-tiles row">
                <div class="container-fluid container-1170 blog p-0">
                    <nav class="recruitment-menu">
                        <a data-tab="home" class="recruitment-menu__item" href="/recrutement">Accueil</a>
                        <a data-tab="offers" class="recruitment-menu__item" href="/recrutement/offres">Rechercher une offre</a>
                        <a data-tab="apply" class="recruitment-menu__item" href="/recrutement/postuler">Candidature spontanée</a>
                    </nav>
                    @yield('recruitment')
                </div>
            </div>
        </div>
    </div>
    @include('partials.footer')
    @isset($job_posting_SEOcontext)
        {!! $job_posting_SEOcontext !!}
    @endisset
@endsection
