@extends('layouts.front')

@section('title')
    @if(isset($metaTitle) && $metaTitle)
        {{$metaTitle}}
    @else
        @lang('meta.title.blog-list')
    @endif
@endsection
@section('description')
    @if(isset($metaDescription) && $metaDescription)
        {{$metaDescription}}
    @else
        @lang('meta.description.blog-list')
    @endif
@endsection

@section('content')
    @include('front.header')
    <div class="container-blog">
        <div class="container-full-w">
            <div class="">
                <div
                @isset($pageGeneric)
                    @if($pageGeneric->image)
                    class="blog__header"
                    style="background: url({{Storage::url($pageGeneric->image)}}) no-repeat; background-size:cover;background-position: center;"
                    @else
                        class="blog__header"
                    @endif
                @else
                    class="blog__header"
                @endisset
                >
                    <div class="in_picture__blue"></div>
                    <div class="blog__header_center">
                        <h1 class="h1 blog__header_title text--uppercase text--uppercase text-white">
                            @isset($pageGeneric)
                                @if($pageGeneric->title)
                                    {{$pageGeneric->title}}
                                @else
                                    @lang('trad.blog.title')
                                @endif
                            @else
                                @lang('trad.blog.title')
                            @endisset
                        </h1>
                        <p class="blog__header_presentation text-white">
                            @lang('trad.blog.subtitle')
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-blog__filtre container--padding-t-50">
            <div class="container-1170 container">
                <div class="d-flex flex-wrap justify-content-center">
                    @if(!$isSearchPage)
                        @forelse ($categories as $category)
                            <h2>
                                <a href="{{route('blog.showAll', $category->url)}}"
                                   class="btn btn--bg-white btn--t-orange btn--bd-orange blog__filtre_btn"
                                   style="margin-left:10px;">
                                    {{$category->title}}
                                </a>
                            </h2>
                        @empty

                        @endforelse
                    @else
                        <h2>@lang('trad.blog.result')</h2>
                    @endif
                </div>
            </div>
        </div>
        <div class="container-1170 container">
            <div class="blog-tiles row">
                <div class="container-fluid container-1170 blog">
                    @include('front.components.blog')
                </div>
            </div>
        </div>
    </div>
    {{ Breadcrumbs::render('blog') }}
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'blog') }}
    {!! $list_item_SEOcontext !!}
    @include('partials.footer')
@endsection
