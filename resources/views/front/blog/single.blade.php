@extends('layouts.front')

@section('title')
    @if($blog->seo_title)
        {{$blog->seo_title}}
    @else
        {{$blog->title}}
    @endif
@endsection
@section('description')
    @if($blog->seo_description)
        {{$blog->seo_description}}
    @else
        {{ str_limit(strip_tags($blog->description), $limit = 150) }}
    @endif
@endsection

@section('meta')
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <!--  meta tags for Facebook  -->
    <meta property="og:title" content="{{ ucfirst( mb_strtolower($blog->title) ) }}">
    <meta property="og:description" content="">
    @if($blog->attachments && $blog->attachments->where('type','img')->first())
        <meta property="og:image" content="{{ App::make('url')->to('/') }}{{ Storage::url($blog->attachments->where('type','img')->first()->src) }}">
    @else
        <meta property="og:image" content="{{url('/images/etirements-femme.png')}} }}">
    @endisset
    <meta property="og:url" content="{{ Request::url() }}">

    <!--  meta tags for Twitter  -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="{{ ucfirst( mb_strtolower($blog->title) ) }}">
    <meta name="twitter:description" content="">
    @if($blog->attachments && $blog->attachments->where('type','img')->first())
        <meta property="twitter:image" content="{{ App::make('url')->to('/') }}{{ Storage::url($blog->attachments->where('type','img')->first()->src) }}">
    @else
        <meta property="twitter:image" content="{{url('/images/etirements-femme.png')}} }}">
    @endisset
    @isset($blog->parent)
        <link rel="canonical" href="{{  url("/blog/".$blog->parent->url."/".$blog->url) }}">
    @endisset
@endsection

@section('content')
    @include('front.header')
    <div class="container-single__blog">
        <div class="container-full-w">
            <div class="blog__header">
                <div class="in_picture__blue"></div>
                <div class="blog__header_center">
                    <p class="h1 blog__header_title text--uppercase text--uppercase text-white">@lang('trad.blog.title')</p>
                    <p class="blog__header_presentation text-white">@lang('trad.blog.subtitle')</p>
                </div>
            </div>
        </div>
        <div class="container container-1170 container-single__article">
            <div class="pt-5">
                <div class="row">
                    <div class="col-md-8">
                        <div class="article__header">
                            <p class="article__header_categorie text--uppercase">
                                @php($parent = $blog->parent)
                                @if($parent != null)
                                    {{$parent->title}}
                                @else
                                    @lang('trad.blog.categ')
                                @endif
                            </p>
                            <h1 class="h2 title--underlined-left--blue article__header_title text--uppercase">
                                {{$blog->title}}</h1>
                            <p class="container-single--date">{{$blog->created_at->format('d/m/Y')}}</p>
                        </div>
                        <div class="article__header__social_links">
                            <p class="text--bold">@lang('trad.blog.share-article')</p>
                            {!! Share::currentPage( $blog->title, ["class" => "lnk__icone lnk__icone--bg-white lnk__icone--t-orange"], '<ul>', '</ul>' )->facebook()->twitter()->linkedin() !!}
                        </div>
                        <div class="article__content container--padding-t-50">
                            @if($blog->attachments && $blog->attachments->where('type','img')->first())
                                <img src="{{Storage::url($blog->attachments->where('type','img')->first()->src)}}"
                                     alt="{!! $blog->attachments->where('owner_id',$blog->id)->first()->title !!}"/>
                            @else
                                <img src="{{url('/images/etirements-femme.png')}}" alt="etirements femme" class="">
                            @endisset
                            <div class="article__content_body container--padding-t-50 container__mobile--padding-b-t-20">
                                @replace($blog->description,"h1","h2")
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <aside class="article__aside container--bg-l-grey">
                            <div class="text--t-center">
                                <span class="h2 article__aside_title-form title--underlined-center--blue text--uppercase">@lang('trad.blog.search')</span>
                                <div class="block-sample pt-5 d-xl-inline-block">
                                    <div class="in_text">
                                        <form method="GET" action="{{ route('blog.search') }}"
                                              class="d-flex align-items-xl-center article__aside_form">
                                            <input type="text" name="tag" id="tag" class="in_text__input"
                                                   required>
                                            <label class="in_text__label"
                                                   for="tag">@lang('trad.form.key-word')</label>
                                            <span class="in_text__validmark"></span>
                                            <button class="ml-2 btn btn--bg-blue btn--t-white btn-responsive">@lang('trad.common.submit-ok')
                                            </button>
                                        </form>
                                    </div>
                                </div>

                                <div class="container--padding-t-50 d-flex flex-wrap justify-content-center article__aside_categ">
                                    @forelse($tags as $tag)
                                        <a href="{{route('blog.showAll', ['tag' => $tag->slug])}}"
                                           class="btn btn--bg-white btn--t-orange btn--bd-orange aside_categ_btn"
                                           style="margin-left:5px;">{{$tag->title}}</a>
                                    @empty
                                    @endforelse
                                </div>
                                <div class="footer-flex__item container--padding-t-50">
                                    <div class="footer-flex__item--title">
                                        <p class="text--bold">@lang('trad.footer.social')</p>
                                    </div>
                                    <div class="footer-flex__item--social">
                                        <a href="https://fr-fr.facebook.com/lorangebleuefrance/" target="_blank"
                                           rel="noopener noreferrer"
                                           class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                                    class="fab fa-facebook-f"></i></a>
                                        <a href="{{ config("link.instagram.url-fr") }}"
                                           target="_blank" rel="noopener noreferrer"
                                           class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                                    class="fab fa-instagram"></i></a>
                                        <a href="https://www.youtube.com/channel/UCSQxGvlqBg7K16PPhEN0J1A"
                                           target="_blank" rel="noopener noreferrer"
                                           class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange"><i
                                                    class="fab fa-youtube"></i></a>
                                    </div>
                                </div>
                                <div class="container-single__promo container--padding-t-50">
                                    <div class="subscription--margin-b-50">
                                        <span class="subscription__title h2 text--t-center title--underlined-center--blue text-uppercase">
                                            @lang('trad.blog.actual-offer')</span>
                                    </div>
                                    @include('front.components.subscription-banner', ['subscriptionUrl' => \App\Subscription::where('type','fitness')->first()->url, 'price' => $formattedBannerPrice, 'aside' => true])
                                </div>
                                <div class="aside__month text-left container--padding-t-50">
                                    <span class="aside__month_title">@lang('trad.blog.old-article')</span>
                                    <div class="pl-2">
                                        @forelse($months as $month => $posts)

                                            <a href="{{route('blog.showAll', ['date' => $posts[0]->created_at->format('Y-m')])}}"
                                               class="aside__month_lnk">{{$month}}</a>
                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        @if(count($blog_lists) > 0)
            <div class="container-fluid container-1170 blog">
                <div class="col-md-12 blog-tiles__desc">
                    <div class="blog__outer-title subscription--margin-b-50">
                        <h2 class="blog__title h2 text--t-center title--underlined-center--blue text-uppercase">
                            @lang('trad.blog.like-article')</h2>
                    </div>
                </div>
                @include('front.components.blog')
            </div>
        @endif
    </div>
    @php($parent = $blog->parent)
    @if($parent != null)
        {{ Breadcrumbs::render('single-blog', $blog, $parent) }}
        {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-blog', $blog, $parent) }}
    @else
        {{ Breadcrumbs::render('single-blog', $blog,'categorie-'.$blog->id) }}
        {{ Breadcrumbs::view('breadcrumbs::json-ld', 'single-blog', $blog,'categorie-'.$blog->id) }}
    @endif
    @include('partials.footer')
    {!! $news_article_SEOcontext !!}
@endsection

