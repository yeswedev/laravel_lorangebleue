<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('partials.head')

<body>
@yield('content')

@include('cookieConsent::index')
<!-- Scripts -->
@include('partials.javascripts')
@stack('mbox')
@stack('tag-commander-event')
<script src="//cdn.tagcommander.com/4392/tc_LOrangeBleue_21.js"></script>
</body>
</html>