@if(session()->has('message'))
    <div style="width: 80%;margin: 0 auto;" class="mt-4">
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    </div>
@endif
