@php
    $env_work = config('app.env');
    if($env_work != 'production'){
        $env_work = 'preprod';
    }
@endphp

@desktop
    @php($env_channel = 'd')
@enddesktop

@mobile
    @php($env_channel = 'm')
@endmobile

@tablet
    @php($env_channel = 't')
@endtablet

<!-- TAG COMMANDER START //-->
<script>
    var env_template = "{{$env_template ?? 'other_generic'}}",
        env_work = "{{$env_work ?? ''}}",
        env_channel = "{{$env_channel ?? ''}}",
        env_language = "{{LaravelLocalization::getCurrentLocale()}}",
        env_country = "{{App::getLocale()}}",
        page_cat1 = "{{$page_cat1 ?? ''}}",
        page_cat2 = "{{$page_cat2 ?? ''}}",
        page_cat3 = "{{$page_cat3 ?? ''}}",
        page_name =  "{{$page_name ?? ''}}",
        page_error =  "{{$page_error ?? ''}}",
        offers = "{{$products ?? ''}}",
        list_products = "",
        search_keywords = "{{$search_keywords ?? ''}}",
        search_results_number = "{{$search_results_number ?? ''}}",
        user_civ = "{{$user_civ ?? ''}}",
        user_birthdate = "{{$user_birthdate ?? ''}}",
        user_postalcode = "{{$user_postalcode ?? ''}}",
        user_recency = "{{$user_recency ?? ''}}",
        user_frequency = "{{$user_frequency ?? ''}}",
        user_email = "{{$user_email ?? ''}}",
        user_status = "{{$user_status ?? ''}}",
        user_amount = "{{$user_amount ?? ''}}",
        date_debut_abo = "{{$date_debut_abo ?? ''}}",
        date_fin_abo = "{{$date_fin_abo ?? ''}}",
        club_name = "{{$club_name ?? ''}}",
        club_id = "{{$club_id ?? ''}}",
        user_ville_club = "{{$user_ville_club ?? ''}}",
        order_id = "{{$order_id ?? ''}}",
        order_payment_method = "{{$order_payment_method ?? ''}}",
        order_amount_ati_total = "{{$order_amount_ati_total ?? ''}}",
        order_amount_tf_total = "{{$order_amount_tf_total ?? ''}}",
        order_amount_ati_paid = "{{$order_amount_ati_paid ?? ''}}",
        order_amount_tf_paid = "{{$order_amount_tf_paid ?? ''}}",
        order_tax_paid = "{{$order_tax_paid ?? ''}}",
        order_tax_total = "{{$order_tax_total ?? ''}}",
        order_currency = "{{$order_currency ?? ''}}",
        order_type = "{{$order_type ?? ''}}",
        order_newcustomer = "{{$order_newcustomer ?? ''}}",
        order_products_number = "{{$order_products_number ?? ''}}",
        order_products = [{
            order_product_id : "{{$order_product_id ?? ''}}",
            order_product_pays_club_formule : "{{$order_product_pays_club_formule ?? ''}}",
            order_product_name : "{{$order_product_name ?? ''}}",
            order_product_duree_abo : "{{$order_product_duree_abo ?? ''}}",
            order_product_student : "{{$order_product_student ?? ''}}",
            order_product_category_id : "{{$order_product_category_id ?? ''}}",
            order_product_category : "{{$order_product_category ?? ''}}",
            order_product_unitprice_ati : "{{$order_product_unitprice_ati ?? ''}}",
            order_payment_method : "{{$order_payment_method ?? ''}}",
        }];

        if(offers) {
            var list_products = JSON.parse(offers.replace(/&quot;/g, '"'));
        }

    //<![CDATA[
    var tc_vars = {
        env_template : env_template,
        env_work : env_work,
        env_channel : env_channel,
        env_language : env_language,
        env_country: env_country,
        env_shop : '',
        user_civ : user_civ,
        user_birthdate : user_birthdate,
        user_postalcode : user_postalcode,
        user_ville_club : user_ville_club,
        user_recency : user_recency,
        user_frequency : user_frequency,
        user_amount : user_amount,
        user_email : user_email,
        user_status : user_status,
        date_debut_abo: date_debut_abo,
        date_fin_abo: date_fin_abo,
        club_name: club_name,
        club_id: club_id,

        page_cat1 : page_cat1,
        page_cat2 : page_cat2,
        page_cat3 : page_cat3,
        page_cat4 : '',
        page_name : page_name,
        page_error : '',
        list_products : list_products,
        search_keywords : search_keywords,
        search_results_number : search_results_number,
        product_id : '',
        product_sku : '',
        product_ean : '',
        product_name : '',
        product_unitprice_ati : '',
        product_discount_ati : '',
        product_unitprice_tf : '',
        product_discount_tf : '',
        product_currency : '',
        product_url_page : '',
        product_url_picture : '',
        product_isbundle : '',
        product_bundle_id : '',
        product_instock : '',
        order_id : order_id,
        order_amount_ati_total : order_amount_ati_total,
        order_amount_tf_total : order_amount_tf_total,
        order_tax_total: order_tax_total,
        order_amount_ati_paid : order_amount_ati_paid,
        order_amount_tf_paid : order_amount_tf_paid,
        order_tax_paid: order_tax_paid,
        order_discount_ati : '',
        order_ship_ati : '',
        order_amount_tf_without_sf : '',
        order_amount_tf_with_sf : '',
        order_type : order_type,
        order_discount_tf : '',
        order_ship_tf : '',
        order_tax : '',
        order_payment_method : order_payment_method,
        order_payment_methods : '',
        order_shipping_method : '',
        order_status : '',
        order_promo_code : '',
        order_currency : order_currency,
        order_newcustomer : order_newcustomer,
        order_products_number : order_products_number,
        order_products : order_products,
        store_filters : '',
        store_id : '',
        store_name : '',
        store_postal_code : '',
    }
    //]]>
</script>

<script src="//cdn.tagcommander.com/4392/tc_LOrangeBleue_20.js"></script>
<!-- TAG COMMANDER END //-->
