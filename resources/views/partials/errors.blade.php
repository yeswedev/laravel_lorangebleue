@if ($errors->count() > 0)
<div style="width: 80%;margin: 0 auto;" class="mt-4">
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif