<div class="alert alert-danger popup-error">
    <span class="closebtn js-closebtn">&times;</span>
    @lang('trad.popup-error.line1')
    <br>
    @lang('trad.popup-error.line2')<a href="https://support.google.com/chrome/answer/142065?hl=fr" target="_blank">@lang('trad.popup-error.link')</a>
    <br>
    @lang('trad.popup-error.line3')<a href="https://support.mozilla.org/fr/kb/firefox-partage-t-il-ma-position-avec-sites-web" target="_blank">@lang('trad.popup-error.link')</a>
</div>