<head>
    <meta charset="utf-8">

    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-dk-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-dk-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-dk-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-dk-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-dk-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-dk-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-dk-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-dk-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-dk-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-dk-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-dk-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-dk-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @yield('meta')

    {{-- TODO refacto this madness : manage head by controller --}}
    @if(strpos(request()->fullUrl(), '/conditions-generales-de-vente') == false
        && strpos(request()->fullUrl(), '/faq') == false
        && strpos(request()->fullUrl(), '/ouvrir-salle-sport-accompagnement') == false
        && strpos(request()->fullUrl(), '/plan-du-site') == false
        && strpos( request()->fullUrl(), '/blog/') == false
    )
        @if(strpos(request()->fullUrl(), '/clubs') !== false)
            <link rel="canonical" href="{{request()->fullUrl()}}/"/>
        @else
            <link rel="canonical" href="{{request()->fullUrl()}}"/>
        @endif
    @endif

    {{-- TODO refacto this madness : manage head by controller --}}
    @if(strpos(request()->fullUrl(), '/clubs/') == false 
        && strpos(request()->fullUrl(), '/gimnasios/') == false
        && strpos(request()->fullUrl(), '/conditions-generales-de-vente') == false
        && strpos(request()->fullUrl(), '/clubs') == false
        && strpos(request()->fullUrl(), '/faq') == false
        && strpos(request()->fullUrl(), '/plan-du-site') == false
        && strpos(request()->fullUrl(), '/ouvrir-salle-sport-accompagnement') == false
        && strpos( request()->fullUrl(), '/blog') == false
        )
        @if(count(LaravelLocalization::getSupportedLocales()) > 1)
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                @if($localeCode == 'ma')
                    <link rel="alternate" hreflang="fr-ma" href="{{ LaravelLocalization::getLocalizedUrlByYWD(null,$localeCode ) }}" />
                    @break
                @endif

                <link rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedUrlByYWD(null,$localeCode ) }}" />
            @endforeach
        @endif
    @endif

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/all.css') }}">

    {!! $sports_organization_SEOcontext !!}

    {!! $website_SEOcontext !!}

    @include('partials.tag-commander')

    @yield('recaptcha-scripts')
</head>
