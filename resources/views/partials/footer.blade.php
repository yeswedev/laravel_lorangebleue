@php
    $pageLocale = LaravelLocalization::getCurrentLocale() ;    
@endphp

<footer>
    @include('front.components.picto')
    @include('partials.success')
    <div id="footer" class="footer-flex container-1170 container">
        @if(config('laravellocalization.showSearchInput') || $pageLocale == 'fr')
            <div class="footer-flex__interaction">
                @else
                    <div class="footer-flex__interaction" style="justify-content: center">
                        @endif
                        @if(config('laravellocalization.showSearchInput') || $pageLocale == 'fr')
                            <div class="footer-flex__item col-xl-4 col-lg-12">
                                <div class="footer-flex__item--title">
                                    <p class="text--bold">@lang('trad.header.find-club')</p>
                                </div>
                                <div class="footer-flex__item--search">
                                    {{ Form::open(array('action' => 'Front\ClubController@search', 'method' => 'get', 'class' => 'd-flex')) }}
                                    <div class="club-in_search in_search InputLocalisation">
                                        <select 
                                            name="search" 
                                            id="footer-club-search" 
                                            class="club__input in_text__input input_city"
                                        >
                                        </select>
                                        <label class="in_text__label"
                                               for="footer-club-search">@lang('trad.form.search-club-label')</label>
                                        <button type="button" class="club-in_search__span js-find-my-position">
                                            <span class="club-in_search__picto pointer-icon"
                                                  style="display: block;"></span>
                                            <span class="loader-position" style="display:none;"></span>
                                        </button>
                                    </div>

                                    <input type="hidden" name="longitude" class="longitude" value="">
                                    <input type="hidden" name="latitude" class="latitude" value="">
                                    <button type="submit" name="button"
                                            class="btn btn--bg-blue btn--t-white btn-responsive">
                                        <i class="fa fa-angle-right fa-2x btn--t-white"></i>
                                        @lang('trad.button.search')
                                    </button>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        @endif
                        <div class="footer-flex__item col-xl-4 col-lg-12">
                            <div class="footer-flex__item--title">
                                <p class="text--bold">@lang('trad.button.newsletter')</p>
                            </div>
                            <div class="footer-flex__item--subscribe">
                                {{ Form::open(array('action' => array('HomeController@newsletter'),
                                    'method' => 'GET',
                                    'class' => 'd-flex',
                                    'onsubmit' => 'return confirm("'.__('newsletter.confirm').'")'
                                )) }}
                                <div class="in_search">
                                    <div class="in_text">
                                        <input type="email" id="footerNews" name="footerNews" value=""
                                               class="in_text__input"
                                               required>
                                        <label class="in_text__label"
                                               for="footerNews">@lang('trad.form.email-label')</label>
                                        <span class="in_text__validmark"></span>
                                        {!! $errors->first('footerNews', '<p class="alert alert-danger" style="margin-top:15px;">:message</p>') !!}
                                    </div>
                                </div>
                                <button type="submit" name="button"
                                        class="js-tag-commander-news in_text__submit btn btn--bg-white btn--t-orange btn--bd-orange btn-responsive">
                                    <i class="fa fa-angle-right fa-2x btn--t-orange"></i>
                                    @lang('trad.footer.subscribe')
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="footer-flex__item col-xl-4 col-lg-12">
                            <div class="footer-flex__item--title">
                                <p class="text--bold">@lang('trad.footer.social')</p>
                            </div>
                            <div class="footer-flex__item--social">
                                <a href="{{config('link.facebook.url-'.$pageLocale)}}"
                                   target="_blank"
                                   class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                @if(config('laravellocalization.showInstagram') || $pageLocale == 'fr')
                                <a href="{{config('link.instagram.url-'.$pageLocale)}}"
                                   target="_blank"
                                   class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                @endif
                                @if(config('laravellocalization.showYoutube') || $pageLocale == 'fr')
                                    <a href="{{config('link.youtube.url-'.$pageLocale)}}"
                                       target="_blank"
                                       class="lnk__icone lnk__icone--bg-white lnk__icone--t-orange">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="container footer-flex__categ">
                        <div class="footer-flex__categ--container">
                            <ul class="footer-flex__headlist">
                                <li class="js-active-state col-lg-3 col-md-6 col-xs-12">
                                    <p class="text--bold">
                                        @lang('trad.header.activity')
                                        <i class="fa fa-angle-up fa-1x"></i>
                                        <i class="fa fa-angle-down fa-1x"></i>
                                    </p>
                                    <ul>
                                        @foreach($activities as $activity)
                                            <li>
                                                <a href="{{ route('activity.retrieve', ['activitieUrl' => $activity->url]) }}">{{$activity->title}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="js-active-state col-lg-3 col-md-6 col-xs-12">
                                    <p class="text--bold">
                                        @lang('trad.footer.equipment')
                                        <i class="fa fa-angle-up fa-1x"></i>
                                        <i class="fa fa-angle-down fa-1x"></i>
                                    </p>
                                    <ul>
                                        @foreach($equipments as $equipment)
                                            <li>
                                                <a href="{{ route('equipment.retrieve', ['equipmentUrl' => $equipment->url]) }}">{{$equipment->title}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="js-active-state col-lg-3 col-md-6 col-xs-12">
                                    <p class="text--bold">
                                        @lang('trad.footer.sport-club')
                                        <i class="fa fa-angle-up fa-1x"></i>
                                        <i class="fa fa-angle-down fa-1x"></i>
                                    </p>
                                    <ul>
                                        @if($pageLocale == 'fr')
                                            <li><a href="{{route('front.club-list', ['haute-garonne' ,'toulouse'])}}">Toulouse</a>
                                            </li>
                                            <li><a href="{{route('front.club-list', ['herault' ,'montpellier'])}}">Montpellier</a>
                                            </li>
                                            <li>
                                                <a href="{{route('front.club-list', ['gironde' ,'bordeaux'])}}">Bordeaux</a>
                                            </li>
                                            <li><a href="{{route('front.club-list', ['ille-et-vilaine' ,'rennes'])}}">Rennes</a>
                                            </li>
                                            <li><a href="{{route('front.club-list', ['bas-rhin' ,'strasbourg'])}}">Strasbourg</a>
                                            </li>
                                        @elseif($pageLocale == 'es')
                                            <li><a href="{{route('front.club-details', ['barcelona-borbo'])}}">Barcelona-Borbó</a>
                                            </li>
                                            <li><a href="{{route('front.club-details', ['barcelona-girona'])}}">Barcelona-Girona</a>
                                            </li>
                                            <li>
                                                <a href="{{route('front.club-details', ['valencia'])}}">Valencia</a>
                                            </li>
                                        @elseif($pageLocale == 'ma')
                                            <li>
                                                <a href="{{ route('front.club-details', ['marrakech-allal-el-fassi']) }}">Marrakech</a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                                    @if ($pageLocale == 'fr')
                                        <li class="js-active-state col-lg-3 col-md-6 col-xs-12">
                                            @if(config('laravellocalization.showBecameCoach') || config('laravellocalization.showPulp'))
                                                <p class="text--bold">
                                                    @lang('trad.footer.network')
                                                    <i class="fa fa-angle-up fa-1x"></i>
                                                    <i class="fa fa-angle-down fa-1x"></i>
                                                </p>
                                            @endif
                                            <ul>
                                                @if(config('laravellocalization.showBecameCoach') || $pageLocale == 'fr')
                                                    <li>
                                                        <a href="{{config('link.encp.url')}}" target="_blank">
                                                            {{ config('link.encp.label') }}
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(config('laravellocalization.showPulp') || $pageLocale == 'fr')
                                                    <li>
                                                        <a href="{{config('link.pulp.url')}}" target="_blank">
                                                            {{ config('link.pulp.label') }}
                                                        </a>
                                                    </li>
                                                @endif
                                                @if(config('laravellocalization.showBlog') || $pageLocale == 'fr')
                                                    <li>
                                                        <a href="{{route('blog.showAll')}}">
                                                            @lang('trad.header.blog')
                                                        </a>
                                                    </li>
                                                @endif
                                                <li>
                                                    <a
                                                        @if(config('laravellocalization.newLinkRecrutement'))
                                                            href="{{route('recruitment.index')}}"
                                                        @else
                                                            href="http://www.lorangebleuerecrute.fr/"
                                                        @endif
                                                    >
                                                        @lang('trad.header.recrutement')
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                            </ul>
                        </div>
                    </div>

                    <div class="footer-flex__bottom">
                        <ul>

                            <li>
                                <a href="{{route('page.retrieve', trans('routes.page.rgpd'))}}" >
                                    @lang('trad.footer.rgpd')
                                </a>
                            </li>
                            <li>-</li>
                            <li>
                                <a href="{{route('page.retrieve', trans('routes.page.legal-mentions'))}}" >
                                    @lang('trad.footer.legal-mention')
                                </a>
                            </li>
                            <li>-</li>
                            <li>
                                <a href="{{route('page.retrieve', trans('routes.page.cgv'))}}">
                                    @lang('trad.footer.cgv')
                                </a>
                            </li>
                            <li>-</li>
                            <li><a href="{{route('sitemap.html')}}">@lang('trad.footer.sitemap')</a></li>
                            <li>-</li>
                            <li><a href="{{route('faq.showAll')}}">@lang('trad.footer.faq')</a></li>
                            <li>-</li>
                            <li>
                                <a href="{{route('yako.showAll')}}">
                                    @lang('trad.footer.yako')
                                </a>
                            </li>
                        </ul>
                        <ul>
                            <li>@lang('trad.common.title')</li>
                            <li>-</li>
                            <li>@lang('trad.footer.right')</li>
                        </ul>
                    </div>
            </div>
</footer>
