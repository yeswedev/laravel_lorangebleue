@extends('nova::layout')

@section('content')
    {!! Form::open(['route' => 'BOScheduleImport', 'method' => 'post', 'files' => 'true']) !!}
        <div class="columns is-centered">
            <div class="column">
                <span class="form-file">
                    <input type="file" name="csv" id="csv">
                </span>
            </div>
            <div class="column mt-3">
                <button type='submit' class="btn btn-default btn-primary">Importer des horaires de clubs</button>
            </div>
        </div>
    {!! Form::close() !!}
    <div class="columns is-centered">
        <div class="column mt-3">
            <a href="{{ route("BOScheduleTemplate", ["template" => "schedules"]) }}" class="btn btn-default btn-primary">Télécharger un modèle</a>
        </div>
    </div>

    @if ($message)
        <div class="toasted-container bottom-right">
            <div class="toasted nova @if(!$is_err) success @else error @endif" style="touch-action: pan-y; -moz-user-select: none; opacity: 1; transform: translateY(-35px);">
                {{ $message }}
            </div>
        </div>
    @endif
@endsection