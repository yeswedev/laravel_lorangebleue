@extends('nova::layout')

@section('content')
    <div class="column mt-3">
        <a href='{{ route('BOExportPlanningsGet') }}' class="btn btn-default btn-primary">Télécharger un zip avec tous les planning</a>
    </div>
    @if ($message)
        <div class="toasted-container bottom-right">
            <div class="toasted nova @if(!$is_err) success @else error @endif" style="touch-action: pan-y; -moz-user-select: none; opacity: 1; transform: translateY(-35px);">
                {{ $message }}
            </div>
        </div>
    @endif
@endsection