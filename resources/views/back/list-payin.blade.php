@extends('nova::layout')

@section('content')

<div class="card relative">
    <div class="py-3 flex items-center border-b border-50">
        <div class="flex items-center">
            <div class="px-3">
                <div class="dropdown relative">
                    <a class="dropdown-trigger h-dropdown-trigger flex items-center cursor-pointer select-none">
                        <div class="checkbox select-none rounded"></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex items-center ml-auto px-3">
            <div class="dropdown relative"></div>
        </div>
    </div>
    <h1>{{count($entities)}} Payins</h1>
    <div class="overflow-hidden overflow-x-auto relative">
        <table cellpadding="0" cellspacing="0" class="table w-full">
            <thead>
                <tr>
                    <th class="w-16">&nbsp;</th>
                    <th class="text-left">
                        <span>Payin date de création</span>
                    </th>
                    <th class="text-left">
                        <span>Montant</span>
                    </th>
                    <th class="text-left">
                        <span>Payin MANGOPAY</span>
                    </th>
                    <th class="text-left">
                        <span>Payment BDD</span>
                    </th>
                    <th class="text-left">
                        <span>Order BDD</span>
                    </th>
                    <th class="text-left">
                        <span>Transfer potentiel</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse($entities as $element)
                    <tr
                        @if(!$element['payment'] || !$element['transfer'])
                            style="background:#ff00004d;"
                        @endif
                    >
                        <td></td>
                        <td>{{$element['date']}}</td>
                        <td>
                            @if($element['entity'])
                                <a href="https://dashboard.mangopay.com/PayIn/{{$element['entity']->Id}}">{{$element['entity']->Id}}</a>
                            @endif
                        </td>
                        <td>
                            @if($element['entity'])
                                {{$element['entity']->DebitedFunds->Amount / 100}}@lang('meta.devise')
                            @endif
                        </td>
                        <td>
                            @if($element['payment'])
                                {{$element['payment']->id}}
                            @endif
                        </td>
                        <td>
                            @if($element['order'])
                                <a href="https://www.lorangebleue.fr/admin/resources/orders/{{$element['order']->id}}">{{$element['order']->id}}</a>
                                {{$element['order']->status}}
                            @endif
                        </td>
                        <td>
                            @if($element['transfer'] && $element['transfer'] == 'A venir')
                                A venir
                            @else
                                @if($element['transfer'])
                                    <a href="https://dashboard.mangopay.com/Transfer/{{$element['transfer']->Id}}" target="_blank">
                                        {{$element['transfer']->Id}}
                                    </a>
                                @else
                                    @if($element['order'])
                                        <a href="{{route('doTransfer', $element['order']->id)}}">
                                            Lancer le transfert
                                        </a>
                                    @endif
                                @endif
                            @endif

                        </td>
                    </tr>
                @empty
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="bg-20 rounded-b">
        <nav class="flex justify-between items-center"></nav>
    </div>
</div>


@endsection
