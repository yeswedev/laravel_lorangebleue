@extends('nova::layout')

@section('content')

<div class="card relative">
    <div class="py-3 flex items-center border-b border-50">
        <div class="flex items-center">
            <div class="px-3">
                <div class="dropdown relative">
                    <a class="dropdown-trigger h-dropdown-trigger flex items-center cursor-pointer select-none">
                        <div class="checkbox select-none rounded"></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex items-center ml-auto px-3">
            <div class="dropdown relative"></div>
        </div>
    </div>
    <h1>{{count($entities)}} Payouts Failed</h1>
    <div class="overflow-hidden overflow-x-auto relative">
        <table cellpadding="0" cellspacing="0" class="table w-full">
            <thead>
                <tr>
                    <th class="w-16">&nbsp;</th>
                    <th class="text-left">
                        <span>Payout date de création</span>
                    </th>
                    <th class="text-left">
                        <span>Payout</span>
                    </th>
                    <th class="text-left">
                        <span>Wallet</span>
                    </th>
                    <th class="text-left">
                        <span>Message</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse($entities as $element)
                    <tr>
                        <td></td>
                        <td>{{$element['date']}}</td>
                        <td>
                            @if($element['entity'])
                                <a href="https://dashboard.mangopay.com/Payout/{{$element['entity']->Id}}" target="_blank">
                                    {{$element['entity']->Id}}
                                </a>
                            @endif
                        </td>
                        <td>
                            @if($element['debitedWallet'])
                                <a href="https://dashboard.mangopay.com/User/{{$element['debitedWallet']->Owners[0]}}/Wallets/{{$element['debitedWallet']->Id}}" target="_blank">
                                    {{$element['debitedWallet']->Description}}
                                </a>
                            @endif
                        </td>
                        <td>
                            @if($element['entity'])
                                {{$element['entity']->ResultMessage}}
                            @endif
                        </td>
                    </tr>
                @empty
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="bg-20 rounded-b">
        <nav class="flex justify-between items-center"></nav>
    </div>
</div>


@endsection
