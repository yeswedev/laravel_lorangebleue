<div class="languages">
    <div class="dropdown-nav">
        <span class="arrow down"></span>
        <span id="fr" class="langues">FR</span>
    </div>
    <ul class="dropdown-menu-nav">
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties) @if($localeCode == 'fr')
        <li class="lang-item-fr">
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                FR
            </a>
        </li>
        @elseif($localeCode == 'en')
        <li class="lang-item-en">
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                EN
            </a>
        </li>
        @elseif($localeCode == 'es')
        <li class="lang-item-en">
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                ES
            </a>
        </li>
        @endif @endforeach
    </ul>
</div>