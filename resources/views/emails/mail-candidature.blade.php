Civilité : {{ $data['civility'] }}{{PHP_EOL}}
@lang('trad.mail.contact-page.lastname'){{ strtoupper($data['name']) }}{{PHP_EOL}}
@lang('trad.mail.contact-page.firstname'){{ ucfirst($data['firstname'])}}{{PHP_EOL}}
@lang('trad.mail.mail-candidature.mail'){{ $data['email'] }}{{PHP_EOL}}
@lang('trad.mail.mail-candidature.phone'){{ $data['phone'] }}{{PHP_EOL}}
@lang('trad.mail.mail-candidature.exam1')@if($data['diploma1'] != ''){{ $data['diploma1'] }}@else @lang('trad.mail.mail-candidature.null')@endif{{PHP_EOL}}
@lang('trad.mail.mail-candidature.exam2')@if($data['diploma2'] != ''){{ $data['diploma2'] }}@else @lang('trad.mail.mail-candidature.null')@endif{{PHP_EOL}}
@lang('trad.mail.mail-candidature.availabality'){{ $data['disponibility'] }}{{PHP_EOL}}
@lang('trad.mail.mail-candidature.mobility')@if(array_key_exists('mobility', $data)){{ implode(", ", $data['mobility']) }}@else@lang('trad.mail.mail-candidature.null') @endif{{PHP_EOL}}
@lang('trad.mail.mail-candidature.hour')@if( $data['hours'] == 'full') @lang('trad.mail.mail-candidature.hour2')@elseif( $data['hours'] == 'part') @lang('trad.mail.mail-candidature.hour3')@endif{{PHP_EOL}}
@lang('trad.mail.mail-candidature.country')
@if( $data['country'] == 'fr')
 @lang('trad.mail.mail-candidature.france'){{PHP_EOL}}
@elseif( $data['country'] == 'es')
 @lang('trad.mail.mail-candidature.espagne'){{PHP_EOL}}
@elseif( $data['country'] == 'ma')
 @lang('trad.mail.mail-candidature.maroc'){{PHP_EOL}}
@endif
@lang('trad.mail.mail-candidature.city')@if($data['city'] != '') {{ $data['city'] }} @else @lang('trad.mail.mail-candidature.null')@endif{{PHP_EOL}}
@lang('trad.mail.mail-candidature.zipcode')@if($data['postal_code'] != '') {{ $data['postal_code'] }} @else @lang('trad.mail.mail-candidature.null')@endif{{PHP_EOL}}
@lang('trad.mail.mail-candidature.attachments'){{PHP_EOL}}