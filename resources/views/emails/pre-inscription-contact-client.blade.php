@component('mail::message')

<div style="text-align:center;">
    <a class="logo" href="{{ config('app.url') }}" style="width: 170px;height: 72px;display: block;margin: 0 auto;"><img src="{{asset('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo l Orange Bleue"></a>
</div>

<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">@lang('trad.mail.pre-inscription-contact-client.title')</h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">

    <div style="text-align:center;padding: 5px 0;">
        <p style="text-align:center;margin:0;font-size:12px;">@lang('trad.mail.pre-inscription-contact-client.description'){{$data['clubTitle']}}.</p>
    </div>

    @include('emails.partials.club-infos-block')

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.pre-inscription-contact-client.sportivement')
            <br>
            @lang('trad.mail.pre-inscription-contact-client.team')
        </p>
    </div>

</div>

<div style="text-align:center;width:100%;display:block;margin:0 auto;">
    @component('mail::button', ['url' => route('front.club-details', $data['clubUrl']), 'color' => 'orangeBleue'])

        @lang('trad.mail.pre-inscription-contact-client.link')

    @endcomponent
</div>

@endcomponent
