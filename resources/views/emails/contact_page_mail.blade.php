@component('mail::message')

<div style="text-align:center;">
    <a class="logo" href="{{ config('app.url') }}" style="width: 170px;height: 72px;display: block;margin: 0 auto;"><img src="{{asset('/images/logo-groupe.svg')}}" width="100%" height="100%"></a>
</div>

<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">@lang('trad.mail.contact-page.title'){{ config('contact.'.$data['subject'].'.label') }}</h3>
</div>
      
<div style="text-align:center;width:90%;display:block;margin:0 auto;">

    <div style="padding: 5px 0;">
        <p style="margin:0;font-size:12px;">@lang('trad.mail.contact-page.description')</p>
        <br>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.mail'){{ $data['email'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.phone'){{ $data['phone'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.lastname'){{ $data['name'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.firstname'){{ $data['firstname'] }}</p>
        @isset($data['zip_code'])
            <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.contact-open-club.mail-zip') : {{ $data['zip_code'] }}</p>
        @endisset
        @isset($data['city'])
            <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.contact-open-club.mail-city') : {{ $data['city'] }}</p>
        @endisset
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.message')</p>
        <p style="text-align:left;margin:0;font-size:12px;">{{ $data['message'] }}</p>
    </div>

</div>

@endcomponent
