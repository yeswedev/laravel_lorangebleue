@component('mail::message')
<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">@lang('trad.mail.mail-contact.title')</h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">
    <p style="margin:0;font-size:12px;">@lang('trad.mail.contact-page.landing-page')</p>
    <div style="padding: 5px 0;">
        <p style="margin:0;font-size:12px;">@lang('trad.mail.contact-page.description')</p>
        <br>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.mail'){{ $data['email'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.phone'){{ $data['phone_number'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.lastname'){{ $data['lastname'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.firstname'){{ $data['firstname'] }}</p>
    </div>
</div>
@endcomponent
