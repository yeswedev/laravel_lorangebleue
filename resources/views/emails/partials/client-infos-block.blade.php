<div style="text-align:left;padding: 5px 0;">
    <ul style="padding:0;margin:0;font-size:12px;">
        <li><span style="font-size:12px;">@lang('trad.mail.confirmation-club.firstname')<strong>{{$data['clientFirstname']}}</strong></span></li>
        <li><span style="font-size:12px;">@lang('trad.mail.confirmation-club.lastname')<strong>{{$data['clientLastname']}}</strong></span></li>
        <li><span style="font-size:12px;">@lang('trad.mail.confirmation-club.mail')<strong>{{$data['clientEmail']}}</strong></span></li>
        <li><span style="font-size:12px;">@lang('trad.mail.confirmation-club.phone')<strong>{{$data['clientPhone']}}</strong></span></li>
    </ul>
</div>
