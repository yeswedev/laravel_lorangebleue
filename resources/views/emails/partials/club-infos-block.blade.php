<div style="text-align:left;padding: 5px 0;">
    <ul style="padding:0;margin:0;font-size:12px;">
        <li style="list-style: none;margin:0;font-size:12px;">{{$data['clubTitle']}}</li>
        <li style="list-style: none;margin:0;font-size:12px;">{{$data['clubAddress']}}</li>
        <li style="list-style: none;margin:0;font-size:12px;">{{$data['clubZipCode']}} {{$data['clubCity']}}</li>
        @if($data['clubPhone'])
            <li style="list-style: none;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.phone'){{$data['clubPhone']}}</li>
        @endif
        @if($data['clubEmail'])
            <li style="list-style: none;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.mail'){{$data['clubEmail']}}</li>
        @endif
    </ul>
</div>
