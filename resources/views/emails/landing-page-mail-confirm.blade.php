@component('mail::message')
<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">@lang('trad.mail.mail-contact.title')</h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">
    <p style="margin:0;font-size:12px;">{{ __('trad.mail.mail-landing-page.message', ['club' => $data['club']->title]) }}</p>
</div>
@endcomponent
