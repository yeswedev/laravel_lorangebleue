@component('mail::message')

<div style="text-align:center;">
    <a href="{{ config('app.url') }}" style="width: 170px;height: 72px;display: block;margin: 0 auto;"><img src="{{asset('/images/logo-groupe.svg')}}" alt="logo l Orange Bleue"></a>
</div>

<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">@lang('trad.mail.pre-inscription-contact.title')</h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:center;margin:0;font-size:12px;">
            @lang('trad.mail.pre-inscription-contact.description') <strong>{{$data['clubTitle']}}</strong> :
        </p>
    </div>

    @include('emails.partials.client-infos-block')

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.pre-inscription-contact.sportivement')
            <br>
            @lang('trad.mail.pre-inscription-contact.team')
        </p>
    </div>

</div>

@endcomponent
