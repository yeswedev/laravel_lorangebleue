@component('mail::message')
<div style="text-align:center;">
    <a class="logo" href="{{ config('app.url') }}" style="width: 170px;height: 72px;display: block;margin: 0 auto;">
        <img src="{{asset('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo l Orange Bleue">
    </a>
</div>

<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">
        @lang('trad.mail.confirmation-client.bonjour'){{$data['clientFirstname']}}
    </h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">
    <div style="text-align:center;padding: 5px 0;">
        <p style="text-align:center;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.bienvenue')</p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-client.merci'){{lcfirst($data['subscriptionType'])}}
            @lang('trad.mail.confirmation-client.merci2')
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-client.confirmation-paiement')
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-client.important')</span></strong>@lang('trad.mail.confirmation-client.finaliser'){{$data['orderCode']}}
            @lang('trad.mail.confirmation-client.finaliser2')
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span
                    style="font-size:12px;">@lang('trad.mail.confirmation-client.date'){{$data['orderCreatedDate']}}</span></strong>
        </p>
    </div>

    <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-client.club')</span></strong>
    @include('emails.partials.club-infos-block')

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span
                    style="font-size:12px;">@lang('trad.mail.confirmation-client.formule')</span> {{$data['offerTitle']}} {{$data['offerPrice']}}
                @lang('meta.devise')</strong>
            <br>
            @isset($data['orderIsStudent'])
                @if($data['orderIsStudent'] == 1)
                    <span>« Sous réserve d’un justificatif à jour »</span>
                @endif
            @endisset
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-client.paiement')</span> @lang('trad.mail.confirmation-client.paiement2')</strong>
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-client.montant')</span> {{$data['totalPrice']}}@lang('meta.devise')</strong>
        </p>
    </div>

    <div style="text-align:center;padding: 5px 0;">
        <p style="text-align:center;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.remerciement')</p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-client.sportivement')
            <br>
            @lang('trad.mail.confirmation-client.team')
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-client.conditions')<a
                href="http://www.lorangebleue.fr/cgv"></a>
        </p>
    </div>

</div>
@endcomponent
