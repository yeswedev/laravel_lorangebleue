@component('mail::message')

<div style="text-align:center;">
    <a class="logo" href="{{ config('app.url') }}" style="width: 170px;height: 72px;display: block;margin: 0 auto;"><img src="{{asset('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo l Orange Bleue"></a>
</div>

<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">@lang('trad.mail.newsletter.title')</h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">

    <div style="text-align:center;padding: 5px 0;">
        <p style="text-align:center;margin:0;font-size:12px;">@lang('trad.mail.newsletter.description')</p>
    </div>

</div>

<div style="text-align:center;width:100%;display:block;margin:0 auto;">
    @component('mail::button', ['url' => route('index'), 'color' => 'orangeBleue'])

        @lang('trad.mail.newsletter.link')

    @endcomponent
</div>

@endcomponent
