@component('mail::message')
      
<div style="text-align:center;width:90%;display:block;margin:0 auto;">

    <div style="padding: 5px 0;">
        <p style="margin:0;font-size:12px;">@lang('trad.mail.mail-contact-tunnel-abo.description')</p>
        <br>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.lastname'){{ $data['lastname'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.firstname'){{ $data['firstname'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.contact-page.mail'){{ $data['email'] }}</p>
        <p style="text-align:left;margin:0;font-size:12px;">@lang('trad.mail.confirmation-client.phone'){{ $data['phone'] }}</p>
    </div>

</div>

@endcomponent
