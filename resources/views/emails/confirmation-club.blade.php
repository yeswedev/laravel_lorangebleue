@component('mail::message')
<div style="text-align:center;">
    <a class="logo" href="{{ config('app.url') }}" style="width: 170px;height: 72px;display: block;margin: 0 auto;">
        <img src="{{asset('/images/logo-groupe.svg')}}" width="100%" height="100%" alt="logo l Orange Bleue">
    </a>
</div>

<div style="text-align:center;">
    <h3 style="text-align:center;text-transform:uppercase;margin-top: 25px;">
       @lang('trad.mail.confirmation-club.title')
    </h3>
</div>

<div style="text-align:center;width:90%;display:block;margin:0 auto;">
    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-club.confirmation-paiement')
        </p>
    </div>

    @include('emails.partials.client-infos-block')

    <div>
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-club.code')<strong>{{$data['orderCode']}}</strong> 
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span
                    style="font-size:12px;">@lang('trad.mail.confirmation-club.date'){{$data['orderCreatedDate']}}</span></strong>
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <ul style="padding:0;margin:0;font-size:12px;">
            <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-club.club'){{$data['clubTitle']}}</span></strong>
            <li style="list-style: none;margin:0;font-size:12px;">{{$data['clubAddress']}}</li>
            <li style="list-style: none;margin:0;font-size:12px;">{{$data['clubZipCode']}} {{$data['clubCity']}}</li>
            @if($data['clubPhone'])
                <li style="list-style: none;margin:0;font-size:12px;">@lang('trad.mail.confirmation-club.phone'){{$data['clubPhone']}}</li>
            @endif
            @if($data['clubEmail'])
                <li style="list-style: none;margin:0;font-size:12px;">@lang('trad.mail.confirmation-club.mail'){{$data['clubEmail']}}</li>
            @endif
        </ul>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span
                    style="font-size:12px;">@lang('trad.mail.confirmation-club.formule')</span> {{$data['offerTitle']}} {{$data['offerPrice']}}
                @lang('meta.devise')</strong>
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-club.paiement')</span> @lang('trad.mail.confirmation-club.paiement2')</strong>
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            <strong><span style="font-size:12px;">@lang('trad.mail.confirmation-club.montant')</span> {{$data['totalPrice']}}@lang('meta.devise')</strong>
        </p>
    </div>

    <div style="text-align:left;padding: 5px 0;">
        <p style="text-align:left;margin:0;font-size:12px;">
            @lang('trad.mail.confirmation-club.sportivement')
            <br>
            @lang('trad.mail.confirmation-club.team')
        </p>
    </div>

</div>
@endcomponent
