<div class="cms-row">
    <div class="cms-col-6">
        <div class="">
            <h2 class="cms-h2">{!! $block->title !!}</h2>
            <div class="cms-content">@replace($block->description,"h1","h2")</div>
        </div>
    </div>
    <div class="cms-col-6">
        @forelse($block->attachments as $attachment)
            <div class="cms-image">
                <img src="{{Storage::url($attachment->src)}}" alt="{{$attachment->src}}"/>
            </div>
        @empty
        @endforelse
    </div>
</div>
