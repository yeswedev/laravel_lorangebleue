<div class="cms-row">
    <div class="cms-col-12">
        <div class="">
            @if($block->title && $block->title != '')
                <h2 class="cms-h2">{!! $block->title !!}</h2>
            @endif
            <div class="cms-content">@replace($block->description,"h1","h2")</div>
        </div>
    </div>
</div>
