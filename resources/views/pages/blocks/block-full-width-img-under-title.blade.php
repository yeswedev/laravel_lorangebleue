<div class="cms-row">
    <div class="cms-col-12">
        <div class="">
            <h2 class="cms-h2">{!! $block->title !!}</h2>
            @forelse($block->attachments as $attachment)
                <div class="img-under-title">
                    <img src="{{Storage::url($attachment->src)}}" alt="{{$attachment->src}}"/>
                </div>
                @empty
            @endforelse
            <div class="cms-content">@replace($block->description,"h1","h2")</div>
        </div>
    </div>
</div>
