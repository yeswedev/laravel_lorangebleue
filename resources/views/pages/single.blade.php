<div class="cms-header">
    <div class="cms-overlay"></div>
    <div class="cms-header__content">
        <h1 class="cms-title text-white"> {{ $page->title }} </h1>
        <div class="cms-description text-white">
            @replace($page->description,"h1","h2")
        </div>
    </div>
</div>

@isset($open_club)
    @include('front.contact.open-club')
@endisset

<div class="cms-container">
    @isset($page)
        @if(count($page->blocks) > 0)
            @forelse($page->blocks as $block)
                @include('pages.blocks.'.$block->layout)
            @empty
            @endforelse
        @endif
    @endisset
</div>
