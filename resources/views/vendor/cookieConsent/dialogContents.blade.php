<div class="js-cookie-consent cookie-consent">
    <div class="cookie-flex">
        <p class="cookie-consent__message">
            {!! trans('cookieConsent::texts.message') !!}
            <a href="{{ route('page.retrieve', trans('routes.page.legal-mentions')) }}"
               class="lnk--blue lnk--underline">@lang('trad.button.more')
            </a>
        </p>

        <button class="js-cookie-consent-agree cookie-consent__agree btn btn--bg-orange btn--t-white">
            {{ trans('cookieConsent::texts.agree') }}
        </button>
    </div>
</div>
