@if (count(Nova::availableResources(request())))
    <h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
        <svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path fill="var(--sidebar-icon)" d="M3 1h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2H3c-1.1045695 0-2-.8954305-2-2V3c0-1.1045695.8954305-2 2-2zm0 2v4h4V3H3zm10-2h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2h-4c-1.1045695 0-2-.8954305-2-2V3c0-1.1045695.8954305-2 2-2zm0 2v4h4V3h-4zM3 11h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2H3c-1.1045695 0-2-.8954305-2-2v-4c0-1.1045695.8954305-2 2-2zm0 2v4h4v-4H3zm10-2h4c1.1045695 0 2 .8954305 2 2v4c0 1.1045695-.8954305 2-2 2h-4c-1.1045695 0-2-.8954305-2-2v-4c0-1.1045695.8954305-2 2-2zm0 2v4h4v-4h-4z"
            />
        </svg>
        <span class="sidebar-label">{{ __('Resources') }}</span>
    </h3>

    @foreach(Nova::groupedResources(request()) as $group => $resources)
        @if (count($resources) > 0)
            @php
                $my_resources = [];
                foreach ($resources as $r) {
                    $my_resources[] = $r::uriKey();
                }
            @endphp

            <collapsible-groups header="{{ $group }}" :last="@json($loop->last)"
                    :expanded="@if(in_array(explode("/",url()->current())[count(explode("/",url()->current()))-1],$my_resources)) true @else false @endif">
                @foreach($resources as $key => $resource)
                    @if (! $resource::$displayInNavigation)
                        @continue
                    @endif

                    <li class="leading-tight mb-4 ml-8 text-sm" key="{{ $key }}">
                        <router-link :to="{
                            name: 'index',
                            params: {
                                resourceName: '{{ $resource::uriKey() }}'
                            }
                        }" class="text-white text-justify no-underline dim">
                            {{ $resource::label() }}
                        </router-link>
                        @if($resource::uriKey() == 'moderations' && count(\YesWeDev\LaravelModo\Moderation::get()) > 0)
                            <strong><span class="notification">{{count(\YesWeDev\LaravelModo\Moderation::get())}}</span></strong>
                        @endif
                    </li>
                @endforeach
            </collapsible-groups>
        @endif
    @endforeach
    @if(auth()->user())
        @if(auth()->user()->isAdmin())
        <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">Mangopay</h4>
        <ul class="list-reset mb-8">
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim" href="{{ route('listPayin') }}">Liste Payins</a></li>
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim"  href="{{ route('listTransferFailed') }}">Liste erreur Transfer</a></li>
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim"  href="{{ route('listPayoutFailed') }}">Liste erreur Payout</a></li>
        </ul>
        <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">Imports</h4>
        <ul class="list-reset mb-8">
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim" href="{{ route('BOSchedule') }}">Importer des horaires</a></li>
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim" href="{{ route('BOIsFreeAccess') }}">Import des accès libre</a></li>
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim" href="{{ route('BOLinkToClub') }}">Liaison Yakos/Activitées Clubs</a></li>
        </ul>
        <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">Exports</h4>
        <ul class="list-reset mb-8">
            <li class="leading-tight mb-4 ml-8 text-sm"><a class="text-white text-justify no-underline dim" href="{{ route('BOExportPlannings') }}">Export du planning des clubs</a></li>
        </ul>
        @endif
    @endisset

@endif
