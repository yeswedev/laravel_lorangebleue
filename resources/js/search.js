import autocomplete from './gplaces-autocomplete'

let lang = document.documentElement.lang;
let mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_FR;

switch (lang) {
    case 'fr':
        mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_FR;
        break;

    case 'es':
        mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_ES;
        break;

    case 'ma':
        mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_MA;
        break;
}

$(document).ready(function() {
    $('.js-find-my-position').click(function() {
        if (navigator.geolocation) {
            $('.loader-position').css({ display: 'block' });
            $('.pointer-icon').hide();
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    $('.latitude').val(position.coords.latitude);
                    $('.longitude').val(position.coords.longitude);

                    $.ajax({
                        // Build it with https://docs.mapbox.com/search-playground
                        url: `https://api.mapbox.com/geocoding/v5/mapbox.places/${position.coords.longitude}%2C${position.coords.latitude}.json?access_token=${mapbox_token}&cachebuster=1562755838346&autocomplete=true&types=place%2Cregion`,
                        success: (resp)=>{
                            const results = resp.features
                            var department = results.find( f=> f.place_type[0] === 'region').text;
                            var city = results.find( f=> f.place_type[0] === 'place').text;

                            $('.department').val(department);
                            $('.jscity').val(city);
                        },
                        dataType: 'json'
                      });

                    const search = document.documentElement.lang.toLowerCase() === 'es'
                      ? 'A mi alrededor'
                      : 'Autour de moi'

                    $('#main-club-search, #footer-club-search')
                        .select2({
                            data: [{id: search, text: search, selected: true}],
                            ajax: autocomplete
                        })

                        $('.loader-position').hide();
                    $('.pointer-icon').show();
                },
                function(error) {
                    if (error.PERMISSION_DENIED) {
                        $('.popup-error').show();
                        if (document.documentElement.lang.toLowerCase() === 'es') {
                            alert(
                                'Por favor habilite la geolocalización de su dispositivo o navegador'
                            );
                        } else {
                            alert(
                                'Veuillez activer la géolocalisation de votre appareil ou navigateur'
                            );
                        }
                    }
                    $('.loader-position').hide();
                    $('.pointer-icon').show();
                }
            );
        }
    });
});
