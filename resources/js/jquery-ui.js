require('./libs/jquery-ui.min');
require('./libs/datepicker-fr');

$(document).ready(function() {
    $('.birthdate-picker').datepicker({
        autoclose: true,
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
        defaultDate: '-20y',
        minDate: '-100Y',
        yearRange: '-100:+0',
        changeMonth: true,
        changeYear: true,
    });
});
