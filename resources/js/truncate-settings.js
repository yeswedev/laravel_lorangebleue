require('./libs/JQuery.fewlines.min');

$('.concepts-tiles__body p').fewlines({
    noAction: true,
    lines: 4,
});

$('.blog-tiles__title.blog-tiles__title').fewlines({
    noAction: true,
    lines: 2,
});

$('meta[property="og:description"]').attr('content', $('.article__content_body').text().substr(0, 415) + "...");
$('meta[name="twitter:description"]').attr('content', $('.article__content_body').text().substr(0, 415) + "...");

// show file title when selected
$('#chooseFile').bind('change', function () {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
      $(".file-upload").removeClass('active');
      $("#noFile").text("No file chosen...");
    }
    else {
      $(".file-upload").addClass('active');
      $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
