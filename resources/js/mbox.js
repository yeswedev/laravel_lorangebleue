var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

let lang = document.documentElement.lang;
mapboxgl.accessToken = process.env.MIX_MAPBOX_ACCESS_TOKEN_FR;

switch (lang) {
    case 'fr':
        mapboxgl.accessToken = process.env.MIX_MAPBOX_ACCESS_TOKEN_FR;
        break;

    case 'es':
        mapboxgl.accessToken = process.env.MIX_MAPBOX_ACCESS_TOKEN_ES;
        break;

    case 'ma':
        mapboxgl.accessToken = process.env.MIX_MAPBOX_ACCESS_TOKEN_MA;
        break;
}

/**
 * Add marker with data on selected map
 * @param {*} data the marker returned by controller
 * @param {mapboxgl.Map} map the map instance
 * @param {String} context the ... context : map on header div, footer map, modal map ...
 */
const putMarkerOnMap = (data, map, context) => {
    const club = data.club
    const coords = [club.longitude, club.latitude]

    const imgMarker = $('<img>')
        .attr('src', club.is_open ? '/images/markerclub.png' : '/images/markerclub-grey.png' )
        .on('click', () => {
            if (context !== 'header-map')
                map.flyTo({
                    center: coords,
                    zoom: 14,
                    curve: 1
                    })
        });

    const infowindow = new mapboxgl.Popup({ offset: 25 })
        .setHTML(data.html);

    new mapboxgl.Marker({
        element: imgMarker[0],
        title: club.title,
    })
        .setLngLat(coords)
        .setPopup(infowindow) 
        .addTo(map)
}

const putOfferMarkerOnMap = (offer, map) => {
    var links = "";
    for (let i = 0; i < offer.names.length; i++) {
        links += "<a target='_blank' href='/recrutement/offres/"+offer.ids[i]+"'>" +
            "<p class='recruitment__content-subtitle' style='color: #004589; display: block; padding-right: 10px;'>" +
            offer.names[i] +
            "</p>" +
            "</a>";
    }

    const infowindow = new mapboxgl.Popup({ offset: 25 })
        .setHTML("<div class='recruitment__content' style='min-width: auto;'>" + links + "</div>");

    const imgMarker = $('<div>')
        .append($('<img>').attr('src', '/images/markerclub.png'))
        .append($('<p>')
                .text(offer.number)
                .addClass('recruitment-sidebar__marker-text')
        )
        
    new mapboxgl.Marker({
        element: imgMarker[0],
        anchor: 'bottom',
    })
        .setLngLat([parseFloat(offer.lng), parseFloat(offer.lat)])
        .setPopup(infowindow) 
        .addTo(map)
}

$(() => {
    loadMap();
});

const loadMap = () => {

    $('<img>').html('images/markerclub.png').append('.fa')

    var lat = $('.lat-lng-club').data('club-lat');
    var lng = $('.lat-lng-club').data('club-lng');

    var clubSingle = $('.lat-lng-club').data('club-single');
    var clubsString = $('.data-clubs').data('clubs');

    const context = $('#map').data('context')

    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        // If no lng/lat, set default to France
        center: [
            parseFloat(lng) ? parseFloat(lng): 2.4,
            parseFloat(lat) ? parseFloat(lat) : 46.8
        ],
        zoom: 10
    })
    $('.js-map-style-changer').on('click', function(){
        switch($(this).attr('id')){
            case 'plan':
                map.setStyle('mapbox://styles/mapbox/streets-v9');
                break;
            case 'satellite':
                map.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
                break;
        }
    })

    map.scrollZoom.disable();
    map.addControl(new mapboxgl.NavigationControl());

    let url ;

    if(clubsString && context === 'find-club')
        url = '/api/v1/clubs?' + clubsString ;
    else if (clubSingle && context === 'club-details')
        url = '/api/v1/clubs/' + clubSingle
    else if (context === 'recruitement-offers'){
        map.setZoom(4);
        map.scrollZoom.enable();
        map.addControl(new mapboxgl.FullscreenControl());

        // TODO format city values to only use one "putMarkerOnMap" function
        offerCities.forEach( city => putOfferMarkerOnMap(city, map))
    }

    if(url)
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url,
            success: function(resp) {
                if (Array.isArray(resp))
                    resp.forEach( data => putMarkerOnMap(data, map))
                else
                    putMarkerOnMap(resp, map)

                if(context === 'club-details'){
                    // On page 'club-details', if "Afficher sur la carte" or "Carte" is clicked, instantiate it.
                    $('.js-switch-state.js-toggle-map, .js-show-on-map').each(function(){$(this).on('click', _=>{
                            const headerMap = new mapboxgl.Map({
                                container: 'header-map',
                                style: 'mapbox://styles/mapbox/streets-v11',
                                center: [parseFloat(lng) + 0.009, parseFloat(lat)],
                                zoom: 15
                            })

                            putMarkerOnMap(resp, headerMap,'header-map')

                            $('.js-map-style-changer').on('click', function(){
                                switch($(this).attr('id')){
                                    case 'plan':
                                        headerMap.setStyle('mapbox://styles/mapbox/streets-v9');
                                        break;
                                    case 'satellite':
                                        headerMap.setStyle('mapbox://styles/mapbox/satellite-streets-v11');
                                        break;
                                }
                            })
                        }
                    )})

                    $(".club__filter__span--btn-photo").on('click', function() {
                        $('.header-map__controls').addClass('header-map__controls--hide')
                    })

                    $(".club__filter__span--carte").on('click', function() {
                        $('.header-map__controls').removeClass('header-map__controls--hide')
                    })
                }
            },
        });
};

export default loadMap;