let commanderAboSelectClub = document.querySelectorAll('.js-tag-commander-abo-select-club');
for (let i = 0; i < commanderAboSelectClub.length; i++) {
    commanderAboSelectClub[i].addEventListener('click', function() {
        let id = 'abo-select-club',
            type_page = 'page_liste',
            user_ville_club = commanderAboSelectClub[i].dataset.city +'_'+ commanderAboSelectClub[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(
                this,
                id,
                {
                    type_page: type_page,
                    env_country: env_country.toUpperCase(),
                    user_ville_club: user_ville_club,
                }
            );
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called');
                console.log('--- tc_elt --- ');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
            }
        }
    });
}

let commanderAboPreInsc = document.querySelectorAll('.js-tag-commander-abo-pre-insc');
for (let i = 0; i < commanderAboPreInsc.length; i++) {
    commanderAboPreInsc[i].addEventListener('click', function() {
        let id = 'abo-pre-insc',
            type_page = 'page_liste',
            user_ville_club = commanderAboPreInsc[i].dataset.city +'_'+ commanderAboPreInsc[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(
                this,
                id,
                {
                    type_page: type_page,
                    env_country: env_country.toUpperCase(),
                    user_ville_club: user_ville_club,
                }
            );
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called');
                console.log('--- tc_elt --- ');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
            }
        }
    });
}

let commanderAboBan = document.querySelectorAll('.js-tag-commander-abo-ban');
for (let i = 0; i < commanderAboBan.length; i++) {
    commanderAboBan[i].addEventListener('click', function(e) {
        let id = 'abo-ban',
            type_page = 'page_liste',
            user_ville_club = commanderAboBan[i].dataset.city +'_'+ commanderAboBan[i].dataset.club,
            ville_club = commanderAboBan[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called');
                console.log('--- tc_elt --- ');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
                console.log('ville_club : ' + ville_club);
            }
            tc_events_20(
                this,
                id,
                {
                    type_page: type_page,
                    env_country: env_country.toUpperCase(),
                    user_ville_club: user_ville_club,
                    ville_club: ville_club
                }
            );

        }
    });
}

let commanderMailTo = document.querySelectorAll('.js-tag-commander-mailto');
for (let i = 0; i < commanderMailTo.length; i++) {
    commanderMailTo[i].addEventListener('submit', function() {
        let id = 'mail-to',
            type_page = "{{$type_page or ''}}",
            user_ville_club = commanderMailTo[i].dataset.city +'_'+ commanderMailTo[i].dataset.club,
            ville_club = commanderMailTo[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, id, {
                type_page: type_page,
                env_country: env_country.toUpperCase(),
                user_ville_club: user_ville_club,
                ville_club: ville_club,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called');
                console.log('--- tc_elt --- ');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
                console.log('ville_club : ' + ville_club);
            }
        }
    });
}

let commanderMail = document.querySelectorAll('.js-tag-commander-mail');
for (let i = 0; i < commanderMail.length; i++) {
    commanderMail[i].addEventListener('click', function() {
        let id = 'clic-mail',
            type_page = "{{$type_page or ''}}",
            user_ville_club = commanderMail[i].dataset.city +'_'+ commanderMail[i].dataset.club,
            ville_club = commanderMail[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, id, {
                type_page: type_page,
                env_country: env_country.toUpperCase(),
                user_ville_club: user_ville_club,
                ville_club: ville_club,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called :');
                console.log('--- tc_elt ---');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
                console.log('ville_club : ' + ville_club);
            }
        }
    });
}

let commanderTel = document.querySelectorAll('.js-tag-commander-tel');
for (let i = 0; i < commanderTel.length; i++) {
    commanderTel[i].addEventListener('click', function() {
        let id = 'clic-tel',
            type_page = "{{$type_page or ''}}",
            user_ville_club = commanderTel[i].dataset.city +'_'+ commanderTel[i].dataset.club,
            ville_club = commanderTel[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, id, {
                type_page: type_page,
                env_country: env_country.toUpperCase(),
                user_ville_club: user_ville_club,
                ville_club: user_ville_club,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called :');
                console.log('--- tc_elt ---');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
                console.log('ville_club : ' + ville_club);
            }
        }
    });
}

let commanderAbo = document.querySelectorAll('.js-tag-commander-abo');
for (let i = 0; i < commanderAbo.length; i++) {
    commanderAbo[i].addEventListener('click', function() {
        let id = 'abo',
            type_page = "{{$type_page or ''}}",
            user_ville_club = commanderAbo[i].dataset.city +'_'+ commanderAbo[i].dataset.club,
            ville_club = commanderAbo[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, id, {
                type_page: type_page,
                env_country: env_country.toUpperCase(),
                user_ville_club: user_ville_club,
                ville_club: ville_club,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called :');
                console.log('--- tc_elt ---');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
                console.log('ville_club : ' + ville_club);
            }
        }
    });
}

let commanderAboListClubPage = document.querySelectorAll('.js-tag-commander-abo-list-club-page');
for (let i = 0; i < commanderAboListClubPage.length; i++) {
    commanderAboListClubPage[i].addEventListener('click', function() {
        let id = 'abo-liste',
            type_page = 'page_liste',
            user_ville_club = commanderAboListClubPage[i].dataset.city +'_'+ commanderAboListClubPage[i].dataset.club,
            ville_club = commanderAboListClubPage[i].dataset.club;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, id, {
                type_page: type_page,
                env_country: env_country.toUpperCase(),
                user_ville_club: user_ville_club,
                ville_club: user_ville_club,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called :');
                console.log('--- tc_elt ---');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
                console.log('ville_club : ' + ville_club);
            }
        }
    });
}

let commanderNews = document.querySelectorAll('.js-tag-commander-news');
for (let i = 0; i < commanderNews.length; i++) {
    commanderNews[i].addEventListener('click', function() {
        let id = 'souscrire',
            user_email = document.getElementById('footerNews').value;

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, id, {
                user_email: user_email,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called :');
                console.log('--- tc_elt ---');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(id);
                console.log('--- tc_array_events ---');
                console.log('user_email : ' + user_email);
            }
        }
    });
}

function commanderAboClubPage(user_ville_club, product_name) {
    let id = 'abo-club',
        type_page = 'page_liste';

    if (typeof tc_events_20 === 'function') {
        tc_events_20(this, id, {
            type_page: type_page,
            env_country: env_country.toUpperCase(),
            user_ville_club: user_ville_club,
            product_name: product_name,
        });
        if (process.env.NODE_ENV === 'development') {
            console.log('tc_events_20 called :');
            console.log('--- tc_elt ---');
            console.log(this);
            console.log('--- tc_id ---');
            console.log(id);
            console.log('--- tc_array_events ---');
            console.log('type_page : ' + type_page);
            console.log('env_country : ' + env_country);
            console.log('user_ville_club : ' + user_ville_club);
            console.log('product-name : ' + product_name);
        }
    }

    let idAboListe = 'abo-liste';

        if (typeof tc_events_20 === 'function') {
            tc_events_20(this, idAboListe, {
                type_page: type_page,
                env_country: env_country.toUpperCase(),
                user_ville_club: user_ville_club,
            });
            if (process.env.NODE_ENV === 'development') {
                console.log('tc_events_20 called :');
                console.log('--- tc_elt ---');
                console.log(this);
                console.log('--- tc_id ---');
                console.log(idAboListe);
                console.log('--- tc_array_events ---');
                console.log('type_page : ' + type_page);
                console.log('env_country : ' + env_country);
                console.log('user_ville_club : ' + user_ville_club);
            }
        }
}
window.commanderAboClubPage = commanderAboClubPage;

function commanderFormLandingPage(event) {
    let form = event.target;
    let sel = form.elements.club;
    let opt = sel.options[sel.selectedIndex];
    let opt_label = opt.label.replace(' ', '_');
    let ville = form.dataset.city;

    let user_ville_club = ville+'_'+opt_label;
    let user_email = form.elements.email.value;

    if (typeof tC.event.form_lp === 'function') {
        tC.event.form_lp(this, {
            user_email: user_email,
            user_ville_club: user_ville_club,
        });

        if (process.env.NODE_ENV === 'development') {
            console.log('tc_events_20 called :');
            console.log('--- tc_elt ---');
            console.log(this);
            console.log('--- tc_id ---');
            console.log(event_id);
            console.log('--- tc_array_events ---');
            console.log('user_email : ' + user_email);
            console.log('user_ville_club : ' + user_ville_club);
        }
    }
}
window.commanderFormLandingPage = commanderFormLandingPage;

function commanderFormContactPage(event) {
    let form = event.target;
    let sel = form.elements.subject;
    let opt = sel.options[sel.selectedIndex];
    let subject = opt.label;
    let user_email = form.elements.email.value;

    if (typeof tC.event.form_contact === 'function') {
        tC.event.form_contact(this, {
            user_email: user_email,
            form_action: subject,
        });

        if (process.env.NODE_ENV === 'development') {
            console.log('tc_events_20 called :');
            console.log('--- tc_elt ---');
            console.log(this);
            console.log('--- tc_id ---');
            console.log(event_id);
            console.log('--- tc_array_events ---');
            console.log('user_email : ' + user_email);
        }
    }
}
window.commanderFormContactPage = commanderFormContactPage;

function commanderFormTunnelModal(event) {
    let form = event.target;
    let sel = form.elements.club_id;
    let opt = sel.options[sel.selectedIndex];
    let user_email = form.elements.email.value;

    if (typeof tC.event.form_pop_in === 'function') {
        let ret = $.ajax({
            url: '/api/v1/clubs/'+opt.value,
            type: 'GET',
            async: false,
            success: function ( data ) {
                let club = data.club;
                let user_ville_club = club.city +'_'+ club.url;
                tC.event.form_pop_in(this,{
                    user_email: user_email,
                    user_ville_club: user_ville_club,
                    form_action: 'Validation formulaire d\'informations pop-in'
                });

                if (process.env.NODE_ENV === 'development') {
                    console.log('tC.event.form_pop_in called :');
                    console.log('--- tc_elt ---');
                    console.log(this);
                    console.log('--- user_email ---');
                    console.log(user_email);
                    console.log('--- user_ville_club ---');
                    console.log('user_ville_club : ' + user_ville_club);
                }
            }
        });
    }
}
window.commanderFormTunnelModal = commanderFormTunnelModal;

function commanderFormOpenclub(event) {
    let form = event.target;
    let user_email = form.elements.email.value;
    let event_id = "form-club";

    if (typeof tC.event.form_contact === 'function') {
        tC.event.form_contact(this, {
            user_email: user_email,
            form_action: "Ouvrir un club",
        });

        if (process.env.NODE_ENV === 'development') {
            console.log('tc_events_20 called :');
            console.log('--- tc_elt ---');
            console.log(this);
            console.log('--- tc_id ---');
            console.log(event_id);
            console.log('--- tc_array_events ---');
            console.log('user_email : ' + user_email);
        }
    }
}
window.commanderFormOpenclub = commanderFormOpenclub;

function commanderFormPreInsc(event, type, country, city) {
    let form = event.target;
    let user_email = form.elements.email.value;
    let event_id = "pre-insc-" + type;

    if (typeof tC.event.form_contact === 'function') {
        tc_events_20(
            this,
            event_id,
            {
                'type_page':'overlay_pre_insc_' + type,
                'env_country':country,
                'user_ville_club':city,
                'user_email': user_email,
                'pre_insc_id':'',
                'user_id':''
            }
        );

        if (process.env.NODE_ENV === 'development') {
            console.log('tc_events_20 called :');
            console.log('--- tc_elt ---');
            console.log(this);
            console.log('--- tc_id ---');
            console.log(event_id);
            console.log('--- tc_array_events ---');
            console.log('env_country : ' + country.toUpperCase());
            console.log('user_ville_club : ' + city);
            console.log('user_email : ' + user_email);
        }
    }
}
window.commanderFormPreInsc = commanderFormPreInsc;
