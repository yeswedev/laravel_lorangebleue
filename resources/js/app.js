/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import 'es6-promise/auto';

require('./bootstrap');
require('./share');
require('./carousel-settings');
require('./truncate-settings');
require('./lang');
require('./search');
require('./jquery-ui');
require('./components/burger');
require('./components/navbar');
require('./components/btn-show-more');
require('./components/footer');
require('./components/switch-state');
require('./components/dropdown');
require('./components/IE');
require('./components/formCheck');
require('./components/fadeEffect');
require('./libs/modernizr-custom');
require('./libs/numeral-fr');
require('./libs/pagination.min');
require('./components/popup-error');
require('./components/recruitment');
require('./components/lazyLoading');
require('./components/modal-contact');
require('./components/tunnel_modal');
require('./gplaces-autocomplete');
require('./components/club-details');
require('./components/landing-page');

window.Vue = require('vue');
import VueI18n from 'vue-i18n';
import messages from './vue/lang/messages';

import store from './vue/vuex/store';
import 'select2'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('offer-component', require('./vue/components/OfferComponent.vue').default);
Vue.component('is-student', require('./vue/components/IsStudentComponent.vue').default);

// Create VueI18n instance with options
window.i18n = new VueI18n({
    locale: 'fr', // set locale
    messages,
});

const app = new Vue({
    i18n,
    store,
    el: '#app',
});
