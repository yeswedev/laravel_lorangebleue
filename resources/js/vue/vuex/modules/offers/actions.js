import axios from 'axios';
import { URL_BASE } from '../../../../configs/configs';

const RESOURCE = 'offers/';

const CONFIG = {
    headers: {
        'content-type': 'multipart/form-data',
    },
};

export default {
    loadOffers(context, clubId) {
        context.commit('LOADING', true);

        return new Promise((resolve, reject) => {
            axios
                .get(`${URL_BASE}${RESOURCE}` + (clubId ? '?clubId=' + clubId : ''))
                .then(response => {
                    let offers = [];
                    if (response.data && response.data.length) {
                        response.data.forEach(function(offer) {
                            offers[offer.id] = offer;
                        });
                    }
                    context.commit('setOffers', offers);
                    window.refreshCarouselContainer();
                })
                .catch(error => reject(error))
                .finally(() => context.commit('LOADING', false));
        });
    },
};
