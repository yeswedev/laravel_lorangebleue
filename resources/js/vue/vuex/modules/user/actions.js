export default {
    setIsStudent(context, value) {
        context.commit('setIsStudent', value);
        window.refreshCarouselContainer(true);
    },
};
