import Vue from 'vue';
import Vuex from 'vuex';

import offers from './modules/offers/offers';
import preloader from './modules/preloader/preloader';
import user from './modules/user/user';
import select from './modules/select/select';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        offers,
        preloader,
        user,
        select,
    },
});
