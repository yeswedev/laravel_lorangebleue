$(document).ready(function() {
    var $this = $('.js-fade-parent');
    var $finder = $this.data('finditem');
    if ($(window).width() >= 1025) {
        $this.hover(
            function() {
                $(this)
                    .find('.' + $finder)
                    .fadeIn('slow');
            },
            function() {
                $(this)
                    .find('.' + $finder)
                    .fadeOut('slow');
            }
        );

        $('.js-fade-parent-figure').hover(
            function() {
                $(this)
                    .find('.network-tiles__lnk')
                    .fadeIn('slow');
            },
            function() {
                $(this)
                    .find('.network-tiles__lnk')
                    .fadeOut('slow');
            }
        );
    }
});
