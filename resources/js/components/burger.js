$(document).ready(function() {
    $('.navicon').click(function() {
        $(this).toggleClass('navicon--toggle');

        if ($(this).hasClass('navicon--toggle')) {
            $('.bottom__header').css({ 'border-bottom': 'none' });
            $('.bottom__header .container--flex .nav--flex')
                .slideToggle()
                .css('display', 'inherit');
            $('.top__header').slideToggle();
        } else {
            $('.bottom__header').css({ 'border-bottom': '1px solid #bcbec0' });
            $('.bottom__header .container--flex .nav--flex').slideToggle();
            $('.top__header').slideToggle();
        }
    });
});
