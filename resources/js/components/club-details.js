export default clubIsOpen;

var moment = require('moment');

$(function () {
    clubIsOpen()
});

function clubIsOpen() {
    if ( $('.js-club__infos--opening-hour').length !== 0 ) {
        let opening_hours = $('.js-club__infos--opening-hour');
        let fopening_hour = opening_hours[0].innerText.split(' - ')[1];
        let now = moment();

        if ( fopening_hour === club_detail.closed ) {
            $('.js-club__infos--is-opened').html( "<span>"+fopening_hour+"</span>" );
            $('.js-club__infos--is-opened span').css({
                'color': 'red',
                'font-weight': '700',
            });
        } else {
            if ( fopening_hour.indexOf(',') >= 0 ) {
                fopening_hour = fopening_hour.split(', ');
                let hours = [];
                for ( let i = 0; i < fopening_hour.length; i++ ) {
                    let el = fopening_hour[i];
                    hours.push(el.split('-'));
                }
                let moment1 = [
                    moment({hours: hours[0][0].split(':')[0], minute: hours[0][0].split(':')[1]}),
                    moment({hours: hours[0][1].split(':')[0], minute: hours[0][1].split(':')[1]})
                ];
                let moment2 = [
                    moment({hours: hours[1][0].split(':')[0], minute: hours[1][0].split(':')[1]}),
                    moment({hours: hours[1][1].split(':')[0], minute: hours[1][1].split(':')[1]}),
                ];

                let isBetweenF = now.isBetween(moment1[0], moment1[1]);
                let isBetweenS = now.isBetween(moment2[0], moment2[1]);

                if ( isBetweenF ) {
                    $('.js-club__infos--is-opened').html( "<span>"+club_detail.opened+"</span> - "+hours[0].join('-')+', '+hours[1].join('-') );
                    $('.js-club__infos--is-opened span').css({
                        'color': 'green',
                        'font-weight': '700',
                    });
                } else if ( isBetweenS ) {
                    $('.js-club__infos--is-opened').html( "<span>"+club_detail.opened+"</span> - "+hours[0].join('-')+', '+hours[1].join('-') );
                    $('.js-club__infos--is-opened span').css({
                        'color': 'green',
                        'font-weight': '700',
                    });
                } else {
                    if ( now.isAfter(moment1[1]) ) {
                        $('.js-club__infos--is-opened').html( "<span>"+club_detail.closed+"</span> - "+ club_detail.opened_at +" "+ hours[1][0] );
                        $('.js-club__infos--is-opened span').css({
                            'color': 'red',
                            'font-weight': '700',
                        });
                    } else if ( now.isAfter(moment2[1]) ) {
                        $('.js-club__infos--is-opened').html( "<span>"+club_detail.closed+"</span>" );
                        $('.js-club__infos--is-opened span').css({
                            'color': 'red',
                            'font-weight': '700',
                        });
                    } else {
                        $('.js-club__infos--is-opened').html( "<span>"+club_detail.closed+"</span>" );
                        $('.js-club__infos--is-opened span').css({
                            'color': 'red',
                            'font-weight': '700',
                        });
                    }
                }

            } else {
                fopening_hour = fopening_hour.split('-');
                let start = fopening_hour[0].split(':');
                let end = fopening_hour[1].split(':');
                let startTime = moment({ hour: start[0], minute: start[1]});
                let endTime = moment({ hour: end[0], minute: end[1]});
                let isBetween = now.isBetween(startTime, endTime);

                if ( isBetween ) {
                    $('.js-club__infos--is-opened').html( "<span>"+club_detail.opened+"</span> - "+fopening_hour.join('-') );
                    $('.js-club__infos--is-opened span').css({
                        'color': 'green',
                        'font-weight': '700',
                    });
                } else {
                    if ( now.isAfter(endTime) ) {
                        $('.js-club__infos--is-opened').html( "<span>"+club_detail.closed+"</span>" );
                        $('.js-club__infos--is-opened span').css({
                            'color': 'red',
                            'font-weight': '700',
                        });
                    } else {
                        $('.js-club__infos--is-opened').html( "<span>"+club_detail.closed+"</span> - "+ club_detail.opened_at +" "+ fopening_hour[0] );
                        $('.js-club__infos--is-opened span').css({
                            'color': 'red',
                            'font-weight': '700',
                        });
                    }
                }
            }
        }
    }
}
