$(document).ready(function() {
    //All show more buttons ( activites/yako/equipements )
    var minData = $('.js-parent-show-more').data('minshow');
    var visible = $('.js-parent-show-more').children(':visible').length;
    if (minData > visible) {
        $('#show-more-desk').hide();
    }
    var child = $('.js-parent-show-more').children();
    if (window.matchMedia('(max-width: 575px)').matches) {
        $('#show-more-mobile').on('click', function() {
            var showMoreXelements = $('.js-parent-show-more').data('showmore');
            var visibleElements = $('.js-parent-show-more').children(':visible').length;
            var i = visibleElements;
            var compteurStart = i + showMoreXelements;

            for (i; i < compteurStart; i++) {
                child.eq(i).show();
            }

            if (i >= child.length) {
                $(this).hide();
            }
        });
    } else {
        $('#show-more-desk').on('click', function() {
            $(this).hide();
            child.show();
        });
    }
});
