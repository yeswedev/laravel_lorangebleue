$(document).ready(function() {
    if (window.matchMedia('(max-width: 575px)').matches) {
        $('.js-toggle-dropdown').on('click', function() {
            $(this).removeClass('js-toggle-dropdown');
            $('.tunnel__step3__recap').addClass('toggle-dropdown');
            $('.js-toggle-dropdown--item').slideToggle();
            $('.js-toggle-dropdown--item').removeClass('js-toggle-dropdown--item');
        });
    }

    $('.js-toggleAddress').on('click', function() {
        $icon = $(this).find('i');
        $icon.toggleClass('fa-chevron-up');
        $icon.toggleClass('fa-chevron-down');
        if ( $(this).css('height') === '50px' ) {
            $(this).css('height', 'auto');
        } else {
            $(this).css('height', '50px');
        }
    });
});
