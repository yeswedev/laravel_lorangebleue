if (window.matchMedia('(max-width: 575px)').matches) {
    $(document).ready(function() {
        $('.js-active-state').on('click', function() {
            $state = $(this)
                .find('ul')
                .is(':visible');
            var up = $('.fa-angle-up');
            var down = $('.fa-angle-down');
            $(this)
                .find('ul')
                .slideToggle();
            if ($state) {
                $(this)
                    .find(up)
                    .hide();
                $(this)
                    .find(down)
                    .show();
            } else {
                $(this)
                    .find(down)
                    .hide();
                $(this)
                    .find(up)
                    .show();
            }
        });
    });
}
