$(function() {
    // Display reCaptcha oncly on modal focus to keep bandwith
    $('.js-contact-modal').on('focus', function(){
        grecaptcha.render(`basicModalCaptcha-${$(this).data('clubId')}`, {
            'sitekey' : process.env.MIX_GOOGLE_RECAPTCHA_KEY,
            'theme' : 'light'
        });
    })

    $('form.js-modal-contact').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting via the browser

        let form = $(this);
        let btn = form.find('button[type=submit]');
        let success = $('<div>').html(
            '<div style="width: 80%;margin: 0 auto;" class="mt-4">' +
                '<div class="alert alert-success">' +
                'Votre mail a bien été envoyé' +
                '</div>' +
                '</div>'
        );
        function error( message ) {
            if ( !message )  {
                message = 'Une erreur est survenue, veuillez réessayer plus tard';
            }

            return '<div style="width: 80%;margin: 0 auto;" class="mt-4">' +
            '<div class="alert alert-danger">' + message +
            '</div>' +
            '</div>';
        }

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function() {
                btn.attr('disabled', true);
                btn.parent().after("<div class='js-form-loader form-loader'></div>");
            },
        })
            .done(function(data) {
                btn.parent().after(success);
                form.trigger('reset');

                setTimeout(function() {
                    $('.alert.alert-success').fadeOut('slow', function() {
                        $('.alert.alert-success').parent().remove();
                    });
                }, 3500);
            })
            .fail(function(data) {

                let errors = Object.values( JSON.parse(data.responseText).errors );
                btn.parent().after( error( errors.join('<br>') ) );

                setTimeout(function() {
                    $('.alert.alert-danger').fadeOut('slow', function() {
                        $('.alert.alert-danger').parent().remove();
                    });
                }, 3500);
            })
            .always(function() {

                $('.js-form-loader').fadeOut('slow', function() {
                    $('.js-form-loader').remove();
                });
                btn.attr('disabled', false);
            });
    });
});
