Mustache = require("mustache");
var moment = require('moment');

$(function() {
    // Nav menu active
    let tabData = $('.js-tab').data('tab');
    $('nav a').each(function(index) {
        if ($(this).data('tab') === tabData) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    paginate();

    // Pagination
    function paginate () {
        $('#paginate').each(function() {
            let currentPage = 0;
            let numPerPage = 10;
            let offers = $(this);
            offers.on('repaginate', function() {
                offers
                    .find('.js-recruitment__item')
                    .hide()
                    .slice(currentPage * numPerPage, (currentPage + 1) * numPerPage)
                    .show();
            });
            offers.trigger('repaginate');
            let numRows = offers.find('.js-recruitment__item').length;
            let numPages = Math.ceil(numRows / numPerPage);
            let paginator = $('.pager').length === 0 ? $('<div class="pager"></div>') : $('.pager').empty();
            let previous = $(
                '<span class="previous">' +
                '<i class="fa fa-play fa-flip-horizontal" aria-hidden="true"></i>' +
                '</span>'
            );
            let next = $(
                '<span class="next">' + '<i class="fa fa-play" aria-hidden="true"></i>' + '</span>'
            );
            paginator.append(previous);
            for (let page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>')
                    .text(page + 1)
                    .on(
                        'click',
                        {
                            newPage: page,
                        },
                        function(event) {
                            currentPage = event.data['newPage'];
                            offers.trigger('repaginate');
                            $(this)
                                .addClass('active')
                                .siblings()
                                .removeClass('active');
                        }
                    )
                    .appendTo(paginator)
                    .addClass('clickable p' + page);
            }
            paginator.append(next);

            previous.on('click', function() {
                if (currentPage !== 0) currentPage--;
                else currentPage = numPages-1;
                offers.trigger('repaginate');
                $('.p' + currentPage)
                    .addClass('active')
                    .siblings()
                    .removeClass('active');
            });

            next.on('click', function() {
                if (currentPage !== numPages - 1) currentPage++;
                else currentPage = 0;
                offers.trigger('repaginate');
                $('.p' + currentPage)
                    .addClass('active')
                    .siblings()
                    .removeClass('active');
            });

            paginator
                .appendTo(offers)
                .find('span.page-number:first')
                .addClass('active');
        });
    }
    // Fin Pagination

    // Custom file inputs
    let labels = document.querySelectorAll('.js-input-file');
    Array.prototype.forEach.call(labels, function(label) {
        let input = label.nextElementSibling,
            labelVal = input.innerHTML;

        label.addEventListener('click', function() {
            this.nextElementSibling.click();
        });

        input.addEventListener('change', function(e) {
            let fileName = '';
            let fileSize = this.files[0].size/1024/1024;
            if (fileSize > 5) {
                alert('Fichier trop large, la taille maximum autorisée est de 5Mo');
                this.replaceWith(this.val('').clone(true));
            }

            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace(
                    '{count}',
                    this.files.length
                );
            else fileName = e.target.value.split('\\').pop();

            if (fileName) label.querySelector('span').innerHTML = fileName;
            else label.innerHTML = labelVal;
        });
    });
    // End Custom file inputs

    // Select2 multiple for mobility
    $("#mobility__select").select2({
        multiple: true,
        placeholder: "Sélectionner",
    });
    // End Select2 multiple for mobility

    // Dynamic filters for country and regions
    let country_filter = $('.js-filter-country');
    let region_filter = $('.js-filter-region');
    let region_filter_clone = region_filter.clone();

    country_filter.on('change', function () {
        let selected_country = $(this).val();
        let k = 0;

        if ( region_filter.html() !== region_filter_clone.html() ) {
            region_filter.html( region_filter_clone.html() );
        }

        region_filter.find('optgroup').each(function() {

            if( selected_country !== $(this).attr('label') ) {
                $(this).remove();
                k++;
            }
        });

        if( k === 0 || k === region_filter_clone.find('optgroup').length ) {
            region_filter.html( region_filter_clone.html() );
        }
    });
    // End Dynamic filters for country and regions

});
