$(document).ready(function() {
    if (!Modernizr.objectfit) {
        $('.img-objectfit').each(function() {
            var $container = $(this),
                img = $container.find('img'),
                imgUrl = img.prop('src');
            if (imgUrl && img.width() !== 0) {
                $container
                    .css({
                        background: 'url(' + imgUrl + ') no-repeat center center',
                        height: img.height(),
                    })
                    .addClass('compat-object-fit');
            }
        });

        $('.subscription__band__title').css('font-size', '20px');
    }
});
