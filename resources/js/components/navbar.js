$(document).ready(function() {
    var header = $('header');

    if (header) {
        var offset = header.offset().top;
        var imgDesktop = $('.logo__desktop').data('img-desktop');
        var imgMobile = $('.logo__mobile').data('img-mobile');
        var desktop = false;
        $(document).scroll(function() {
            var scrollTop = $(document).scrollTop();
            if (scrollTop > offset) {
                if (desktop == false) {
                    desktop = true;
                    $('.bottom__header').addClass('bottom__header-scroll');
                    $('.bottom__header-container').addClass('bottom__header-container-scroll');
                    $('.logo__desktop img').attr('src', imgDesktop + '/logo-mobile.svg');
                    $('.logo__mobile img').attr('src', imgMobile + '/logo-mobile.svg');
                    $('.js-logo').addClass('js-logo-bar');
                    $('.bottom__header .li-nth').hide();

                    if (window.matchMedia('(max-width: 1024px)').matches) {
                        $('.navicon').removeClass('navicon--toggle');
                        $('.nav--flex').hide();
                        $('.btn__subscription_mobile').addClass('btn__subscription_mobile-scroll');
                        $('.bottom__header .li-nth').show();
                    } else {
                        $('.bottom__header .btn__pulp').hide();
                    }
                }
            } else {
                if (desktop == true) {
                    desktop = false;
                    $('.bottom__header').removeClass('bottom__header-scroll');
                    $('.bottom__header-container').removeClass('bottom__header-container-scroll');
                    $('.bottom__header .li-nth').show();
                    $('.logo__desktop img').attr('src', imgDesktop + '/logo-groupe.svg');
                    $('.logo__mobile img').attr('src', imgMobile + '/logo-groupe.svg');
                    $('.js-logo').removeClass('js-logo-bar');

                    if (window.matchMedia('(max-width: 1024px)').matches) {
                        $('.bottom__header-container .navicon').show();
                        $('.btn__subscription_mobile').removeClass(
                            'btn__subscription_mobile-scroll'
                        );
                    } else {
                        $('.bottom__header .btn__pulp').show();
                    }
                }
            }
        });
    }
});
