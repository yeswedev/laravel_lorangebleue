import LazyLoad from 'vanilla-lazyload/dist/lazyload.js';

var myLazyLoad = new LazyLoad({
    elements_selector: '.lazy',
});
