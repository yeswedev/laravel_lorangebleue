$(function(){
    let t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;
    window.onclick = resetTimer;
    window.onscroll = resetTimer;
    window.onkeypress = resetTimer;

    let modal_options = {
        show: true,
        backdrop: true,
        keyboard: false,
        focus: true,
    };

    let inactivity_time = null;

    if ( window.modal ) {
        if ( window.modal.inactivity_time ) {
            inactivity_time = window.modal.inactivity_time*1000;
        } else {
            inactivity_time = 10000;
        }
    }

    function modalShow() {
        $("#tunnel_modal").modal(modal_options);
    }

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(modalShow, inactivity_time);
    }
});