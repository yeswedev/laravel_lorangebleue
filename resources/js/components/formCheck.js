$(document).ready(function() {
    $('.js-date-parent .in_popover__span').on('click', function() {
        $('.birthdate-picker').datepicker('show');
    });

    function validateEmail(email) {
        let emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegEx.test(email);
    }

    function validatePhoneOnly(input) {
        let numberRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        return numberRegex.test(input);
    }

    function validateNumberOnly(input) {
        let numberRegex = /^\d+$/;
        return numberRegex.test(input);
    }

    function validateDate(date) {
        let dateRegex = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        return dateRegex.test(date);
    }

    function actionsRegexValidation($this, $regex) {
        $actualValue = $($this).val();
        if ($regex($actualValue)) {
            $this.addClass('not-empty in_text__input--valid');
            $this.parent().addClass('js-in_text__valid');
            $this.parent().removeClass('js-in_text__invalid');
            $this.removeClass('in_text__input--invalid');
        } else {
            $this.removeClass('not-empty in_text__input--valid');
            $this.addClass('in_text__input--invalid');
            $this.parent().removeClass('js-in_text__valid');
            $this.parent().addClass('js-in_text__invalid');

            if ($this.val() == '') {
                $this.removeClass('in_text__input--invalid in_text__input--valid');
                $this.parent().removeClass('js-in_text__valid js-in_text__invalid');
            }
        }
    }

    let $input = $('.tunnel__step3__form').find('.in_text__input');
    $input.each(function() {
        $(this).on('change', function() {
            let $type = $(this).attr('type');
            tmpval = $(this).val();
            if (tmpval == '') {
                $(this).addClass('is-empty');
                $(this).removeClass('not-empty in_text__input--valid');
                $(this)
                    .parent()
                    .removeClass('js-in_text__valid js-in_text__invalid');
            } else {
                $(this).addClass('not-empty in_text__input--valid');
                $(this)
                    .parent()
                    .addClass('js-in_text__valid in_text--labelUp');
                $(this).removeClass('is-empty');
            }

            if ($type === 'tel') {
                actionsRegexValidation($(this), validatePhoneOnly);
            }

            if ($(this).hasClass('js-email')) {
                actionsRegexValidation($(this), validateEmail);
            }

            if ($(this).hasClass('js-postal-code')) {
                actionsRegexValidation($(this), validateNumberOnly);
            }

            if ($(this).hasClass('js-date')) {
                actionsRegexValidation($(this), validateDate);
            }
        });
    });
});
