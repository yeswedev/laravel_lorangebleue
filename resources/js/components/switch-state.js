$(document).ready(function() {
    $('.js-switch-state-generic').on('click', function() {
        let classSwitched = $(this).data('addclass');
        $('.' + classSwitched).removeClass(classSwitched);
        $(this).addClass(classSwitched);
    });

    //Add-remove une class passé en data.
    $('.js-switch-state').on('click', function() {
        let classSwitched = $(this).data('addclass');
        $('.' + classSwitched).removeClass(classSwitched);
        $(this).addClass(classSwitched);

        if ($('.club__filter__span--btn-photo').hasClass('club__filter__span--active-filter')) {
            $('.js-map').css('visibility', 'hidden');
            $('.js-photo').css('visibility', 'visible');
        } else {
            if (window.matchMedia('(max-width: 575px)').matches) {
                $('.js-photo').css('margin-top', '178%');
            }
            $('.js-map').css('visibility', 'visible');
        }

        var $contentHide = $(this).data('contentrelated') ? $(this).data('contentrelated') : '';
        if ($contentHide === 'lat-lng-club') {
            $('.search__content__map').show();
            $('.lat-lng-club').hide();
        } else {
            $('.search__content__map').hide();
            $('.lat-lng-club').show();
        }
    });

    if (!window.matchMedia('(max-width: 575px)').matches) {
        $('.js-show-on-map').on('click', function() {
            $('.club__filter__span--carte').trigger('click');
        });
    } else {
        $('.js-show-on-map').on('click', function() {
            $(this).attr('href', '#map-club');
        });
    }

    $('.js-switch-state').filter(function() {
        if (
            $(this)
                .find(".btn-offer-round input:radio[name='radio']")
                .is(':checked')
        ) {
            $(this).addClass('subscription-tiles__item--active');
        }
        return $(this)
            .find(".btn-offer-round input:radio[name='radio']")
            .is(':checked');
    });

    // request ajax to get prices, per month or per year
    if ($('.js-ajax-loaded').length) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url:
                $('.step3__recap__totalInscription').data('appUrl') +
                '/api/v1/orders/' +
                $('.step3__recap__totalInscription').data('orderId'),
            success: function(data) {
                $('.js-switch-state').on('click', function() {
                    if ($(this).hasClass('paiement__choice__rounded--active')) {
                        $(this)
                            .find(".btn-offer-round-tiny input:radio[name='mode']")
                            .prop('checked', true);
                        if (
                            $(this)
                                .find(".btn-offer-round-tiny input:radio[name='mode']")
                                .is(':checked') &&
                            $(this)
                                .find(".btn-offer-round-tiny input:radio[name='mode']")
                                .val() == 'unique'
                        ) {
                            $('.js-state-total').slideUp('fast');
                            $('#totalPrice').text(
                                JSON.stringify(data.totalUniquePrice) / 100 + '€'
                            );
                        } else {
                            $('.js-state-total').slideDown('fast');
                            $('#totalPrice').text(JSON.stringify(data.totalPrice) / 100 + '€');
                        }
                    }
                });
            },
            error: function(error) {
                console.log(error);
            },
        });
    }

    //Slider maison sur la page single d'un club.
    $('.container-club-details--item').on('click', function() {
        var image = $(this)
            .clone()
            .html();
        $('.container-club-details--bigimage').html(image);
    });

    $('.yako__list__item').on('click', function() {
        $('.yako__lnk__discover').hide();
        $(this)
            .next('.yako__lnk__discover')
            .show();
    });

    //switch status show/hide sur page single club -> yakos
    $('.js-yako-illustration-selector').on('click', function() {
        var type = $(this).attr('yako_illustration_type');
        var src;

        if ($(this).attr('yako_illustration_src') == '/storage/') {
            src = '/images/yakosingle.png';
        } else {
            src = $(this).attr('yako_illustration_src');
        }

        switch (type) {
            case 'image':
                $('#js-yako-image').attr('src', src);
                $('#js-yako-video-arrow').hide();
                $('#js-yako-video').hide();
                $('#js-yako-image').show();
                break;
            case 'video':
                $('#js-yako-video').attr('src', src);
                $('#js-yako-image').hide();
                $('#js-yako-video').show();
                $('#js-yako-video-arrow').show();
                break;
        }
    });
    $('.js-yako-illustration-selector')
        .first()
        .trigger('click');

    // autoplay video on click
    $('#js-yako-video-arrow').on('click', function() {
        $('#js-yako-video')[0].src += '?autoplay=1';
        $(this).hide();
    });
});
