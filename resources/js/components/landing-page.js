import loadMap from '../mbox' ;
import clubIsOpen from "./club-details";

$(function () {
    let h2s = $(".cms-h2");
    for( let i = 0; i < h2s.length; i++ ){
        if( $(h2s[i].nextElementSibling).has("h3").length ) {
            $(h2s[i]).addClass('not-underlined');
        }
    }

    if ( $('.landing-page__form').length !== 0 ) {
        let select = $('.landing-page__form select');
        let lp_id = select.attr('data-lp');

        select.on('change', function() {
            let club_id = select.val();
            let url = '/api/v1/clubs/' + club_id;
            let schedule = [];
            let subscription_url = '';
            let isFreeAccess_badge = '<img class="js-is_free_access_badge" src="/images/pastille-libre-acces.svg" alt="Badge Accès Libre"/>';
            let color = '#44c8f5';
            let background_color = '#004589';

            $.ajax('/api/v1/clubs/'+club_id+'/subscription', {
                success: function ( subscription ) {
                    subscription_url = '/abonnements/'+ subscription.url +'/tunnel-etape2?club_id='+club_id;
                }
            });

            $.ajax('/api/v1/clubs/'+club_id+'/isfitness/'+window.location.pathname.split('/')[2], {
                success: function( data ) {
                    let new_src = $('.js-img-changing').attr('src');
                    let new_srcset = $('.js-img-changing').attr('srcset');

                    if ( typeof data.sub === "boolean" ) {
                        if ( data.sub ) {
                            $('.js-changing_color').css("color", '');
                            $('.js-changing_background').css("background-color", '');
                            $('.js-abo-details').hasClass('hidden') ? $('.js-abo-details').removeClass('hidden') : '';
                            $('.js-text_changing').each(function () {
                                $(this).text($(this).text().replace(/wellness/g, "fitness"));
                                $(this).text($(this).text().replace(/Wellness/g, "Fitness"));
                            });
                            new_src = $('.js-img-changing').attr('src').replace('wellness', 'fitness');
                            new_srcset = $('.js-img-changing').attr('srcset').replace(/wellness/gi, 'fitness');

                            if ( data.lp.concept_list_fitness ) {
                                $('.js-list-concept').html(data.lp.concept_list_fitness);
                            }
                        } else {
                            $('.js-changing_color').css("color", color);
                            $('.js-changing_background').css("background-color", background_color);
                            !$('.js-abo-details').hasClass('hidden') ? $('.js-abo-details').addClass('hidden') : '';
                            $('.js-text_changing').each(function () {
                                $(this).text($(this).text().replace(/fitness/g, "wellness"));
                                $(this).text($(this).text().replace(/Fitness/g, "Wellness"));
                            });
                            new_src = $('.js-img-changing').attr('src').replace('fitness', 'wellness');
                            new_srcset = $('.js-img-changing').attr('srcset').replace(/fitness/gi, 'wellness');

                            if ( data.lp.concept_list_wellness ) {
                                $('.js-list-concept').html(data.lp.concept_list_wellness);
                            }
                        }
                        $('.js-img-changing').attr('src', new_src);
                        $('.js-img-changing').attr('srcset', new_srcset);
                    }
                }
            });

            $.ajax('/api/v1/clubs/'+club_id+'/schedule', {
                success: function ( data ) {
                    schedule = data;
                }
            });

            $.ajax(url, {
                success: function ( data ) {
                    let img_marker = $('#map-club').data('img-marker');
                    let scheduleString = '<span class="js-club__infos--is-opened"></span>';
                    window.history.pushState({}, data.club.title, '/lp/'+lp_id+'/'+data.club.city.toLowerCase()+"/"+data.club.id);
                    $('title').text(data.club.title);

                    if ( data.club.is_free_access ) {
                        if ( !$('.club__infos__badge')[0].contains($('.js-is_free_access_badge')[0]) ) {
                            $('.club__infos__badge').append( isFreeAccess_badge );
                        }
                    } else {
                        $('.js-is_free_access_badge').remove();
                    }

                    $(".landing-page__body .js-club_name").html('Votre salle de sport <br> ' + data.club.title );
                    $(".landing-page__body .js-club_address").html(data.club.address + '<br><span class="text--uppercase">' + data.club.zip_code + data.club.city + '</span>');

                    if ( schedule !== [] ) {
                        let entries = Object.entries( schedule );
                        let number = 0;
                        for (let [index, element] of entries ) {
                            let classes = [
                                "js-club__infos--opening-hour",
                            ];
                            if ( number === 0 ) {
                                classes.push("club__infos--opening-hour")
                            }
                            scheduleString += '<span class="'+ classes.join(' ') +'">'+ index +' - '+ element +'</span>';
                            number++;
                        }
                    } else {
                        scheduleString ='<div class="club__infos__text">' + club_detail.schedule + '</div>';
                    }

                    $(".js-schedule").html(scheduleString);

                    let map = $('<div '+
                        'id="map-club" '+
                        'class="lat-lng-club data-clubs map-container" '+
                        'data-app-url="'+ window.location.origin +'" '+
                        'data-img-marker="'+ img_marker +'" '+
                        'data-club-single="'+ data.club.id +'" '+
                        'data-club-lat="'+ data.club.latitude +'" '+
                        'data-club-lng="'+ data.club.longitude +'">'+
                        '<div id="map" class="full-height" data-context="club-details"></div>'+
                        '</div>');

                    $("#map-club").remove();
                    $(".landing-page__body").append(map);

                    loadMap();
                    clubIsOpen();
                },
            });

        });
    }

    $('.in_text__input[name="email"]').keydown(function() {
        shrinkToFill(this, 14, "", "inherit");
    })
});

//Function to convert rgb color to hex format
let hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function measureText(txt, fontSize) {
    let id = 'text-width-tester',
        $tag = $('#' + id);
    if (!$tag.length) {
        $tag = $('<span id="' + id + '" style="display:none;font-size:' + fontSize + ';">' + txt + '</span>');
        $('body').append($tag);
    } else {
        $tag.css({"font-size":fontSize}).html(txt);
    }
    return {
        width: $tag.width(),
        height: $tag.height()
    }
}

function shrinkToFill(input, fontSize) {
    let $input = $(input),
        txt = $input.val(),
        maxWidth = $input.width() - 8;
    let textWidth = measureText(txt, fontSize+ "px").width;

    if (textWidth > maxWidth) {

        fontSize = fontSize * maxWidth / textWidth * .9;
        fontSize = fontSize + "px";
        $input.css({"font-size":fontSize});

        console.log(fontSize);
    } else {
        $input.css({"font-size":fontSize});
    }
}