$(document).ready(function() {
    var url = $(location).attr('href');
    var segments = url.split('/');
    var lang = segments[3].substring(0, 2);

    if (lang == 'fr') {
        $('.langues').attr('id', 'fr');
        $('.langues').html('FR');
    } else if (lang == 'en') {
        $('.langues').attr('id', 'en');
        $('.langues').html('EN');
    } else if (lang == 'es') {
        $('.langues').attr('id', 'es');
        $('.langues').html('ES');
    }

    // dropdown lang
    $('.dropdown-nav').on('click', function(event) {
        event.preventDefault();

        $(this)
            .parent()
            .find('ul')
            .first()
            .toggle(300);

        $(this)
            .parent()
            .siblings()
            .find('ul')
            .hide(200);

        //Hide menu when clicked outside
        $(this)
            .parent()
            .find('ul')
            .mouseleave(function() {
                var thisUI = $(this);
                $('html').click(function() {
                    thisUI.hide();
                    $('html').unbind('click');
                });
            });
    });
});
