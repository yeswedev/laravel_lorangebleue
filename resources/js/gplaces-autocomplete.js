import 'select2/dist/js/i18n/fr.js'
import 'select2/dist/js/i18n/es.js'

let lang = document.documentElement.lang;
let mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_FR;

switch (lang) {
    case 'fr':
        mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_FR;
        break;

    case 'es':
        mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_ES;
        break;

    case 'ma':
        mapbox_token = process.env.MIX_MAPBOX_ACCESS_TOKEN_MA;
        break;
}

const autocomplete = {
    url: function(params){
        if (params.term && params.term.length > 2){
            // TODO https://select2.org/data-sources/ajax#request-parameters
            const country = document.documentElement.lang.toLowerCase()
                ? document.documentElement.lang.toLowerCase()
                : 'fr%2Ces%2Cma' 

            return `https://api.mapbox.com/geocoding/v5/mapbox.places/${params.term}.json?access_token=${mapbox_token}&cachebuster=1562755838346&autocomplete=true&country=${country}&types=region%2Clocality%2Cplace%2Cpostcode&limit=5` ;
        }
    },
    dataType: 'json',
    minimumInputLength: 3,
    language: 'fr',
    processResults: function (data) {
        return {
            results: data.features.map( feat => {return{
                id: feat.place_name,
                text: feat.place_name}
            })
        };
    }
}

$(function() {
    const placeholder = document.documentElement.lang.toLowerCase() === 'es'
        ? 'Escriba al menos 3 caracteres.'
        : 'Tapez au moins 3 caractères.'

    $('#main-club-search, #footer-club-search')
        .select2({ajax: autocomplete})
        .one('select2:open', function(e) {
            $('input.select2-search__field').prop('placeholder', placeholder);
        });


    $('.select2-selection').addClass('club__input in_text__input input_city')
})

export default autocomplete ;