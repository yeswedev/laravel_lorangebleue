require('./libs/owl.carousel.min');

window.refreshCarouselContainer = function(force) {
    if ($('.js-carousel-container').data('owlCarousel')) {
        if (!force) return;
    }
    if (window.matchMedia('(max-width: 575px)').matches) {
        $('.js-carousel-container').trigger('destroy.owl.carousel');
        setTimeout(function() {
            $('.js-carousel-container').owlCarousel({
                stagePadding: 20,
                loop: false,
                items: 1,
                nav: false,
                autoWidth: true,
            });
        }, 100);
    }
};

$(document).ready(function() {
    $('.carousel-testimony').owlCarousel({
        stagePadding: 5,
        loop: false,
        items: 1,
        nav: true,
        dots: false,
    });

    if (window.matchMedia('(max-width: 1024px)').matches) {
        $('.js-carousel-blog').owlCarousel({
            loop: false,
            items: 1,
            nav: false,
            center: true,
        });
    }

    if (window.matchMedia('(max-width: 575px)').matches) {
        $('.advantage-carousel').removeClass('row');
        //A refactor en css -> Aucun rapport avec carousel
        $('.plan__outer-title')
            .appendTo('.plan .row')
            .insertBefore('.plan__container');
        $('.plan__text')
            .appendTo('.plan .row')
            .insertBefore('.plan__container');

        $('.js-carousel-blog,.js-carousel-concept').owlCarousel({
            loop: false,
            items: 1,
            nav: false,
        });

        $('.advantage-carousel').addClass('owl-carousel owl-theme');
        $('.advantage-carousel').owlCarousel({
            stagePadding: 20,
            items: 1,
            nav: false,
        });

        $('.slider-presentation').owlCarousel({
            stagePadding: 30,
            loop: false,
            items: 1,
            nav: false,
        });

        $('.details-yako-slider').addClass('owl-carousel owl-theme');
        $('.details-yako-slider').owlCarousel({
            stagePadding: 30,
            loop: false,
            items: 1,
            nav: false,
            dots: false,
        });

        $('.js-carousel-tunnel').owlCarousel({
            loop: false,
            items: 2,
            nav: false,
            autoWidth: true,
        });
    }
});
