<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Resources Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default names of resources.
    |
    */
    'archive_names' => [
        'archive_label' => 'Catégories',
        'archive_singularlabel' => 'Catégorie',
    ],
    'fichier_names' => [
        'fichier_label' => 'Fichiers',
        'fichier_singularlabel' => 'Fichier',
    ],
    'bloc_names' => [
        'bloc_label' => 'Blocs',
        'bloc_singularlabel' => 'Bloc',
    ],
    'post_names' => [
        'post_label' => 'Articles',
        'post_singularlabel' => 'Article',
    ],
    'filtre_names' => [
        'filtre_label' => 'Mot clés',
        'filtre_singularlabel' => 'Mot clé',
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources Group Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default group names of resources.
    |
    */
    'resources' => [
        'archive' => [
            'group'       => 'Blog',
        ],
        'attachment' => [
            'group'       => 'Other',
        ],
        'block' => [
            'group'       => 'Other',
        ],
        'post' => [
            'group'       => 'Blog',
        ],
        'tag' => [
            'group'       => 'Blog',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources Register and Display
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default register and display configuration of resources.
    |
    */
    'archive_resource'  => [
        'archive_register' => true,
        'archive_display'  => true,
    ],
    'fichier_resource'  => [
        'fichier_register' => true,
        'fichier_display'  => false,
    ],
    'bloc_resource'     => [
        'bloc_register' => true,
        'bloc_display'  => false,
    ],
    'post_resource'     => [
        'post_register' => true,
        'post_display'  => true,
    ],
    'filtre_resource'   => [
        'filtre_register' => true,
        'filtre_display'  => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Fields Display
    |--------------------------------------------------------------------------
    |
    | For each resource, select the fields shown in the back-office.
    |
    */
    'fields'            => [
        'archive'    => [
            'title'       => true,
            'description' => false,
            'location'    => false,
            'publication' => true,
            'reference'   => true,
            'url'         => true,
            'priority'    => true,
            'attachments' => true,
            'blocks'      => false,
            'tags'        => true,
            'children'    => true,
            'parent'      => false,
            'wysiwyg'     => 'CKEditor',
        ],
        'attachment' => [
            'src'         => true,
            'title'       => true,
            'description' => true,
            'parent'      => false,
            'wysiwyg'     => 'CKEditor',
        ],
        'block'      => [
            'layout'         => true,
            'title'          => true,
            'description'    => true,
            'priority'       => true,
            'layout_options' => [
                'block-full-width' => 'Block texte de largeur 100%',
                'block-full-width-img-under-title' => 'Block texte de largeur 100% avec image dessous le titre',
                'block-img-left' => 'Block texte avec image à gauche',
                'block-img-right' => 'Block texte avec image à droite',
            ],
            'attachments'    => true,
            'parent'         => false,
            'wysiwyg'     => 'CKEditor',
        ],
        'post'       => [
            'title'       => true,
            'description' => true,
            'location'    => true,
            'publication' => true,
            'reference'   => true,
            'url'         => true,
            'priority'    => true,
            'index'       => [
                'title'       => true,
                'description' => false,
                'location'    => true,
                'url'         => true,
            ],
            'attachments' => true,
            'blocks'      => true,
            'tags'        => true,
            'parent'      => true,
            'wysiwyg'     => 'CKEditor',
        ],
        'tag'        => [
            'title'       => true,
            'description' => true,
            'parent_archives' => true,
            'parent_posts' => true,
            'wysiwyg'     => 'CKEditor',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Attachments types
    |--------------------------------------------------------------------------
    |
    | Set the list of allowed types for attachments.
    |
    */
    'attachments-types' => [
        'banner'    => 'Bannière',
        'document'  => 'Document',
        'icon'      => 'Icône',
        'img'       => 'Image',
        'thumbnail' => 'Miniature',
    ],

    'seo-fields' => true,

    'seo-fields-options' => [
        'seo_h1' => true,
        'seo_title' => true,
        'seo_description' => true,
    ]

];
