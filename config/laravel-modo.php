<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Moderator Role
    |--------------------------------------------------------------------------
    |
    | Define the role which can be moderate.
    |
    */
    'moderation_role' => 'administrateur',

    /*
    |--------------------------------------------------------------------------
    | Role Column
    |--------------------------------------------------------------------------
    |
    | Define the role column name or slug which to be use. default is 'name'
    |
    */
    'role_slug' => 'slug',

    /*
    |--------------------------------------------------------------------------
    | Lang Action Moderations
    |--------------------------------------------------------------------------
    |
    | Define the role which can moderate.
    |
    */
    'action_name' => 'Tout Moderer',
    'approve' => 'Accepter',
    'decline' => 'Refuser',

    /*
    |--------------------------------------------------------------------------
    | Field 'Field Name'
    |--------------------------------------------------------------------------
    |
    | Define the name which can be show instead of the column name.
    | Set the key as property and change the value to be display using
    |
    | Example:
    |
    | 'field_names' => [
    |    'YourModel' => [
    |        'title' => 'title',
    |    ]
    | ],
    |
    */
    'field_names' => [
        'Club' => [
            'title' => 'Titre',
            'description' => 'Description',
            'country' => 'Pays',
            'address' => 'Adresse',
            'zip_code' => 'Code postal',
            'city' => 'Ville',
            'phone' => 'Téléphone',
            'email' => 'Email',
            'schedule' => 'Horaire du club',
            'social_link_fb' => 'Lien Facebook',
            'social_link_insta' => 'Lien Instagram',
            'team_title' => 'Titre d\'équipe',
            'team_description' => 'Description d\'équipe',
            'image' => 'Image du club',
            'team_image' => 'Image de l\'équipe',
            'url' => 'Url',
            'price_offer' => 'Prix de l\'avantage (en centimes)',
            'planning_image' => 'Image du planning',
            'mangopay_wallet_id' => 'Mangopay WalletId',
            'meta_title' => 'Méta Titre',
            'meta_description' => 'Méta Description',
            'h1' => 'H1',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Field Type (Model)
    |--------------------------------------------------------------------------
    |
    | Define the name which can be show instead of the class name.
    | Set the key as model and change the value to be display using
    |
    | Example:
    |
    | 'type_names' => [
    |    'YourModel' => 'Value'
    | ],
    |
    */
    'type_names' => [
        'Club' => 'Club'
    ],

    /*
    |--------------------------------------------------------------------------
    | Field Type Name
    |--------------------------------------------------------------------------
    |
    | Define the property which can be show instead of id.
    |
    | Example:
    |
    | 'properties_name' => [
    |    'title' => 'title',
    | ],
    |
    */
    'properties_name' => [
        'title' => 'title',
        'email' => 'email',
    ],

    /*
    |--------------------------------------------------------------------------
    | Fields Display
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default display configuration of fields.
    |
    */
    'field_type_id' => true,

    /*
    |--------------------------------------------------------------------------
    | Resources Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default names of resources.
    |
    */
    'moderation_names' => [
        'moderation_label' => 'Moderations',
        'moderation_singularlabel' => 'Moderation',
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources Group Names
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default group names of resources.
    |
    */
    'resources' => [
        'moderation' => [
            'group' => 'Utilisateur',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Field Label
    |--------------------------------------------------------------------------
    |
    | Contains an array with the default label of field.
    |
    */
    'fields' => [
        'button-moderation' => [
            'label-field'  => 'Nom du champ',
            'label-type'  => 'Section concernée',
            'label-type_id'  => 'Infos du club',
            'label-current_value'  => 'Ancienne modifications',
            'label-new_value'  => 'Nouvelles modifications',
            'label-status'  => 'Action',
        ],
    ],
];
