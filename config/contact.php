<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 21/12/18
 * Time: 18:28
 */
return [
    'from' => [
        'email' => env('MAIL_FROM'),
        'name' => env('MAIL_FROM_NAME'),
    ],
    'contact_club' => [
        'value' => env('MAIL_CONTACT_CLUB', ''),
        'label-fr' => 'Contacter un club',
        'label-ma' => 'Contacter un club',
        'label-es' => 'Contactarnos',
    ],
    'open_club' => [
        'value' => env("MAIL_OPEN_CLUB", 'nicolas@lorangebleue.fr'),
        'label-fr' => 'Ouvrir un club',
        'label-ma' => 'Ouvrir un club',
        'label-es' => 'Abrir un gimnasio',
    ],
    'become_coach' => [
        'value' => env("MAIL_BECOME_COACH", 'bp.af@lorangebleue.fr'),
        'label-fr' => 'Devenir coach',
        'label-ma' => 'Devenir coach',
        'label-es' => 'Ser entrenador',
    ],
    'rgpd_partners' => [
        'value' => env("MAIL_RGPD_PARTNERS", 'maelle.billon@lorangebleue.fr'),
        'label-fr' => 'RGPD partenaires',
        'label-ma' => 'RGPD partenaires',
        'label-es' => 'RGPD asociados',
    ],
    'rgpd_employees' => [
        'value' => env("MAIL_RGPD_EMPLOYEES", 'julie.coris@lorangebleue.fr'),
        'label-fr' => 'RGPD salariés',
        'label-ma' => 'RGPD salariés',
        'label-es' => 'RGPD salarios',
    ],
    'else' => [
        'value' => env("MAIL_ELSE", 'claire@lorangebleue.fr'),
        'label-fr' => 'Autre',
        'label-ma' => 'Autre',
        'label-es' => 'Otra',
    ]
];
