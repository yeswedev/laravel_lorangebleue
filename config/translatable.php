<?php

return [
    'locales' => [
        'fr' => 'Français',
        'es' => 'Espagnol',
        'ma' => 'Marocain',
    ]
];
