<?php
return [
    'encp' => [
        'label' => 'ENCP',
        'url' => env('LINK_ENCP', 'http://www.encp.fr/'),
    ],
    'pulp' => [
        'label' => 'Pulp',
        'url' => env("LINK_PULP", 'https://pulp.lorangebleue.fr/?_ga=2.69920595.292387762.1545026874-1710321067.1542613201'),
    ],
    'youtube' => [
        'url-fr' => env('LINK_YOUTUBE_FR', 'https://www.youtube.com/channel/UCSQxGvlqBg7K16PPhEN0J1A'),
        'url-es' => env('LINK_YOUTUBE_ES', '')
    ],
    'facebook' => [
        'url-wellness'=> env('LINK_FACEBOOK_WELLNESS_DEFAULT', 'https://www.facebook.com/LOrangeBleueMonCoachWellnessFrance/'),
        'url-fr' => env('LINK_FACEBOOK_FR', 'https://www.facebook.com/lorangebleuefrance'),
        'url-es' => env('LINK_FACEBOOK_ES', 'https://www.facebook.com/lorangebleueespana/'),
        'url-ma' => env('LINK_FACEBOOK_MA', 'https://www.facebook.com/lorangebleuemarrakech/'),
    ],
    'instagram' => [
        'url-fr' => env('LINK_INSTAGRAM_FR', 'https://www.instagram.com/lorangebleuefrance/'),
        'url-es' => env('LINK_INSTAGRAM_ES', '')
    ],
];
