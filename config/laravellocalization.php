<?php

return [

    // Uncomment the languages that your site supports - or add new ones.
    // These are sorted by the native name, which is the order you might show them in a language selector.
    // Regional languages are sorted by their base language, so "British English" sorts as "English, British"
    'supportedLocales' => [
        //'ca'          => ['name' => 'Catalan',                'script' => 'Latn', 'native' => 'català', 'regional' => 'ca_ES', 'tld' => '.cat','subdomain' => 'cat.'],
        //'en'          => ['name' => 'English',                'script' => 'Latn', 'native' => 'English', 'regional' => 'en_GB'],
        //'en-GB'       => ['name' => 'British English',        'script' => 'Latn', 'native' => 'British English', 'regional' => 'en_GB'],
        'es'          => ['name' => 'Spanish',                'script' => 'Latn', 'native' => 'español', 'regional' => 'es_ES', 'tld' => env('ES_TLD', '.es'),'subdomain' => 'es.'],
        'fr'          => ['name' => 'French',                 'script' => 'Latn', 'native' => 'français', 'regional' => 'fr_FR', 'tld' => env('FR_TLD', '.fr'),'subdomain' => 'fr.'],
        'ma'          => ['name' => 'Morocco',                'script' => 'Latn', 'native' => 'français', 'regional' => 'ma_MA', 'tld' => env('MA_TLD', '.ma'),'subdomain' => 'ma.'],
    ],

    // Negotiate for the user locale using the Accept-Language header if it's not defined in the URL?
    // If false, system will take app.php locale attribute
    'useAcceptLanguageHeader' => true,

    // If LaravelLocalizationRedirectFilter is active and hideDefaultLocaleInURL
    // is true, the url would not have the default application language
    //
    // IMPORTANT - When hideDefaultLocaleInURL is set to true, the unlocalized root is treated as the applications default locale "app.locale".
    // Because of this language negotiation using the Accept-Language header will NEVER occur when hideDefaultLocaleInURL is true.
    //
    'hideDefaultLocaleInURL' => false,

    // IMPORTANT - When showWellnessSubscription is set to false, don't show link in home menu,
    // the section "mon coach wellness" on the homepage and the wellness block in "les concepts l'orangebleue" on the home too.
    'showWellnessSubscription' => false,

    // IMPORTANT - When showBecameCoach is set to false, don't show link in top header and in footer too and in sitemap.html.
    //
    'showBecameCoach' => false,

    // IMPORTANT - When showPulp is set to false, don't show the CTA on header, section "bons plans adhérents" on the home and the link in footer too and in sitemap.html.
    //
    'showPulp' => true,

    // IMPORTANT - When showBlog is set to false, don't show the link on header nav, section "blog" on the home and the link in footer too and in sitemap.html.
    //
    'showBlog' => false,

    // IMPORTANT - When showYoutube is set to false, don't show the link on top header nav and the link in footer too.
    //
    'showYoutube' => false,

    // IMPORTANT - When showInstagram is set to false, don't show the link on top header nav and the link in footer too.
    //
    'showInstagram' => false,

    // IMPORTANT - When showSearchInput is set to false, don't show the search input in find-club-blade, in home the section and in footer.
    //
    'showSearchInput' => false,

    // IMPORTANT - When newLinkRecrutement is set to false, don't show the good link in top header, in footer and in home section'ÚNASE A LA RED' .
    //
    'newLinkRecrutement' => true,



    // If you want to display the locales in particular order in the language selector you should write the order here.
    //CAUTION: Please consider using the appropriate locale code otherwise it will not work
    //Example: 'localesOrder' => ['es','en'],
    'localesOrder' => [],

];
