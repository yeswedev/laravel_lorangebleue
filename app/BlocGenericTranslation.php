<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlocGenericTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'image_alt',
        'image_alt_2',
    ];
}
