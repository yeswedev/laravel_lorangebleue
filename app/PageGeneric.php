<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageGeneric extends Model
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'title',
        'subtitle',
        'description',
        'text',
        'text_2',
        'text_3',
    ];

    protected $fillable = [
        'type',
        'image',
        'concept',
        'image_wellness',
        'image_alt_wellness',
        'image_plan',
        'image_presentation',
        'image_alt_presentation',
        'image_reseau_1',
        'image_alt_reseau_1',
        'image_reseau_2',
        'image_alt_reseau_2',
    ];

    protected $with = [
        'translations'
    ];

    public function cities()
    {
        return $this->belongsToMany(City::class, 'page_generic_city', 'page_generic_id', 'city_id');
    }
}
