<?php

namespace App\Exports;

use App\Club;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;

class ClubsExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function headings(): array
	{
		return [
            'Id du Club',
            'Nom du Club',
            'URL de la page',
            'Latitude',
            'Longitude',
            'Horaire d\'ouverture',
            'Numéro de téléphone du Club'
        ];
    }

    public function map($club): array
    {
        $array = [];
        foreach($club->getSchedule() as $day => $hour)
        {
            array_push($array, $day.' - '.$hour);
        }
        $schedule = implode("','",$array);

        if($club->url)
        {
            $url = route('front.club-details', $club->url);
        }
        else
        {
            $url = 'Pas d\'url';
        }

        return [
            $club->id,
            $club->title,
            $url,
            $club->latitude,
            $club->longitude,
            $schedule,
            $club->phone,
        ];
    }

    public function query()
    {
        return Club::query();
    }
}
