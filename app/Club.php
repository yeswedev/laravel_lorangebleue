<?php

namespace App;

use App\Lib\Geocoding;
use App\Traits\HasLatitudeLongitude;
use App\Traits\HasMetaFields;
use App\Traits\HasYWDFields;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use YesWeDev\LaravelModo\Traits\Moderate;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;
use LaravelLocalization;

class Club extends Model
{
    use HasLatitudeLongitude;
    use HasYWDFields;
    use HasMetaFields;
    use Moderate;
    use SoftDeletes;

    protected $rolesModeration = [
        'gerant'
    ];

    protected $fieldsModeration = [
        'title' => 'title',
        'description' => 'description',
        'country' => 'country',
        'address' => 'address',
        'zip_code' => 'zip_code',
        'city' => 'city',
        'phone' => 'phone',
        'email' => 'email',
        'schedule' => 'schedule',
        'social_link_fb' => 'social_link_fb',
        'social_link_insta' => 'social_link_insta',
        'team_title' => 'team_title',
        'team_description' => 'team_description',
        'image' => 'image',
        'team_image' => 'team_image',
        'url' => 'url',
        'price_offer' => 'price_offer',
        'planning_image' => 'planning_image',
        'mangopay_wallet_id' => 'mangopay_wallet_id',
        'meta_title' => 'meta_title',
        'meta_description' => 'meta_description',
        'h1' => 'h1',
    ];

    protected $fillable = [
        'is_sauna',
        'is_access',
        'is_try',
        'is_clim',
        'is_defibrillator',
        'price_offer',
        'latitude',
        'longitude',
        'team_image',
        'planning_image',
        'image',
        'phone',
        'club_surface',
        'courses_number',
        'coachs_number',
        'is_access_option',
        'team_users',
        'schedule',
        'mangopay_wallet_id',
        'social_link_insta',
        'social_link_fb',
        'title',
        'description',
        'address',
        'email',
        'team_title',
        'team_description',
        'zip_code',
        'city',
        'url',
        'country',
        'is_open',
    ];

    protected $casts = [
        'schedule' => 'array',
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function advantages()
    {
        return $this->hasMany(Advantage::class);
    }

    public function medias()
    {
        return $this->hasMany(Media::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_club', 'club_id', 'user_id');
    }

    public function specialoffers()
    {
        return $this->belongsToMany(Specialoffer::class, 'specialoffer_club', 'club_id', 'specialoffer_id');
    }

    public function equipments()
    {
        return $this->belongsToMany(Equipment::class, 'equipment_club', 'club_id', 'equipment_id');
    }

    public function activities()
    {
        return $this->belongsToMany(Activity::class, 'activity_club', 'club_id', 'activity_id');
    }

    public function yakos()
    {
        return $this->belongsToMany(Yako::class, 'yako_club', 'club_id', 'yako_id')->withPivot('status', 'version');
    }

    public function offers()
    {
        return $this->belongsToMany(Offer::class, 'club_offer', 'club_id', 'offer_id');
    }

    public function permanentOffers() {
        return $this->offers()
            ->where("is_permanent", true);
    }

    public function ponctualOffers() {
        return $this->offers()
            ->where("is_permanent", false)
            ->whereDate("start_date", "<=", Carbon::now()->format("Y-m-d"))
            ->whereDate("end_date", ">=", Carbon::now()->format("Y-m-d"));
    }

    public function activeOffers() {
        $ponctualOffers = $this->ponctualOffers()->get();
        $rejectedPermanentOffers = array_filter($this->ponctualOffers()->pluck('offerToReplace')->toArray());
        $permanentOffers = $this->permanentOffers()->whereNotIn( 'offer_id', $rejectedPermanentOffers )->get();

        return $ponctualOffers->merge( $permanentOffers );
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function initMetaInformations()
    {
        $this->metaTitleDefaultTransKey = $this->getTable().'-'.$this->subscription->type;
        $this->metaTitleDefaultTransArgs = ['name' => $this->title];
        $this->metaDescriptionDefaultTransKey = $this->getTable();
        $this->metaDescriptionDefaultTransArgs = ['name' => $this->title];
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        if (!$this->latitude || !$this->longitude) {
            $geoData = Geocoding::findLongLatFromAddress($this->city, $this->country);

            if ($geoData['lat'] && $geoData['lng']) {
                $this->latitude = $geoData['lat'];
                $this->longitude = $geoData['lng'];
            }

            $this->department = $geoData['department'];
            if (!$this->zip_code) {
                $this->zip_code = $geoData['zipcode'];
            }
        }

        return parent::save($options);
    }

    public function buildSeoContext()
    {
        $images = [];
        foreach ($this->medias as $media) {
            $images[] = [
                Storage::disk('public')->url($media->file),
            ];
        }

        $properties = [
            "@context" => "http://schema.org",
            "@type" => "ExerciseGym",
            'name' => $this->title,
            'image' => $images,
            'url' => route('front.club-details', $this->url),
            'telephone' => $this->phone,
            "priceRange" => "de 19.90€ à 29.90€",
            'address' => [
                "@type" => "PostalAddress",
                'streetAddress' => $this->address,
                'addressLocality' => $this->city,
                'addressCountry' => strtoupper($this->country),
                'postalCode' => $this->zip_code,
            ],
            'geo' => [
                "@type" => "GeoCoordinates",
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
            ],
            'openingHoursSpecification' => [
                "@type" => "OpeningHoursSpecification",
                "dayOfWeek" => [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                    "Sunday"
                ],
                "opens" => "06:00",
                "closes" => "23:00"
            ],
            "sameAs" => [
                $this->social_link_fb,
                $this->social_link_insta
            ]
        ];

        return "<script type=\"application/ld+json\">" . json_encode($properties, JSON_UNESCAPED_UNICODE) . "</script>";
    }


    /**
     * Parcours tous les clubs et remplace les _ et " " par -
     */
    public static function cleanUrl()
    {
        $clubs = self::get();
        foreach ($clubs as $club) {
            $club->url = str_replace("_", "-", $club->url);
            $club->url = str_replace(" ", "-", $club->url);
            $club->save();
        }
    }

    public static function getClubsFrom50MainCities()
    {
        $citySlugs = City::get50MainCitiesName();
        return self::whereIn('city', $citySlugs)->get();
    }

    public function getSchedule()
    {

        if (LaravelLocalization::getCurrentLocale() == 'ma')
            $locale = 'fr';
        else 
            $locale = LaravelLocalization::getCurrentLocale() ;
        Date::setLocale($locale);

        $timestamp = Date::now();
        $days = [];
        for ($i = 0; $i < 7; $i++) {
            $days[] = ucfirst( $timestamp->format('l') );
            $timestamp = $timestamp->addDay();
        }

        $schedule = [];
        if ( $this->schedule == null ) {
            $this->schedule = "[]";
        }
        foreach ($this->schedule as $time_slot) {
            if( !array_key_exists( $time_slot['weekday'], $schedule) ){
                $schedule[$time_slot['weekday']] = $time_slot['b_hour']."-".$time_slot['e_hour'];
            } else {
                $schedule[$time_slot['weekday']] = $schedule[$time_slot['weekday']].", ".$time_slot['b_hour']."-".$time_slot['e_hour'];
            }
        }

        $schedule_clone = $schedule;
        $schedule = [];
        foreach ($days as $day) {
            if ( ! array_key_exists($day, $schedule_clone) ) {
                $schedule[$day] = trans('trad.club-detail.closed');
            } else {
                $schedule[$day] = $schedule_clone[$day];
            }
        }

        return $schedule;
    }

    public function landingPages() {
        $this->belongsToMany(LandingPage::class, 'landing_pages_clubs', 'club_id', 'landing_page_id');
    }
}
