<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class SubscriptionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewSubscription', 'manageSubscription'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewSubscription', 'manageSubscription'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageSubscription');
    }

    public function update($user, $post)
    {
        return $user->can('manageSubscription', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageSubscription', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageSubscription', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageSubscription', $post);
    }
}
