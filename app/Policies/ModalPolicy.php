<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class ModalPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewModal', 'manageModal'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewModal', 'manageModal'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageModal');
    }

    public function update($user, $post)
    {
        return $user->can('manageModal', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageModal', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageModal', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageModal', $post);
    }
}
