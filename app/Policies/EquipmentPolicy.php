<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class EquipmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewEquipment', 'manageEquipment'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewEquipment', 'manageEquipment'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageEquipment');
    }

    public function update($user, $post)
    {
        return $user->can('manageEquipment', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageEquipment', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageEquipment', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageEquipment', $post);
    }
}
