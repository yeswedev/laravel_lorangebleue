<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class ActivityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewActivity', 'manageActivity'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewActivity', 'manageActivity'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageActivity');
    }

    public function update($user, $post)
    {
        return $user->can('manageActivity', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageActivity', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageActivity', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageActivity', $post);
    }
}
