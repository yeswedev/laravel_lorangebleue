<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class LandingPagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewLandingPage', 'manageLandingPage'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewLandingPage', 'manageLandingPage'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageLandingPage');
    }

    public function update($user, $post)
    {
        return $user->can('manageLandingPage', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageLandingPage', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageLandingPage', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageLandingPage', $post);
    }
}
