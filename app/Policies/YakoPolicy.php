<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class YakoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewYako', 'manageYako'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewYako', 'manageYako'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageYako');
    }

    public function update($user, $post)
    {
        return $user->can('manageYako', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageYako', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageYako', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageYako', $post);
    }
}
