<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class ArchivePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewArchive', 'manageArchive'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewArchive', 'manageArchive'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageArchive');
    }

    public function update($user, $post)
    {
        return $user->can('manageArchive', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageArchive', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageArchive', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageArchive', $post);
    }
}
