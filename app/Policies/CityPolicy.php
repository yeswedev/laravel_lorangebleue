<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class CityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewCity', 'manageCity'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewCity', 'manageCity'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageCity');
    }

    public function update($user, $post)
    {
        return $user->can('manageCity', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageCity', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageCity', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageCity', $post);
    }
}
