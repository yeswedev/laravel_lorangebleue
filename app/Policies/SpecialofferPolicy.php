<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class SpecialofferPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewSpecialoffer', 'manageSpecialoffer'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewSpecialoffer', 'manageSpecialoffer'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageSpecialoffer');
    }

    public function update($user, $post)
    {
        return $user->can('manageSpecialoffer', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageSpecialoffer', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageSpecialoffer', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageSpecialoffer', $post);
    }
}
