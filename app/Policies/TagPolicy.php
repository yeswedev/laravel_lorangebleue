<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class TagPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewTag', 'manageTag'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewTag', 'manageTag'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageTag');
    }

    public function update($user, $post)
    {
        return $user->can('manageTag', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageTag', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageTag', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageTag', $post);
    }
}
