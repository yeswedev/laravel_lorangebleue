<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class AdvantagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewAdvantage', 'manageAdvantage'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewAdvantage', 'manageAdvantage'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageAdvantage');
    }

    public function update($user, $post)
    {
        return $user->can('manageAdvantage', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageAdvantage', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageAdvantage', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageAdvantage', $post);
    }
}
