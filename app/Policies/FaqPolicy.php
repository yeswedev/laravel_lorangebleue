<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class FaqPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewFaq', 'manageFaq'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewFaq', 'manageFaq'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageFaq');
    }

    public function update($user, $post)
    {
        return $user->can('manageFaq', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageFaq', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageFaq', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageFaq', $post);
    }
}
