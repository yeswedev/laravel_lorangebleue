<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class BlockPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewBlock', 'manageBlock'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewBlock', 'manageBlock'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageBlock');
    }

    public function update($user, $post)
    {
        return $user->can('manageBlock', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageBlock', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageBlock', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageBlock', $post);
    }
}
