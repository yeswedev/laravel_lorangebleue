<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class OfferPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewOffer', 'manageOffer'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewOffer', 'manageOffer'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageOffer');
    }

    public function update($user, $post)
    {
        return $user->can('manageOffer', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageOffer', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageOffer', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageOffer', $post);
    }
}
