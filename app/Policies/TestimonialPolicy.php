<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class TestimonialPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewTestimonial', 'manageTestimonial'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewTestimonial', 'manageTestimonial'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageTestimonial');
    }

    public function update($user, $post)
    {
        return $user->can('manageTestimonial', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageTestimonial', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageTestimonial', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageTestimonial', $post);
    }
}
