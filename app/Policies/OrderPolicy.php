<?php

namespace App\Policies;

use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewOrder', 'manageOrder'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewOrder', 'manageOrder'], $user, $post);
    }

    public function create($user)
    {
        //return $user->can('manageOrder');
    }

    public function update($user, $post)
    {
        if ($user->isAdmin()) {
            return $user->can('viewOrder', 'manageOrder', $post);
        }
    }

    public function delete($user, $post)
    {
        if ($user->isAdmin()) {
            return $user->can('manageOrder', $post);
        }
    }

    public function restore($user, $post)
    {
        return $user->can('manageOrder', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageOrder', $post);
    }
}
