<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class ClubPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewClub', 'manageClub'], $user);
    }

    public function view($user, $post)
    {
        if ($post->users->contains($user->id)) {
            return Gate::any(['viewClub', 'manageClub'], $user, $post);
        } elseif ($user->isAdmin()) {
            return Gate::any(['viewClub', 'manageClub'], $user, $post);
        }
    }

    public function create($user)
    {
        return $user->can('manageClub');
    }

    public function update($user, $post)
    {
        if ($user->isAdmin()) {
            return $user->can('viewClub', 'manageClub', $post);
        }
    }

    public function delete($user, $post)
    {
        return $user->can('manageClub', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageClub', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageClub', $post);
    }
}
