<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class DepartmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewDepartment', 'manageDepartment'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewDepartment', 'manageDepartment'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageDepartment');
    }

    public function update($user, $post)
    {
        return $user->can('manageDepartment', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageDepartment', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageDepartment', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageDepartment', $post);
    }
}
