<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class AttachmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return Gate::any(['viewAttachment', 'manageAttachment'], $user);
    }

    public function view($user, $post)
    {
        return Gate::any(['viewAttachment', 'manageAttachment'], $user, $post);
    }

    public function create($user)
    {
        return $user->can('manageAttachment');
    }

    public function update($user, $post)
    {
        return $user->can('manageAttachment', $post);
    }

    public function delete($user, $post)
    {
        return $user->can('manageAttachment', $post);
    }

    public function restore($user, $post)
    {
        return $user->can('manageAttachment', $post);
    }

    public function forceDelete($user, $post)
    {
        return $user->can('manageAttachment', $post);
    }
}
