<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Silvanite\Brandenburg\Traits\HasRoles;
use YesWeDev\DocXChange\Traits\HasDocumentAuth;
use YesWeDev\LaravelModo\Traits\HasRoleModerated;

class User extends Authenticatable
{
    use Notifiable, HasRoles, HasRoleModerated, HasDocumentAuth;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'firstname',
    ];

    protected $attributes = [
        'name'    => null,
        'email'      => null,
        'password'     => null,
        'role_id'    => null,
        'firstname'  => null,
        'created_at'    => null,
        'updated_at'    => null,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userClub()
    {
        return $this->belongsToMany(Club::class, 'user_club', 'user_id', 'club_id');
    }

    public function isAdmin()
    {
        if ($this->roles->contains(1)) {
            return true;
        }
    }

    public function hasRole($role)
    {
        if ($this->role()->where('title', $role)->first()) {
            return true;
        }
        return false;
    }

    public static function createFromArray(array $attributes)
    {
        $user = new self;
        $user->updateFromArray($attributes);
        return $user;
    }

    public function updateFromArray(array $attributes)
    {
        foreach ($this->attributes as $name => $value) {
            if (isset($attributes[$name])) {
                $this->$name = $attributes[$name];
            }
        }

        return $this->save();
    }

    public function hasClub()
    {
        if (count($this->userClub) > 0) {
            return true;
        }
        return false;
    }
}
