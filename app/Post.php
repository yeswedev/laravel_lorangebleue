<?php

namespace App;

use Laravel\Scout\Searchable;
use YesWeDev\LaravelCMS\Post as PostModel;

class Post extends PostModel
{
    use Searchable;

    public function toSearchableArray()
    {
        if ($this->type == 'post') {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'description' => $this->description,
                'location' => $this->location,
            ];
        }

        return null;
    }
}
