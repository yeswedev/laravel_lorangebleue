<?php
namespace App\Traits;

trait HasMetaFields
{
    protected $metaTitleDefaultTransKey = null;
    protected $metaTitleDefaultTransArgs = null;
    protected $metaDescriptionDefaultTransKey = null;
    protected $metaDescriptionDefaultTransArgs = null;
    protected $metaH1DefaultTransKey = null;
    protected $metaH1DefaultTransArgs = null;

    public function extraConstructHastMetaFields() : void
    {
        $this->fillable = array_merge($this->fillable, ['meta_title','meta_description','h1','image_alt']);
        if ($this->translatedAttributes) {
            $this->translatedAttributes = array_merge($this->translatedAttributes, ['meta_title','meta_description','h1','image_alt']);
        }
    }

    public function getMetaTitle() : String
    {
        if (method_exists($this, 'initMetaInformations')) {
            $this->initMetaInformations();
        }
        $metaTitle = '';
        $arrayOrString = trans('meta.title.'.$this->getTable());

        if ($this->meta_title) {
            return $this->meta_title;
        }
        if ($this->metaTitleDefaultTransKey) {
            return trans('meta.title.'.$this->metaTitleDefaultTransKey, $this->metaTitleDefaultTransArgs);
        }
        if ($this->url) {
            if (is_array($arrayOrString)) {
                if (array_key_exists($this->url, $arrayOrString)) {
                    $metaTitle = $arrayOrString[$this->url];
                } elseif (array_key_exists('default', $arrayOrString)) {
                    $metaTitle = $arrayOrString['default'];
                }
            }
        }

        return $metaTitle;
    }

    public function getMetaDescription(array $arr = null, $param = null) : String
    {
        if (method_exists($this, 'initMetaInformations')) {
            $this->initMetaInformations();
        }
        $metaDescription = '';
        $arrayOrString = trans('meta.description.'.$this->getTable());

        if ($this->meta_description) {
            return $this->meta_description;
        }
        if ($this->metaDescriptionDefaultTransKey) {
            return trans('meta.description.'.$this->metaDescriptionDefaultTransKey, $this->metaDescriptionDefaultTransArgs);
        }
        if ($this->url) {
            if (is_array($arrayOrString)) {
                if (array_key_exists($this->url, $arrayOrString)) {
                    $metaDescription = $arrayOrString[$this->url];
                } elseif (array_key_exists('default', $arrayOrString)) {
                    $metaDescription = $arrayOrString['default'];
                }
            }
        }

        return $metaDescription;
    }

    public function getH1() : String
    {
        $h1 = '';

        if ($this->h1) {
            $h1 = $this->h1;
        } elseif ($this->title) {
            $h1 = $this->title;
        }

        return $h1;
    }

    public function getImgAlt() : String
    {
        $image_alt = '';

        if ($this->image_alt) {
            $image_alt = $this->image_alt;
        } elseif ($this->title) {
            $image_alt = $this->title;
        }

        return $image_alt;
    }
}
