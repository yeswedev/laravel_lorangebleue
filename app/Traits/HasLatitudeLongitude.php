<?php
namespace App\Traits;

trait HasLatitudeLongitude
{
    public function extraConstructHasLatitudeLongitude() : void
    {
        $this->fillable = array_merge($this->fillable, ['latitude','longitude']);
    }

    /*
    Provide the Location value to the Nova field
    */
    public function getLocationAttribute()
    {
        return (object) [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }

    /*
    Transform the returned value from the Nova field
    */
    public function setLocationAttribute($value)
    {
        $latitude = round(object_get($value, 'latitude'), 7);
        $longitude = round(object_get($value, 'longitude'), 7);
        $this->attributes['latitude'] = $latitude;
        $this->attributes['longitude'] = $longitude;
    }
}
