<?php
namespace App\Traits;

trait HasYWDFields
{
    public function __construct()
    {
        // parent refers not to the class being mixed into, but its parent
        parent::__construct();

        // Find and execute all methods beginning 'extraConstruct'
        $mirror = new \ReflectionClass($this);
        foreach ($mirror->getMethods() as $method) {
            if (strpos($method->getName(), 'extraConstruct') === 0) {
                $method->invoke($this);
            }
        }
    }
}
