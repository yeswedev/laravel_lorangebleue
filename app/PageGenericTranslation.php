<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageGenericTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'text',
        'text_2',
        'text_3',
    ];
}
