<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use Translatable;
    use SoftDeletes;

    const DEFAULT_URL = 'concepts';

    public $translatedAttributes = [
        'title',
        'description',
        'url',
        'subtitle',
        'title1',
        'presentation1',
        'image1',
        'title2',
        'presentation2',
        'image2',
        'title3',
        'presentation3',
        'image3',
        'title4',
        'presentation4',
        'image4',
    ];

    protected $fillable = [
        'image',
        'type'
    ];

    public function clubs()
    {
        return $this->hasMany(Club::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function testimonials()
    {
        return $this->hasMany(Testimonial::class);
    }

    public function subscriptionYako()
    {
        return $this->belongsToMany(Yako::class, 'subscription_yako', 'subscription_id', 'yako_id');
    }

    public static function getSubscriptionByUrl($subscriptionUrl, $locale)
    {
        $subscription = self::whereHas('translations', function ($q) use ($subscriptionUrl, $locale) {
            $q->where('url', $subscriptionUrl);
            if ($locale) {
                $q->where('locale', $locale);
            }
        })->first();

        if ($subscription) {
            return $subscription;
        } else {
            return null;
        }
    }

    public function getConcept() : string
    {
        return $this->type;
    }

    public function isFitness() : bool
    {
        if($this->getConcept() == 'fitness')
        {
            return true;
        }
        return false;
    }

    public function isWellness() : bool
    {
        if($this->getConcept() == 'wellness')
        {
            return true;
        }
        return false;
    }
}
