<?php

namespace App;

use App\Traits\HasMetaFields;
use App\Traits\HasYWDFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasYWDFields, HasMetaFields;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'url',
        'title',
        'description',
        'country',
    ];

    public function initMetaInformations()
    {
        $this->metaTitleDefaultTransKey = $this->getTable();
        $this->metaTitleDefaultTransArgs = ['name' => $this->name];
        $this->metaDescriptionDefaultTransKey = $this->getTable();
        $this->metaDescriptionDefaultTransArgs = ['name' => $this->name];
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function clubs()
    {
        return $this->hasMany(Club::class);
    }
}
