<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Modal extends Model
{
    use SoftDeletes, Translatable;

    public $translatedAttributes = [
        'title', 'text',
    ];

    protected $fillable = [
        'title', 'text', 'inactivity_time',
    ];
}
