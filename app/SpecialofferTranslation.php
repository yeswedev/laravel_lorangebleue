<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialofferTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'subtitle',
        'description',
    ];
}
