<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'description',
        'url',
        'subtitle',
        'title1',
        'presentation1',
        'image1',
        'title2',
        'presentation2',
        'image2',
        'title3',
        'presentation3',
        'image3',
        'title4',
        'presentation4',
        'image4',
    ];

    /**
     * Set the url's attribute.
     *
     * @param  string  $value
     * @return void
     */
    public function setTitleAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->url = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
        $this->attributes['title'] = $value;
    }
}
