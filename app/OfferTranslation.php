<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'url',
        'type',
        'description',
        'is_on_home',
        'is_on_banner',
        'is_highlighted',
    ];

    /**
     * Set the url's attribute.
     *
     * @param  string  $value
     * @return void
     */
    public function setTitleAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->url = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
        $this->attributes['title'] = $value;
    }
}
