<?php

namespace App\Providers;

use App\LandingPage;
use App\Policies\LandingPagePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use Silvanite\Brandenburg\Traits\ValidatesPermissions;

class AuthServiceProvider extends ServiceProvider
{
    use ValidatesPermissions;

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\Club::class => \App\Policies\ClubPolicy::class,
        \App\Activity::class => \App\Policies\ActivityPolicy::class,
        \App\Advantage::class => \App\Policies\AdvantagePolicy::class,
        \App\Equipment::class => \App\Policies\EquipmentPolicy::class,
        \App\Offer::class => \App\Policies\OfferPolicy::class,
        \App\Modal::class => \App\Policies\ModalPolicy::class,
        \App\Specialoffer::class => \App\Policies\SpecialofferPolicy::class,
        \App\Subscription::class => \App\Policies\SubscriptionPolicy::class,
        \App\Yako::class => \App\Policies\YakoPolicy::class,
        \App\Order::class => \App\Policies\OrderPolicy::class,
        \App\City::class => \App\Policies\CityPolicy::class,
        \App\Department::class => \App\Policies\DepartmentPolicy::class,
        \App\Testimonial::class => \App\Policies\TestimonialPolicy::class,
        \App\Faq::class => \App\Policies\FaqPolicy::class,
        \App\Post::class => \App\Policies\PostPolicy::class,
        \App\LandingPage::class => \App\Policies\LandingPagePolicy::class,

        \YesWeDev\LaravelCMS\Post::class => \App\Policies\PostPolicy::class,
        \YesWeDev\LaravelCMS\Page::class => \App\Policies\PagePolicy::class,
        \App\PageGeneric::class => \App\Policies\PagePolicy::class,
        \YesWeDev\LaravelCMS\Archive::class => \App\Policies\ArchivePolicy::class,
        \YesWeDev\LaravelCMS\Attachment::class => \App\Policies\AttachmentPolicy::class,
        \YesWeDev\LaravelCMS\Block::class => \App\Policies\BlockPolicy::class,
        \App\BlocGeneric::class => \App\Policies\BlockPolicy::class,
        \YesWeDev\LaravelCMS\Tag::class => \App\Policies\TagPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        collect([
            'viewClub',
            'manageClub',

            'viewPost',
            'managePost',

            'viewArchive',
            'manageArchive',

            'viewAttachment',
            'manageAttachment',

            'viewBlock',
            'manageBlock',

            'viewTag',
            'manageTag',

            'viewActivity',
            'manageActivity',

            'viewAdvantage',
            'manageAdvantage',

            'viewEquipment',
            'manageEquipment',

            'viewOffer',
            'manageOffer',

            'viewModal',
            'manageModal',

            'viewPage',
            'managePage',

            'viewSpecialoffer',
            'manageSpecialoffer',

            'viewSubscription',
            'manageSubscription',

            'viewYako',
            'manageYako',

            'viewOrder',
            'manageOrder',

            'viewCity',
            'manageCity',

            'viewDepartment',
            'manageDepartment',

            'viewTestimonial',
            'manageTestimonial',

            'viewFaq',
            'manageFaq',

            'viewLandingPage',
            'manageLandingPage',
        ])->each(function ($permission) {
            Gate::define($permission, function ($user) use ($permission) {
                if ($this->nobodyHasAccess($permission)) {
                    return true;
                }

                return $user->hasRoleWithPermission($permission);
            });
        });

        $this->loadViewsFrom(__DIR__.'../nova/resources/views', 'nova');
        $this->loadTranslationsFrom(__DIR__.'../nova/resources/lang', 'nova');
        $this->loadJsonTranslationsFrom(resource_path('lang/vendor/nova'));

        $this->registerPolicies();
    }
}
