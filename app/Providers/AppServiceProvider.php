<?php

namespace App\Providers;

use App\Http\View\TunnelModalViewComposer;
use Blade;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

use Illuminate\Support\ServiceProvider;
use App\Http\View\ViewsComposer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Put Url as Slack username when error occurs
        if(url())
            // Jamais trop prudent ...
            if(url()->current())
                config(['logging.channels.slack.username' => url()->current()]);

        View::composer('*', ViewsComposer::class);
        View::composer('front.tunnel.components.tunnel_modal', TunnelModalViewComposer::class);

        $propertiesSportsOrganization = [
            "@context" => "http://schema.org",
            "@type" => "SportsOrganization",
            "name" => config('app.name'),
            "url" => config('app.url'),
            "logo" => config('app.url').'/images/logo-groupe.svg',
            "sameAs" => [
                "https://www.facebook.com/lorangebleuefrance/",
                "https://twitter.com/lorangebleuefr?lang=fr",
                "https://www.instagram.com/lorangebleuerennesstade/",
                "https://www.youtube.com/channel/UCSQxGvlqBg7K16PPhEN0J1A"
            ]
        ];

        $sports_organization_SEOcontext = "<script type=\"application/ld+json\">" . json_encode($propertiesSportsOrganization, JSON_UNESCAPED_UNICODE) . "</script>";

        view()->share('sports_organization_SEOcontext', $sports_organization_SEOcontext);

        $propertiesWebSite = [
            "@context" => "http://schema.org",
            "@type" => "WebSite",
            "name" => config('app.name'),
            "url" => config('app.url'),
        ];

        $website_SEOcontext = "<script type=\"application/ld+json\">" . json_encode($propertiesWebSite, JSON_UNESCAPED_UNICODE) . "</script>";

        view()->share('website_SEOcontext', $website_SEOcontext);

        Blade::directive('replace', function ($arguments) {
            if (count(explode(',', $arguments)) < 3) {
                return '';
            }
            list($expression, $search, $replace) = explode(',', $arguments);
            return "<?php echo str_replace($search, $replace, ($expression)); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Lib\LaravelLocalization::class, function () {
            return new \App\Lib\LaravelLocalization();
        });

        $this->app->alias(\App\Lib\LaravelLocalization::class, 'laravellocalization');
    }
}
