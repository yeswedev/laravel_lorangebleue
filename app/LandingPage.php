<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use YesWeDev\LaravelCMS\Traits\HasBlocks;

class LandingPage extends Model
{
    use HasBlocks, Translatable;

    public $translatedAttributes = [
        'h1',
        'subtitle',
        'price',
        'video_title',
        'video_text',
        'concept_list_fitness',
        'concept_list_wellness',
    ];

    protected $casts = [
        'concept_list_fitness' => 'array',
        'concept_list_wellness' => 'array',
    ];

    public function clubs() {
        return $this->belongsToMany(Club::class, 'landing_pages_clubs', 'landing_page_id', 'club_id');
    }

    protected $with = [
        'translations'
    ];
}
