<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LandingContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $subject = config('app.name').' | Pack Digital - Vous avez un nouveau prospect pour le club de '. $this->data["club"]->title;

        return $this->markdown('emails.landing-page-mail')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyTo($this->data['email'])
            ->subject($subject);
    }
}
