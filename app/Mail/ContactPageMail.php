<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactPageMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $subject = config('app.name').' | '.trans('trad.contact-open-club.new-msg');

        if (array_key_exists('file', $this->data)) {
            $this->attach(
                $this->data['file']->getRealPath(),
                [
                    'as' => $this->data['file']->getClientOriginalName(),
                    'mime' => $this->data['file']->getMimeType()]
            );
        }

        return $this->markdown('emails.contact_page_mail')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyto($this->data['email'])
            ->subject($subject);
    }
}
