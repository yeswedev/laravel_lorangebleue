<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CandidatureMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $subject = 'Candidature Spontanée';
        if ( isset($this->data['job_ref']) ) {
            $subject = $this->data['job_nomposte']." ".$this->data['job_ref'];
        }

        $this->to('fr-lorangebleue@redirection-eolia.com');
        $this->subject($subject);
        $this->from( config('mail.from.address'), config('mail.from.name'));
        $this->replyto($this->data['email']);

        if (array_key_exists('cv', $this->data)) {
            $this->attach(
                $this->data['cv']->getRealPath(),
                [
                    'as' => 'cv.' . $this->data['cv']->getClientOriginalExtension(),
                    'mime' => $this->data['cv']->getMimeType()]
            );
        }

        if (array_key_exists('motivation', $this->data)) {
            $this->attach(
                $this->data['motivation']->getRealPath(),
                [
                    'as' => 'motivation_letter.' . $this->data['motivation']->getClientOriginalExtension(),
                    'mime' => $this->data['motivation']->getMimeType()]
            );
        }

        return $this->text('emails.mail-candidature');
    }
}
