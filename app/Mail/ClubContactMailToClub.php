<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClubContactMailToClub extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $subject = config('app.name').' | Nouveau message ';

        return $this->markdown('emails.club-contact-mail-to-club')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyTo($this->data['clientEmail'])
            ->subject($subject);
    }
}
