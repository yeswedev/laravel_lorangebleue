<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmationSubscriptionClientMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $subject = config('app.name').' | Confirmation de la prise d\'abonnement ';

        return $this->markdown('emails.confirmation-client')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyto($this->data['clubEmail'])
            ->subject($subject);
    }
}
