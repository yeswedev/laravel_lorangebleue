<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLocalization;
use Carbon\Carbon;

class Offer extends Model
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'title',
        'type',
        'url',
        'legal_mentions',
        'is_on_home',
        'is_on_banner',
        'is_highlighted',
        'is_on_home_es',
        'is_on_banner_es',
        'is_highlighted_es',
        'is_on_home_ma',
        'is_on_banner_ma',
        'is_highlighted_ma',
    ];

    protected $fillable = [
        'commitment',
        'price',
        'type',
        'is_student',
        'carryover_days',
        'description',
        'is_permanent',
        'start_date',
        'end_date',
    ];

    protected $attributes = [
        'offerToReplace' => null,
    ];

    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function specialoffers()
    {
        return $this->hasMany(Specialoffer::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function Subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function offerSpecialoffer()
    {
        return $this->belongsToMany(Specialoffer::class, 'offer_specialoffer', 'offer_id', 'specialoffer_id');
    }

    public function availableSpecialOffers()
    {
        return $this->offerSpecialoffer()->whereDate('start_date', '<=', date('Y-m-d'))->whereDate('end_date', '>=', date('Y-m-d'));
    }

    public function offerClub()
    {
        return $this->belongsToMany(Club::class, 'club_offer', 'offer_id', 'club_id');
    }

    public function offerToReplace() {
        return $this->belongsTo(Offer::class, 'offerToReplace');
    }

    public static function ponctualOffers( $builder ) {
        return $builder->where("is_permanent", false)
            ->whereDate("start_date", "<=", Carbon::now()->format("Y-m-d"))
            ->whereDate("end_date", ">=", Carbon::now()->format("Y-m-d"));
    }

    public static function permanentOffers( $builder ) {
        return $builder->where("is_permanent", true);
    }

    public static function activeOffers( $builder ) {
        $ponctualOffers = self::ponctualOffers( clone $builder )->get();
        $rejectedPermanentOffers = self::ponctualOffers( clone $builder )->pluck('offerToReplace')->toArray();
        $permanentOffers = self::permanentOffers( clone $builder )->whereNotIn( 'id', $rejectedPermanentOffers )->get();

        return $ponctualOffers->merge( $permanentOffers );
    }

    /**
     * @return bool | Offer
     */
    public function getBestAvailableOffers($clubId)
    {
        if (!count($this->availableSpecialOffers)) {
            return false;
        }
        $specialOffer = $this->availableSpecialOffers->first();
        if (!$specialOffer) {
            return false;
        }
        if ($clubId && !$specialOffer->specialofferClub->contains($clubId)) {
            return false;
        }
        return $specialOffer;
    }

    public function getFirstMonthPrice($prices)
    {
        if (!$prices || !count($prices)) {
            $prices = $this->getPrices();
        }
        return $prices['reduced_commitment'] ? $prices['reduced_price'] : $prices['price'];
    }

    public function getPriceOffer($prices)
    {
        if (!$prices || !count($prices)) {
            $prices = $this->getPrices();
        }
        return $prices['reduced_price_offer'];
    }

    /**
     * @return array
     */
    public function getPrices($clubId = null)
    {
        $prices = [
            'id' => $this->id,
            'price' => $this->price,
            'reduced_price' => $this->reduced_price,
            'price_offer' => $this->price_offer,
            'reduced_price_offer' => $this->reduced_price_offer,
            'commitment' => $this->commitment,
            'reduced_commitment' => $this->reduced_commitment,
            'is_student' => $this->is_student,
            'is_on_home' => $this->is_on_home,
            'is_on_banner' => $this->is_on_banner,
            'is_highlighted' => $this->is_highlighted,
            'title' => $this->title,
            'carryover_days' => $this->carryover_days,
        ];

        $specialOffer = $this->getBestAvailableOffers($clubId);
        if ($specialOffer) {
            $prices['reduced_price'] = 490;
            if ($this->commitment == 12) {
                $prices['commitment'] = 13;
                $prices['reduced_commitment'] = 1;
            }
        }

        return $prices;
    }

    public static function getFormattedBannerPrice()
    {
        $price = false;
        $bannerOffer = self::activeOffers( self::getIsOnBannerOffersFilter(self::query()) )->first();
        if ($bannerOffer) {
            $prices = $bannerOffer->getPrices();
            $price = number_format(
                ($prices['reduced_price']?$prices['reduced_price']:$prices['price']) / 100,
                2,
                '.',
                ''
            );
        }
        return $price;
    }

    public static function getAvantageDiscount()
    {
        $price = false;
        $bannerOffer = self::getIsOnBannerOffersFilter(self::query())->first();
        if ($bannerOffer)
            $price = ($bannerOffer->price_offer - $bannerOffer->reduced_price_offer) / 100 ;

        return $price;
    }

    public function getUniqueTotalPriceOrder($prices)
    {
        if (!$prices) {
            $prices = $this->getPrices($this->club_id);
        }
        $firstMonthTotal = $prices['reduced_price'] * $prices['reduced_commitment'];
        $monthLeftTotal = $prices['price'] * ($prices['commitment'] - $prices['reduced_commitment']);
        $priceOrder = $this->getPriceOffer($prices);

        return $firstMonthTotal + $monthLeftTotal + $priceOrder;
    }

    public static function getOffersLocalizationFilter($attr, $offers, $locale = null)
    {
        if (!$locale) {
            $locale = LaravelLocalization::getCurrentLocale();
        }
        if (LaravelLocalization::checkLocaleInSupportedLocales($locale)) {

            $queryTranslation = $offers->whereHas('translations', function($query) use($locale, $attr){
                $query->where('locale', $locale)
                ->where($attr, true);
            });
            return $queryTranslation;
        }

        $defaultLocale = LaravelLocalization::getDefaultLocale();

        $queryTranslation = $offers->whereHas('translations', function($query) use($defaultLocale, $attr){
            $query->where('locale', $defaultLocale)
            ->where($attr, true);
        });
        return $queryTranslation;
    }

    public static function getIsOnHomeOffersFilter($offers, $locale = null)
    {
        return self::getOffersLocalizationFilter('is_on_home', $offers, $locale);
    }

    public static function getIsHighlightedOffersFilter($offers, $locale = null)
    {
        return self::getOffersLocalizationFilter('is_highlighted', $offers, $locale);
    }

    public static function getIsOnBannerOffersFilter($offers, $locale = null)
    {
        return self::getOffersLocalizationFilter('is_on_banner', $offers, $locale);
    }
}
