<?php

use App\Lib\Geocoding;
use GuzzleHttp\Exception\RequestException;
use Carbon\Carbon;

function getEoliaJobs()
{
    $client = new GuzzleHttp\Client();

    $jobs = ['job' => []];
    try {
        $request = $client->request('GET', config('app.eoliaurl'));

        $xml = simplexml_load_string($request->getBody(), 'SimpleXMLElement', LIBXML_NOCDATA);

        $json = json_encode($xml);
        $jobs = json_decode($json, true);
        usort($jobs['job'], function ($a, $b) {
            $timestampA = Carbon::createFromFormat('d/m/Y H:i:s', $a['datecreation']);
            $timestampB = Carbon::createFromFormat('d/m/Y H:i:s', $b['datecreation']);
            return $timestampB > $timestampA;
        });
    } catch (RequestException $e) {
    }
    return $jobs;
}

function orderJobsByCities( $jobs_array ) {
    foreach ( $jobs_array as $job ) {
        preg_match('#(.*)\s\((.*)\)#', $job['saisie2'], $city);

        if( $city != [] ) {
            $job_city = $city[1];
            $cities_with_jobs[ $city[1] ] = [
                'postal_code'   => $city[2],
            ];

            foreach ( $jobs_array as $key => $job2 ) {
                preg_match('#(.*)\s\((.*)\)#', $job2['saisie2'], $city);

                if ( $city != [] && $job['pays'] == $job2['pays'] && $job_city == $city[1] ) {
                    preg_match("#(.*\S) ".$job_city."#", $job2['nomposte'], $post_name);

                    if ( array_key_exists('number', $cities_with_jobs[ $city[1] ]) ) {
                        $cities_with_jobs[ $city[1] ]['number']++;
                        $cities_with_jobs[ $city[1] ]['ids'][] = $job2['id'];
                        $cities_with_jobs[ $city[1] ]['names'][] = $post_name != [] ? $post_name[1] : $job2["nomposte"];
                    } else {
                        $city_infos = Geocoding::findLongLatFromAddress( $job_city, $job2['pays'] );
                        $cities_with_jobs[ $city[1] ]['lat'] = $city_infos['lat'];
                        $cities_with_jobs[ $city[1] ]['lng'] = $city_infos['lng'];
                        $cities_with_jobs[ $city[1] ]['number'] = 1;
                        $cities_with_jobs[ $city[1] ]['ids'][] = $job2['id'];
                        $cities_with_jobs[ $city[1] ]['names'][] = $post_name != [] ? $post_name[1] : $job2["nomposte"];
                    }
                }
            }
        }
    }
    ksort($cities_with_jobs);
    return $cities_with_jobs;
}

function getPaysAndCountrySelects( $jobs ) {
    $pays_unique = [];
    $countries = [];

    foreach ($jobs['job'] as $key => $job) {
        preg_match('#\((.*?)\)#', $job['saisie2'], $match);
        $job_pays = ucfirst( strtolower($job['pays']) );
        $str = '';
        if(!empty($match))
        {
            $countries[ $job_pays ][] = $job['region'];
            $countries[ $job_pays ] = array_values(array_unique($countries[ $job_pays ]));
            sort( $countries[ $job_pays ] );

            $pays[] = $job_pays;
            $count_pays = array_count_values($pays);

            foreach ($pays as $str) {
                $str = str_replace($job_pays, $job_pays.' ('.$count_pays[$job_pays].')', $str);
            }
            $pays_unique[$job_pays] = $str;
        }
    }

    return [
        "countries"     => $countries,
        "pays_unique"   => $pays_unique
    ];
}

function filterJobs( $jobs, $inputs ) {
    $filter = $jobs['job'];

    foreach ($jobs['job'] as $key => $job) {

        if ( ( isset($inputs['country']) && $inputs['country'] != null && $inputs['country'] != "") &&
            $job['region'] != $inputs['country'] ) {
            unset( $filter[$key] );
        }

        if ( ($inputs['job'] != null && $inputs['job'] != "") &&
            !preg_match("/".strtolower($inputs['job'])."/i", strtolower($job['activite']) ) ) {
            unset( $filter[$key] );
        }

        if ( ( isset($inputs['pays']) && $inputs['pays'] != null && $inputs['pays'] != "") &&
            strtoupper($job['pays']) != strtoupper($inputs['pays']) ) {
            unset( $filter[$key] );
        }
    }

    return $filter;
}