<?php

use Illuminate\Support\Facades\Lang as Lang;

function includeAsJsString($template, $object = null)
{
    $string = view($template)->with('object', $object);
    return (string)$string;
}

function formatPrices($price)
{
    return number_format($price, 2, ',', '');
}

function issetTrad($trad)
{
    return Lang::has($trad);
}

function isActive(array $argument)
{
    $activeUrl = request()->url();
    $tabUrls = explode('/', $activeUrl);
    $cpt = 0;
    foreach ($tabUrls as $tabUrl) {
        if (in_array($tabUrl, $argument)) {
            $cpt++;
        }
    }

    if ($cpt > 0) {
        return 'menu_item--active';
    }
}
