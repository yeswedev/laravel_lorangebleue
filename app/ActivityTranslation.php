<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title','presentation', 'description', 'url', 'objectives', 'targeted_part', 'calories'];

    /**
     * Set the url's model.
     *
     * @param  string  $value
     * @return void
     */
    public function setUrlAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->attributes['url'] = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
    }
}
