<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YakoTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'description',
        'url',
        'detail',
        'objectives',
        'targeted_part',
        'calories',
        'structure',
        'advantage',
        'public',
        'subtitle'
    ];

    /**
     * Set the url's model.
     *
     * @param  string  $value
     * @return void
     */
    public function setUrlAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->attributes['url'] = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
    }
}
