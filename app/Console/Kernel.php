<?php

namespace App\Console;

use App\Console\Commands\CheckMangopayPayin;
use App\Console\Commands\CronTransfer;
use App\Console\Commands\RemoveUselessOrders;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\CronTransfer',
        '\App\Console\Commands\CheckMangopayPayin',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (config('app.env') == 'production') {
            $schedule->command(CheckMangopayPayin::class)
                ->dailyAt('3:00');
            $schedule->command(CronTransfer::class)
                ->dailyAt('3:10');
            $schedule->command(RemoveUselessOrders::class)
                ->dailyAt('2:50');
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
