<?php

namespace App\Console\Commands;

use App\Club;
use Illuminate\Console\Command;

class FixClubDescription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:club-description';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parcours tous les clubs, cherche la meta description dans la description et la supprime si existante.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = 0;
        foreach (Club::get() as $club) {
            $search = "-vous en ligne !<br><br>";
            $pos = strpos($club->description, $search);
            if ($pos > -1 && $pos < 200) {
                $count ++;
                $newDescription = '<div>' . str_after($club->description, $search);
                $club->description = $newDescription;
                $club->save();
            }
        }
        echo $count.' clubs updated!';
    }
}
