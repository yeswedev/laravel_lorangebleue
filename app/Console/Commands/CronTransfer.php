<?php

namespace App\Console\Commands;

use \MangoPay\MangoPayApi;
use App\Lib\Mangopay;

use App\Order;

use App\Payment;

use Carbon\Carbon;
use Illuminate\Console\Command;

class CronTransfer extends Command
{
    private $mangopay;

    // value in days
    const TIME_TRANSFERT = 15;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronTransfer:crontransfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test cron transfer and payout';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MangoPayApi $mangopay)
    {
        parent::__construct();
        $this->mangopay = $mangopay;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mango = new Mangopay($this->mangopay);
        
        $dateAfter = Carbon::today()->subDays(self::TIME_TRANSFERT);
        $dateBefore = Carbon::today()->subDays(self::TIME_TRANSFERT - 1);

        $orders = Order::where('created_at', '>', $dateAfter)
            ->where('created_at', '<=', $dateBefore)
            ->where('status', Order::Succeeded)
            ->whereHas('payments', function ($q) {
                $q->where('status', Order::Succeeded);
            })
            ->get();

        foreach ($orders as $order) {
            $payment = $order->payments->where('status', Payment::Succeeded)->last();
            if (!$payment) {
                continue;
            }
            $payInId = $payment->operation_id;
            $amount = $payment->amount / 100;

            try {
                $payIn = $mango->getPayIn($payInId);
            } catch (\Exception $e) {
                \Log::error($e->getMessage()."\n");
                echo("\033[31m\033[1m".$e->getMessage()."\n");
                continue;
            }

            $club = $order->club;

            if($club)
            {
                $mango->doTransfer($payIn, $club->id, $amount);
            }
            else
            {
                \Log::error("Club non existant");
            }
        }
    }
}
