<?php

namespace App\Console\Commands;

use App\Club;
use Illuminate\Console\Command;

class FixClubPlanningImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:clubplanningimage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parcours tous les clubs et supprime "/storage/" des image de planning ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Club::get() as $club) {
            $pos = strpos($club->planning_image, "/storage/");
            if ($pos > -1) {
                $club->planning_image = str_replace("/storage/", "", $club->planning_image);
                $club->save();
            }
        }
    }
}
