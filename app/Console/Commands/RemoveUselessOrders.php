<?php

namespace App\Console\Commands;

use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveUselessOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:remove-useless-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Supprimer les orders avec email NULL ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Order::whereNull('email')->whereDate('created_at', '<', Carbon::now()->subMonth(1))->delete();
    }
}
