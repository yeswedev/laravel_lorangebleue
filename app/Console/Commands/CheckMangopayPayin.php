<?php

namespace App\Console\Commands;

use \MangoPay\MangoPayApi;

use App\Lib\Mangopay;

use Carbon\Carbon;
use Illuminate\Console\Command;

use MangoPay\EventType;
use MangoPay\SortDirection;

class CheckMangopayPayin extends Command
{
    private $mangopay;

    const LAST_DAYS_NUMBER = 5;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckMangopayPayin:checkpayin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check last successfull payin and handle them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MangoPayApi $mangopay)
    {
        parent::__construct();
        $this->mangopay = $mangopay;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mango = new Mangopay($this->mangopay);

        $today = new Carbon();
        $firstDay = $today->copy()->subDays(5);
        $lastDay = $today;
        $BeforeDate = $lastDay->timestamp;
        $AfterDate = $firstDay->timestamp;
        $sortDirection = SortDirection::DESC;
        $ItemPerPage = 50;
        $EventType = EventType::PayinNormalSucceeded;

        $allEvents = [];
        $page = 1;
        $events = $mango->GetAllEvents($ItemPerPage, $page, $EventType, $BeforeDate, $AfterDate, $sortDirection);
        while (count($events)) {
            $allEvents = array_merge($allEvents, $events);
            $page++;
            $events = $mango->GetAllEvents($ItemPerPage, $page, $EventType, $BeforeDate, $AfterDate, $sortDirection);
        }

        foreach ($allEvents as $event) {
            $mango->updatePayment($event->ResourceId);
        }

        return;
    }
}
