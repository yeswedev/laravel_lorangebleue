<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RenameClubs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:clubs-07032019';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'L\'OB demande à renommer ses clubs, cette commande met en place ce renaming (nom + url)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clubsName = [
            "angoulins"=>"la-rochelle-angoulins",
            "azay-le-brule"=>"saint-maixent-lecole-azay-le-brule",
            "baule"=>"meung-sur-loire-baule",
            "beaupreau"=>"beaupreau-en-mauges",
            "bouguenais"=>"nantes-bouguenais",
            "brive"=>"brive-la-gaillarde",
            "buxerolles"=>"poitiers-buxerolles",
            "carhaix"=>"carhaix-plouguer",
            "cesson-sevigne"=>"cesson-sevigne-la-rigourdiere",
            "cesson-sevigne-2"=>"cesson-sevigne",
            "chalezeule"=>"besancon-chalezeule",
            "champniers"=>"angouleme-champniers",
            "decines"=>"lyon-decines",
            "semecourt"=>"semecourt-feves",
            "feytiat"=>"limoges-feytiat",
            "bouliac"=>"bouliac-floirac",
            "getigne"=>"clisson-getigne",
            "gruchet-le-valasse"=>"bolbec-gruchet-le-valasse",
            "guilherand-granges"=>"valence-guilherand-granges",
            "englos"=>"englos-haubourdin",
            "jardres-chauvigny"=>"chauvigny-jardres",
            "la-teste-de-buch"=>"arcachon-la-teste-de-buch",
            "lahonce"=>"bayonne-lahonce",
            "lavau"=>"troyes-lavau",
            "le-soler"=>"perpignan-le-soler",
            "longuenesse"=>"saint-omer-longuenesse",
            "malemort"=>"malemort-sur-correze",
            "masny"=>"douai-masny",
            "mellac"=>"quimperle-mellac",
            "mondeville"=>"caen-mondeville",
            "montfort-sur-meu"=>"montfort-sur-meu-bedee",
            "peronnas"=>"bourg-en-bresse-peronnas",
            "querqueville"=>"cherbourg-querqueville",
            "rivieres"=>"la-rochefoucauld-rivieres",
            "sable-sur-sarthe"=>"sable-sur-sarthe-solesmes",
            "saint-egreve"=>"grenoble-saint-egreve",
            "saintes"=>"saintes-saint-georges-des-coteaux",
            "saint-jean-de-braye"=>"orleans-saint-jean-de-braye",
            "saint-marcel-vernon"=>"vernon-saint-marcel",
            "avranches"=>"avranches-saint-martin-des-champs",
            "morlaix"=>"morlaix-saint-martin-des-champs",
            "melle"=>"melle-saint-martin-les-melle",
            "martigues"=>"martigues-saint-mitre-les-remparts",
            "tourlaville"=>"cherbourg-tourlaville",
            "tregueux"=>"saint-brieuc-tregueux",
            "trie-chateau-gisors"=>"gisors-trie-chateau",
            "trignac"=>"saint-nazaire-trignac",
            "urrugne"=>"saint-jean-de-luz-urrugne",
            "verquigneul"=>"bethune-verquigneul",
            "villebarou"=>"blois-villebarou",
            "villemandeur"=>"montargis-villemandeur",
            "wittenheim"=>"mulhouse-wittenheim",
        ];

        foreach (\App\Club::get() as $club) {
            if (isset($clubsName[$club->url])) {
                \Log::info('url '.$club->url. ' => '.$clubsName[$club->url]);
                $club->url = $clubsName[$club->url];
                $club->save();
            }
        }
    }
}
