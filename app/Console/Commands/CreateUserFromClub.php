<?php

namespace App\Console\Commands;

use App\Club;
use App\User;
use Illuminate\Console\Command;

class CreateUserFromClub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createuserfromclub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create users with role 'gerant' for each clubs";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Club::get() as $key => $club) {
            try {
                if (User::where('email', '=', $club->email)->exists()) {
                    $user = User::where('email', '=', $club->email)->first();

                    if (!$user) {
                        throw new Exception('User not found');
                    }
                } else {
                    $user = new User;
                    $user->email = $club->email;
                    echo("\033[34m\033[1m".$user->email."\n");
                    $user->name = 'gerant';
                    $user->firstname = $club->title;
                    $user->password = bcrypt('ob-'.$club->email);
                    $user->save();
    
                    $user->roles()->attach(2);
                }
    
                $user->userClub()->attach($club->id);
                $user->save();
            } catch (\Exception $e) {
                echo("\033[31m\033[1m".'Error: user'.$user->email.'cannot be created from the club'."\n");
                \Log::error('Error: user'.$user->email.'cannot be created from the club');
            }
        }
    }
}
