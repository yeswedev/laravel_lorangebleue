<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use App\Traits\HasMetaFields;
use App\Traits\HasYWDFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use Translatable, HasYWDFields, HasMetaFields;
    use SoftDeletes;

    public $translatedAttributes = [
        'title','presentation' , 'description', 'url', 'objectives', 'targeted_part', 'calories'
    ];

    protected $fillable = [
        'title','presentation' , 'image', 'description', 'url', 'rate_calorie','type'
    ];

    public function resolveRouteBinding($value)
    {
        return $this->whereTranslation('url', $value)->first() ?? abort(404);
    }

    public function activityYako()
    {
        return $this->belongsToMany(Yako::class);
    }

    public function activityClub()
    {
        return $this->belongsToMany(Club::class, 'activity_club', 'activity_id', 'club_id');
    }
}
