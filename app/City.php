<?php

namespace App;

use App\Lib\Geocoding;
use App\Traits\HasLatitudeLongitude;
use App\Traits\HasMetaFields;
use App\Traits\HasYWDFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLocalization;

class City extends Model
{
    use HasYWDFields, HasMetaFields, HasLatitudeLongitude;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'url',
        'title',
        'description',
        'department_id',
        'latitude',
        'longitude',
        'country',
        'image_tiny',
        'image_tiny_alt',
    ];

    public function initMetaInformations()
    {
        $this->metaTitleDefaultTransKey = $this->getTable();
        $this->metaTitleDefaultTransArgs = ['name' => $this->name];
        $this->metaDescriptionDefaultTransKey = $this->getTable();
        $this->metaDescriptionDefaultTransArgs = ['name' => $this->name];
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        if (!$this->latitude || !$this->longitude) {
            $geoData = Geocoding::findLongLatFromAddress($this->name, $this->country);

            if ($geoData['lat'] && $geoData['lng']) {
                $this->latitude = $geoData['lat'];
                $this->longitude = $geoData['lng'];
            }
        }


        return parent::save($options);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * @return array
     */
    public static function getMainCitiesName()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        if ($locale == 'fr')
        {
            return ['paris', 'rennes', 'bordeaux', 'toulouse', 'strasbourg', 'lyon', 'montpellier', 'le mans', 'orleans', 'caen', 'rouen', 'nantes'];
        }
        elseif($locale == 'es')
        {
            return self::where('country', 'es')->pluck('url')->toArray();
        }
        else
        {
            return self::where('country', 'ma')->pluck('url')->toArray();
        }
    }

    /**
     * @return mixed
     */
    public static function getMainCities()
    {
        $citySlugs = self::getMainCitiesName();
        return self::whereIn('url', $citySlugs)->get();
    }


    /**
     * @return array
     */
    public static function get50MainCitiesName()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        if ($locale == 'fr') {
            return ["toulouse", "montpellier", "bordeaux", "rennes", "strasbourg", "marseille", "dijon nord", "angers", "tours", "aix en provence", "annecy", "poitiers", "amiens", "le mans", "pau", "brest", "reims", "versailles", "avignon", "clichy", "vannes", "colmar", "cergy", "niort", "quimper", "bayonne", "montrouge", "montauban", "lorient", "valenciennes", "albi", "massy", "argenteuil", "tarbes", "thionville", "puteaux", "laval", "troyes", "belfort", "cholet", "agen", "villeneuve d ascq", "arras", "bourges", "colombes", "blois", "biarritz", "chartres", "evreux", "martigues", "brive", "buxerolles", "reims farman", "palaiseau", "remilly", "lavau", "villebarou", "le vieil evreux", "pau lescar", "lahonce", "brest centre"];
        } else {
            return self::where('country', $locale)->pluck('url')->toArray();
        }
    }
}
