<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentTranslation extends Model
{
    protected $table = 'equipments_translations';
    public $timestamps = false;
    protected $fillable = ['title','type', 'description', 'url', 'link'];

    /**
     * Set the url's model.
     *
     * @param  string  $value
     * @return void
     */
    public function setUrlAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->attributes['url'] = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
    }
}
