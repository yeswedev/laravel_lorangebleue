<?php

namespace App\Jobs;

use App\Mail\ClubContactMailToClub;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

/**
 * Class ClubContactMailToClubJob
 * @package App\Jobs
 */
class ClubContactMailToClubJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * @var
   */
  public $emailData;
  /**
   * @var
   */
  public $to;


  /**
   * ClubContactMailToClubJob constructor.
   * @param string $to
   * @param array $emailData
   */
  public function __construct(string $to, array $emailData)
  {
    $this->to = $to;
    $this->emailData = $emailData;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $email = new ClubContactMailToClub($this->emailData);

    Mail::to($this->to)->send($email);
  }
}
