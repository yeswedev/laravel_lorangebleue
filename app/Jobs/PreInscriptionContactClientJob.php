<?php

namespace App\Jobs;

use App\Mail\PreInscriptionContactClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class PreInscriptionContactClientJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  public $emailData;
  public $to;

  /**
   * PreInscriptionContactClientJob constructor.
   * @param string $to
   * @param array $emailData
   */
  public function __construct(string $to, array $emailData)
  {
    $this->to = $to;
    $this->emailData = $emailData;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $email = new PreInscriptionContactClient($this->emailData);

    Mail::to($this->to)->send($email);
  }
}
