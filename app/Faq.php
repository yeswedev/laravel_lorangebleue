<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'question',
        'answer',
    ];
}
