<?php

namespace App;

use App\Mail\ConfirmationSubscriptionClientMail;
use App\Mail\ConfirmationSubscriptionClubMail;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Keygen\Keygen;

use Mail;

class Order extends Model
{
    use SoftDeletes;

    const Created = 'CREATED';
    const Succeeded = 'SUCCEEDED';
    const Failed = 'FAILED';

    protected $fillable = [
        'civility',
        'firstname',
        'lastname',
        'birthdate',
        'email',
        'phone_number',
        'address',
        'alt_address',
        'zip_code',
        'city',
        'step',
        'newsletter',
        'order_token',
        'status',
    ];

    protected $dates = [
        'birthdate',
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function specialoffer()
    {
        return $this->belongsTo(Specialoffer::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function getTotalPriceOrder()
    {
        $prices = $this->offer->getPrices($this->club_id);

        return $this->offer->getFirstMonthPrice($prices) + $this->offer->getPriceOffer($prices);
    }

    public function getUniqueTotalPriceOrder($prices)
    {
        if (!$prices) {
            $prices = $this->offer->getPrices($this->club_id);
        }
        $firstMonthTotal = $prices['reduced_price'] * $prices['reduced_commitment'];
        $monthLeftTotal = $prices['price'] * ($prices['commitment'] - $prices['reduced_commitment']);
        $priceOrder = $this->offer->getPriceOffer($prices);

        return $firstMonthTotal + $monthLeftTotal + $priceOrder;
    }

    public function getTotalPrice($modePayment, $prices)
    {
        if ($modePayment == 'unique') {
            return $this->getUniqueTotalPriceOrder($prices);
        } else {
            return $this->getTotalPriceOrder();
        }
    }


    public function setStatus($newStatus)
    {
        if ($newStatus != $this->status && $newStatus == Order::Succeeded) {
            $this->status = $newStatus;
            $this->code = $this->code ?: strtoupper(Keygen::alphanum(9)->generate());
            $this->save();
            $payment = Payment::where('order_id', $this->id)->where('status', Payment::Succeeded)->first();

            $initParams = [
                'clientEmail' => $this->email,
                'clientFirstname' => $this->firstname,
                'clientLastname' => $this->lastname,
                'clientPhone' => $this->phone_number,
                'clubTitle' => $this->club->title,
                'clubAddress' => $this->club->address,
                'clubZipCode' => $this->club->zip_code,
                'clubCity' => $this->club->city,
                'clubPhone' => $this->club->phone,
                'clubEmail' => $this->club->email,
                'clubUrl' => $this->club->url,
                'subscriptionType' => $this->club->subscription->type,
                'offerTitle' => $this->offer->title,
                'totalPrice' => number_format($payment->amount / 100, 2, ',', ' '),
                'offerPrice' => number_format($this->offer->price / 100, 2, ',', ' '),
                'orderCreatedDate' => $this->created_at->format('d/m/Y'),
                'orderCode' => $this->code,
                'orderIsStudent' => $this->is_student,
            ];
            \Log::info($initParams);

            Mail::to($this->club->email)->send(new ConfirmationSubscriptionClubMail($initParams));
            Mail::to($this->email)->send(new ConfirmationSubscriptionClientMail($initParams));

            // Call Tagcommander URL
            $productId = preg_replace('/[^0-9]/', '', $this->offer->title) == $this->offer->commitment ?
                preg_replace('/ /', '_', strtoupper($this->offer->title)) :
                strtoupper($this->offer->title) . '_' . $this->offer->commitment;
            $query = [
                'CID' => $this->ga_user,
                'ORDER_ID' => $this->id,
                'ORDER_AMOUNT_ATI_TOTAL' => number_format(($this->getUniqueTotalPriceOrder(false) / 100), 2, '.', ''),
                'ORDER_TAX_TOTAL' => number_format((Order::getTaxFromTTC($this->getUniqueTotalPriceOrder(false)) / 100), 2, '.', ''),
                'ORDER_PRODUCT_PAYS_CLUB_FORMULE' => strtoupper($this->club->country) . '_'. str_replace(' ', '_', $this->club->title) . '_' . strtoupper($this->offer->title),
                'ORDER_PRODUCT_UNITPRICE_ATI' => number_format(($this->offer->price / 100), 2, '.', ''),
                'ORDER_PRODUCT_ID' => $productId,
                'ORDER_PRODUCT_CATEGORY' => $this->offer->Subscription->url,
                'ORDER_PRODUCT_NAME' => $this->offer->title,
                'USER_VILLE_CLUB' => $this->club->city."_".$this->club->title,
                'VILLE_CLUB' => $this->club->city."_".$this->club->title,
                'HOSTNAME' => request()->root(),
            ];
            \Log::info('Tagcommander');
            \Log::info($query);
            try {
                $client = new Client();
                $client->request('GET', 'http://serverside4392.tagcommander.com/22/', [
                    'query' => $query
                ]);
            } catch (\RequestException $e) {
                \Log::error($e);
            }
        } else {
            $this->status = $newStatus;
            $this->save();
        }

        return $this;
    }

    public function createPayment($modePayment, $service, $operation_id)
    {
        $payment = new Payment;
        $payment->order_id = $this->id;
        $payment->type = $modePayment;
        $payment->service = $service;
        $payment->operation_id = $operation_id;

        if ($payment->type == 'unique') {
            $payment->amount = $this->getUniqueTotalPriceOrder(false);
        } else {
            $payment->amount = $this->getTotalPriceOrder(false);
        }

        $payment->save();

        return $payment;
    }

    public static function getHTFromTTC($ttc)
    {
        if (!$ttc) {
            return 0;
        }
        return intval($ttc) / (1 + (intval(config('tax.pourcentage'))/100));
    }

    public static function getTaxFromTTC($ttc)
    {
        if (!$ttc) {
            return 0;
        }
        return intval($ttc) - self::getHTFromTTC($ttc);
    }
}
