<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlocGeneric extends Model
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'title',
        'subtitle',
        'description',
        'image_alt',
        'image_alt_2',
    ];

    protected $fillable = [
        'type',
        'image',
        'image_2',
    ];

    protected $with = [
        'translations'
    ];
}
