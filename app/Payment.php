<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const Created = 'CREATED';
    const Succeeded = 'SUCCEEDED';
    const Failed = 'FAILED';

    protected $fillable = [
        'amount',
        'status',
        'type',
        'service',
        'operation_id',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function setStatus($status)
    {
        $this->status = $status;

        $this->save();

        if ($this->status == self::Succeeded) {
            $this->order->setStatus(Order::Succeeded);
        }

        return $this;
    }
}
