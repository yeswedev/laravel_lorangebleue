<?php

namespace App\Lib;

use Illuminate\Database\Eloquent\Collection;

use Log;
use Exception;

class YwdTools
{
    public static function getOfferDepartmentName($code)
    {
        $path = resource_path('data/departments.json');
        $json = file_get_contents($path);

        try {

            if($json)
            {
                $departments = json_decode($json, true);

                foreach($departments as $department)
                {
                    if($code == $department['code'])
                    {
                        $result = $department['nom']; 
                        return $result;
                    }
                }
            }

        } catch (Exception $e) {
            Log::info('Fichier introuvable');
        }
    }
}
