<?php

namespace App\Lib;

use \MangoPay\FilterEvents;
use \MangoPay\MangoPayApi;
use \MangoPay\Money;
use \MangoPay\Pagination;
use \MangoPay\PayIn;
use \MangoPay\PayInExecutionDetailsWeb;
use \MangoPay\PayInPaymentDetailsCard;
use \MangoPay\PayOut;
use \MangoPay\PayOutPaymentDetailsBankWire;
use \MangoPay\Sorting;
use \MangoPay\Transfer;
use \MangoPay\UserNatural;
use \MangoPay\Wallet;
use App\Club;
use App\Payment;
use Exception;

use Illuminate\Support\Facades\Cache;

class Mangopay
{
    private $mangopay;

    /** MangoPay static fees in € */
    const MANGOPAY_STATIC_FEES = 0.05;
    /** MangoPay percent fees in % */
    const MANGOPAY_PERCENT_FEES = 0.9;

    /* MANGOPAY_DEFAULT_CACHE_DURATION 45jours*/
    const MANGOPAY_DEFAULT_CACHE_DURATION = 45 * 24 * 60;

    public function __construct(MangoPayApi $mangopay)
    {
        $this->mangopay = $mangopay;
    }

    public function GetAllEvents($ItemPerPage = null, $PageNumber = null, $EventType = null, $BeforeDate = null, $AfterDate = null, $sortDirection = null)
    {
        $pagination = $ItemPerPage ? new Pagination(($PageNumber?$PageNumber:1), $ItemPerPage) : null;
        $filter = $EventType ? new FilterEvents() : null;

        if ($filter) {
            $filter->EventType = $EventType;
            $filter->BeforeDate = $BeforeDate;
            $filter->AfterDate = $AfterDate;
        }

        $sorting = $sortDirection ? new Sorting() : null;
        $sorting ? $sorting->AddField('CreationDate', $sortDirection) : null;

        $key = '';
        if ($ItemPerPage) {
            $key .= '-'.$ItemPerPage;
        }
        if ($PageNumber) {
            $key .= '-'.$PageNumber;
        }
        if ($filter) {
            if ($EventType) {
                $key .= '-'.$EventType;
            }
            if ($filter->BeforeDate) {
                $key .= '-'.$filter->BeforeDate;
            }
            if ($filter->AfterDate) {
                $key .= '-'.$filter->AfterDate;
            }
        }
        if ($sorting) {
            $key .= '-'.$sortDirection;
        }

        $events = Cache::remember('mangopay-events-'.$key, self::MANGOPAY_DEFAULT_CACHE_DURATION, function () use ($pagination, $filter, $sorting) {
            try {
                return $this->mangopay->Events->GetAll($pagination, $filter, $sorting);
            } catch (\Exception $e) {
                dd([
                    $pagination, $filter, $sorting,$e
                ]);
            }
        });

        return $events;
    }

    public function GetAll()
    {
        $users = $this->mangopay->Users->GetAll();

        return $users;
    }

    /**
     * @param integer $userId
     * @return UserNatural
     */
    public function GetSubscriber($userId)
    {
        $user = $this->mangopay->Users->GetNatural($userId);

        return $user;
    }

    /**
     * @param integer $userId
     * @return UserLegal
     */
    public function GetClubSubscriber($userId)
    {
        $user = $this->mangopay->Users->GetLegal($userId);

        return $user;
    }

    /**
     * @param integer $userId
     * @return integer
     */
    public function GetSubscriberWalletId($userId)
    {
        $wallets = $this->mangopay->Users->GetWallets($userId);
        if (empty($wallets)) {
            return null;
        }

        $wallet = $wallets[0];

        return $wallet->Id;
    }

    /**
     * @param integer $clubId
     * @return integer
     */
    private function GetClubWalletId($clubId)
    {
        $club = Club::find($clubId);

        if (!$club) {
            return null;
        }

        if ($club->mangopay_wallet_id == null) {
            \Log::error('Le club : '.$club->title.', id : '.$club->id.' n\'a pas de wallet ID');
            return null;
        } else {
            $walletId = $club->mangopay_wallet_id;
        }

        return $walletId;
    }

    public function doTransfer($payIn, $clubId, $amount = null)
    {
        $subWalletId = $payIn->CreditedWalletId;
        $clubWalletId = $this->GetClubWalletId($clubId);
        if (!$this->GetWallet($clubWalletId)) {
            return;
        }
        $authorId = $payIn->AuthorId;

        if (! is_numeric($clubWalletId)) {
            throw new Exception('Format incorrect '.$clubWalletId);
        }
        if (! is_numeric($subWalletId)) {
            throw new Exception('Format incorrect '.$subWalletId);
        }

        try {
            if ($amount === null) {
                throw new Exception('Payment has not amount found');
            }

            $transfer = $this->createTransfer($authorId, $amount, $clubWalletId, $subWalletId);

            if ($transfer->Status != 'SUCCEEDED') {
                throw new Exception('NOT SUCCEEDED');
            }
        } catch (Exception $e) {
            throw new Exception('Transfer failed between subscriber ' . $payIn->AuthorId . ' and club ' . $this->GetWallet($clubWalletId)->Owners[0]);
        }

        try {
            $userClubId = $this->GetWallet($clubWalletId)->Owners[0];
            $bankaccountid = $this->GetBankAccountId($userClubId);
            $payout = $this->createPayout($userClubId, $clubWalletId, $amount, $bankaccountid);
        } catch (Exception $e) {
            throw new Exception('Transfer to bank account for subscriber ' . $userClubId . ' and club ' . $clubWalletId);
        }
    }

    /**
     * @param integer $walletId
     * @return Wallet
     */
    public function GetWallet($walletId)
    {
        return $this->getEntity('Wallet', $walletId);
    }

    public function GetBankAccountId($userid)
    {
        $bankAccounts = $this->mangopay->Users->GetBankAccounts($userid);
        if (! empty($bankAccounts)) {

            foreach($bankAccounts as $bankAccount)
            {
                if($bankAccount->Active === true)
                {
                    return $bankAccount->Id;
                }
                else
                {
                    continue;
                    \Log::error('Le Bank account de : '.$bankAccount->OwnerName.', Id : '.$bankAccount->id.' n\'est pas valide');
                }
            }
        }

        return null;
    }

    /**
         * Get fees of an amount given in euros (not centimes)
         *
         * @param $amount
         *
         * @return mixed
         */
    protected function GetAmountFees($amount)
    {
        return $amount * (self::MANGOPAY_PERCENT_FEES / 100) + self::MANGOPAY_STATIC_FEES;
    }

    /**
     * @param array $data
     * @return UserNatural
     */
    public function createUser(array $data)
    {
        $createdUser = null;

        $user = new UserNatural;
        $user->FirstName = $data['firstname'];
        $user->LastName = $data['lastname'];
        $user->Email = $data['email'];
        $user->Birthday = $data['birthdate'];
        $user->Nationality = 'FR';
        $user->CountryOfResidence = 'FR';
        try {
            $createdUser = $this->mangopay->Users->Create($user);
        } catch (Exception $e) {
            \Log::error('Prenom : '.$data['firstname'].' Nom : '.$data['lastname'].' Email : '.$data['email'].' Data de naissance : '.$data['birthdate']);
        }

        return $createdUser;
    }

    /**
     * @param object $user
     * @return Wallet
     */
    public function createWallet($user)
    {
        $Wallet = new Wallet;
        $Wallet->Owners = [$user->Id];
        $Wallet->Description = "Abonnement L'Orange Bleue";
        $Wallet->Currency = "EUR";
        $createdWallet = $this->mangopay->Wallets->Create($Wallet);

        return $createdWallet;
    }

    public function createTransfer($authorId, $amount, $creditedWalletId, $debitedWalleId)
    {
        $Transfer = new Transfer;
        $Transfer->AuthorId = $authorId;
        $Transfer->DebitedFunds = new Money;
        $Transfer->DebitedFunds->Currency = "EUR";
        $Transfer->DebitedFunds->Amount = $amount * 100;
        $Transfer->Fees = new Money;
        $Transfer->Fees->Currency = "EUR";
        $Transfer->Fees->Amount = 0;
        $Transfer->DebitedWalletId = $debitedWalleId;
        $Transfer->CreditedWalletId = $creditedWalletId;

        return $this->mangopay->Transfers->Create($Transfer);
    }


    public function createPayout($authorid, $walletid, $amount, $bankaccountid)
    {
        $PayOut = new PayOut;
        $PayOut->AuthorId = $authorid;
        $PayOut->DebitedWalletId = $walletid;
        $PayOut->DebitedFunds = new Money;
        $PayOut->DebitedFunds->Currency = "EUR";
        $PayOut->DebitedFunds->Amount = $amount * 100;
        $PayOut->Fees = new Money;
        $PayOut->Fees->Currency = "EUR";
        $PayOut->Fees->Amount = $this->GetAmountFees($amount) * 100;
        $PayOut->PaymentType = "BANK_WIRE";
        $PayOut->MeanOfPaymentDetails = new PayOutPaymentDetailsBankWire;
        $PayOut->MeanOfPaymentDetails->BankAccountId = $bankaccountid;

        return $this->mangopay->PayOuts->Create($PayOut);
    }

    /**
     * @param array $data
     * @return PayIn
     */
    public function createPayIn(array $data)
    {
        $PayIn = new PayIn;
        $PayIn->CreditedWalletId = $data['wallet']->Id;
        $PayIn->AuthorId = $data['user']->Id;
        $PayIn->PaymentType = "CARD";
        $PayIn->PaymentDetails = new PayInPaymentDetailsCard;
        $PayIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
        $PayIn->DebitedFunds = new Money;
        $PayIn->DebitedFunds->Currency = "EUR";
        $PayIn->DebitedFunds->Amount = $data['amount'];
        $PayIn->Fees = new Money;
        $PayIn->Fees->Currency = "EUR";
        $PayIn->Fees->Amount = 0;
        $PayIn->ExecutionType = "WEB";
        $PayIn->ExecutionDetails = new PayInExecutionDetailsWeb;
        $PayIn->ExecutionDetails->ReturnURL = route('subscription.confirmation', $data['url']);
        $PayIn->ExecutionDetails->Culture = "FR";
        $PayIn->ExecutionDetails->SecureMode = "FORCE";

        return $this->mangopay->PayIns->Create($PayIn);
    }

    private function getEntity($type, $id)
    {
        $entity = Cache::remember('mangopay-'.$type.'-'.$id, self::MANGOPAY_DEFAULT_CACHE_DURATION, function () use ($type, $id) {
            try {
                return $this->mangopay->{$type . 's'}->Get($id);
            } catch (Exception $e) {
                \Log::info($e);
                return null;
            }
        });

        return $entity;
    }

    public function getPayIn($id)
    {
        return $this->getEntity('PayIn', $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPayOut($id)
    {
        return $this->getEntity('PayOut', $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTransfer($id)
    {
        return $this->getEntity('Transfer', $id);
    }

    public function updatePayment(string $Id)
    {
        if (!$Id) {
            return false;
        }
        $payIn = $this->getPayIn($Id);
        if (!$payIn) {
            \Log::error('PayIn unknown : '.$Id);
            return false;
        }
        $payment = Payment::where('operation_id', $Id)->first();
        if (!$payment) {
            \Log::error('Payment unknown : '.$Id);
            return false;
        }
        if ($payIn->Status == \MangoPay\PayInStatus::Succeeded) {
            $payment = $payment->setStatus(Payment::Succeeded);
        } else {
            $payment = $payment->setStatus(Payment::Failed);
        }
        return $payment;
    }

    public function verifyRateLimits()
    {
        // This is an array of 4 RateLimit objects.
        $users = $this->mangopay->Users->GetAll();
        $rateLimits = $this->mangopay->RateLimits;
        print "There were " . $rateLimits[0]->CallsMade . " calls made in the last 15 minutes<br>";
        print "You can do " . $rateLimits[0]->CallsRemaining . " more calls in the next 15 minutes<br>";
        print "The 15 minutes counter will reset in " . $rateLimits[0]->ResetTimeMillis/1000/60 . " min<br><br><br><br>";
        print "There were " . $rateLimits[2]->CallsMade . " calls made in the last 60 minutes<br>";
        print "You can do " . $rateLimits[2]->CallsRemaining . " more calls in the next 60 minutes<br>";
        print "The 60 minutes counter will reset in " . $rateLimits[1]->ResetTimeMillis/1000/60 . " min<br><br><br><br>";
        print "There were " . $rateLimits[3]->CallsMade . " calls made in the last 24 hours<br>";
        print "You can do " . $rateLimits[3]->CallsRemaining . " more calls in the next 24 hours<br>";
        print "The 60 minutes counter will reset in " . $rateLimits[1]->ResetTimeMillis/1000/60 . " min<br>";
    }
}
