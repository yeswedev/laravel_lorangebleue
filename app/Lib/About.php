<?php

namespace App\Lib;

use YesWeDev\LaravelCMS\Page;

class About
{
    public static function getAboutSlug()
    {
        return 'a-propos';
    }

    public static function getAboutPage()
    {
        return Page::where('slug', '=', self::getAboutSlug())->first();
    }
}
