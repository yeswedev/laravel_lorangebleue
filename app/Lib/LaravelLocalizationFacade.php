<?php

namespace App\Lib;

class LaravelLocalizationFacade extends \Mcamara\LaravelLocalization\LaravelLocalization
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravellocalization';
    }
}
