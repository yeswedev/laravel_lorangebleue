<?php

namespace App\Lib;

use App\Activity;
use App\Equipment;
use App\Post;
use App\Subscription;
use App\Yako;
use YesWeDev\LaravelCMS\Archive;
use YesWeDev\LaravelCMS\Page;

class LaravelLocalization extends \Mcamara\LaravelLocalization\LaravelLocalization
{
    private function translateLaravelCmsUrl(string $url, array $attributes, string $locale)
    {
        foreach ($attributes as $routeParam => $slug) {
            $query = null;
            switch ($routeParam) {
                case 'equipmentUrl':
                    $query = Equipment::query();
                break;
                case 'yakoUrl':
                    $query = Yako::query();
                break;
                case 'categoryUrl':
                    $query = Archive::query();
                break;
                case 'articleUrl':
                    $query = Post::query();
                break;
                case 'pageUrl':
                    $query = Page::query();
                break;
                case 'subscriptionUrl':
                    $query = Subscription::query();
                break;
                case 'activitieUrl':
                    $query = Activity::query();
                break;
            }
            if ($query) {
                $entity = $query->whereHas('translations', function ($q) use ($slug) {
                    $q->where('url', $slug);
                })->first();

                if ($entity) {
                    $entityTranslated = $entity->translate($locale);
                    if ($entityTranslated && $entityTranslated->url && $slug != $entityTranslated->url) {
                        $url = str_replace($slug, $entityTranslated->url, $url);
                    }
                }
            }
        }
        return $url;
    }

    /**
     * @param string|null $url
     * @param array $params
     * @param string $localeCode
     * @return string LocalizedUrl from an url and the local choosed, ex: www.lorangebleue.fr if locale = fr,  www.lorangebleue.es if locale = es
     */
    public function getLocalizedUrlByYWD(string $url = null, string $localeCode = null)
    {
        if (empty($url)) {
            $url = $this->request->fullUrl();
        }
        $attributes = $this->extractAttributes($url, self::getCurrentLocale());

        $toReplaceArray = [];
        foreach ($this->getSupportedLocales() as $localeCode2 => $properties) {
            $toReplaceArray[] = '.' . self::getCurrentLocale() . '/' . $localeCode2;
        }
        $url = $this->getLocalizedURL($localeCode, $url, [], true);
        $url = str_replace($toReplaceArray, "." . $localeCode, $url);
        if ($localeCode == 'fr') {
            $url = str_replace('gimnasios', "clubs/", $url);
        }
        $url = $this->translateLaravelCmsUrl($url, $attributes, $localeCode);
        return $url;
    }
}
