<?php

namespace App\Lib;

use Illuminate\Database\Eloquent\Collection;
use LaravelLocalization;
use Illuminate\Support\Facades\Cache;

class Geocoding
{
    public static function findLongLatFromAddress(String $address, String $country = null)
    {
        $minutes_in_a_month = 43800;

        $value = Cache::remember('address_'.str_slug($address.$country), $minutes_in_a_month, function () use ($address, $country) {
            $locale = $country?:$locale = LaravelLocalization::getCurrentLocale();

            if (!$locale) {
                $locale = 'fr';
            }
            if ($locale == 'fr')
            {
                $mapbox_access_token = config('app.geocoding_fr');
            }
            elseif($locale == 'es')
            {
                $mapbox_access_token = config('app.geocoding_es');
            }
            else
            {
                $mapbox_access_token = config('app.geocoding_ma');
            }


            // Mapbox needs a lngLat coords in order to get the postcode, so we'll have a first query to fetch these coords :
            $lngLatQuery = 
            'https://api.mapbox.com/geocoding/v5/mapbox.places/'.urlencode($address).'.json?access_token='.$mapbox_access_token.'&cachebuster=1562765283570&types=country%2Cregion%2Cdistrict%2Cpostcode%2Clocality%2Cplace%2Cneighborhood%2Caddress%2Cpoi' ;
            $lngLatQuery = \json_decode(file_get_contents($lngLatQuery));

            // Then, true query :
            $query = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'
                .$lngLatQuery->features[0]->center[0].
                '%2C'
                .$lngLatQuery->features[0]->center[1].
                '.json?access_token='.$mapbox_access_token;

            $response = \json_decode(file_get_contents($query));

            $address = $response->features[0]->context ;

            $department = null ;
            $city = null ;
            $postalCode = null ;

            foreach($address as $ad){
                if (strpos($ad->id, 'region') !== false )
                    $department = $ad->text;
                if (strpos($ad->id, 'place') !== false )
                    $city = $ad->text;
                if (strpos($ad->id, 'postcode') !== false )
                    $postalCode = $ad->text;
            }

            $lat = $response->features[0]->center[1];
            $lng = $response->features[0]->center[0];

            return [
                'lat' => $lat,
                'lng' => $lng,
                'department' => $department,
                'city' => $city,
                'zipcode' => $postalCode,
            ];
        });

        return $value;
    }


    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } elseif ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }



    public static function filterByLatLong($geolocatedItems, $latitude, $longitude, $maxDistance = 30)
    {
        $result = new Collection;
        if ($latitude && $longitude) {
            foreach ($geolocatedItems as $geolocatedItem) {
                $distance = self::distance($latitude, $longitude, $geolocatedItem->latitude, $geolocatedItem->longitude);

                if ($distance <= $maxDistance) {
                    $geolocatedItem->distance = $distance;
                    $result->prepend($geolocatedItem);
                }
            }
            $result = $result->sortBy('distance');
        } else {
            $result = $geolocatedItems;
        }

        return $result;
    }

    public static function filterClubsByLatLong($clubs, $latitude, $longitude, $maxDistance = 10)
    {
        return self::filterByLatLong($clubs, $latitude, $longitude, $maxDistance);
    }
}
