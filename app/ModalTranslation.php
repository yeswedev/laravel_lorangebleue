<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModalTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'position',
        'title',
        'text',
        'label',
        'url',
    ];
}
