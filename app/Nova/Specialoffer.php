<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;

use NovaAttachMany\AttachMany;

class Specialoffer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Specialoffer';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Abonnement';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Offres spéciales';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Offre spéciale';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            (new Tabs('Tabs', [
                'Détails' => [
                    Select::make('Pays concerné par l\'offre spéciale', 'country')->options([
                        'fr' => 'France',
                        'es' => 'Espagne',
                        'ma' => 'Maroc',
                    ])->rules('required', 'max:255')->hideFromIndex(),

                    Text::make('Titre', 'title')->rules('required', 'max:255'),
                    Text::make('URL', 'url')->rules('required', 'max:255'),
                    Text::make('Sous-titre', 'subtitle')->rules('required', 'max:255'),
                    Text::make('Description', 'description')->hideFromIndex()->rules('required', 'max:255'),
                    Trix::make('Mentions légales', 'legal_mentions')->hideFromIndex(),
                    Trix::make('Mentions légales - page d\'accueil', 'legal_mentions_home')->rules('required')->hideFromIndex(),

                    Date::make('Date de début', 'start_date'),
                    Date::make('Date de fin', 'end_date'),

                    BelongsToMany::make('Club', 'specialofferClub'),
                    AttachMany::make('Club')->showCounts(),
                ],
                'SEO' => [
                    Text::make('Meta Titre', 'meta_title')->hideFromIndex(),
                    Textarea::make('Meta Description', 'meta_description')->hideFromIndex(),
                    Text::make('H1', 'h1')->hideFromIndex(),
                ]
            ]))->withToolbar(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
