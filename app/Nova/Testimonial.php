<?php

namespace App\Nova;

use App\Nova\Invokables\StoreAttachment;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Image;
use NovaFields\Youtube\Youtube;
use YesWeDev\Nova\Translatable\Translatable;

class Testimonial extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Testimonial';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'author';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => [ 'author' ],
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Abonnement';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Témoignages';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Témoignage';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Concept', 'Subscription', Subscription::class)->rules('required', 'max:255'),
            Translatable::make('Auteur du témoignage', 'author')->singleLine()->rules('required', 'max:255'),
            Translatable::make('Description', 'description')->hideFromIndex()->rules('required', 'max:255'),
            Youtube::make('Lien Youtube', 'link'),
            Image::make('Image', 'image')
                 ->store(new StoreAttachment('image'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
