<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Titasgailius\SearchRelations\SearchesRelations;
use Dtannen\NovaDependencyContainer\HasDependencies;

use Laravel\Nova\Fields\Text;
use Waynestate\Nova\CKEditor;
use YesWeDev\Nova\Translatable\Translatable;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use App\Nova\Invokables\StoreAttachment;
use Dtannen\NovaDependencyContainer\NovaDependencyContainer;
use NovaAttachMany\AttachMany;

class PageGeneric extends Resource
{
    use SearchesRelations, HasDependencies;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\PageGeneric';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['translations'];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => [
            'title',
            'subtitle',
            'description',
        ],
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Administration';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Pages';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Page';
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [

            Select::make('Type de page', 'type')->options([
                'home'  => 'Page d\'accueil',
                'activity' => 'Page d\'activités',
                'equipment' => 'Page d\'équipements',
                'club' => 'Page Trouver mon club',
                'blog' => 'Page Blog',
            ])->displayUsingLabels()->rules('required'),

            Translatable::make('Titre', 'title')->singleLine(),

            NovaDependencyContainer::make([
                Translatable::make('Sous-titre', 'subtitle')->singleLine()->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                CKEditor::make('Description', 'description')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'activity'),

            NovaDependencyContainer::make([
                CKEditor::make('Description', 'description')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'equipment'),

            Image::make('Image d\'en-tête', 'image')
                 ->store(new StoreAttachment('image'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),

            NovaDependencyContainer::make([
                CKEditor::make('Salle de sport l\'orange bleue à proximité', 'description')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'club'),

            NovaDependencyContainer::make([
                Image::make('Image Salle de sport l\'orange bleue à proximité', 'image_wellness')
                 ->store(new StoreAttachment('image_wellness'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),
                Text::make('Image Salle de sport l\'orange bleue à proximité ALT', 'image_alt_wellness')->hideFromIndex(),
            ])->dependsOn('type', 'club'),

            NovaDependencyContainer::make([
                Image::make('Image Mon Coach Wellness', 'image_wellness')
                 ->store(new StoreAttachment('image_wellness'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),
                Text::make('Image Mon Coach Wellness ALT', 'image_alt_wellness')->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                CKEditor::make('Bons plans adhérents', 'text')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                Image::make('Image Arrière plan Bons plans adhérents', 'image_plan')
                 ->store(new StoreAttachment('image_plan'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                CKEditor::make('Les activités à la carte', 'text')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'activity'),

            NovaDependencyContainer::make([
                CKEditor::make('Les équipements à la carte', 'text')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'equipment'),

            NovaDependencyContainer::make([
                CKEditor::make('Présentation l\'orange bleue', 'text_2')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                Image::make('Image Présentation l\'orange bleue', 'image_presentation')
                 ->store(new StoreAttachment('image_presentation'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),
                Text::make('Image Présentation l\'orange bleue ALT', 'image_alt_presentation')->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                CKEditor::make('Rejoignez le réseau l\'orange bleue', 'text_3')->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            NovaDependencyContainer::make([
                Image::make('Image 1 Rejoignez le réseau l\'orange bleue', 'image_reseau_1')
                 ->store(new StoreAttachment('image_reseau_1'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),
                Text::make('Image 1 Rejoignez le réseau l\'orange bleue ALT', 'image_alt_reseau_1')->hideFromIndex(),

                Image::make('Image 2 Rejoignez le réseau l\'orange bleue', 'image_reseau_2')
                 ->store(new StoreAttachment('image_reseau_2'))
                 ->rules('dimensions:max_width=1920', 'max:100')
                 ->help('Largeur max: 1920px, Taille max: 100ko'),
                Text::make('Image 2 Rejoignez le réseau l\'orange bleue ALT', 'image_alt_reseau_2')->hideFromIndex(),
            ])->dependsOn('type', 'home'),

            AttachMany::make('Choix des villes', 'cities', City::class)
            ->showCounts()
            ->rules('max:12')
            ->help('Liste de sélection pour la page <b>Trouver mon club</b> <br> <b>Aide :</b> Sélection maximale de <span style="color:red;"><b>12</b></span> villes'),
        ];

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
