<?php

namespace App\Nova\Invokables;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class StoreAttachment
{
    private $column;

    /**
     * StoreAttachment constructor.
     */
    public function __construct($column)
    {
        $this->column = $column;
    }


    /**
     * Store the incoming file upload.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Database\Eloquent\Model $model
     *
     * @return array
     */
    public function __invoke(Request $request, $model)
    {
        $originalName = $request->{$this->column}->getClientOriginalName();
        $image        = $request->{$this->column}->storeAs('/', $originalName, 'public');

        $storageFileUrl = Storage::disk('public')->path($image);
        ImageOptimizer::optimize($storageFileUrl);

        return [
            $this->column => $image,
        ];
    }
}
