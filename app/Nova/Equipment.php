<?php

namespace App\Nova;

use App\Nova\Invokables\StoreAttachment;
use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use YesWeDev\Nova\Translatable\Translatable;

use NovaAttachMany\AttachMany;

class Equipment extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Equipment';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => ['title',],
    ];

    public static $globallySearchable = true;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Club';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Équipements';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Équipement';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            (new Tabs('Tabs', [
                'Détails' => [
                    Translatable::make('Titre', 'title')->singleLine()->rules('required', 'max:255'),
                    Translatable::make('Description', 'description')->trix()->asHtml()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Url', 'url')->singleLine()->rules('required', 'max:255'),
                    Select::make('Type')->options([
                        'fitness' => 'fitness',
                        'wellness' => 'wellness',
                    ]),

                    Image::make('Bannière', 'banner')
                        ->store(new StoreAttachment('banner'))
                        ->rules('dimensions:max_width=1920', 'max:100')
                        ->help('Largeur max: 1920px, Taille max: 1ko'),

                    Image::make('image')
                        ->store(new StoreAttachment('image'))
                        ->rules('dimensions:max_width=1920', 'max:1000')
                        ->help('Largeur max: 1920px, Taille max: 1Mo'),

                    AttachMany::make('Club', 'equipmentClub')->showCounts(),
                ],
                'SEO' => [
                    Translatable::make('Meta Titre', 'meta_title')->singleLine()->hideFromIndex(),
                    Translatable::make('Meta Description', 'meta_description')->hideFromIndex(),
                    Translatable::make('H1', 'h1')->singleLine(),
                    Translatable::make('Image ALT', 'image_alt')->singleLine()->hideFromIndex(),
                ],
                'Relations' => [
                    AttachMany::make('Club', 'equipmentClub')->showCounts(),
                ]
            ]))->withToolbar(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
