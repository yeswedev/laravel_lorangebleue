<?php

namespace App\Nova;

use Fourstacks\NovaRepeatableFields\Repeater;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Http\Requests\NovaRequest;
use NovaAttachMany\AttachMany;
use Waynestate\Nova\CKEditor;
use YesWeDev\LaravelCMS\Nova\Block;
use YesWeDev\Nova\Translatable\Translatable;

class LandingPage extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\LandingPage';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['translations'];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Club';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Landing Pages';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Landing Page';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Titre Landing Page', 'title')
                ->help("Uniquement visible dans le BO"),
            Translatable::make('H1', 'h1')
                ->trix()
                ->asHtml()
                ->hideFromIndex(),
            Translatable::make('Sous titre', 'subtitle')
                ->trix()
                ->asHtml()
                ->hideFromIndex(),
            Translatable::make('Accroche (Prix)', 'price')
                ->trix()
                ->asHtml()
                ->help("Mettre le prix en gras")
                ->hideFromIndex(),
            Translatable::make('Liste Concepts Fitness', 'concept_list_fitness')
                ->trix()
                ->asHtml()
                ->help("Obligatoire")
                ->hideFromIndex()
                ->rules('required'),
            Translatable::make('Liste Concepts Wellness', 'concept_list_wellness')
                ->trix()
                ->asHtml()
                ->help("Facultatif - Si non renseigné, le contenu fitness sera utilisé")
                ->hideFromIndex(),
            Translatable::make('Titre section vidéo', 'video_title')->singleLine()->hideFromIndex(),
            Translatable::make('Texte section vidéo', 'video_text')->trix()->asHtml()->hideFromIndex(),
            Text::make('Vidéo', 'video_link')->hideFromIndex(),
            AttachMany::make('Club', 'clubs')->showCounts()->showPreview(),
            HasMany::make('Blocs', 'blocks', Block::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
