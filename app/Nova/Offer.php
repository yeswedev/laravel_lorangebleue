<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Vyuldashev\NovaMoneyField\Money;
use Waynestate\Nova\CKEditor;
use YesWeDev\Nova\Translatable\Translatable;

use NovaAttachMany\AttachMany;

class Offer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Offer';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public function title()
    {
        return $this->description;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => ['title',],
    ];

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'description'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Abonnement';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Offres';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Offre';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Concept', 'Subscription', Subscription::class)
                ->rules('required', 'max:255')
                ->hideFromIndex(),
            Translatable::make('Titre', 'title')
                ->singleLine()
                ->rules('required', 'max:255')
                ->locales([
                    'fr' => 'Français',
                    'es' => 'Espagnol',
                    'ma' => 'Marocain',
                ])
                ->hideFromIndex(),

            Text::make('Description', 'description'),
            Money::make('Prix de l\'offre', 'EUR', 'price')
                ->locale('fr-FR')
                ->storedInMinorUnits()
                ->help(config('nova.help.mad'))
                ,
            Number::make('Durée d\'engagement en mois', 'commitment')->min(1),


            Money::make('Prix réduit premier(s) mois', 'EUR', 'reduced_price')
                ->locale('fr-FR')
                ->storedInMinorUnits()
                ->hideFromIndex()
                ->help(config('nova.help.mad')),
            Number::make('Nombre de mois à prix réduit', 'reduced_commitment')->min(0)->hideFromIndex(),

            Boolean::make('Offre étudiante ?', 'is_student'),

            Boolean::make('Offre permanente ?', 'is_permanent'),

            Date::make('Date de début', 'start_date')->format('D/M/Y')->hideFromIndex(),

            Date::make('Date de fin', 'end_date')->format('D/M/Y')->hideFromIndex(),

            BelongsTo::make('Offre à remplacer', 'offerToReplace', Offer::class)->hideFromIndex()->nullable(),

            CKEditor::make('Mentions légales', 'legal_mentions')
                ->options([
                    'toolbar' => config('ckeditor-field.options.toolbar') ?: null
                ])->hideFromIndex(),

            Money::make('Prix Pack avantages web', 'EUR', 'reduced_price_offer')->locale('fr-FR')->storedInMinorUnits()->help(config('nova.help.mad')),
            Money::make('Prix Pack avantages réduit (barré)', 'EUR', 'price_offer')->locale('fr-FR')->storedInMinorUnits()->hideFromIndex()->help(config('nova.help.mad')),

            Number::make('Nombre de jours de report', 'carryover_days')->rules('required')->min(0)->hideFromIndex(),

            AttachMany::make('Club', 'offerClub')->showCounts(),

            //FR
            Boolean::make('Présence sur page d\'accueil FR', 'is_on_home', function(){
                if($this->translate('fr'))
                {
                    return $this->translate('fr')->is_on_home;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('fr')->is_on_home = (int) $request->is_on_home;
                $this->save();
            })
            ->hideFromIndex(),
            Boolean::make('Bannière accueil FR', 'is_on_banner', function(){
                if($this->translate('fr'))
                {
                    return $this->translate('fr')->is_on_banner;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('fr')->is_on_banner = (int) $request->is_on_banner;
                $this->save();
            })
            ->hideFromIndex(),
            Boolean::make('Est mis en avant sur la page d\'accueil FR', 'is_highlighted', function(){
                if($this->translate('fr'))
                {
                    return $this->translate('fr')->is_highlighted;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('fr')->is_highlighted = (int) $request->is_highlighted;
                $this->save();
            })
            ->hideFromIndex(),

            //ES
            Boolean::make('Présence sur page d\'accueil ES', 'is_on_home_es', function(){
                if($this->translate('es'))
                {
                    return $this->translate('es')->is_on_home;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('es')->is_on_home = (int) $request->is_on_home_es;
                $this->save();
            })
            ->hideFromIndex(),
            Boolean::make('Bannière accueil ES', 'is_on_banner_es', function(){
                if($this->translate('es'))
                {
                    return $this->translate('es')->is_on_banner;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('es')->is_on_banner = (int) $request->is_on_banner_es;
                $this->save();
            })
            ->hideFromIndex(),
            Boolean::make('Est mis en avant sur la page d\'accueil ES', 'is_highlighted_es', function(){
                if($this->translate('es'))
                {
                    return $this->translate('es')->is_highlighted;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('es')->is_highlighted = (int) $request->is_highlighted_es;
                $this->save();
            })
            ->hideFromIndex(),

            //MA
            Boolean::make('Présence sur page d\'accueil MA', 'is_on_home_ma', function(){
                if($this->translate('ma'))
                {
                    return $this->translate('ma')->is_on_home;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('ma')->is_on_home = (int) $request->is_on_home_ma;
                $this->save();
            })
            ->hideFromIndex(),
            Boolean::make('Bannière accueil MA', 'is_on_banner_ma', function(){
                if($this->translate('ma'))
                {
                    return $this->translate('ma')->is_on_banner;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('ma')->is_on_banner = (int) $request->is_on_banner_ma;
                $this->save();
            })
            ->hideFromIndex(),
            Boolean::make('Est mis en avant sur la page d\'accueil MA', 'is_highlighted_ma', function(){
                if($this->translate('ma'))
                {
                    return $this->translate('ma')->is_highlighted;
                }
            })
            ->fillUsing(function($request, $model, $attribute, $requestAttribute){
                $this->translateOrNew('ma')->is_highlighted = (int) $request->is_highlighted_ma;
                $this->save();
            })
            ->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
