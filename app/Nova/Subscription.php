<?php

namespace App\Nova;

use App\Nova\Invokables\StoreAttachment;
use Illuminate\Http\Request;

use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use YesWeDev\Nova\Translatable\Translatable;

class Subscription extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Subscription';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'type';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => [ 'title', 'description' ],
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Abonnement';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Concepts';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Concept';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Select::make('Concept', 'type')->options([
                'fitness'  => 'Fitness',
                'wellness' => 'Wellness'
            ])->displayUsingLabels(),
            Translatable::make('Titre', 'title')->singleLine()->rules('required', 'max:255'),
            Translatable::make('Sous-titre', 'subtitle')->hideFromIndex()->singleLine()->rules('required', 'max:255'),
            Translatable::make('Description', 'description')->hideFromIndex()->rules('required', 'max:255'),

            Translatable::make('Titre présentation 1', 'title1')->singleLine()->hideFromIndex(),
            Translatable::make('Présentation 1', 'presentation1')->hideFromIndex(),
            Image::make('Image présentation 1', 'image1')->hideFromIndex()
                 ->store(new StoreAttachment('image1'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),

            Translatable::make('Titre présentation 2', 'title2')->singleLine()->hideFromIndex(),
            Translatable::make('Présentation 2', 'presentation2')->hideFromIndex(),
            Image::make('Image présentation 2', 'image2')->hideFromIndex()
                 ->store(new StoreAttachment('image2'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),

            Translatable::make('Titre présentation 3', 'title3')->singleLine()->hideFromIndex(),
            Translatable::make('Présentation 3', 'presentation3')->hideFromIndex(),
            Image::make('Image présentation 3', 'image3')->hideFromIndex()
                 ->store(new StoreAttachment('image3'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),

            Translatable::make('Titre présentation 4', 'title4')->singleLine()->hideFromIndex(),
            Translatable::make('Présentation 4', 'presentation4')->hideFromIndex(),
            Image::make('Image présentation 4', 'image4')->hideFromIndex()
                 ->store(new StoreAttachment('image4'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),

            Image::make('Image', 'image')
                 ->store(new StoreAttachment('image'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),

            HasMany::make('Choix du club', 'clubs', Club::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
