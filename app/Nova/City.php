<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Image;
use App\Nova\Invokables\StoreAttachment;

use Laravel\Nova\Fields\Trix;

class City extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\City';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name', 'url',
    ];

    public static $globallySearchable = true;

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Villes';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Ville';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            (new Tabs('Tabs', [
                'Détails' => [
                    Text::make('Nom', 'name'),
                    Text::make('Titre', 'title')->rules('required'),
                    Trix::make('Description', 'description'),
                    Text::make('Url', 'url')->rules('required'),
                    Image::make('Image miniature', 'image_tiny')
                        ->store(new StoreAttachment('image_tiny'))
                        ->rules('dimensions:max_width=1920', 'max:1000')
                        ->help('Largeur max: 1920px, Taille max: 1Mo'),
                    Text::make('Image miniature ALT', 'image_tiny_alt')->hideFromIndex(),
                    // TODO go Mapbox : check if a Nova Mapbox package can help
                    Text::make('Latitude')
                        ->rules('required')
                        ->help('Aidez-vous de ce <a href="https://www.coordonnees-gps.fr/" target="_blank" rel="noopener noreferrer" style="font-weight: 700;">site</a> pour obtenir la Latitude')
                        ->hideFromIndex(),
                    Text::make('Longitude')
                        ->rules('required')
                        ->help('Aidez-vous de ce <a href="https://www.coordonnees-gps.fr/" target="_blank" rel="noopener noreferrer" style="font-weight: 700;">site</a> pour obtenir la Longitude')
                        ->hideFromIndex(),
                    Select::make('Pays', 'country')->options([
                        'fr' => 'France',
                        'es' => 'Espagne',
                        'ma' => 'Maroc',
                    ]),
                    BelongsTo::make('Department', 'Department', Department::class)->rules('required'),
                ],
                'SEO' => [
                    Text::make('Meta Titre', 'meta_title')->hideFromIndex(),
                    Textarea::make('Meta Description', 'meta_description')->hideFromIndex(),
                    Text::make('H1', 'h1'),
                ]
            ]))->withToolbar(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
