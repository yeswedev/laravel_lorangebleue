<?php

namespace App\Nova;

use App\Nova\Invokables\StoreAttachment;
use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

use Laravel\Nova\Fields\Trix;
use Fourstacks\NovaRepeatableFields\Repeater;

use Laravel\Nova\Http\Requests\NovaRequest;

use Vyuldashev\NovaMoneyField\Money;

use Tyjow\NovaCustomBoolean\CustomBoolean;

class Club extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Club';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';


    public static $search = [
        'title',
        'zip_code',
        'city',
    ];

    public static $globallySearchable = true;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Club';

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user()->hasClub()) {
            return $query->whereHas('users', function ($q) use ($request) {
                $q->where('user_id', $request->user()->id);
            });
        } else {
            return $query;
        }
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            (new Tabs('Tabs', [
                'Détails' => [
                    BelongsTo::make('Abonnement', 'Subscription', Subscription::class)->hideFromIndex()->rules('required', 'max:255'),

                    CustomBoolean::make('Statut du Club', 'is_open')->labels('Club ouvert', 'Club fermé')->colorLabels()->rules('required', 'max:255'),

                    CustomBoolean::make('Yako 360', 'is_yako360')->labels('Oui', 'Non')->colorLabels(),
                    CustomBoolean::make('Accès libre 6h/23h', 'is_free_access')->labels('Oui', 'Non')->colorLabels(),

                    Text::make('Titre', 'title')->rules('required', 'max:255'),

                    Trix::make('Description du club', 'description')->hideFromIndex(),
                    Text::make('Adresse', 'address')->rules('required', 'max:255')->hideFromIndex(),
                    Text::make('Code postal', 'zip_code')->rules('required', 'max:255')->hideFromIndex(),
                    Text::make('Ville', 'city')->rules('required')->rules('required', 'max:255'),
                    BelongsTo::make('Department', 'department')->rules('required')->hideFromIndex(),
                    Select::make('Pays', 'country')->options([
                        'fr' => 'France',
                        'es' => 'Espagne',
                        'ma' => 'Maroc',
                    ])->rules('required', 'max:255'),
                    Text::make('Visite 360', 'virtual-tour_link', function($field){
                        return $field ? '<a href="'.$field.'" target="_blank">Lien</a>' : null ;
                    })->asHtml()->onlyOnIndex(),
                    Text::make('Téléphone', 'phone')->hideFromIndex(),
                    Text::make('Email')
                        ->sortable()
                        ->hideFromIndex(),
                    Repeater::make('Horaire du club', 'schedule')
                        ->addField(
                            [
                                'label' => 'Jour',
                                'name' => 'weekday',
                                'width' => 'w-1/4',
                            ]
                        )
                        ->addField(
                            [
                                'label' => 'Heure de début',
                                'placeholder' => 'Heure de début (Ex: 08:15)',
                                'name' => 'b_hour',
                            ]
                        )
                        ->addField(
                            [
                                'label' => 'Heure de fin',
                                'placeholder' => 'Heure de fin (Ex: 08:15)',
                                'name' => 'e_hour',
                            ]
                        )
                        ->addButtonText('Ajouter un horaire')
                        ->hideFromIndex(),
                    Trix::make('Horaires en libre accès', 'free_access_schedule')->hideFromIndex(),
                    Image::make('Image du planning', 'planning_image')
                        ->store(new StoreAttachment('planning_image'))
                        ->hideFromIndex()
                        ->rules('dimensions:max_width=1920', 'max:1000')
                        ->help('Largeur max: 1920px, Taille max: 1Mo')
                        ->hideFromIndex(),
                    Text::make('Lien Facebook', 'social_link_fb')->hideFromIndex(),
                    Text::make('Lien Instagram', 'social_link_insta')->hideFromIndex(),

                    Text::make('Titre d\'équipe', 'team_title')->hideFromIndex(),
                    Trix::make('Description d\'équipe', 'team_description')->hideFromIndex(),

                    Image::make('Bannière', 'banner')
                         ->store(new StoreAttachment('banner'))
                         ->rules('dimensions:max_width=1920', 'max:1000')
                         ->help('Largeur max: 1920px, Taille max: 1Mo'),

                    Image::make('Image du club', 'image')
                         ->store(new StoreAttachment('image'))
                         ->hideFromIndex()
                         ->help('Largeur max: 1920px, Taille max: 1Mo'),

                    Image::make('Image de l\'équipe', 'team_image')
                         ->store(new StoreAttachment('team_image'))
                         ->hideFromIndex()
                         ->help('Largeur max: 1920px, Taille max: 1Mo'),

                    Text::make('Lien de visite virtuelle', 'virtual-tour_link')->hideFromIndex(),

                    Text::make('Url', 'url')->hideFromIndex(),

                    Money::make('Prix de l\'avantage', 'EUR', 'price_offer')->locale('fr-FR')->storedInMinorUnits()->rules('required', 'max:255')->hideFromIndex()->help(config('nova.help.mad')),

                    Text::make('Mangopay WalletId', 'mangopay_wallet_id')->rules('max:255')->hideFromIndex(),

                            // TODO go Mapbox : check if a Nova Mapbox package can help
                            Text::make('Latitude')
                                ->rules('required')
                                ->help('Aidez-vous de ce <a href="https://www.coordonnees-gps.fr/" target="_blank" rel="noopener noreferrer" style="font-weight: 700;">site</a> pour obtenir la Latitude')
                                ->hideFromIndex(),
                            Text::make('Longitude')
                                ->rules('required')
                                ->help('Aidez-vous de ce <a href="https://www.coordonnees-gps.fr/" target="_blank" rel="noopener noreferrer" style="font-weight: 700;">site</a> pour obtenir la Longitude')
                                ->hideFromIndex(),
                        ],
                'SEO' => [
                    Text::make('Meta Titre', 'meta_title')->hideFromIndex(),
                    Textarea::make('Meta Description', 'meta_description')->hideFromIndex(),
                    Text::make('H1', 'h1')->hideFromIndex(),
                    Text::make('Image ALT', 'image_alt')->hideFromIndex(),
                    Text::make('Image ALT Équipe', 'team_image_alt')->hideFromIndex(),
                ],
                'Relations' => [
                    BelongsToMany::make('Offre', 'offers', Offer::class),
                    BelongsToMany::make('Equipment', 'equipments'),
                    BelongsToMany::make('Yako', 'yakos'),
                    BelongsToMany::make('Specialoffer', 'specialoffers'),
                    HasMany::make('Media', 'medias', Media::class),
                    HasMany::make('Avantage', 'advantages', Advantage::class),
                    BelongsToMany::make('User', 'users'),
                ]
            ]))->withToolbar(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\ClubMissingInformation,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function relatableQuery( NovaRequest $request, $query ) {
        return $query->where('is_open', true);
    }
}
