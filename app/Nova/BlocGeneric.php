<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Titasgailius\SearchRelations\SearchesRelations;
use Dtannen\NovaDependencyContainer\HasDependencies;

use YesWeDev\Nova\Translatable\Translatable;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use App\Nova\Invokables\StoreAttachment;
use Dtannen\NovaDependencyContainer\NovaDependencyContainer;

class BlocGeneric extends Resource
{
    use SearchesRelations, HasDependencies;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\BlocGeneric';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = ['translations'];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => [
            'title',
            'subtitle',
            'description',
        ],
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Administration';

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Blocs de contenu';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Bloc de contenu';
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Select::make('Type de bloc', 'type')->options([
                'concept'  => 'Bloc Les concept de l\'orange bleue',
            ])->displayUsingLabels()->rules('required'),

            NovaDependencyContainer::make([
                Image::make('Image Fitness', 'image')
                 ->store(new StoreAttachment('image'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),
                Translatable::make('Image Fitness ALT', 'image_alt')->singleLine()->hideFromIndex(),
            ])->dependsOn('type', 'concept'),

            NovaDependencyContainer::make([
                Image::make('Image Wellness', 'image_2')
                 ->store(new StoreAttachment('image_2'))
                 ->rules('dimensions:max_width=1920', 'max:1000')
                 ->help('Largeur max: 1920px, Taille max: 1Mo'),
                Translatable::make('Image Wellness ALT', 'image_alt_2')->singleLine()->hideFromIndex(),
            ])->dependsOn('type', 'concept'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
