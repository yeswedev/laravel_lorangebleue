<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Order';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'lastname', 'code', 'phone_number'
    ];

    public static $globallySearchable = true;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Abonnement';

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->code . ' - '.$this->lastname;
    }

    public static function label()
    {
        return 'Commandes';
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Commande';
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user()->hasClub()) {
            return $query->whereHas('club', function ($q) use ($request) {
                $q->whereHas('users', function ($r) use ($request) {
                    $r->where('user_id', $request->user()->id);
                });
            })->where('email', '!=', null);
        } elseif ($request->user()->isAdmin()) {
            return $query->where('email', '!=', null);
        } else {
            return;
        }
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        $object = \App\Club::find($this->club_id);
        $objectOffer = \App\Offer::find($this->offer_id);
        $objectPaymentMod = \App\Payment::where('order_id', $this->id)->first();

        $userCo= \App\User::where('id', '5')->first();



        if (stripos($_SERVER['REQUEST_URI'], '/orders/') !== false) {
            if ($objectPaymentMod != null) {
                $paymentType = $objectPaymentMod->type;
                $paymentAmount = formatPrices($objectPaymentMod->amount / 100) . ' €';
            } else {
                $paymentType = '';
                $paymentAmount = '';
            }
        } else {
            $paymentType = '';
            $paymentAmount = '';
        }


        return [
            ID::make()->sortable(),

            Select::make('Civilité', 'civility')->options([
                'madame' => 'Madame',
                'monsieur' => 'Monsieur',
            ])->displayUsingLabels(),

            Text::make('Prénom', 'firstname'),
            Text::make('Nom', 'lastname'),
            Date::make('Date de naissance', 'birthdate')->format('DD-MM-YYYY')->hideFromIndex(),
            Text::make('Email'),
            DateTime::make("Date et l'heure de commande", 'created_at')->format('DD-MM-YYYY HH:mm:ss')->exceptOnForms(),
            Boolean::make('Newsletter', 'newsletter')->hideFromIndex(),
            Text::make('Téléphone', 'phone_number'),
            Text::make('Adresse', 'address')->hideFromIndex(),
            Text::make('Complément d\'adresse', 'alt_address')->hideFromIndex(),
            Text::make('Code postal', 'zip_code')->hideFromIndex(),
            Text::make('Ville', 'city')->hideFromIndex(),
            Text::make('Code', 'code')->exceptOnForms(),
            Text::make('Statut', 'status')
                ->withMeta($this->status ? [] : ["value" => "<div style='color:#cca300;'>Incomplète</div>"])
                ->displayUsing(function () {
                    if ($this->status == 'SUCCEEDED') {
                        $result = "<div style='color:#00cc99;'>Validée</div>";
                    } elseif ($this->status == 'FAILED') {
                        $result = "<div style='color:#eb4748;'>Échouée</div>";
                    } else {
                        $result = "<div style='color:#cca300;'>Incomplète</div>";
                    }
                    return $result;
                })->asHtml(),


            Text::make('Club', 'club_id')
                ->displayUsing(function () use ($object) {
                    return $object->title;
                }),
            Text::make('Formule choisie', 'offer_id')->hideFromIndex()
                ->displayUsing(function () use ($objectOffer) {
                    return $objectOffer->title;
                }),


            Text::make('Mode de paiement', 'id')->hideFromIndex()
                ->displayUsing(function () use ($paymentType) {
                    if ($paymentType == 'automatique') {
                        $result = "Prélèvement automatique mensuel";
                    } else {
                        $result = "Paiement comptant à l'inscription";
                    }
                    return $result;
                }),

            Text::make('Montant payé à l\'inscription', 'id')->hideFromIndex()
                ->displayUsing(function () use ($paymentAmount) {
                    return $paymentAmount;
                }),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\OrderStatment,
            new Filters\OrdersByClub,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
