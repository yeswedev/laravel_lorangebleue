<?php

namespace App\Nova;

use Laravel\Nova\Http\Requests\NovaRequest;

class Archive extends \YesWeDev\LaravelCMS\Nova\Archive
{
    public static $displayInNavigation = false;

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return parent::indexQuery($request, $query)
            ->where('type', 'archive')
            ->where('slug', '!=', 'a-propos');
    }
}
