<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class ClubMissingInformation extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        if ($value == 'without-email') {
            return $query->where('email', '=', '');
        }
        if ($value == 'without-facebook') {
            return $query->whereNull('social_link_fb');
        }
        if ($value == 'without-instagram') {
            return $query->whereNull('social_link_insta');
        }
        if ($value == 'without-phone') {
            return $query->whereNull('phone');
        }
        if ($value == 'without-position') {
            return $query->whereNull('latitude')->orWhereNull('longitude');
        }
        if ($value == 'without-schedule') {
            return $query->whereNull('schedule');
        }
        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'Email manquant' => 'without-email',
            'Facebook manquant' => 'without-facebook',
            'Instagram manquant' => 'without-instagram',
            'Téléphone manquant' => 'without-phone',
            'Position manquant' => 'without-position',
            'Horaires manquant' => 'without-schedule',
        ];
    }
}
