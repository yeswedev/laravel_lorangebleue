<?php

namespace App\Nova\Filters;

use App\Club;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Filters\Filter;

class OrdersByClub extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  mixed $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('club_id', '=', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function options(Request $request)
    {
        $user = Auth::user();
        $club = Club::get();

        if ($user->role_id != '1') {
            $clubsUser = $user->userClub->pluck('id', 'title')->all();
            return $clubsUser;
        } else {
            $clubsUser = $club->pluck('id', 'title')->all();
            return $clubsUser;
        }
    }
}
