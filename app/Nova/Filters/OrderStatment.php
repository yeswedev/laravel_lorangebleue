<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class OrderStatment extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        if ($value == 'valide') {
            return $query->where('status', '=', 'SUCCEEDED');
        }
        if ($value == 'incomplete') {
            return $query->whereNull('status');
        }
        if ($value == 'echouee') {
            return $query->where('status', '=', 'FAILED');
        }
        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'Validée' => 'valide',
            'Incomplète' => 'incomplete',
            'Échouée' => 'echouee',
        ];
    }
}
