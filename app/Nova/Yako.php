<?php

namespace App\Nova;

use App\Nova\Invokables\StoreAttachment;
use Eminiarts\Tabs\Tabs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use NovaFields\Youtube\Youtube;
use Timothyasp\Color\Color;
use YesWeDev\Nova\Translatable\Translatable;

use NovaAttachMany\AttachMany;

class Yako extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Yako';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'translations' => [ 'title' ],
    ];
    public static $globallySearchable = true;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Club';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            (new Tabs('Tabs', [
                'Détails' => [
                    ID::make()->sortable(),
                    Color::make('Choix de la couleur', 'color')->rules('required', 'max:255'),
                    Translatable::make('Titre', 'title')->singleLine()->rules('required', 'max:255'),
                    Image::make('picto')
                        ->store(new StoreAttachment('picto'))
                        ->rules('dimensions:max_width=1920', 'max:1000')
                        ->help('Largeur max: 1920px, Taille max: 1Mo'),
                    Translatable::make('Description', 'description')->trix()->asHtml()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Sous-titre', 'subtitle')->singleLine()->rules('required', 'max:255'),
                    Translatable::make('Détail', 'detail')->trix()->asHtml()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('url', 'url')->singleLine()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Info structure', 'structure')->trix()->asHtml()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Info avantages', 'advantage')->trix()->asHtml()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Info Public', 'public')->trix()->asHtml()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Info objectif', 'objectives')->singleLine()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Partie ciblée', 'targeted_part')->singleLine()->hideFromIndex()->rules('required', 'max:255'),
                    Translatable::make('Info calorie', 'calories')->singleLine()->hideFromIndex()->rules('required', 'max:255'),
                    Image::make('image')
                        ->store(new StoreAttachment('image'))
                        ->rules('dimensions:max_width=1920', 'max:1000')
                        ->help('Largeur max: 1920px, Taille max: 1Mo'),
                    Youtube::make('Lien Youtube', 'link'),
                    Select::make('Note /6 des calorie', 'rate_calorie')->options([
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                    ])->hideFromIndex(),
                ],
                'SEO' => [
                    Translatable::make('Meta Titre', 'meta_title')->singleLine()->hideFromIndex(),
                    Translatable::make('Meta Description', 'meta_description')->hideFromIndex(),
                    Translatable::make('H1', 'h1')->singleLine(),
                    Translatable::make('Image ALT', 'image_alt')->singleLine()->hideFromIndex(),
                ],
                'Relations' => [
                    AttachMany::make('Activity', 'yakoActivity')->showCounts(),
                    AttachMany::make('Club', 'yakoClub')->showCounts(),
                ]
            ]))->withToolbar(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
