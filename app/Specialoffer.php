<?php

namespace App;

use App\Traits\HasMetaFields;

use App\Traits\HasYWDFields;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LaravelLocalization;

class Specialoffer extends Model
{
    use HasYWDFields;
    use HasMetaFields;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'free_month',
        'price',
        'price_offer',
        'start_date',
        'end_date',
        'country',
        'title',
        'description',
        'url',
        'subtitle',
        'legal_mentions',
        'legal_mentions_home',
    ];

    protected $dates = [
        'start_date',
        'end_date',
    ];

    public function specialofferClub()
    {
        return $this->belongsToMany(Club::class, 'specialoffer_club', 'specialoffer_id', 'club_id');
    }

    public function club()
    {
        return $this->belongsToMany(Club::class, 'specialoffer_club', 'specialoffer_id', 'club_id');
    }

    public function specialofferOffer()
    {
        return $this->belongsToMany(Offer::class, 'offer_specialoffer', 'specialoffer_id', 'offer_id');
    }

    public static function getCurrentSpecialoffer($default = false)
    {
        $specialoffer = Specialoffer::whereDate('start_date', '<=', Carbon::today())->whereDate('end_date', '>=', Carbon::today())->where('country', '=', LaravelLocalization::getCurrentLocale())->first();

        if (!$specialoffer && $default) {
            $specialoffer = Specialoffer::get()->first();
        }
        return $specialoffer;
    }
}
