<?php

namespace App\Imports;

use App\Club;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ClubFromWPImport implements ToCollection, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            if ($row) {
                $slugName = str_slug($row['name']);
                $found = false;
                foreach (Club::with('translations')->get() as $club) {
                    if (str_slug($club->title) == $slugName || str_slug($club->city) == $slugName || str_slug($club->url) == $slugName) {
                        $found = true;
                        $club->schedule = nl2br($row['schedule']);
                        $club->social_link_insta = $row['instagram'];
                        $club->social_link_fb = $row['facebook'];
                        $club->wp_post_id = $row['id'];
                        $club->mangopay_wallet_id = $row['mangopay_wallet_id'];
                        $club->save();
                    }
                }
                if (!$found) {
                    \Log::info($row['name']. ' - '.$slugName. " not found\n");
                }
            }
        }
    }
}
