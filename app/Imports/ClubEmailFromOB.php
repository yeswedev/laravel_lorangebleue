<?php

namespace App\Imports;

use App\Club;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ClubEmailFromOB implements ToCollection, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            if ($row) {
                $slugName = str_slug($row['ville']);
                $found = false;
                foreach (Club::with('translations')->get() as $club) {
                    if (str_slug($club->title) == $slugName) {
                        $found = true;
                        $club->email = $row['email'];
                        $club->save();
                    }
                }
                if (!$found) {
                    \Log::info($row['ville']. ' - '.$slugName. " not found\n");
                }
            }
        }
    }
}
