<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact' => 'required',
            'email' => 'required|string|email|max:255',
            'firstname' => 'required',
            'lastname' => 'required',
            'phone_number' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contact.required' => __('trad.request-message.contact'),
            'email.required' => __('trad.request-message.email'),
            'firstname.required' => __('trad.request-message.firstname'),
            'lastname.required' => __('trad.request-message.name'),
            'phone_number.required' => __('trad.request-message.phone'),
        ];
    }
}
