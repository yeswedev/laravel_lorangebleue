<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terms' => 'required',
            'mode' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'terms.required' => trans('validation.custom.terms.required'),
            'mode.required' => trans('validation.custom.mode.required'),
        ];
    }
}
