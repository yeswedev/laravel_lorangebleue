<?php

namespace App\Http\Requests;

use App\Rules\Recaptcha;
use Illuminate\Foundation\Http\FormRequest;

class ContactUsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255',
            'message-modal' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'phone_number' => 'required',
            'g-recaptcha-response' => ['required', new Recaptcha],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __('trad.request-message.email'),
            'message-modal.required' => __('trad.request-message.message'),
            'firstname.required' => __('trad.request-message.firstname'),
            'lastname.required' => __('trad.request-message.name'),
            'phone_number.required' => __('trad.request-message.phone'),
            'g-recaptcha-response.required' => __('trad.request-message.g-recaptcha-response'),
        ];
    }
}
