<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Step4FormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'civility' => 'required',
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'birthdate' => 'required',
            'address' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'zip_code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'civility.required' => trans('validation.custom.civility.required'),
            'firstname.required' => trans('validation.custom.firstname.required'),
            'lastname.required' => trans('validation.custom.lastname.required'),
            'email.required' => trans('validation.custom.email.required'),
            'birthdate.required' => trans('validation.custom.birthdate.required'),
            'address.required' => trans('validation.custom.address.required'),
            'city.required' => trans('validation.custom.city.required'),
            'zip_code.required' => trans('validation.custom.zip_code.required'),
        ];
    }
}
