<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EquipmentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'    => 'required',
            'type' => 'required',
            'description'   => 'required',
            'url' => 'required',
            'link' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' =>  __('trad.request-message.title'),
            'type.required' =>  __('trad.request-message.type'),
            'description.required' =>  __('trad.request-message.description'),
            'url.required' =>  __('trad.request-message.url'),
            'link.required' =>  __('trad.request-message.link'),
        ];
    }
}
