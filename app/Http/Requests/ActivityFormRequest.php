<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'    => 'required',
            'description'   => 'required',
            'presentation'     => 'required',
            'url' => 'required',
            'link' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => __('trad.request-message.title'),
            'description.required' =>  __('trad.request-message.description'),
            'presentation.required' =>  __('trad.request-message.presentation'),
            'url.required' =>  __('trad.request-message.url'),
            'link.required' =>  __('trad.request-message.link'),
        ];
    }
}
