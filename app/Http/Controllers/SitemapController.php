<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Club;

use App\Equipment;
use App\Post;

use App\Subscription;
use App\Yako;
use LaravelLocalization;
use Spatie\Sitemap\Sitemap;
use YesWeDev\LaravelCMS\Archive;
use YesWeDev\LaravelCMS\Page;

class SitemapController extends Controller
{
    public function index()
    {
        $currentLocale = LaravelLocalization::getCurrentLocale() ;
        $path = public_path('sitemap-'.$currentLocale.'.xml');
        $sitemap = Sitemap::create(\URL::to('/'))
            ->add(route('index'))
        ;

        $pages = Page::where('type', ['page','orphan'])->whereHas('translations')->get();
        $categories = Archive::has('children')
        ->whereNotIn('slug', ['a-propos', 'ouvrir-salle-sport'])
        ->get();
        if ($currentLocale == 'fr') {
            $subscriptions = Subscription::whereHas('translations')->get();
        } else {
            $subscriptions = Subscription::whereHas('translations')->where('type', 'fitness')->get();
        }
        $equipments = Equipment::whereHas('translations')->get();

        // TODO : maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma'){
            foreach ($equipments as $key=>$equ) {
                if($equ->type == 'wellness')
                    unset($equipments[$key]);
            }
        }

        $locale = $currentLocale;
        $locales = [$locale => $locale];

        foreach ($locales as $localeCode => $properties) {
            foreach ($pages as $page) {
                $sitemap->add(
                    route('page.retrieveChild', ['pageUrl' => $page->url])
                );
            }
        }

        if ($currentLocale == 'fr') {
            //        // all blog route
            $sitemap->add(route('blog.showAll'));

//        // blog route
            foreach ($locales as $localeCode => $properties) {
                foreach ($categories as $category) {
                  foreach ($category->children as $blog) {
                    $sitemap->add(
                      route('blog.retrieve', [
                          'categoryUrl' => $category->url,
                          'articleUrl' => $blog->url,
                        ]
                      )
                    );
                  }
                }
            }
        }


        // all subscription route
        foreach ($locales as $localeCode => $properties) {
            foreach ($subscriptions as $subscription) {
                $sitemap->add(
                    route('subscription.index', $subscription->url)
                );
            }
        }

        // special offer route
        $specialOffer = \App\Specialoffer::getCurrentSpecialoffer() ;
        if ($specialOffer) {
            $sitemap->add(
                route('specialoffer.index', $specialOffer->url)
            );
        }

        // all equipment route
        $sitemap->add(
            route('equipment.showAll')
        );

//        // equipment route
        foreach ($locales as $localeCode => $properties) {
            foreach ($equipments as $equipment) {
                $sitemap->add(
                    route('equipment.retrieve', ['equipmentUrl' => $equipment->url])
                );
            }
        }

        $yakos = Yako::get();
        foreach ($locales as $localeCode => $properties) {
            foreach ($yakos as $yako) {
                $sitemap->add(
                    route('yako.retrieve', ['yakoUrl' => $yako->url])
                );
            }
            $sitemap->add(
                route('yako.showAll')
            );
        }

        $clubs = Club::where('country', $currentLocale)->get();

        foreach ($locales as $localeCode => $properties) {
            $departments = [];
            $departmentCityCouples = [];
            foreach ($clubs as $club) {
                if ($club->department) {
                    $departmentUrl = $club->department->url;
                    $city = str_slug($club->city);
                    $departments[$departmentUrl] = $club->department;
                    if (!array_key_exists($departmentUrl, $departmentCityCouples)) {
                        $departmentCityCouples[$departmentUrl] = [];
                    }
                    if (!in_array($city, $departmentCityCouples[$departmentUrl])) {
                        $departmentCityCouples[$departmentUrl][] = $city;
                    }
                    $sitemap->add(
                        route('front.club-details', ['clubUrl' => $club->url])
                    );
                }
            }

            $departments = \App\Department::where('country', $currentLocale)->get();
            foreach ($departments as $department) 
                $sitemap->add(
                    route('front.club-details', ['department' => $department->url])
                );
        }

        foreach ($departmentCityCouples as $department => $cities) {
            foreach ($cities as $city) {
                $sitemap->add(
                    route('front.club-list', ['department' => $department, 'city' => $city])
                );
            }
        }
        $sitemap->add(
            route('front.departments')
        );
        if ($currentLocale == 'fr') {
            foreach ($departments as $department) {
                $sitemap->add(
                    route('front.club-list-by-department', ['departmentUrl' => $department->url])
                );
            }
        }

        if(\LaravelLocalization::getCurrentLocale() == 'fr'){
            if (config('laravellocalization.newLinkRecrutement')) {
                $sitemap->add(
                    route('recruitment.index')
                );
            }
        }

        $activities = Activity::get();

        // TODO : maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma'){
            foreach ($activities as $key=>$e) {
                if($e->type == 'wellness')
                    unset($activities[$key]);
            }
        }

        foreach ($locales as $localeCode => $properties) {
            foreach ($activities as $activity) {
                $sitemap->add(
                    route('activity.retrieve', ['activitieUrl' => $activity->url])
                );
            }
            $sitemap->add(
                route('activity.showAll')
            );
        }

        $sitemap->writeToFile($path);

        return redirect()->to(\URL::to('/').'/sitemap-'.$currentLocale.'.xml');
    }
}
