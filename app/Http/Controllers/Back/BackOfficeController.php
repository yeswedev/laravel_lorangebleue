<?php

namespace App\Http\Controllers\Back;

use App\Activity;
use App\Club;
use App\Yako;
use Illuminate\Support\Facades\Response;
use Jenssegers\Date\Date;
use League\Csv\Statement;
use \MangoPay\EventType;
use \MangoPay\MangoPayApi;

use \MangoPay\SortDirection;
use App\Console\Commands\CronTransfer;
use App\Http\Controllers\Controller;
use App\Lib\Mangopay;

use App\Order;
use App\Payment;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Csv\Reader;

class BackOfficeController extends Controller
{
    private $mangopay;

    public function __construct(MangoPayApi $mangopay)
    {
        $this->mangopay = new Mangopay($mangopay);
    }

    private function getEvents($EventType, Carbon $AfterDate = null, Carbon $BeforeDate = null)
    {
        $now = new Carbon();
        $firstDay = $now->copy()->subDays(44);

        $ItemPerPage = 50;
        if (!$AfterDate) {
            $AfterDate = $firstDay;
        }
        $sortDirection = SortDirection::DESC;

        $allEvents = [];
        $page = 1;
        $events = $this->mangopay->GetAllEvents($ItemPerPage, $page, $EventType, $BeforeDate?$BeforeDate->timestamp:null, $AfterDate?$AfterDate->timestamp:null, $sortDirection);
        while (count($events)) {
            $allEvents = array_merge($allEvents, $events);
            $page++;
            $events = $this->mangopay->GetAllEvents($ItemPerPage, $page, $EventType, $BeforeDate?$BeforeDate->timestamp:null, $AfterDate?$AfterDate->timestamp:null, $sortDirection);
        }

        return $allEvents;
    }

    public function listPayin()
    {
        if (!Auth::user()) {
            return redirect()->route('index');
        }

        $list = [];
        $firstDay = Carbon::now()->copy()->subDays(45);
        $lastDay = Carbon::now();
        foreach ($this->getEvents(EventType::PayinNormalSucceeded, $firstDay, $lastDay) as $event) {
            $payIn = $this->mangopay->getPayIn($event->ResourceId);
            $payment = Payment::where('operation_id', $payIn->Id)->where('status', Payment::Succeeded)->first();

            $order = null;
            if ($payment) {
                $order = Order::where('id', $payment->order_id)->where('status', Order::Succeeded)->first();
            }

            $transfer = null;
            if ($order && $order->created_at->lt(Carbon::now()->subDays(CronTransfer::TIME_TRANSFERT))) {
                $transferEvents = $this->getEvents(EventType::TransferNormalSucceeded, $firstDay);
                foreach ($transferEvents as $transferEvent) {
                    $currentTransfer = $this->mangopay->getTransfer($transferEvent->ResourceId);
                    if ($currentTransfer && $currentTransfer->DebitedWalletId == $payIn->CreditedWalletId) {
                        $transfer = $currentTransfer;
                    }
                }
            } else {
                $transfer = 'A venir';
            }

            $list[] = [
                'date' => $payIn ? Carbon::createFromTimestamp($payIn->CreationDate)->format('d/m/Y') : '-',
                'entity' => $payIn,
                'payment' => $payment,
                'order' => $order,
                'transfer' => $transfer,
            ];
        }

        $params = [
            'entities' => $list,
        ];

        return view('back.list-payin')->with($params);
    }


    public function listPayoutFailed()
    {
        if (!Auth::user()) {
            return redirect()->route('index');
        }

        $list = [];

        foreach ($this->getEvents(EventType::PayoutNormalFailed) as $event) {
            $payOut = $this->mangopay->getPayOut($event->ResourceId);

            $list[] = [
                'date' => $payOut ? Carbon::createFromTimestamp($payOut->CreationDate)->format('d/m/Y') : '-',
                'entity' => $payOut,
                'debitedWallet' => $this->mangopay->getWallet($payOut->DebitedWalletId)
            ];
        }

        return view('back.list-payout-failed')->with([
            'entities' => $list,
        ]);
    }

    public function listTransferFailed()
    {
        if (!Auth::user()) {
            return redirect()->route('index');
        }

        $list = [];

        foreach ($this->getEvents(EventType::TransferNormalFailed) as $event) {
            $entity = $this->mangopay->getTransfer($event->ResourceId);

            $list[] = [
                'date' => $entity ? Carbon::createFromTimestamp($entity->CreationDate)->format('d/m/Y') : '-',
                'entity' => $entity,
                'debitedWallet' => $this->mangopay->getWallet($entity->DebitedWalletId),
                'creditedWallet' => $this->mangopay->getWallet($entity->CreditedWalletId)
            ];
        }

        return view('back.list-transfer-failed')->with([
            'entities' => $list,
        ]);
    }

    public function doTransfer(Order $order)
    {
        $payment = $order->payments->where('status', Payment::Succeeded)->last();
        if (!$payment) {
            abort(404);
        }
        $payInId = $payment->operation_id;
        $amount = $payment->amount / 100;

        try {
            $payIn = $this->mangopay->getPayIn($payInId);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->mangopay->doTransfer($payIn, $order->club->id, $amount);

        return 'OK';
    }

    public function verifyRateLimits()
    {
        $this->mangopay->verifyRateLimits();
    }

    public function schedule( Request $request )
    {
        $message = null;
        $is_err = false;
        if ( $request->session()->get("error") ) {
            $message = $request->session()->get("error");
            $is_err = true;
        } elseif ( $request->session()->get("success") ) {
            $message = $request->session()->get("success");
        }

        return view('back.schedule')->with([
            "message"   => $message,
            "is_err"    => $is_err,
        ]);
    }

    public function scheduleImport( Request $request ) {
        if ( !$request->hasFile('csv') ) {
            return redirect()->route("BOSchedule")->with([
                'error' => 'Pour importer des horaires, veuillez uploader un fichier'
            ]);
        }

        $file = $request->file('csv');
        $file_ext = $file->getClientOriginalExtension();
        $ext = ['csv'];

        if ( !$file->isValid() || !in_array($file_ext, $ext) ) {
            return redirect()->route('BOSchedule')->with([
                'error' => 'Le fichier n\'est pas valide, veuillez utiliser un CSV'
            ]);
        }

        $csv = Reader::createFromPath($file->getPathname(), 'r');
        $csv->setDelimiter(';');
        $records = (new Statement())->process($csv);

        foreach ( $records as $key => $row ) {
            if ( $key == 0 ) {
                $days = [
                    $row[2],
                    $row[3],
                    $row[4],
                    $row[5],
                    $row[6],
                    $row[7],
                    $row[8],
                ];
                continue;
            }

            $club_name = $row[0];
            $club_id = $row[1];
            $monday = $row[2];
            $tuesday = $row[3];
            $wednesday = $row[4];
            $thursday= $row[5];
            $friday = $row[6];
            $saturday = $row[7];
            $sunday = $row[8];

            $club = Club::find($club_id);

            if ( $club ) {
                $weekdays = [
                    $days[0] => $monday,
                    $days[1] => $tuesday,
                    $days[2] => $wednesday,
                    $days[3] => $thursday,
                    $days[4] => $friday,
                    $days[5] => $saturday,
                    $days[6] => $sunday,
                ];

                foreach ( $weekdays as $key => $weekday ) {
                    $crenaux = explode(', ', $weekday);
                    foreach ( $crenaux as $creneau ) {
                        $b_hour = explode('-', $creneau)[0];
                        if ( isset(explode('-', $creneau)[1]) ) {
                            $e_hour = explode('-', $creneau)[1];

                            $schedule[] = [
                                "weekday" => $key,
                                "b_hour" => $b_hour,
                                "e_hour" => $e_hour,
                            ];
                        }
                    }
                }
                $club->schedule = json_encode($schedule);
                $club->save();
            }

            $schedule = [];
        }

        return redirect()->route("BOSchedule")->with([
            'success' => 'Import effectué avec succès'
        ]);
    }

    public function isFreeAccess( Request $request ) {
        $message = null;
        $is_err = false;
        if ( $request->session()->get("error") ) {
            $message = $request->session()->get("error");
            $is_err = true;
        } elseif ( $request->session()->get("success") ) {
            $message = $request->session()->get("success");
        }

        return view('back.isFreeAccess')->with([
            "message"   => $message,
            "is_err"    => $is_err,
        ]);
    }

    public function changeIsFreeAccess( Request $request ) {
        if ( !$request->hasFile('csv') ) {
            return redirect()->route("BOIsFreeAccess")->with([
                'error' => 'Veuillez uploader un fichier'
            ]);
        }

        $file = $request->file('csv');
        $file_ext = $file->getClientOriginalExtension();
        $ext = ['csv'];

        if ( !$file->isValid() || !in_array($file_ext, $ext) ) {
            return redirect()->route("BOIsFreeAccess")->with([
                'error' => 'Le fichier n\'est pas valide, veuillez utiliser un CSV'
            ]);
        }

        $csv = Reader::createFromPath($file->getPathname(), 'r');
        $csv->setDelimiter(';');
        $records = (new Statement())->offset(1)->process($csv);

        foreach ( $records as $key => $row ) {
            $club_id = $row[0];
            $is_free_access = $row[1] == 'OUI' ? true : false;

            if ( !empty($is_free_access) ) {
                $club = Club::where("id", $club_id)->first();
                $club->is_free_access = $is_free_access;
                $club->save();
            }
        }

        return redirect()->route("BOIsFreeAccess")->with([
            'success' => 'Import effectué avec succès'
        ]);
    }

    public function linkToClub( Request $request ) {
        $message = null;
        $is_err = false;
        if ( $request->session()->get("error") ) {
            $message = $request->session()->get("error");
            $is_err = true;
        } elseif ( $request->session()->get("success") ) {
            $message = $request->session()->get("success");
        }

        return view('back.link-to-club')->with([
            "message"   => $message,
            "is_err"    => $is_err,
        ]);
    }

    public function goLinkToClub( Request $request ) {
        if ( !$request->hasFile('csv') ) {
            return redirect()->route("BOLinkToClub")->with([
                'error' => 'Veuillez uploader un fichier'
            ]);
        }

        $file = $request->file('csv');
        $file_ext = $file->getClientOriginalExtension();
        $ext = ['csv'];

        if ( !$file->isValid() || !in_array($file_ext, $ext) ) {
            return redirect()->route("BOLinkToClub")->with([
                'error' => 'Le fichier n\'est pas valide, veuillez utiliser un CSV'
            ]);
        }

        $csv = Reader::createFromPath($file->getPathname(), 'r');
        $csv->setDelimiter(';');
        $records = (new Statement())->process($csv);

        $rangeYako = 0;
        $rangeActivities = 0;
        $yakoids = [];
        $activitiesids = [];

        foreach ( $records as $key => $row ) {
            $yakosToAdd = [];
            $activitiesToAdd = [];

            if ( $key == 0 ) {
                foreach ($row as $k => $item) {
                    if ( $item != "" && strtoupper($item) == "YAKO" ) {
                        $rangeYako = $k;
                    } elseif ( $item != "" && strtoupper($item) == "ACTIVITES" ) {
                        $rangeActivities = $k;
                    }
                }
            }

            if ( $key == 1 ) {
                for ( $i = $rangeYako; $i < $rangeActivities; $i++ ) {
                    $yakoids[] = $row[$i];
                }
                for ( $i = $rangeActivities; $i < count($row); $i++ ) {
                    $activitiesids[] = $row[$i];
                }
            }

            if ( $key > 1 ) {
                $club = Club::where('id', $row[0])->first();

                if ( $club != null ) {
                    array_splice($row, 0, 2);

                    foreach ( $row as $ky => $bool ) {
                        if ( $ky >= $rangeYako-2 && $ky < $rangeActivities-2 ) {
                            if ( $bool == 'OUI' ) {
                                if( Yako::where('id', $yakoids[$ky])->first() != null ) {
                                    $yakosToAdd[] = $yakoids[$ky];
                                }
                            }
                        } else {
                            if ($bool == 'OUI') {
                                if (Activity::where('id', $activitiesids[$ky - abs($rangeActivities - $rangeYako)])->first() != null) {
                                    $activitiesToAdd[] = $activitiesids[$ky - abs($rangeActivities - $rangeYako)];
                                }
                            }
                        }
                    }

                    $club->yakos()->sync($yakosToAdd);
                    $club->activities()->sync($activitiesToAdd);
                }
            }

        }

        return redirect()->route('BOLinkToClub')->with([
            'success' => 'Import effectué avec succès'
        ]);
    }

    public function exportPlannings( Request $request ) {
        $message = null;
        $is_err = false;

        if ( $request->session()->get("error") ) {
            $message = $request->session()->get("error");
            $is_err = true;
        }

        return view('back.exportPlannings')->with([
            "message"   => $message,
            "is_err"    => $is_err,
        ]);
    }

    public function exportPlanningsGet() {
        $allClubs = Club::whereNotNull("planning_image")->pluck("planning_image", 'title');

        $zip = new \ZipArchive();
        $zip_name = 'plannings.zip';
        $result = true;
        $planning = '';

        if ($zip->open($zip_name, \ZipArchive::CREATE | \ZipArchive::OVERWRITE) === TRUE) {
            foreach ( $allClubs as $club => $planning ) {
                $ext = explode('.', $planning);
                $ext = $ext[count($ext)-1];

                if ( file_exists( public_path("storage/".$planning) ) ) {
                    $result = $zip->addFile(public_path("storage/".$planning), $club.".".$ext);

                    if ( !$result ) {
                        break;
                    }
                }
            }

            $zip->close();

            if ( $result ) {

                $headers = [
                    'Content-Type'                 => 'application/zip',
                    "Content-Transfer-Encoding"     =>  "binary",
                    'Content-Disposition'           =>  'attachment; filename='.$zip_name,
                    'Content-Length'                =>  filesize($zip_name)
                ];
                return response()->download(public_path()."/".$zip_name, $zip_name, $headers);

            } else {
                return redirect()->route('BOExportPlannings')->with([
                    'error' => "Un fichier n'existe pas : ".$planning
                ]);
            }
        } else {
            return redirect()->route('BOExportPlannings')->with([
                'error' => "Can't open zip file"
            ]);
        }
    }

    public function getTemplate( Request $request ) {
        $template_type = $request->template;
        $template_name = "import-".$template_type."_template.csv";
        if ($template_type) {

            $headers = [
                'Content-Type'                  => 'text/csv',
                "Content-Transfer-Encoding"     =>  "binary",
                'Content-Disposition'           =>  'attachment; filename='.$template_name,
                'Content-Length'                =>  filesize(public_path()."/csv_templates/".$template_name)
            ];

            return response()->download(public_path()."/csv_templates/".$template_name, $template_name, $headers);
        } else {
            return redirect()->route('BOSchedule')->with([
                'error' => "File does not exist"
            ]);
        }
    }
}
