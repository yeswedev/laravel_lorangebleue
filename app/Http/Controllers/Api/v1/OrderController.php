<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    public function getTotalPrice(Order $order)
    {
        $data = [
            'totalPrice' => $order->getTotalPriceOrder(),
            'totalUniquePrice' => $order->getUniqueTotalPriceOrder(false),
        ];
        
        return response()->json($data);
    }
}
