<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;
use Carbon\Carbon;

class RecruitmentController extends Controller
{
    public function getOffers( Request $request ) {

        if (\App::getLocale() !== 'fr') {
            abort(404);
        }

        $jobs = getEoliaJobs();
        $inputs = $request->input();

        $filter = [];
        if ($jobs['job'] && $inputs) {
            $filter = filterJobs( $jobs, $inputs );
        }

        if ($filter) {
            $filter = array_unique($filter, SORT_REGULAR);
            $jobs['job'] = $filter;
        }

        return response()->json([
            'jobs' => $jobs['job'],
        ]);
    }
}