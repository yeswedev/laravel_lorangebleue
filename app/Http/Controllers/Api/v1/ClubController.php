<?php

namespace App\Http\Controllers\Api\v1;

use App\Club;
use App\Http\Controllers\Controller;
use App\LandingPage;
use Illuminate\Http\Request;

use View;

class ClubController extends Controller
{
    public function getClubsIds($id = null, Request $request)
    {
        if ($id) {
            $club = Club::find($id);
            $html = '';
            if($club){
                $view = View::make('front.components.popup-map', compact('club'));
                $html = $view->render();
            }

            $data = [
                'club' => $club,
                'html' => $html,
            ];

            return response()->json($data);
        } else {
            $clubs = [];

            foreach ($request->all() as $key => $id) {
                $club = Club::find($id);

                if ($club) {
                    $clubs[$key]['club'] = $club;

                    $view = View::make('front.components.popup-map', compact('club'));

                    $clubs[$key]['html'] = $view->render();
                } else {
                    \Log::info('Club inconnu : '. $id. ' key : ' .$key);
                }
            }

            return response()->json($clubs);
        }
    }

    public function getSchedule( $club_id )
    {
        $club = Club::find($club_id);

        if ( $club ) {
            $schedule = $club->getSchedule();

            return response()->json($schedule);
        }

        return response()->json(['message' => 'Not Found.'], 404);
    }

    public function isFitness( $club_id, $landing_page_id ) {
        $club = Club::where('id', $club_id)->first();
        $landing_page = LandingPage::where('id', $landing_page_id)->first();

        return $club != null ? response()->json([
            "lp"  => $landing_page ? $landing_page : null,
            "sub"   => $club->subscription->type == "fitness" ? true : false,
        ]) : response()->json([
            'message' => 'Not Found.'
        ], 404);
    }

    public function getSubscription( $club_id ) {
        $club = Club::where('id', $club_id)->first();

        return response()->json( $club->subscription );
    }
}
