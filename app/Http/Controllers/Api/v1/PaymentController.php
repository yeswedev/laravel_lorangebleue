<?php

namespace App\Http\Controllers\Api\v1;

use App\Lib\Mangopay;
use Illuminate\Http\Request;
use MangoPay\MangoPayApi;

class PaymentController
{
    private $mangopay;

    public function __construct(MangoPayApi $mangopay)
    {
        $this->mangopay = $mangopay;
    }

    public function mangopay(Request $request)
    {
        \Log::info($request);
        response()->make('', 200);

        $mango = new Mangopay($this->mangopay);

        if ($request['RessourceId']) {
            $mango->updatePayment($request['RessourceId']);
        }

        return;
    }
}
