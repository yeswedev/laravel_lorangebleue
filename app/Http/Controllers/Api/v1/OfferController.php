<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Offer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    public function show(Offer $offer)
    {
        return response()->json($offer);
    }

    public function showAll(Request $request)
    {
        $allOffers = Offer::get();

        foreach ($allOffers as $offer) {
            $offers[] = $offer->getPrices($request->get('clubId'));
        }

        return response()->json($offers);
    }
}
