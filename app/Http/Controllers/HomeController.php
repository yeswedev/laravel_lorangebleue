<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsletterFormRequest;
use App\Mail\NewsletterMail;
use App\Offer;
use App\Post;
use App\Subscription;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use JsonLd\Context;
use LaravelLocalization;
use Mail;
use Newsletter;
use App\PageGeneric;

use App\Exports\ClubsExport;
use Maatwebsite\Excel\Facades\Excel;


use Spatie\Robots\Robots;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Redirection 301 - start
        if ($request->get('s')) {
            $search = $request->get('s');
            if ($search == 'Recrutement') {
                return redirect()->route('recruitment.index', [], 301);
            } elseif ($search == 'Résiliation') {
                return redirect()->route('index', [], 301);
            } elseif ($search == 'Zumba') {
                return redirect()->route('activity.showAll', [], 301);
            } else {
                return redirect()->route('index', [], 301);
            }
        } elseif ($request->get('ck') || $request->get('uk') || $request->get('scode') || $request->get('loc') || $request->get('fbclid')) {
            return redirect()->route('index', [], 301);
        } elseif (
            $request->fullUrl() == config('app.url').'/?id_departement=&ville='
        ) {
            return redirect()->route('front.find-club', [], 301);
        } elseif ($request->has('_sm_byp')) {
            return redirect()->route('front.find-club', [], 301);
        }
        // Redirection 301 - end

        $blog_lists = \YesWeDev\LaravelCMS\Post::where('type', 'post')->take(3)->get();
        $app_subscription_fitness = Subscription::where('type', 'fitness')->first();
        $app_subscription_wellness = Subscription::where('type', 'wellness')->first();
        $app_subscriptions = Subscription::get();

        $offers = Offer::activeOffers(Offer::getIsOnHomeOffersFilter(Offer::query()));

        $params = [
            'blog_lists' => $blog_lists,
            'offers' => $offers,

            'env_template' => 'homepage',
            'page_name' => '/',
        ];

        $offer_id = $app_subscription_fitness->offers->first()->id;
        $offer_title = $app_subscription_fitness->offers->first()->title;
        $offer_commitment = $app_subscription_fitness->offers->first()->commitment;
        $offer_is_student = $app_subscription_fitness->offers->first()->is_student;
        $subscription_id = $app_subscription_fitness->id;
        $subscription_url = $app_subscription_fitness->url;

        $params['app_subscription_fitness'] = $app_subscription_fitness;
        $params['app_subscription_wellness'] = $app_subscription_wellness;
        $params['app_subscriptions'] = $app_subscriptions;
        $params['offer_id'] = $offer_id;
        $params['offer_title'] = $offer_title;
        $params['offer_commitment'] = $offer_commitment;
        $params['offer_is_student'] = $offer_is_student;
        $params['subscription_id'] = $subscription_id;
        $params['subscription_url'] = $subscription_url;

        $search_box_SEOcontext = Context::create('search_box', [
            'url' => request()->fullUrl(),
            'potentialAction' => [
                'target' => route('front.find-club').'?search={search_term}',
                'query-input' => 'required name=search_term',
            ],
        ]);

        $params['search_box_SEOcontext'] = $search_box_SEOcontext;
        $params['formattedBannerPrice'] = Offer::getFormattedBannerPrice();
        $params['avantageDiscount'] = Offer::getAvantageDiscount();

        $params['pageGeneric'] = PageGeneric::where('type', 'home')->first();
        $params['hasStudentOffers'] = false ;

        foreach ($offers as $offer) {
            if ($offer->is_student) {
                $params['hasStudentOffers'] = true;
                break;
            }
        }

        return view('front.home')->with($params);
    }

    public function redirect()
    {
        $user = Auth::user();
        if (!$user) {
            return redirect('/');
        }
        $role = Auth::user()->role;
        switch ($role->title) {
            case 'admin':
                return redirect('/admin');
        }
        return redirect('/');
    }

    public function newsletter(NewsletterFormRequest $request)
    {
        $email = $request->get('footerNews');

        Newsletter::subscribeOrUpdate($email);

        Mail::to($email)->send(new NewsletterMail());
        if (LaravelLocalization::getCurrentLocale() == 'fr' || LaravelLocalization::getCurrentLocale() == 'ma') {
            return Redirect::to(URL::previous() . "#footer")->with('message', 'Votre inscription a bien été prise en compte');
        } else {
            return Redirect::to(URL::previous() . "#footer")->with('message', 'Tu registro ha sido tenido en cuenta');
        }
    }

    public function robotstxt()
    {
        $robots = new \App\Lib\Robots();
        // If on the live server, serve a nice, welcoming robots.txt.
        if (config('app.env') == 'production' ||config('app.env') == 'preprod') {
            $robots->addUserAgent('*');
            // Needed ?
            $robots->addDisallow('/wp-admin/');
            $robots->addDisallow('/blog?date=*');
            $robots->addSitemap(\URL::to('/').'/sitemapxml');
        } else {
            $robots->addDisallow('*');
        }

        return response()->make($robots->generate(), 200, ['Content-Type' => 'text/plain']);
    }

    public function exportMV()
    {
        return (new ClubsExport)->download('exportmv.csv', \Maatwebsite\Excel\Excel::CSV);
    }
}
