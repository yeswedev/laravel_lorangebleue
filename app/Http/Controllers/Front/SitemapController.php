<?php

namespace App\Http\Controllers\Front;

use App\Club;
use App\Department;
use App\Subscription;

use App\Http\Controllers\Controller;
use LaravelLocalization;

class SitemapController extends Controller
{
    public function show()
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $clubs = Club::getClubsFrom50MainCities();
        $clubsByDepartment = [];
        foreach (Department::where('country', $locale)->get() as $department) {
            $clubsByDepartment[$department->id] = [
                'department' => $department,
                'clubs' => []
            ];
        }
        foreach ($clubs as $club) {
            if ($club->department) {

                $clubsByDepartment[$club->department->id]['clubs'][str_slug($club->city)] = $club;

                if($club->city == 'dijon nord' || $club->city == 'dijon')
                {
                    $club->formattedTitle = 'Dijon';
                }
                elseif($club->city == 'buxerolles' || $club->city == 'poitiers')
                {
                    $club->formattedTitle = 'Poitiers';
                }
                elseif($club->city == 'pau' || $club->city == 'pau lescar')
                {
                    $club->formattedTitle = 'Pau';
                }
                elseif($club->city == 'lahonce' || $club->city == 'bayonne')
                {
                    $club->formattedTitle = 'Bayonne';
                }
                elseif($club->city == 'brest centre' || $club->city == 'brest')
                {
                    $club->formattedTitle = 'Brest';
                }
                elseif($club->city == 'palaiseau' || $club->city == 'massy')
                {
                    $club->formattedTitle = 'Massy';
                }
                elseif($club->city == 'remilly')
                {
                    $club->formattedTitle = 'Thionville';
                }
                elseif($club->city == 'lavau')
                {
                    $club->formattedTitle = 'Troyes';
                }
                elseif($club->city == 'villebarou' || $club->city == 'blois')
                {
                    $club->formattedTitle = 'Blois';
                }
                elseif($club->city == 'le vieil evreux' || $club->city == 'evreux')
                {
                    $club->formattedTitle = 'Evreux';
                }
                else
                {
                    $club->formattedTitle = $club->city;
                }

            }
        }

        $subscriptions = Subscription::get() ;

        // TODO Maroc : delete when they'll have the wellness thing
        if (\LaravelLocalization::getCurrentLocale() == 'ma') 
            foreach ($subscriptions as $key=>$sub)
                if ($sub->type === 'wellness')
                    unset($subscriptions[$key]);

        $params = [
            'clubsByDepartment' => $clubsByDepartment,
            'specialOffer' => \App\Specialoffer::getCurrentSpecialoffer(),
            'subscriptions' => $subscriptions
        ];

        return view('front.sitemap')->with($params);
    }
}
