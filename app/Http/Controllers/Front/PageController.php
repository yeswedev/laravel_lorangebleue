<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use LaravelLocalization;
use YesWeDev\LaravelCMS\Page;
use App\Mail\ContactPageMail;
use Illuminate\Support\Facades\Input;
use Mail;
use Validator;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function retrieve($pageUrl)
    {
        $locale = LaravelLocalization::getCurrentLocale();

        $page = Page::whereHas('translations', function ($q) use ($pageUrl, $locale) {
            $q->where('url', $pageUrl);
            $q->where('locale', $locale);
        })->first();

        if (!isset($page)) {
            return abort(404);
        }

        // Need to declare these, otherwise compact() will throw an error
        $open_club = null;
        $pdf = null;

        if($page->url == trans('routes.page.ouvrir-salle-sport') || $page->url == trans('routes.page.ouvrir-salle-sport-accompagnement'))
        {
            $open_club = true;
            if ( $page->attachments->first() != null ) {
                $pdf = Storage::url( $page->attachments->first()->src );
            }
        }

        return view('front.pages.single', compact('page', 'open_club', 'locale', 'pdf', 'pageUrl'));
    }

    public function contact()
    {
        $clientData = Input::all();

        $v = Validator::make($clientData, [
            'firstname' => 'required',
            'phone' => 'required',
            'zip_code' => 'required',
            'city' => 'required',
            'name' => 'required',
            'email' => 'required|string|email|max:255',
            'message' => 'required',
            'contact' => 'required',
        ]);

        if ($v ->fails()) {
            if($clientData['page_url'] == trans('routes.page.ouvrir-salle-sport-accompagnement'))
            {
                return redirect()->route('page.retrieve', trans('routes.page.ouvrir-salle-sport-accompagnement'))->withErrors($v);
            }
            else
            {
                return redirect()->route('page.retrieve', trans('routes.page.ouvrir-salle-sport'))->withErrors($v);
            }
        }

        Mail::to(config('mail.open_club.value'))->send(new ContactPageMail($clientData));
        $message = __('validation.emailForm.valid');
        if (Mail::failures()) {
            $message = __('validation.emailForm.error');
        }
        if($clientData['page_url'] == trans('routes.page.ouvrir-salle-sport-accompagnement'))
        {
            return redirect()->route('page.retrieve', trans('routes.page.ouvrir-salle-sport-accompagnement'))->with('message-open-club', $message);
        }
        else
        {
            return redirect()->route('page.retrieve', trans('routes.page.ouvrir-salle-sport'))->with('message-open-club', $message);
        }
    }

    public function retrieveChild($childUrl)
    {
        return $this->retrieve($childUrl);
    }
}
