<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Offer;
use App\Post;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;
use JsonLd\Context;
use LaravelLocalization;
use YesWeDev\LaravelCMS\Archive;

use App\PageGeneric;

use YesWeDev\LaravelCMS\Tag;

class ArticleController extends Controller
{
    private function isValidateDate($date, $format = 'Y-m')
    {
        try {
            $d = Date::createFromFormat($format, $date);

            return $d && $d->format($format) === $date;
        } catch (Exception $e) {
            return false;
        }
    }

    public function retrieve($categoryUrl, $articleUrl, Request $request)
    {
        $locale                   = LaravelLocalization::getCurrentLocale();

        $blog = \YesWeDev\LaravelCMS\Post::where('type', 'post')->whereHas('translations', function ($q) use ($articleUrl, $locale) {
            $q->where('url', $articleUrl);
            if ($locale) {
                $q->where('locale', $locale);
            }
        })->first();

        if (! $blog) {
            abort(404);
        }

        $parent = $blog->parent;
        if ($parent != null) {
            $category   = $parent;
            $blog_lists = $category->children()->where('id', '!=', $blog->id)->get();
            $temp_array = collect();

            foreach($blog_lists as $child)
            {
                $child = \YesWeDev\LaravelCMS\Post::where('id', $child->id)->with('attachments')->first();
                if ( $child ) {
                    $temp_array[] = $child;
                }
            }
            $blog_lists = $temp_array;
            
        } else {
            $blog_lists = [];
        }

        $queryTag = $request->get('tag');
        $tags     = $blog->tags;

        if ($queryTag && $queryTag != '') {
            $tags = Tag::where('slug', $queryTag)->get();
        }

        $months = [];
        $posts  = \YesWeDev\LaravelCMS\Post::all();
        foreach ($posts as $post) {
            $date              = Date::createFromFormat('Y-m', $post->created_at->format('Y-m'))->format('F Y');
            $months[ $date ][] = $post;
        }

        $params                         = [
            'blog_lists'   => $blog_lists,
            'tags'         => $tags,
            'months'       => $months,
            'env_template' => 'blog',
            'page_name'    => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1'    => request()->segment(1),
            'page_cat2'    => request()->segment(2),
            'page_cat3'    => request()->segment(3),
        ];
        $params['formattedBannerPrice'] = Offer::getFormattedBannerPrice();
        $params['avantageDiscount'] = Offer::getAvantageDiscount();

        if (count($blog->attachments) > 0) {
            $imageUrl = Storage::disk('public')->url($blog->attachments->first()->src);
        } else {
            $imageUrl = null;
        }

        $parent = $blog->parent;
        if ($parent != null) {
            $entityUrl = route('blog.retrieve', [ $parent->url, $blog->url ]);
        } else {
            $entityUrl = null;
        }

        $news_article_SEOcontext = Context::create('news_article', [
            'headline' => $blog->title,
            'mainEntityOfPage' => [
                'url' => $entityUrl,
            ],
            'image' => [
                'url' => $imageUrl,
                'height' => 772,
                'width' => 543,
            ],
            'datePublished' => $blog->created_at->format('Y-m-d'),
            'dateModified' => $blog->updated_at->format('Y-m-d'),
            'author' => [
                'name' => config('app.name'),
            ],
            'publisher' => [
                'name' => config('app.name'),
                'logo' => [
                    'url' => config('app.url'),
                ]
            ],
        ]);

        $params['news_article_SEOcontext'] = $news_article_SEOcontext;

        return view('front.blog.single', compact('blog'))->with($params);
    }

    public function showAll($slug = null, Request $request)
    {
        // Redirection 301 - start
        if ($request->get('s')) {
            return redirect()->route('blog.showAll', [], 301);
        }
        // Redirection 301 - end

        if (!$slug) {
            $tag = $request->get('tag');
            if ($tag && $tag != '') {
                $tag_obj    = Tag::whereHas('translations', function ($query) use ($slug) {
                    $query->where('title', 'like', '%'.$slug.'%');
                })->first();
                if ($tag_obj) {
                    $posts = $tag_obj->posts()->get();
                } else {
                    $posts = [];
                }
            } else {
                $posts = \YesWeDev\LaravelCMS\Post::all();
            }
        } elseif ($this->isValidateDate($slug)) {
            $date = Date::createFromFormat('Y-m', $slug);
            $dateMonth  = $date->format('m');
            $posts = \YesWeDev\LaravelCMS\Post::where('type', 'post')
                ->whereMonth('created_at', '=', $dateMonth)
                ->get();
            $metaTitle = "Les Actualités sportives de " . $date->localeMonth . " ". $date->year . " - ".config('app.name');
            $metaDescription = "Retrouvez toutes les actualités de " . $date->localeMonth . " ". $date->year . " sur le blog de l'Orange Bleue découvrez des exercices de musculation et fitness, des conseils en nutrition…";
        } else {
            $archive = Archive::whereHas('translations', function ($query) use ($slug) {
                $query->where('url', $slug);
            })->first();

            if ($archive) {
                $metaTitle = $archive->title." les actualités - ".config('app.name');
                $metaDescription = "Retrouvez toutes les actualités " . $archive->title . " sur le blog de l'Orange Bleue et découvrez tous les conseils de nos professionnels du sport.";
                $posts = \YesWeDev\LaravelCMS\Post::where('parent_id', $archive->id)->get();
            } else {
                abort(404);
            }
        }

        $posts = $posts->sortByDesc('created_at');

        $categories = Archive::has('children')
            ->whereNotIn('slug', ['a-propos', 'ouvrir-salle-sport'])
            ->get();

        $params = [
            'blog_lists'   => $posts,
            'categories'   => $categories,
            'env_template' => 'blog',
            'page_name'    => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1'    => request()->segment(1),
            'page_cat2'    => request()->segment(2),
            'page_cat3'    => request()->segment(3),
            'isSearchPage' => !empty($keyword),
        ];
        if (isset($metaTitle)) {
            $params['metaTitle']   = $metaTitle;
        }
        if (isset($metaDescription)) {
            $params['metaDescription']   = $metaDescription;
        }

        $list_seo = [];

        foreach ($posts as $key => $blog) {
            $list_seo[] = [
                "@type"    => "ListItem",
                'position' => $key,
                'url'      => route('blog.retrieve', [ $blog->url ]),
            ];
        }

        $properties = [
            "@context"        => "http://schema.org",
            "@type"           => "ItemList",
            'itemListElement' => $list_seo,
        ];

        $list_item_SEOcontext = "<script type=\"application/ld+json\">" . json_encode($properties, JSON_UNESCAPED_UNICODE) . "</script>";

        $params['list_item_SEOcontext'] = $list_item_SEOcontext;
        $params['pageGeneric'] = PageGeneric::where('type', 'blog')->first();

        return view('front.blog.list')->with($params);
    }
}
