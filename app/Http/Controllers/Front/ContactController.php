<?php

namespace App\Http\Controllers\Front;

use App\Club;
use App\Http\Controllers\Controller;
use App\Mail\ContactPageMail;
use App\Mail\ModalTunnelAboMail;
use App\Rules\Recaptcha;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Mail;
use Validator;

class ContactController extends Controller
{
    public function index()
    {
        return view('front.contact.contact');
    }

    public function mail()
    {
        $clientData = Input::all();

        $v = Validator::make($clientData, [
            'name' => 'required',
            'firstname' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|max:255',
            'subject' => 'required',
            'message' => 'required',
            'check' => 'required',
            'g-recaptcha-response'=>['required', new Recaptcha],
        ]);

        if ($v ->fails()) {
            return redirect()->route('contact.index')->withErrors($v);
        }

        if ($clientData['subject'] != 'contact_club') {
            Mail::to(config('mail.'.$clientData['subject'].'.value'))->send(new ContactPageMail($clientData));
            $message = __('validation.emailForm.valid');
            if (Mail::failures()) {
                $message = __('validation.emailForm.error');
            }
            return redirect(url()->previous())->with('message', $message);
        } else {
            return redirect()->route('front.find-club');
        }
    }

    public function mailFromModalTunnelAbo( Request $request ) {
        $data = $request->input();

        $v = Validator::make($data, [
            'club_id' => 'required',
            'lastname' => 'required',
            'firstname' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|max:255',
        ]);

        if ($v ->fails()) {
            return redirect()->route('subscription.step1', 'concepts')->withErrors($v);
        }

        $club = Club::find( $data["club_id"] );
        if ( $club ) {
            $data['club'] = $club;
            Mail::to($club->email)->send( new ModalTunnelAboMail( $data ) );
            $message = __('validation.emailForm.valid');
            if (Mail::failures()) {
                $message = __('validation.emailForm.error');
            }
            return redirect(url()->previous())->with('message', $message);
        }

        $message = trans("trad.error.occured");
        return redirect(url()->previous())->withErrors($message);
    }
}
