<?php

namespace App\Http\Controllers\Front;

use App\Activity;
use App\ActivityTranslation;
use App\Http\Controllers\Controller;
use App\Yako;
use App\PageGeneric;

class ActivityController extends Controller
{
    public function retrieve(Activity $activity)
    {
        // TODO Maroc : delete when they'll have the wellness thing
        if (\LaravelLocalization::getCurrentLocale() == 'ma' && $activity->type == 'wellness') 
            abort(404);
    
        $activity_lists = Activity::where('id', '!=', $activity->id)->get();

        // TODO : maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma')

        foreach ($activity_lists as $key=>$e) {
            if($e->type == 'wellness')
            unset($activity_lists[$key]);

            // Override both : only display Fitness tag on activities page
            if($e->type == 'both')
                $e->type = 'fitness' ;
            
            }
        // stop remove to this

        if( !$activity->url ) {
            abort(404);
        }

        $params = [
            'activity_lists' => $activity_lists,
            'env_template' => 'activites',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => request()->segment(1),
            'page_cat2' => request()->segment(2),
            'activity' => $activity
        ];

        return view('front.activities.single-activity')->with($params);
    }

    public function showAll()
    {
        $activity_lists = Activity::all();

        // TODO : maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma')
            foreach ($activity_lists as $key=>$e)
                if($e->type == 'wellness')
                    unset($activity_lists[$key]);
        // stop remove to this

        $yako_lists = Yako::take(3)->get();
        $params = [
            'activity_lists' => $activity_lists,
            'yako_lists' => $yako_lists,
            'env_template' => 'activites',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => request()->segment(1),
            'page_cat2' => request()->segment(2),
        ];
        $params['pageGeneric'] = PageGeneric::where('type', 'activity')->first();

        // TODO maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma')
            foreach ($activity_lists as $key=>$e) {
                if($e->type == 'wellness')
                unset($activity_lists[$key]);

                // Override both : only display Fitness tag on activities page
                if($e->type == 'both')
                    $e->type = 'fitness' ;
                
            }
        // stop remove to this
            
        return view('front.activities.list-activities')->with($params);
    }
}
