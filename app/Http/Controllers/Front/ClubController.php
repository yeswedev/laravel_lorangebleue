<?php

namespace App\Http\Controllers\Front;

use App\City;
use App\Club;
use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;

use App\Http\Requests\ContactUsFormRequest;
use App\Jobs\PreInscriptionContactClientJob;
use App\Jobs\PreInscriptionContactJob;
use App\Lib\Geocoding;
use App\Mail\ClubContactMailToClient;
use App\Mail\ClubContactMailToClub;

use App\Offer;
use App\Order;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\PageGeneric;

use LaravelLocalization;

use Mail;

class ClubController extends Controller
{
    public function search(Request $request)
    {
        $locale = LaravelLocalization::getCurrentLocale();

        $latitude = $request->get('latitude', null);
        $longitude = $request->get('longitude', null);
        $search = $request->get('search', null);

        $aroundMe = false;
        if ($search == 'Autour de moi' || $search == 'A mi alrededor') {
            $aroundMe = true;
            $search = null;
        }

        if ($search) {
            //We first look for a club in our database
            foreach (City::get() as $city) {
                if (str_slug($search) == $city->url) {
                    $routeParams = ['department' => $city->department->url, 'city' => $city->url];
                    if ($request->get('type')) {
                        $routeParams['type'] = $request->get('type');
                    }
                    return redirect()->route('front.club-list', $routeParams);
                }
            }

            //Else > Geocoding of search
            $position = Geocoding::findLongLatFromAddress($request->input('search'), $locale);
            $latitude = $position['lat'];
            $longitude = $position['lng'];
        }

        $cities = City::orderBy('name')->get();

        $filteredCities = City::getMainCities(); //default value, we show main cities
        if ($latitude && $longitude) {
            $filteredCities = Geocoding::filterByLatLong($cities, $latitude, $longitude, 50);
        } else {
            foreach (City::get() as $city) {
                if (str_slug($search) == $city->url) {
                    $routeParams = ['department' => $city->department->url, 'city' => $city->url];
                    if ($request->get('type')) {
                        $routeParams['type'] = $request->get('type');
                    }
                    return redirect()->route('front.club-list', $routeParams);
                }
            }
        }
        $firstCity = $filteredCities->first();

        //If arroundMe, client wants to see the map
        if ($firstCity && ($aroundMe || $search)) {
            $routeParams = ['department' => $firstCity->department->url, 'city' => $firstCity->url];
            if ($request->get('type')) {
                $routeParams['type'] = $request->get('type');
            }
            if ($request->get('latitude')) {
                $routeParams['latitude'] = $request->get('latitude');
            }
            if ($request->get('longitude')) {
                $routeParams['longitude'] = $request->get('longitude');
            }
            return redirect()->route('front.club-list', $routeParams);
        }

        $params = [
            'cities' => $filteredCities,
            'env_template' => 'clubs',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => request()->segment(1),
        ];
        $pageGeneric = PageGeneric::where('type', 'club')->first();

        if($locale != 'fr')
            $pageGeneric->cities = City::where('country', $locale)->get();

        $params['pageGeneric'] = $pageGeneric ;
        return view('front.clubs.find-club')
            ->with($params)
            ->with('locale', $locale);
    }


    public function searchbydepartment(Request $request)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $departments = Department::orderBy('name')->where('country',$locale)->get();

        $params = [
            'departments' => $departments,
            'env_template' => 'clubs',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => request()->segment(1),
        ];

        return view('front.clubs.find-club-by-department')
            ->with($params);
    }

    public function show($clubUrl, Request $request)
    {
        // Redirection 301 - start
        if ($request->get('sa') || $request->get('wb48617274') || $request->get('xtor') || $request->get('ref')) {
            return redirect()->route('front.club-details', ['clubUrl' => $clubUrl], 301);
        }
        // Redirection 301 - end

        $locale = LaravelLocalization::getCurrentLocale();

        $club = Club::where('url', $clubUrl)->first();

        if ($club) {
            if($locale !== $club->country){
                abort(404);
            }
            $yako_lists = [];
            foreach ($club->yakos as $yako) {
                $illustration_type = ($yako->link) ? "video" : "image";
                $illustration_src = ($yako->link) ? $yako->getVideoEmbedLink() : Storage::url($yako->image);
                $yako_lists[$yako->id] = [
                    'object' => $yako,
                    'illustration_type' => $illustration_type,
                    'illustration_src' => $illustration_src
                ];
            }

            $schedule = $club->getSchedule();

            $legal_mentions = array_column( $club->offers->toArray(), "legal_mentions" );
            $empty = 0;
            foreach ( $legal_mentions as $legal_mention ) {
                if ( $legal_mention != null ) {
                    $empty++;
                }
            }
            $no_legal_mentions = false;
            if ( $empty == 0 ) {
                $no_legal_mentions = true;
            }

            $params = [
                'modal' => true,
                'env_template' => collect(request()->segments())->last(),
                'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
                'page_cat1' => request()->segment(1),
                'type_page' => 'page-club',
                'club_name' => $club->title,
                'club_id' => $club->id,
                'local_business_SEOcontext' => $club->buildSeoContext(),
                'subscription' => $club->subscription,
                'club_equipments' => $club->equipments,
                'advantages' => $club->advantages,
                'locale' => $locale,
                'yakos' => $club->yakos,
                'club' => $club,
                'schedule' => $schedule,
                'no_legal_mentions' => $no_legal_mentions,
                'yako_lists' => $yako_lists,
            ];
            $params['formattedBannerPrice'] = Offer::getFormattedBannerPrice();
            $params['avantageDiscount'] = Offer::getAvantageDiscount();

            $params["hasStudentOffers"] = false;
            foreach ( $club->activeOffers() as $offer ) {
                if ( $offer->is_student ) {
                    $params["hasStudentOffers"] = true;
                    break;
                }
            }
            foreach ($club->medias as $key => $media) {
                if (Storage::url($media->file) === '/storage/')
                    unset($club->medias[$key]) ;
            }
            return view('front.clubs.club-details')
                ->with($params);
        } else {
            $department = Department::where('url', $clubUrl)->first();

            if($department)
            {
                $cities = City::where('department_id', $department->id)->get();
            }
            else
            {
                $cities = City::get();
            }
            

            $club_lists = [];
            if ($department) {
                if($locale !== $department->country){
                    abort(404);
                }
                $club_lists = $department->clubs;
            }

            if (count($club_lists) > 0) {
                $club_ids = [];
                foreach ($club_lists as $club) {
                    $club_ids[] = $club->id;
                }
                $params = [
                    'entity' => $department,
                    'club_lists' => $club_lists,
                    'department' => $department,
                    'club_ids' => $club_ids,
                    'cities' => $cities,
                ];

                return view('front.clubs.find-club-step2')->with($params);
            } elseif ($clubUrl == 'villes') {
                return $this->listMostClubsPerCity();
            } else {
                return redirect()->route('front.find-club', [], 301);
            }
        }
    }

    public function preConfirmation($clubUrl)
    {
        $club = Club::where('url', $clubUrl)->first();

        $params = [
            'env_template' => collect(request()->segments())->last(),
            'modal' => true
        ];

        return view('front.tunnel.confirm-pre', compact('club'))->with($params);
    }

    public function clubsList($departmentUrl, $cityUrl, Request $request)
    {
        $country = LaravelLocalization::getCurrentLocale();
        $city = City::where(['url' => $cityUrl])->first();
        $department = Department::where('url', $departmentUrl)->first();
        if (!$city || !$department) {
            abort(404);
        }
        $locale = LaravelLocalization::getCurrentLocale();
        if($locale !== $department->country){
            abort(404);
        }
        $latitude = $city->latitude;
        $longitude = $city->longitude;

        $clubs = [];
        if ($request->get('type')) {
            $subscription = Subscription::where('type', $request->get('type'))->first();
            if ($subscription) {
                $clubs = Club::where('subscription_id', $subscription->id)->get();
            }
        }
        if (!count($clubs)) {
            if ($country == 'es' || $country == 'Espagne')
            {
                $clubs = Club::where('country', 'es')->orWhere('country', 'Espagne')->get();
            }
            elseif($country == 'fr' || $country == 'France')
            {
                $clubs = Club::where('country', 'fr')->orWhere('country', 'France')->get();
            }
            elseif($country == 'ma' || $country == 'Maroc')
            {
                $clubs = Club::where('country', 'ma')->orWhere('country', 'Maroc')->get();
            }
        }

        $club_lists = Geocoding::filterClubsByLatLong($clubs, $latitude, $longitude, 60);

        $club_ids = [];
        foreach ($club_lists as $club) {
            $club_ids[] = $club->id;
        }

        $params['latitude'] = $latitude;
        $params['longitude'] = $longitude;
        $params['department'] = $department;
        $params['search'] = $city->name;
        $params['result'] = 1;
        $params['country'] = $country;
        $params['modal'] = true;
        $params['env_template'] = 'clubs';
        $params['page_name'] = str_replace(request()->root(), '', request()->fullUrl());
        $params['page_cat1'] = request()->segment(1);
        $params['city'] = $city;
        $params['entity'] = $city;

        return view('front.clubs.find-club-step2')
            ->with($params)
            ->with('club_lists', $club_lists)
            ->with('club_ids', $club_ids);
    }

    public function listDepartmentAvailableClub()
    {
        $locale = LaravelLocalization::getCurrentLocale();

        $params = [
            'departments' => Department::where('country', $locale)->orderBy('name')->get()
        ];

        return view('front.clubs.clubs-departments')->with($params);
    }

    public function listMostClubsPerCity()
    {
        $city_lists = City::getMainCities();

        $params = [
            'city_lists' => $city_lists,
        ];

        if (count($city_lists) > 0) {
            return view('front.clubs.clubs-cities')->with($params);
        } else {
            abort(404);
        }
    }

    public function contact(ContactFormRequest $request, Club $club)
    {
        $clientEmail = $request->input('email');
        $clientFirstname = $request->input('firstname');
        $clientLastname = $request->input('lastname');
        $clientPhone = $request->input('phone_number');

        $orders = Order::where('email', $clientEmail)->get();

        if ($club->subscription) {
            if ($club->subscription->type == 'fitness') {
                $env_template = "pre_insc_conf_fit";
            } elseif ($club->subscription->type == 'wellness') {
                $env_template = "pre_insc_conf_well";
            }
        } else {
            $env_template = "pre_insc_conf";
        }


        $params = [
            'club' => $club,
            'env_template' => $env_template,
            'page_cat1' => 'pre_insc_conf',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'user_email' => $clientEmail,
            'user_ville_club' => $club->city."_".$club->title,
            'ville_club' => $club->city."_".$club->title,
            'user_status' => count($orders) > 0 ? 1 : 0,
            'order_type' => 'P',
        ];

        $initParams = [
            'clubTitle' => $club->title,
            'clubAddress' => $club->address,
            'clubZipCode' => $club->zip_code,
            'clubCity' => $club->city,
            'clubPhone' => $club->phone,
            'clubEmail' => $club->email,
            'clubUrl' => $club->url,
            'clientFirstname' => $clientFirstname,
            'clientLastname' => $clientLastname,
            'clientPhone' => $clientPhone,
            'clientEmail' => $clientEmail,
        ];

        if($club->email)
        {
            dispatch(new PreInscriptionContactJob($club->email, $initParams));
        }
        if($club->url)
        {
            dispatch(new PreInscriptionContactClientJob($clientEmail, $initParams));
        }

        return redirect()->route('front.tunnel.confirm-pre', $club->url)->with($params);
    }

    public function contactUs(ContactUsFormRequest $request)
    {
        $clientEmail = $request->input('email');
        $message = $request->input('message-modal');
        $clientFirstname = $request->input('firstname');
        $clientLastname = $request->input('lastname');
        $clientPhone = $request->input('phone_number');

        $clubId = $request->input('club-modal');
        $club = Club::find($clubId);

        $initParams = [
            'clubTitle' => $club->title,
            'clubAddress' => $club->address,
            'clubZipCode' => $club->zip_code,
            'clubCity' => $club->city,
            'clubPhone' => $club->phone,
            'clubEmail' => $club->email,
            'clubUrl' => $club->url,
            'message' => $message,
            'clientFirstname' => $clientFirstname,
            'clientLastname' => $clientLastname,
            'clientPhone' => $clientPhone,
            'clientEmail' => $clientEmail,
        ];

        Mail::to($club->email)->send(new ClubContactMailToClub($initParams));
        Mail::to($clientEmail)->send(new ClubContactMailToClient($initParams));

        return redirect()->back();
    }
}
