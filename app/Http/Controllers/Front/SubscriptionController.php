<?php

namespace App\Http\Controllers\Front;

use \MangoPay\MangoPayApi;
use App\Club;
use App\Equipment;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentFormRequest;
use App\Lib\Geocoding;
use App\Lib\Mangopay;
use App\Mail\NewsletterMail;
use App\Offer;
use App\Order;
use App\Payment;
use App\Subscription;
use App\Yako;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use LaravelLocalization;

use Mail;

use Newsletter;

class SubscriptionController extends Controller
{
    private $mangopay;

    public function __construct(MangoPayApi $mangopay)
    {
        $this->mangopay = $mangopay;
    }

    public function index($subscriptionUrl)
    {
        $locale = LaravelLocalization::getCurrentLocale();

        // TODO Maroc : delete when they'll have the wellness thing
        if ($locale == 'ma' && $subscriptionUrl == 'mon-coach-wellness') 
            abort(404);

        $subscription = Subscription::getSubscriptionByUrl($subscriptionUrl, $locale);
        if (!$subscription) {
            $subscription = Subscription::where('type', 'fitness')->first();
        }

        $equipments_lists = Equipment::take(12)->where('type', '!=', 'wellness')->get();

        $yakos = Yako::get();
        $yako_lists = [];
        foreach ($yakos as $yako) {
            $illustration_type = ($yako->link) ? "video" : "image";
            $illustration_src = ($yako->link) ? $yako->getVideoEmbedLink() : Storage::url($yako->image);
            $yako_lists[$yako->id] = [
                'object' => $yako,
                'illustration_type' => $illustration_type,
                'illustration_src' => $illustration_src
            ];
        }

        $params = [
            'yako_lists' => $yako_lists,
            'yakos' => $yakos,
            'equipments_lists' => $equipments_lists,

            'env_template' => 'abonnements',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => request()->segment(1),
            'page_cat2' => request()->segment(2),
            'subscription_id' => $subscription->id,
            'subscription_url' => $subscription->url,
        ];

        if (isset($subscription->url)) {
            if (\strpos($subscription->url, 'wellness') !== false && LaravelLocalization::getCurrentLocale() == 'es') {
                return abort(404);
            } else {
                if (count($subscription->offers) > 0) {
                    $offer_id = $subscription->offers->first()->id;
                    $offer_title = $subscription->offers->first()->title;
                    $offer_commitment = $subscription->offers->first()->commitment;
                    $offer_is_student = $subscription->offers->first()->is_student;
                } else {
                    $offer_id = '';
                    $offer_title = '';
                    $offer_commitment = '';
                    $offer_is_student = '';
                }

                if (count($subscription->offers) > 0) {
                    foreach ($subscription->offers as $key => $offer) {
                        $products[$key] = [
                            'list_product_unitprice_ati' => number_format($offer->getFirstMonthPrice(false) / 100, 2, '.', ''),
                            'list_product_id' => preg_replace('/[^0-9]/', '', $offer->title) == $offer->commitment ? preg_replace('/ /', '_', strtoupper($offer->title)) : strtoupper($offer->title) . '_' . $offer->commitment,
                            'list_product_name' => preg_replace('/[^0-9]/', '', $offer->title) == $offer->commitment ? trim(preg_replace('/[0-9]+/', '', strtoupper($offer->title))) : strtoupper($offer->title),
                            'list_product_duree_abo' => $offer->commitment,
                            'list_product_student' => $offer->is_student,
                            'list_product_category_id' => '1',
                            'list_product_category' => $subscription->url,
                        ];
                    }

                    $params['products'] = json_encode($products);
                }

                $params['offer_id'] = strtoupper($offer_title) . '_' . $offer_commitment;
                $params['offer_title'] = strtoupper($offer_title);
                $params['offer_commitment'] = $offer_commitment;
                $params['offer_is_student'] = $offer_is_student;
                $params['formattedBannerPrice'] = Offer::getFormattedBannerPrice();
                $params['avantageDiscount'] = Offer::getAvantageDiscount();
                
                $params['hasStudentOffers'] = false ;

                foreach ($subscription->offers as $offer) {
                    if ($offer->is_student) {
                        $params['hasStudentOffers'] = true;
                        break;
                    }
                }

                if (count($subscription->testimonials) > 0) {
                    $params['testimonials'] = $subscription->testimonials;
                }

                return view('front.subscription.single-subscription', compact('subscription'))->with($params);
            }
        } else {
            return abort(404);
        }
    }

    /**
     * @param $subscriptionUrl
     * @return Subscription
     */
    private function getSubscription($subscriptionUrl)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $subscription = Subscription::getSubscriptionByUrl($subscriptionUrl, $locale);
        return $subscription;
    }

    private function updateSession($order)
    {
        Session::put('order', $order);
        Session::put('step', $order->step - 1);
    }

    private function getCurrentOrder($subscription, $createIfNull = false)
    {
        $order = Session::get('order');
        if ($order) {
            $order = Order::find($order->id);
        }
        if (!$order) {
            if ($createIfNull) {
                $order = new Order;

                if ($subscription) {
                    $order->subscription_id = $subscription->id;
                }
            } else {
                return false;
            }
        }
        return $order;
    }

    public function getStep1Title(string $subscriptionUrl)
    {
        $subscription = $this->getSubscription($subscriptionUrl);

        if (isset($subscription)) {
            if ($subscription->type == 'fitness') {
                $step1Title = trans('meta.title.fitness-tunnel-step1');
            } else {
                $step1Title = trans('meta.title.wellness-tunnel-step1');
            }
        } else {
            $step1Title = trans('meta.title.global-tunnel-step1');
        }

        return $step1Title;
    }

    public function getStep1MetaDescription($subscriptionUrl)
    {
        $subscription = $this->getSubscription($subscriptionUrl);

        if (isset($subscription)) {
            if ($subscription->type == 'fitness') {
                $step1MetaDescription = trans('meta.description.fitness-tunnel-step1');
            } else {
                $step1MetaDescription = trans('meta.description.wellness-tunnel-step1');
            }
        } else {
            $step1MetaDescription = trans('meta.description.global-tunnel-step1');
        }

        return $step1MetaDescription;
    }

    public function getStep1H1(string $subscriptionUrl)
    {
        $subscription = $this->getSubscription($subscriptionUrl);

        if (isset($subscription)) {
            if ($subscription->type == 'fitness') {
                $step1H1 = trans('trad.tunnel-step-1.fitness-title');
            } else {
                $step1H1 = trans('trad.tunnel-step-1.wellness-title');
            }
        } else {
            $step1H1 = trans('trad.tunnel-step-1.global-title');
        }

        return $step1H1;
    }

    private function updateOrderGaUserFromCookie(Order $order, Request $request)
    {
        if (!$order) {
            return false;
        }
        if ($request->headers && $request->headers->all()) {
            if(array_key_exists('cookie', $request->headers->all()))
            {
                $cookies = $request->headers->all()['cookie'];
                preg_match('/_ga=GA1\.2\.([^;]*)/', $cookies[0], $outputArray);
                if (count($outputArray) > 1) {
                    $order->ga_user = $outputArray[1];
                }
                $order->save();
            }
        }
    }

    public function step1($subscriptionUrl, Request $request)
    {
        // TODO : Remove this when Morrocco will have more than 1 clubs
        if(LaravelLocalization::getCurrentLocale() == 'ma')
            return $this->searchTunnelStep1($subscriptionUrl, $request);

//         Redirection 301 - start
        if ($request->get('ck') || $request->get('sk') || $request->get('pk')) {
            return redirect()->route('subscription.step1', ['subscriptionUrl' => $subscriptionUrl], 301);
        }
        // Redirection 301 - end

        Session::forget('order');
        Session::forget('step');

        $subscription = $this->getSubscription($subscriptionUrl);
        if ($subscription) {
            $subUrl = $subscription->url;
        } else {
            $subUrl = \App\Subscription::DEFAULT_URL;
        }



        $order = $this->getCurrentOrder($subscription, true);
        $order->step = 2;
        $order->save();

        $all_clubs = array_flip(
            Club::where('country', 'LIKE', LaravelLocalization::getCurrentLocale())
                ->where("is_open", true)
                ->orderBy('title', 'asc')
                ->pluck('id', 'title')
                ->toArray()
        );

        $params = [
            'subscriptionUrl' => $subUrl,
            'step1Title' => $this->getStep1Title($subscriptionUrl),
            'step1MetaDescription' => $this->getStep1MetaDescription($subscriptionUrl),
            'step1H1' => $this->getStep1H1($subscriptionUrl),
            'env_template' => 'funnel_club',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => collect(request()->segments())->last(),
            'order_id' => $order->id,
            'clubs' => $all_clubs,
        ];

        $this->updateSession($order);

        return view('front.tunnel.step1', compact('subscription'))->with($params);
    }



    public function searchTunnelStep1($subscriptionUrl, Request $request)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $allClubs = [];
        $latitude = $request->get('latitude', null);
        $longitude = $request->get('longitude', null);
        $subscription = $this->getSubscription($subscriptionUrl);
        if ($subscription) {
            $subUrl = $subscription->url;
        } else {
            $subUrl = \App\Subscription::DEFAULT_URL;
        }

        $params = [
            'modal' => true,
            'allClubs' => $allClubs,
            'subscriptionUrl' => $subUrl,
            'step1Title' => $this->getStep1Title($subscriptionUrl),
            'step1MetaDescription' => $this->getStep1MetaDescription($subscriptionUrl),
            'step1H1' => $this->getStep1H1($subscriptionUrl),
            'env_template' => 'funnel_club',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => collect(request()->segments())->last(),
            'locale' => $locale,
        ];

        if ($request->has('search')
            && $request->input('search') != ''
            && $request->input('search') != 'Autour de moi'
            && $request->input('search') != 'Around me') {
                $position = Geocoding::findLongLatFromAddress($request->input('search'), $locale);
                $latitude = $position['lat'];
                $longitude = $position['lng'];
                $params['latitude'] = $position['lat'];
                $params['longitude'] = $position['lng'];
                $params['search'] = $request->input('search');
                $params['result'] = $request->input('result');
        }

        $thisRoute = $request->url();

        // TODO : Remove this when Morrocco will have more than 1 clubs
        if(LaravelLocalization::getCurrentLocale() == 'ma')
            $club_lists = Club::where('country', 'ma')->get() ;
        elseif (strstr($thisRoute, 'mon-coach-wellness')) {
        // if (strstr($thisRoute, 'mon-coach-wellness')) {
            $subscription = Subscription::getSubscriptionByUrl('mon-coach-wellness', $locale);
            $club_lists = Geocoding::filterByLatLong($subscription->clubs, $latitude, $longitude, $request->get('distance', 50));
        } elseif (strstr($thisRoute, 'mon-coach-fitness')) {
            $subscription = Subscription::getSubscriptionByUrl('mon-coach-fitness', $locale);
            $club_lists = Geocoding::filterByLatLong($subscription->clubs, $latitude, $longitude, $request->get('distance', 50));
        } else {
            $full_club_list = Club::where('country', $locale)->where("is_open", true)->get();
            $club_lists = Geocoding::filterByLatLong($full_club_list, $latitude, $longitude, $request->get('distance', 50));
        }

        $clubs = [];
        foreach ( $club_lists as $club ) {
            $clubs[ $club->id ] = $club->title;
        }

        return view('front.tunnel.step1', compact('subscription'))
            ->with($params)
            ->with('clubs', $clubs)
            ->with('club_lists', $club_lists);
    }

    public function step2($subscriptionUrl, Request $request)
    {
        $subscription = $this->getSubscription($subscriptionUrl);
        $order = $this->getCurrentOrder($subscription, true);
        $order->step = 3;

        if ($request->get('clubId')) {
            $order->club_id = intval($request->get('clubId'));
        }
        if (!$order->club_id) {
            return redirect()->route('subscription.step1', $subscriptionUrl);
        }

        $club = Club::find($order->club_id);
        if (!$club) {
            return redirect()->route('subscription.step1', $subscriptionUrl);
        }
        $order->save();

        $hasStudentOffers = true;
        foreach ( $club->activeOffers() as $offer ) {
            if ( $offer->is_student ) {
                $hasStudentOffers = true;
                break;
            }
        }

        $params = [
            'hasStudentOffers' => $hasStudentOffers,
            'club' => $club,
            'order' => $order,
            'env_template' => 'funnel_formule',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => collect(request()->segments())->last(),
            'user_ville_club' => $club->city."_".$club->title,
            'ville_club' => $club->city."_".$club->title,
            'club_name' => $club->title,
            'club_id' => $club->id,
            'order_id' => $order->id,
            'order_products_number' => 1,
        ];

        $this->updateSession($order);

        return view('front.tunnel.step2', compact('subscription'))->with($params);
    }

    public function step3($subscriptionUrl, Request $request)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $subscription = $this->getSubscription($subscriptionUrl);
        if (!$request->get('offerId') || !$request->get('clubId')) {
            $validator = Validator::make($request->all(), [
                'radio' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $order = $this->getCurrentOrder($subscription, true);
        if ($request->get('clubId')) {
            $club = Club::find($request->get('clubId'));
            if($club){
                $order->club_id = $club->id;
            }
        }
        if (!$order->club_id) {
            return redirect()->route('subscription.step1', $subscriptionUrl);
        }
        if ($request->input('radio')) {
            foreach ($subscription->offers as $offer) {
                if ($request->input('radio') == $offer->id) {
                    $order->offer_id = $offer->id;
                    break;
                }
            }
        }
        if ($request->get('offerId')) {
            $order->offer_id = $request->get('offerId');
        }
        $offer = Offer::find($order->offer_id);
        if (!$offer) {
            return redirect()->route('subscription.step2', $subscriptionUrl);
        }

        $order->is_student = $offer->is_student;
        $order->step = 4;
        $order->save();

        $params = [
            'offer' => $offer,
            'prices' => $offer->getPrices($order->club_id),
            'club' => $order->club,
            'order' => $order,
            'env_template' => 'funnel_coordonnees',
            'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
            'page_cat1' => collect(request()->segments())->last(),
            'user_ville_club' => $order->club->city."_".$order->club->title,
            'ville_club' => $order->club->city."_".$order->club->title,
            'date_debut_abo' => Carbon::now()->format('Y-m-d'),
            'date_fin_abo' => Carbon::now()->addMonths($offer->commitment)->format('Y-m-d'),
            'club_name' => $order->club->title,
            'club_id' => $order->club->id,
            'order_id' => $order->id,
            'order_products_number' => 1,
            'order_product_id' => preg_replace('/[^0-9]/', '', $offer->title) == $offer->commitment ? preg_replace('/ /', '_', strtoupper($offer->title)) : strtoupper($offer->title) . '_' . $offer->commitment,
            'order_product_pays_club_formule' => 'FR_' . str_replace(' ', '_', $order->club->title) . '_' . $offer->title,
            'order_product_name' => $offer->title,
            'order_product_duree_abo' => $offer->commitment,
            'order_product_student' => $offer->is_student,
            'order_product_category_id' => '1',
            'order_product_category' => $offer->Subscription->url,
            'order_product_unitprice_ati' => (string)($offer->price / 100),
        ];

        $this->updateSession($order);

        return view('front.tunnel.step3', compact('subscription'))->with($params);
    }

    private function initTagCommanderParams($order, Request $request)
    {
        $this->updateOrderGaUserFromCookie($order, $request);
        $club = $order->club;
        $offer = $order->offer;
        $orders = Order::where('email', $order->email)->get();
        if ($order->civility == 'monsieur') {
            $user_civ = 0;
        } elseif ($order->civility == 'madame') {
            $user_civ = 1;
        } else {
            $user_civ = 2;
        }

        return [
            'user_ville_club' => $club->city."_".$club->title,
            'ville_club' => $club->city."_".$club->title,
            'user_civ' => $user_civ ?: 0,
            'user_birthdate' => $order->birthdate ? Carbon::createFromFormat('Y-m-d H:i:s', $order->birthdate)->format('Y-m-d') : null,
            'user_postalcode' => $order->zip_code,
            'user_recency' => $order->created_at->format('Y-m-d'),
            'user_frequency' => count($orders->where('status', 'SUCCEEDED')),
            'user_email' => $order->email,
            'user_status' => count($orders) > 0 ? 1 : 0,
            'date_debut_abo' => Carbon::now()->format('Y-m-d'),
            'date_fin_abo' => Carbon::now()->addMonths($offer->commitment)->format('Y-m-d'),
            'club_name' => $club->title,
            'club_id' => $club->id,
            'order_id' => $order->id,
            'order_amount_ati_total' => number_format(($order->getUniqueTotalPriceOrder(false) / 100), 2, '.', ''),
            'order_amount_tf_total' => number_format((Order::getHTFromTTC($order->getUniqueTotalPriceOrder(false)) / 100), 2, '.', ''),
            'order_currency' => 'EUR',
            'order_tax_total' => number_format((Order::getTaxFromTTC($order->getUniqueTotalPriceOrder(false)) / 100), 2, '.', ''),
            'order_products_number' => 1,
            'order_product_id' => preg_replace('/[^0-9]/', '', $offer->title) == $offer->commitment ? preg_replace('/ /', '_', strtoupper($offer->title)) : strtoupper($offer->title) . '_' . $offer->commitment,
            'order_product_pays_club_formule' => 'FR_' . str_replace(' ', '_', $club->title) . '_' . strtoupper($offer->title),
            'order_product_name' => preg_replace('/[^0-9]/', '', $offer->title) == $offer->commitment ? trim(preg_replace('/[0-9]+/', '', strtoupper($offer->title))) : strtoupper($offer->title),
            'order_product_duree_abo' => $offer->commitment,
            'order_product_student' => $offer->is_student,
            'order_product_category_id' => '1',
            'order_product_category' => $offer->Subscription->url,
            'order_payment_method' => 'CB',
        ];
    }

    public function step4($subscriptionUrl, Request $request)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $subscription = $this->getSubscription($subscriptionUrl);
        $order = $this->getCurrentOrder($subscription, false);
        if (!$order) {
            return redirect()->route('subscription.step1', $subscription->url);
        }
        if ($request->get('civility') || $request->get('firstname') || $request->get('lastname') || $request->get('email') || $request->get('birthdate') || $request->get('address') || $request->get('city') || $request->get('zip_code')) {
            $validator = Validator::make($request->all(), [
                'civility' => 'required',
                'firstname' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'birthdate' => 'required|date_format:d/m/Y',
                'address' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'zip_code' => 'required',
            ], [
                'civility.required' => trans('validation.custom.civility.required'),
                'firstname.required' => trans('validation.custom.firstname.required'),
                'lastname.required' => trans('validation.custom.lastname.required'),
                'email.required' => trans('validation.custom.email.required'),
                'birthdate.required' => trans('validation.custom.birthdate.required'),
                'birthdate.date_format' => trans('validation.custom.birthdate.date_format'),
                'address.required' => trans('validation.custom.address.required'),
                'city.required' => trans('validation.custom.city.required'),
                'zip_code.required' => trans('validation.custom.zip_code.required'),
            ]);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }
        $order->civility = $request->get('civility') ?: $order->civility;
        $order->lastname = $request->get('lastname') ?: $order->lastname;
        $order->firstname = $request->get('firstname') ?: $order->firstname;
        $order->birthdate = $request->get('birthdate') ? Carbon::createFromFormat('d/m/Y', $request->get('birthdate'))->format('Y-m-d') : $order->birthdate;
        $order->email = $request->get('email') ?: $order->email;
        $order->phone_number = $request->get('phone_number') ?: $order->phone_number;
        $order->address = $request->get('address') ?: $order->address;
        $order->alt_address = $request->get('alt_address') ?: $order->alt_address;
        $order->zip_code = $request->get('zip_code') ?: $order->zip_code;
        $order->city = $request->get('city') ?: $order->city;
        $order->step = 5;
        $order->status = null;
        $order->save();

        $club = $order->club;
        $offer = $order->offer;

        if ($request->has('news')) {
            $order->newsletter = true;
            $order->save();
            $email = $request->get('email') ? $request->get('email') : $order->email;
            $firstname = $request->get('firstname') ? $request->get('firstname') : $order->firstname;
            $lastname = $request->get('lastname') ? $request->get('lastname') : $order->lastname;

            if (config('newsletter.apikey') && config('newsletter.lists.subscribers.id')) {
                Newsletter::subscribeOrUpdate($email, [
                    'firstName' => $firstname,
                    'lastname' => $lastname,
                ]);
            }

            Mail::to($email)->send(new NewsletterMail());
        }

        $params = array_merge(
            $this->initTagCommanderParams($order, $request),
            [
                'env_template' => 'funnel_paiement',
                'page_name' => str_replace(request()->root(), '', request()->url()),
                'page_cat1' => collect(request()->segments())->last(),
                'user_amount' => number_format(($order->getUniqueTotalPriceOrder(false) / 100), 2, '.', ''),
                'order_product_unitprice_ati' => number_format(($offer->price / 100), 2, '.', ''),
                'order_amount_ati_paid' => number_format(($order->getTotalPriceOrder(false) / 100), 2, '.', ''),
                'order_amount_tf_paid' => number_format((Order::getHTFromTTC($order->getTotalPriceOrder(false)) / 100), 2, '.', ''),
                'order_tax_paid' => number_format((Order::getTaxFromTTC($order->getTotalPriceOrder(false)) / 100), 2, '.', ''),
            ]
        );

        $params['prices'] = $offer->getPrices($club->id);
        $params['order'] = $order;
        $params['offer'] = $offer;
        $params['club'] = $club;

        $this->updateSession($order);

        return view('front.tunnel.step4', compact('subscription'))->with($params);
    }

    public function payment($subscriptionUrl, PaymentFormRequest $request)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $subscription = $this->getSubscription($subscriptionUrl);

        $order = $this->getCurrentOrder($subscription, false);
        if (!$order || $order->status == Order::Succeeded) {
            return redirect()->route('subscription.step1', $subscription->url);
        }

        $modePayment = $request->get('mode');

        Session::put('mode', $modePayment);

        $mango = new Mangopay($this->mangopay);

        $dataUser = [
            'firstname' => $order->firstname,
            'lastname' => $order->lastname,
            'email' => $order->email,
            'birthdate' => strtotime($order->birthdate),
        ];

        $user = $mango->createUser($dataUser);

        if (!isset($user)) {
            $msg = [
                'message' => 'Une erreur est survenue',
            ];

            \Log::error('problème de creation de user sur l\'order ID : ' . $order->id);
            return redirect()->route('subscription.step1', $subscription->url)->with($msg);
        }

        $wallet = $mango->createWallet($user);

        $dataPayIn = [
            'user' => $user,
            'wallet' => $wallet,
            'url' => $subscriptionUrl
        ];

        $dataPayIn['amount'] = $order->getTotalPrice($modePayment, false);

        $payIn = $mango->createPayIn($dataPayIn);
        $order->createPayment($modePayment, 'mangopay', $payIn->Id);

        if ($payIn->ExecutionDetails->RedirectURL) {
            return redirect($payIn->ExecutionDetails->RedirectURL);
        } else {
            \Log::error('Erreur lors de la redirection du paiement.');
        }

        Session::put('order', $order);
    }

    public function stepConfirmation($subscriptionUrl, Request $request)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        \Log::info($request);
        $subscription = $this->getSubscription($subscriptionUrl);

        $mango = new Mangopay($this->mangopay);

        if (!$request->transactionId) {
            return redirect()->route('subscription.step4', $subscriptionUrl)->withErrors(['Erreur sur le paiement.']);
        }

        $payment = $mango->updatePayment($request->transactionId);
        if (!$payment || !$payment->order || $payment->status != Payment::Succeeded || $payment->order->status != Payment::Succeeded) {
            return redirect()->route('subscription.step4', $subscriptionUrl)->withErrors(['Erreur sur le paiement.']);
        } elseif ($payment->order->updated_at->diffInMinutes(new Carbon(), false) > 10) {
            return redirect()->route('subscription.step1', $subscriptionUrl)->withErrors(['Transaction terminée']);
        }

        $order = $payment->order;
        $club = $order->club;
        $offer = $order->offer;

        $params = array_merge(
            $this->initTagCommanderParams($order, $request),
            [
                'env_template' => 'funnel_confirmation',
                'page_name' => str_replace(request()->root(), '', request()->fullUrl()),
                'page_cat1' => 'tunnel-etape5',
                'order_type' => 'C',
                'order_amount_ati_paid' => '0.00',
                'order_amount_tf_paid' => '0.00',
                'order_tax_paid' => '0.00',
                'order_newcustomer' => count(Order::where('email', $order->email)->get()) > 0 ? 0 : 1,
            ]
        );

        $payment = $order->payments->where('status', Payment::Succeeded)->first();

        $params['order'] = $order;
        $params['offer'] = $offer;
        $params['club'] = $club;
        $params['order_amount_ati_paid'] = number_format(($payment->amount / 100), 2, '.', '');
        $params['order_amount_tf_paid'] = number_format((Order::getHTFromTTC($payment->amount) / 100), 2, '.', '');
        $params['order_tax_paid'] = number_format((Order::getTaxFromTTC($payment->amount) / 100), 2, '.', '');
        $params['message'] = 'Votre commande a bien été prise en compte. Nous vous remercions de votre confiance.';
        $params['payment'] = $payment;

        $amount = $payment->amount / 100 ?: 0;

        $params['user_amount'] = number_format($amount, 2, '.', '');
        $params['order_product_unitprice_ati'] = number_format(($offer->price/100), 2, '.', '');

        $params['subscription'] = $subscription;

        if(Session::get('order') != null)
        {
            $params['is_student'] = Session::get('order')->is_student;
        }

        Session::forget('order');
        Session::forget('step');

        return response()->view('front.tunnel.confirm', $params)
            ->header("Refresh", "600; url=" . route('subscription.step1', $subscription->url));
    }
}
