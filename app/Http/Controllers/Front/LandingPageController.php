<?php

namespace App\Http\Controllers\Front;

use App\Club;
use App\Http\Controllers\Controller;
use App\LandingPage;
use App\Mail\LandingContactMail;
use App\Mail\LandingContactMailConfirm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LandingPageController extends Controller
{
    private static function tagCommVars() {
        $env_template = 'landing_page';
        $page_name = \Request::getRequestUri();

        return [
            "env_template" => $env_template,
            "page_name" => $page_name,
        ];
    }

    public function club( Request $request, LandingPage $landingPage, $city, $club_id = null )
    {
        if ( $landingPage != null ) {
            $clubs = $landingPage->clubs()->where('city', $city)->pluck('title', 'id')->toArray();

            if ( $clubs != null && $clubs != [] ) {
                $club = null;

                if ( $club_id ) {
                    $club = Club::where('id', $club_id)->where('city', $city)->first();

                    if ( $club == null ) {
                        abort(404);
                    }
                } else {
                    $club = Club::where('id', array_key_first($clubs))->where('city', $city)->first();
                }

                $schedule = $club->getSchedule();

                $exploded = explode('/', $landingPage->video_link);
                $video_id = end( $exploded );
                if ( preg_match("/watch\?v=/", $video_id) ) {
                    $video_id = preg_replace('/watch\?v=/', '', $video_id);
                }
                $video_link = '//www.youtube.com/embed/'.$video_id;

                $params = [
                    "clubs" => $clubs,
                    "club" => $club,
                    "schedule" => $schedule,
                    "landingPage" => $landingPage,
                    "video_link" => $video_link,
                ];
                $params = array_merge( $params, self::tagCommVars() );

                return view('front.landing-pages.club')->with($params);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function clubContact( Request $request ) {

        $clientData = $request->input();
        $club = Club::where('id', $clientData['club'])->first();

        Mail::to( $club->email )->send(new LandingContactMail(array_merge($clientData, ["club" => $club])));
        Mail::to( $clientData["email"] )->send(new LandingContactMailConfirm(array_merge($clientData, ["club" => $club])));

        $message = __('validation.emailForm.valid');
        if (Mail::failures()) {
            $message = __('validation.emailForm.error');
        }
        return redirect(url()->previous())->with('message', $message);
    }
}
