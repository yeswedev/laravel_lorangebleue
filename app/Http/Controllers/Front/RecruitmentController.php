<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\CandidatureMail;
use Carbon\Carbon;

use GuzzleHttp;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Mail;
use Validator;
use LaravelLocalization;
use App\Lib\Geocoding;

class RecruitmentController extends Controller
{
    public function index()
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $jobs = getEoliaJobs();

        return view('front.components.recruitment-home')
            ->with($jobs);
    }

    public function listOffers(Request $req)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $inputs = $req->query();

        $jobs = getEoliaJobs();
        $cities_with_jobs = [];
        $countries = [];
        $pays_unique = [];

        if ($jobs['job']) {
            $jobs_array = $jobs['job'];

            $cities_with_jobs = orderJobsByCities( $jobs_array );

            $var = getPaysAndCountrySelects( $jobs );
            $countries = $var["countries"];
            $pays_unique = $var["pays_unique"];
        }

        $filter = [];
        if ($jobs['job'] && $inputs) {
            $filter = filterJobs( $jobs, $inputs );
        }

        if ( array_key_exists( 'job', $inputs ) || array_key_exists( 'pays', $inputs ) || array_key_exists( 'country', $inputs ) ) {
            $filter = array_unique($filter, SORT_REGULAR);
            $jobs['job'] = $filter;
        }

        return view('front.components.recruitment-offers')
            ->with([
                'countries' => $countries,
                'numb_offers' => count( $jobs['job'] ),
                'pays' => $pays_unique,
                'functions' => [
                        'siège' => 'Siège',
                        'fitness' => 'Mon Coach Fitness',
                        'wellness' => 'Mon Coach Wellness',
                        'international' => 'International'
                ],
                'cities_with_jobs' => $cities_with_jobs,
            ])
            ->with($jobs);
    }

    public function offerDetails($offer_id)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $jobs = getEoliaJobs();

        $params = [];

        if(!count($jobs["job"]))
            abort(503);

        foreach ($jobs["job"] as $job) {
            if ($job["id"] == $offer_id) {
                $the_job = ["job" => json_decode(json_encode($job))];
                break;
            } else {
                $the_job = null;
            }
        }

        $stringJobCountryCount = (intval('-' . strlen($job['pays'])) + 2);
        $jobLocale = substr($job['pays'], 0, $stringJobCountryCount);

        $string = $job['saisie2'];
        preg_match('#\((.*?)\)#', $string, $match);
        $jobZipCode = "";
        if (count($match) > 1) {
            $jobZipCode = $match[1];
        }

        $properties = [
            "@context" => "http://schema.org",
            "@type" => "JobPosting",
            "title" => $job['saisie1'],
            "description" => $job['memo1'],
            "hiringOrganization" => [
                "@type" => "Organization",
                "name" => config('app.name'),
                "sameAs" => config('app.url')
            ],
            "industry" => "Salle de sport",
            "employmentType" => "FULL_TIME",
            "datePosted" => Carbon::createFromFormat('d/m/Y H:i:s', $job['datedernierepublication'])->format('Y-m-d'),
            "validThrough" => Carbon::createFromFormat('d/m/Y H:i:s', $job['date1'])->format('Y-m-d H:i:s'),
            "jobLocation" => [
                "@type" => "Place",
                "address" => [
                    "@type" => "PostalAddress",
                    "streetAddress" => $job['saisierecrut1'],
                    "addressLocality" => $job['saisierecrut1'],
                    "postalCode" => $jobZipCode ?: 'non renseigné',
                    "addressRegion" => $job['region'],
                    "addressCountry" => $jobLocale
                ]
            ]
        ];

        $job_posting_SEOcontext = "<script type=\"application/ld+json\">" . json_encode($properties, JSON_UNESCAPED_UNICODE) . "</script>";

        $params['job_posting_SEOcontext'] = $job_posting_SEOcontext;

        return view('front.components.recruitment-offer')
            ->with($the_job)
            ->with($params);
    }

    public function apply(Request $r)
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $jobs = getEoliaJobs();

        $the_job = [];
        $countries = ["Toute la France" => "Toute la France"];

        $client = new GuzzleHttp\Client();
        $response = $client->request("GET", "https://geo.api.gouv.fr/regions/?fields=nom")->getBody();
        $regions = array_column( json_decode( $response, true ), 'nom' );
        $regions = array_combine($regions, $regions);
        asort($regions);
        $countries["France"] = $regions;

        foreach ($jobs["job"] as $job) {
            if ($job["id"] == $r->query("offerID")) {
                $the_job = json_decode(json_encode($job));
                break;
            } else {
                $the_job = [];
            }
        }

        foreach ($jobs["job"] as $job) {
            preg_match('#\((.*?)\)#', $job['saisie2'], $match);
            $job_pays = ucfirst( strtolower($job['pays']) );
            if(!empty($match))
            {
                if ( $job_pays != "France" ) {
                    $countries[ $job_pays ][ $job['region'] ] = $job['region'];
                    $countries[ $job_pays ] = array_unique($countries[ $job_pays ]);
                    asort( $countries[ $job_pays ] );
                }
            }
        }

        return view('front.components.recruitment-apply')
            ->with([
                'job' => $the_job,
                "countries" => $countries
            ]);
    }

    public function applied()
    {
        if (LaravelLocalization::getCurrentLocale() !== 'fr') {
            abort(404);
        }
        $clientData = Input::all();

        $v = Validator::make($clientData, [
            'cv' => 'required|mimes:doc,docx,odt,rtf,pdf|max:5000',
            'motivation' => 'mimes:doc,docx,odt,rtf,pdf|max:5000',
            'civility' => 'required',
            'name' => 'required',
            'firstname' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'country' => 'required',
        ]);

        if ($v->fails()) {
            $msgs = $v->messages();
            return redirect()->route('recruitment.apply')->withErrors($v);
        }

        Mail::send(new CandidatureMail($clientData));
        $message = __('validation.emailForm.valid');
        if (Mail::failures()) {
            $message = __('validation.emailForm.error');
        }
        return redirect(url()->previous())->with('message', $message);
    }
}
