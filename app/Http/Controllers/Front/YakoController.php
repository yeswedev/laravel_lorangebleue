<?php

namespace App\Http\Controllers\Front;

use App\Activity;
use App\Http\Controllers\Controller;
use App\Yako;
use Illuminate\Support\Facades\Storage;
use LaravelLocalization;

class YakoController extends Controller
{
    public function retrieve($yakoUrl)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $yako = Yako::whereHas('translations', function ($q) use ($yakoUrl, $locale) {
            $q->where('url', $yakoUrl);
            if ($locale) {
                $q->where('locale', $locale);
            }
        })->first();

        if (!isset($yako)) {
            return abort(404);
        }

        $illustration_type = ($yako->link) ? "video" : "image";
        $illustration_src = ($yako->link) ? $yako->getVideoEmbedLink() : Storage::url($yako->image);
        $yako_details = [
            'object' => $yako,
            'illustration_type' => $illustration_type,
            'illustration_src' => $illustration_src
        ];

        $yako_lists = Yako::where('id', '!=', $yako->id)->take(3)->get();

        $params = [
            'yako_lists' => $yako_lists,
            'yako_details' => $yako_details,
        ];

        return view('front.yakos.single-yako', compact('yako'))->with($params);
    }

    public function showAll()
    {
        $yako_lists = Yako::all();
        $activity_lists = Activity::all();

        // TODO : maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma')
            foreach ($activity_lists as $key=>$e)
                if($e->type == 'wellness')
                    unset($activity_lists[$key]);
        // stop remove to this

        $params = [
            'yako_lists' => $yako_lists,
            'activity_lists' => $activity_lists
        ];

        return view('front.yakos.list-yakos')->with($params);
    }
}
