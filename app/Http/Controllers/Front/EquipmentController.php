<?php

namespace App\Http\Controllers\Front;

use App\Activity;
use App\Equipment;
use App\Http\Controllers\Controller;
use App\PageGeneric;

class EquipmentController extends Controller
{
    public function retrieve(Equipment $equipment)
    {

        // TODO Maroc : delete when they'll have the wellness thing
        if (\LaravelLocalization::getCurrentLocale() == 'ma' && $equipment->type == 'wellness') 
            abort(404);

        $activity_lists = Activity::all();

            $equipments_lists = Equipment::where('id', '!=', $equipment->id)->get();

            // TODO : maroc : remove from this when they'll have Wellness activities
            if(\LaravelLocalization::getCurrentLocale() == 'ma'){
            foreach ($activity_lists as $key=>$e) {
                if($e->type == 'wellness')
                unset($activity_lists[$key]);
    
                // Override both : only display Fitness tag on activities page
                if($e->type == 'both')
                    $e->type = 'fitness' ;
                }
            
                foreach ($equipments_lists as $key=>$equ) {
                    if($equ->type == 'wellness')
                        unset($equipments_lists[$key]);
                }
            }
            // stop remove to this
    
        if ( !$equipment->url ) {
            return abort(404);
        }
        $params = [
            'activity_lists' => $activity_lists,
            'equipments_lists' => $equipments_lists,
            'equipment' => $equipment
        ];

        return view('front.equipments.single-equipment')->with($params);
    }

    public function showAll()
    {
        $equipments_lists = Equipment::all();
        $params['pageGeneric'] = PageGeneric::where('type', 'equipment')->first();

        // TODO maroc : remove from this when they'll have Wellness activities
        if(\LaravelLocalization::getCurrentLocale() == 'ma')
            foreach ($equipments_lists as $key=>$e) {
                if($e->type == 'wellness')
                unset($equipments_lists[$key]);

                // Override both : only display Fitness tag on activities page
                if($e->type == 'both')
                    $e->type = 'fitness' ;
                
            }
        // stop remove to this

        return view('front.equipments.list-equipments', compact('equipments_lists'))->with($params);
    }
}
