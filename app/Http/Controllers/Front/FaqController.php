<?php

namespace App\Http\Controllers\Front;

use App\Faq;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function showAll()
    {
        $faq_list = Faq::get();

        $params['modal'] = true;

        foreach($faq_list as $key=>$faq){
            if (empty($faq->question))
            unset($faq_list[$key]);
        }
        
        return view('front.faq.faq', compact('faq_list'))->with($params);
    }
}
