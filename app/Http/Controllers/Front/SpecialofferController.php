<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Specialoffer;
use App\Subscription;

class SpecialofferController extends Controller
{
    public function index(string $specialofferUrl)
    {
        $specialoffer = Specialoffer::where([
            'url' => $specialofferUrl,
            'country' => \LaravelLocalization::getCurrentLocale()
        ])->first();

        if (!$specialoffer) {
            abort(404);
        }

        $clubs = [];

        if ($specialoffer->specialofferClub) {
            $clubs = $specialoffer->specialofferClub()->get()->sortBy('url');
        }

        $app_subscription_fitness = Subscription::where('type', 'fitness')->first();

        $params = [
            'url' => $specialofferUrl,
            'clubs' => $clubs,
            'app_subscription_fitness' => $app_subscription_fitness,
        ];

        return view('front.specialoffer.special-offer', compact('specialoffer'))->with($params);
    }
}
