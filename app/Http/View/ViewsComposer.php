<?php


namespace App\Http\View;

use App\Activity;
use App\Equipment;
use Illuminate\View\View;
use App\Repositories\UserRepository;

class ViewsComposer
{
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //Footer data
        $activities = Activity::whereIn('id', [ 37, 41, 43, 44, 48 ])->get();
        $equipments = Equipment::take(5)->get();

        // TODO Maroc : delete when they'll have the wellness thing
        if (\LaravelLocalization::getCurrentLocale() == 'ma') 
            foreach ($equipments as $key=>$equ)
                if ($equ->type === 'wellness')
                    unset($equipments[$key]);

        $view->with([
            'activities' => $activities,
            'equipments' => $equipments,
        ]);
    }
}