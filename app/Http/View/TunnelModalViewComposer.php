<?php


namespace App\Http\View;

use App\Modal;
use Illuminate\View\View;

class TunnelModalViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $modal = Modal::where('position', 'tunnel_abo')->first();

        if ( $modal ) {
            $view->with("modal", $modal);
        } else {
            $view->with("modal", false);
        }
    }
}