<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class OrderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $step_uri = preg_replace('/.*(.)$/', '$1', \Request::segment(3));

        if (Session::get('order') && $step_uri > Session::get('step') && isset(Session::get('order')->step) && $step_uri > Session::get('order')->step) {
            if (!Session::get('order')->subscription) {
                return redirect()->route('subscription.step1', 'concepts');
            }
            $url = Session::get('order')->subscription->url;
            $step = Session::get('step');
            return redirect()->route('subscription.step'.$step, $url);
        }
        return $next($request);
    }
}
