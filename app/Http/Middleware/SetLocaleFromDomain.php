<?php namespace App\Http\Middleware;

use Closure;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SetLocaleFromDomain
{
    public function handle($request, Closure $next)
    {
        $languages = LaravelLocalization::getSupportedLocales();
        $host = $request->getHost();

        foreach ($languages as $locale => $language) {
            if (((!empty($language['tld'])) && ends_with($host, $language['tld']))
                || ((!empty($language['subdomain'])) && starts_with($host, $language['subdomain']))) {
                LaravelLocalization::setLocale($locale);
                break;
            }
        }

        return $next($request);
    }
}
