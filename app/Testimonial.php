<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'author',
        'description',
    ];

    protected $fillable = [
        'link',
        'image',
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
