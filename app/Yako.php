<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use App\Traits\HasMetaFields;
use App\Traits\HasYWDFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use YesWeDev\LaravelCMS\Traits\HasBlocks;

class Yako extends Model
{
    use Translatable;
    use HasBlocks;
    use HasYWDFields;
    use HasMetaFields;
    use SoftDeletes;

    public $translatedAttributes = [
        'title', 'description', 'url', 'detail', 'objectives', 'targeted_part', 'calories', 'structure', 'advantage', 'public', 'subtitle'
    ];

    protected $fillable = [
        'image', 'rate_calorie', 'link'
    ];

    public function yakoSubscription()
    {
        return $this->belongsToMany(Subscription::class, 'subscription_yako', 'yako_id', 'subscription_id');
    }

    public function yakoClub()
    {
        return $this->belongsToMany(Club::class, 'yako_club', 'yako_id', 'club_id')->withPivot('status', 'version');
    }

    public function yakoActivity()
    {
        return $this->belongsToMany(Activity::class);
    }

    public function getVideoEmbedLink()
    {
        $link = $this->link;
        $video_link = preg_replace('/https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:[\'"][^<>]*>|<\/a>))[?=&+%\w.-]*/i', 'https://www.youtube.com/embed/$1', $link);
        return $video_link;
    }
}
