<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use YesWeDev\LaravelModo\Traits\Moderate;


class ClubTranslation extends Model
{
    use Moderate;

    protected $rolesModeration = [
        'gerant'
    ];

    protected $fieldsModeration = [
        'title' => 'title',
        'description' => 'description',
    ];

    public $timestamps = false;

    /**
     * Set the url's model.
     *
     * @param  string  $value
     * @return void
     */
    public function setUrlAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->attributes['url'] = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
    }

    public function club()
    {
        return $this->belongsTo('App\Club');
    }
}
