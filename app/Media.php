<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'file',
        'ratio',
        'width',
        'height',
        'size',
        'type',
        'description',
        'status',
        'version',
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
