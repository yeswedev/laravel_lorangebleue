<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advantage extends Model
{
    use Translatable;
    use SoftDeletes;

    public $translatedAttributes = [
        'title',
        'description',
    ];

    protected $fillable = [
        'image',
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
