<?php

namespace App;

use \Astrotomic\Translatable\Translatable;
use App\Traits\HasMetaFields;
use App\Traits\HasYWDFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Model
{
    use Translatable, HasYWDFields, HasMetaFields;
    use SoftDeletes;

    public $translatedAttributes = [
        'title', 'description', 'url', 'link',
    ];

    protected $fillable = [
        'title', 'image', 'description', 'url', 'link','type'
    ];

    public function resolveRouteBinding($value)
    {
        return $this->whereTranslation('url', $value)->first() ?? abort(404);
    }

    public function equipmentClub()
    {
        return $this->belongsToMany(Club::class, 'equipment_club', 'equipment_id', 'club_id');
    }
}
